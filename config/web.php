<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'SGL',
    'language' => 'es',
    'timeZone' => 'America/Bogota',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'modules' => [
        'rbac' => [
        'class' => 'yii2mod\rbac\Module',
        'as access' => [
            'class' => yii2mod\rbac\filters\AccessControl::class
        ],
    ],
        'db-manager' => [
            'class' => 'bs\dbManager\Module',
            // path to directory for the dumps
            'path' => '@app/backups',
            // list of registerd db-components
            'dbList' => ['db'],
            'as access' => [
                'class' => 'yii\filters\AccessControl',
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [],
                    ],
                ],
            ],
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ],
        'gridviewKrajee' => [
            'class' => '\kartik\grid\Module',
        // your other grid module settings
        ]
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest', 'user'],
        ],
        'i18n' => [
            'translations' => [
                'yii2mod.rbac' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages', // if advanced application, set @frontend/messages
                    'sourceLanguage' => 'es',
                    'fileMap' => [
                        'main' => 'app.php',
                    ],
                ],
            ],
        ],
        'backup' => [
            'class' => 'demi\backup\Component',
            // The directory for storing backups files
            'backupsFolder' => dirname(dirname(__DIR__)) . '/somlogic/backups', // <project-root>/backups
            // Directories that will be added to backup
            'directories' => [
            //    'images' => '@frontend/web/images',
            //      'uploads' => '@backend/uploads',
            ],
        ],
        'html2pdf' => [
            'class' => 'yii2tech\html2pdf\Manager',
            'viewPath' => '@app/pdf',
            'converter' => 'mpdf',
        ],
        'reCaptcha' => [
            'name' => 'reCaptcha',
            'class' => 'himiklab\yii2\recaptcha\ReCaptcha',
            'siteKey' => '6LcCN8EUAAAAAJ6BJD4s5hHsbcy0zDwzzRiApExg',
            'secret' => '6LcCN8EUAAAAAB1vt8UPA8kuxIzFxz3z9AFvStRA',
        ],
        'assetManager' => [
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-blue-light',
                ],
            ],
        ],
        'view' => [
            /* 'theme' => [
              'pathMap' => [
              '@app/views' => '@vendor/dmstr/yii2-adminlte-asset/example-views/testing/app'
              ],
              ], */
            'theme' => [
                'basePath' => '@app/themes/somlogic',
                'baseUrl' => '@web/themes/somlogic',
                'pathMap' => [
                    '@app/views' => '@app/themes/somlogic',
                ],
            ],
        ],
        'request' => [
// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'qCiENsCfs_qu-yYYaBWq_tB4-hsuId20',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'frontendCache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => dirname(dirname(__DIR__)) .'/somlogic/runtime/cache'
        ],
        'user' => [
            'identityClass' => 'app\models\VwActor',
            'enableAutoLogin' => true,
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebookb' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'clientId' => '346332395931459',
                    'clientSecret' => '6f4f2394a2ec1a5a29df7264047a19f4',
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '459354836371-gv59q2ronbk25p8hq1spfibjasl44633.apps.googleusercontent.com',
                    'clientSecret' => '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',
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'poliaulink@gmail.com',
                'password' => 'fgdqirtftmblsngn',
                'port' => '465',
                'encryption' => 'ssl',
            ],
            'useFileTransport' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => [
                'user/<user_id:\d+>' => 'user/<user_id>',
                'user/<user_id:\d+>/messages' => 'messages/<user_id>',
            ]
        ],
    
    ],
    'as access' => [
        'class' => yii2mod\rbac\filters\AccessControl::class,
        'allowActions' => [
            'site/*',
            'rbac/*',
            //'gii/*'
        // The actions listed here will be allowed to everyone including guests.
        // So, 'admin/*' should not appear here in the production, of course.
        // But in the earlier stages of your development, you may probably want to
        // add a lot of actions here until you finally completed setting up rbac,
        // otherwise you may not even take a first step.
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20'],
        'generators' => [//here
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [
                    'adminlte' => '@vendor/dmstr/yii2-adminlte-asset/gii/templates/crud/simple',
                ]
            ]
        ],
    ];
}

return $config;
