<?php
namespace app\components;

use yii\helpers\Html;

class MyNotifyWidget extends \kartik\growl\Growl{
    
    public function init(){
        parent::init();
    }
    
    protected function registerAssets() {
        $view = $this->getView();
        if (in_array($this->type, self::$_themes)) {
            \kartik\growl\GrowlAsset::register($view)->addTheme($this->type);
        } else {
            \kartik\growl\GrowlAsset::register($view);
        }
        if ($this->useAnimation) {
            \kartik\base\AnimateAsset::register($view);
        }
        $this->registerPluginOptions('notify');
    }
    
}
?>