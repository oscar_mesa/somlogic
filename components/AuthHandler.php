<?php

namespace app\components;

use app\models\Autenticacion;
use app\models\Actor;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;

/**
 * AuthHandler handles successful authentication via Yii auth component
 */
class AuthHandler {

    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client) {
        $this->client = $client;
    }

    public static function checkPermisionUser($PARAMS_VERIFIC = []){
        $PARAMS_VERIFIC[] = '/*';
        foreach ($PARAMS_VERIFIC as $value) {
            if(Yii::$app->user->can($value))
                return true;
        }
        return false;
    }

    public function handle() {
        $attributes = $this->client->getUserAttributes();
//echo '<pre>';        print_r($attributes); exit;
        switch ($this->client->name) {
            case 'facebook':
                $email = ArrayHelper::getValue($attributes, 'email');
                $id = ArrayHelper::getValue($attributes, 'id');
                $nickname = explode(" ", ArrayHelper::getValue($attributes, 'name'));
                $DS_NOMBRES_ACTOR = "";
                $DS_APELLIDOS_ACTOR = "";
                if (count($nickname) == 2) {
                    $DS_NOMBRES_ACTOR = $nickname[0];
                    $DS_APELLIDOS_ACTOR = $nickname[1];
                } else if (count($nickname) == 1) {
                    $DS_NOMBRES_ACTOR = $nickname[0];
                } else if (count($nickname) == 3) {
                    $DS_NOMBRES_ACTOR = $nickname[0] . " " . $nickname[1];
                    $DS_APELLIDOS_ACTOR = $nickname[2];
                } else if (count($nickname) == 4) {
                    $DS_NOMBRES_ACTOR = $nickname[0] . " " . $nickname[1];
                    $DS_APELLIDOS_ACTOR = $nickname[2] . " " . $nickname[3];
                    ;
                } else {
                    $DS_NOMBRES_ACTOR = ArrayHelper::getValue($attributes, 'name');
                }
                break;
            case 'google':
                $email = ArrayHelper::getValue($attributes, 'emails.0.value');
                if(empty($email)){
                    $email = ArrayHelper::getValue($attributes, 'email');
                    $id = ArrayHelper::getValue($attributes, 'id');
                    $DS_NOMBRES_ACTOR = ArrayHelper::getValue($attributes, 'given_name');
                    $DS_APELLIDOS_ACTOR = ArrayHelper::getValue($attributes, 'family_name');
                }else{
                    $id = ArrayHelper::getValue($attributes, 'id');
                    $DS_NOMBRES_ACTOR = ArrayHelper::getValue($attributes, 'name.givenName');
                    $DS_APELLIDOS_ACTOR = ArrayHelper::getValue($attributes, 'name.familyName');
                }
                ;
                break;
        }
        if (empty($email)) {
            Yii::$app->getSession()->setFlash('error', ['type' => 'error',
                'duration' => 5000,
                'icon' => 'fa fa-warning',
                'message' => Yii::t('app', \yii\bootstrap\Html::encode(Yii::t('app', "El ACTOR con el que intenta iniciar no puede ser identificado."))),
                'title' => Yii::t('app', \yii\bootstrap\Html::encode('Error')),]
            );
            return Yii::$app->getResponse()->redirect(["site/login"]);
        }

        /* @var Autenticacion $auth */
        $auth = Autenticacion::find()->where([
                    'ORIGEN' => $this->client->getId(),
                    'ID_ORIGEN' => $id,
                ])->one();

        if (Yii::$app->user->isGuest) {
            if ($auth) { // login
                /* @var Actor $user */
                $user = $auth->aCTOR;
                if ($this->updateUserInfo($user)) {
                    Yii::$app->user->login($user, Yii::$app->params['user.rememberMeDuration']);
                } else {
                    Yii::$app->getSession()->setFlash('error', ['type' => 'error',
                        'duration' => 5000,
                        'icon' => 'fa fa-warning',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('El usuario se encuentra inactivo o tiene un proceso para restaurar contraseña.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Error')),]
                    );
                    return Yii::$app->getResponse()->redirect(["site/login"]);
                }
            } else { // signup
                $existingUser = Actor::findOne(['DS_CORREO' => $email]);
                if ($existingUser) {
                    $aut = new Autenticacion(['ID_ACTOR' => $existingUser->id,
                        'ORIGEN' => $this->client->getId(),
                        'ID_ORIGEN' => (string) $id,]);

                    if ($this->updateUserInfo($existingUser) && $aut->save()) {
                        Yii::$app->user->login($existingUser, Yii::$app->params['user.rememberMeDuration']);
                    } else {
                        Yii::$app->getSession()->setFlash('error', ['type' => 'error',
                            'duration' => 5000,
                            'icon' => 'fa fa-warning',
                            'message' => Yii::t('app', \yii\bootstrap\Html::encode('El usuario se encuentra inactivo.')),
                            'title' => Yii::t('app', \yii\bootstrap\Html::encode('Error')),]
                        );
                        return Yii::$app->getResponse()->redirect(["site/login"]);
                    }
                } else {
                    $password = Yii::$app->security->generateRandomString(12);
                    $user = new Actor([
                        'DS_NOMBRES_ACTOR' => $DS_NOMBRES_ACTOR,
                        'DS_APELLIDOS_ACTOR' => $DS_APELLIDOS_ACTOR,
                        'NM_ESTADO_ID' => \app\models\EstadoActor::ACTIVO,
                        'NM_DOCUMENTO_ID' => null,
                        'DS_CORREO' => $email,
                        'DS_CONTRASENA' => $password,
                        'NM_TIPO_ACTOR_ID' => 12,
                        'NM_TIPO_DOCUMENTO_ID' => null
                    ]);

                    $user->scenario = Actor::SCENARIO_REDES;

                    $transaction = Actor::getDb()->beginTransaction();

                    if ($user->save()) {
                        $auth = new Autenticacion([
                            'ID_ACTOR' => $user->id,
                            'ORIGEN' => $this->client->getId(),
                            'ID_ORIGEN' => (string) $id,
                        ]);
                        if ($auth->save()) {
                            $transaction->commit();
                            Yii::$app->user->login($user, Yii::$app->params['user.rememberMeDuration']);
                        } else {
                            $transaction->rollBack();

                            Yii::$app->getSession()->setFlash('error', ['type' => 'error',
                                'duration' => 5000,
                                'icon' => 'fa fa-warning',
                                'message' => Yii::t('app', \yii\bootstrap\Html::encode(Yii::t('app', 'No se puede guardar {client} cuenta: {errors}', [
                                                    'client' => $this->client->getTitle(),
                                                    'errors' => json_encode($auth->getErrors()),
                                ]))),
                                'title' => Yii::t('app', \yii\bootstrap\Html::encode('Error')),]
                            );
                            return Yii::$app->getResponse()->redirect(["site/login"]);
                        }
                    } else {
                        $transaction->rollBack();
                        Yii::$app->getSession()->setFlash('error', ['type' => 'error',
                            'duration' => 5000,
                            'icon' => 'fa fa-warning',
                            'message' => Yii::t('app', 'No se puede guardar usuario: {errors}', [
                                'client' => $this->client->getTitle(),
                                'errors' => json_encode($user->getErrors()),
                            ]),
                            'title' => Yii::t('app', \yii\bootstrap\Html::encode('Usuario registrado')),]
                        );
                        return Yii::$app->getResponse()->redirect(["site/login"]);
                    }
                }
            }
        } else { // user already logged in
            if (!$auth) { // add auth provider
                $auth = new Autenticacion([
                    'ID_ACTOR' => Yii::$app->user->id,
                    'ORIGEN' => $this->client->getId(),
                    'ID_ORIGEN' => (string) $attributes['id'],
                ]);
                if ($auth->save()) {
                    /** @var Actor $actor */
                    $user = $auth->user;
                    $this->updateUserInfo($user);
                    Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-users',
                        'message' => Yii::t('app', 'Cuenta {client} vinculada.', ['client' => $this->client->getTitle()]),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Exito')),]
                    );
                } else {
                    Yii::$app->getSession()->setFlash('error', ['type' => 'error',
                        'duration' => 5000,
                        'icon' => 'fa fa-warning',
                        'message' => Yii::t('app', 'No se puede vincular la cuenta de {client}: {errors}', ['client' => $this->client->getTitle(), 'errors' => json_encode($auth->getErrors()),]),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Error')),]
                    );
                    return Yii::$app->getResponse()->redirect(["site/login"]);
                }
            } else { // there's existing auth
                Yii::$app->getSession()->setFlash('error', ['type' => 'error',
                    'duration' => 5000,
                    'icon' => 'fa fa-warning',
                    'message' => Yii::t('app', 'No se puede vincular la cuenta de {client}. Hay otro usuario usándolo.', ['client' => $this->client->getTitle()]),
                    'title' => Yii::t('app', \yii\bootstrap\Html::encode('Error')),]
                );
                return Yii::$app->getResponse()->redirect('site/login');
            }
        }
    }

    /**
     * @param Actor $user
     */
    private function updateUserInfo(Actor $user) {
        return $user->NM_ESTADO_ID !== \app\models\EstadoActor::INNACTIVO && $user->NM_ESTADO_ID !== \app\models\EstadoActor::PENDIENTE_RESTAURAR_CONTRASENA;
    }

}
