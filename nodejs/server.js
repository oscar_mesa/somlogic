var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var Promise = require('es6-promise').Promise;
var Web3 = require('web3');


var web3 = new Web3(new Web3.providers.HttpProvider("http://10.11.13.130:8501"));

console.log(web3.eth.coinbase);


if(!web3.isConnected())
    console.log("not connected");
else
    console.log("connected");


var abi=[{"constant": true,"inputs": [{"name": "cadena","type": "uint256"}],"name": "consulta_cadena","outputs": [{"name": "","type": "uint256"},{"name": "","type": "string"},{"name": "","type": "string"},{"name": "","type": "string"},{"name": "","type": "string"}],"payable": false,"stateMutability": "view","type": "function"},{"constant": false,"inputs": [{"name": "_cuenta","type": "string"},{"name": "_cadena","type": "uint256"},{"name": "_pedidos","type": "string"},{"name": "_indicadores","type": "string"},{"name": "_fecha","type": "string"}],"name": "Nueva_medicion","outputs": [],"payable": false,"stateMutability": "nonpayable","type": "function"}];
// creation of contract object

var MyContract = web3.eth.contract(abi);

// initiate contract for an address
var myContractInstance = MyContract.at('0xbb7bbb2ee4a19ea0f3d02f1e07678e25232e36d2');

/*
var cadena="16";
var fecha="25/8/2019-29/8/2019";
var cuenta="Makro";
var indicador="0_1";
var nombre="Indicadofddsfsdfsdr 1";
var resultado="0.75";
var pedido="Pedido 1";
var ponderacion="10_Porcentaje de significacia";

var dataescritura2=myContractInstance.Nuevo_indicador(cadena,fecha,cuenta,indicador,nombre,resultado,pedido,ponderacion,{gas: 900000 });
console.log(dataescritura2);
*/

io.on('connection', function(socket) {
  console.log('Alguien se ha conectado con Sockets');
  socketident=socket;
  socketidentid=socket.id;


  socket.on('consultar', function(data) {
    
  console.log("Consultando cadena "+data);
 
   var result=myContractInstance.consulta_cadena(data);
   console.log(result);
   socket.emit('consulta', result);
  
});

socket.on('escribir', function(data) {

web3.eth.defaultAccount='0x8E78Bb9d32A93bcf607BeCE4F5BCD886BE0dA936';
web3.personal.unlockAccount("0x8E78Bb9d32A93bcf607BeCE4F5BCD886BE0dA936","Poli2020",5000);
console.log(data.cuenta+","+data.cadena+","+JSON.stringify(data.pedidos)+","+JSON.stringify(data.indicadores)+","+data.fecha);

//var dataescritura2=myContractInstance.Nuevo_indicador(data.cadena,data.fecha,data.cuenta,"data.indicadores","nombre","resultado",data.pedidos,"ponderacion",{gas: 900000 });
//console.log(dataescritura2);

var dataescritura=myContractInstance.Nueva_medicion(data.cuenta,data.cadena,data.pedidos,data.indicadores,data.fecha,{gas: 9000000});

console.log(dataescritura);
socket.emit('escrituraexitosa', dataescritura);

});


 socket.on('disconnect', function() {
      
      console.log('Se desconecto '+socket.id);
     
      //desconectar(socket.id);
  });
});


server.listen(8280, function() {
  console.log("Servidor corriendo en http:localhost:8280");
});
