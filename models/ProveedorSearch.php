<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Proveedor;

/**
 * ProveedorSearch represents the model behind the search form of `app\models\Proveedor`.
 */
class ProveedorSearch extends Proveedor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_PROVEEDOR', 'ID_TIPO_PROVEEDOR'], 'integer'],
            [['RAZON_SOCIAL_PROVEEDOR', 'NIT', 'DIRECCION', 'TELEFONO', 'NOMBRE_CONTACTO', 'CARGO_CONTACTO', 'SITIO_WEB'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Proveedor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID_PROVEEDOR' => $this->ID_PROVEEDOR,
            'ID_TIPO_PROVEEDOR' => $this->ID_TIPO_PROVEEDOR,
        ]);

        $query->andFilterWhere(['like', 'RAZON_SOCIAL_PROVEEDOR', $this->RAZON_SOCIAL_PROVEEDOR])
            ->andFilterWhere(['like', 'NIT', $this->NIT])
            ->andFilterWhere(['like', 'DIRECCION', $this->DIRECCION])
            ->andFilterWhere(['like', 'TELEFONO', $this->TELEFONO])
            ->andFilterWhere(['like', 'NOMBRE_CONTACTO', $this->NOMBRE_CONTACTO])
            ->andFilterWhere(['like', 'CARGO_CONTACTO', $this->CARGO_CONTACTO])
            ->andFilterWhere(['like', 'SITIO_WEB', $this->SITIO_WEB]);

        return $dataProvider;
    }
}
