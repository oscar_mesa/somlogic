<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PedidoDetalle]].
 *
 * @see PedidoDetalle
 */
class PedidoDetalleQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PedidoDetalle[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PedidoDetalle|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
