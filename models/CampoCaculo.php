<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%campo_caculo}}".
 *
 * @property int $ID_CAMPO
 * @property string $DS_CAMPO_CALCULO
 *
 * @property CampoCadenaIndicador[] $campoCadenaIndicadors
 */
class CampoCaculo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%campo_caculo}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DS_CAMPO_CALCULO'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_CAMPO' => Yii::t('app', 'ID'),
            'DS_CAMPO_CALCULO' => Yii::t('app', 'Campo Calculo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampoCadenaIndicadors()
    {
        return $this->hasMany(CampoCadenaIndicador::className(), ['ID_CAMPO' => 'ID_CAMPO']);
    }

    /**
     * {@inheritdoc}
     * @return CampoCaculoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CampoCaculoQuery(get_called_class());
    }
}
