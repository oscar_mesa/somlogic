<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vw_idl_pedidos".
 *
 * @property string $DS_CODIGO_PEDIDO
 * @property int $CS_PEDIDO_ID
 * @property int $NM_CADENA
 * @property double $TOTAL
 */
class VwIdlPedidos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vw_idl_pedidos';
    }

    /**
     * @inheritdoc$primaryKey
     */
    public static function primaryKey()
    {
        return ["CS_PEDIDO_ID"];
    }    

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NM_CADENA'], 'integer'],
            [['TOTAL'], 'number'],
            [['DS_CODIGO_PEDIDO'], 'string', 'max' => 40],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DS_CODIGO_PEDIDO' => Yii::t('app', 'Ds Codigo Pedido'),
            'NM_CADENA' => Yii::t('app', 'Nm Cadena'),
            'TOTAL' => Yii::t('app', 'Total'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return VwIdlPedidosQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VwIdlPedidosQuery(get_called_class());
    }
}
