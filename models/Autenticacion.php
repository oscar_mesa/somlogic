<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autenticacion".
 *
 * @property int $ID
 * @property int $ID_ACTOR
 * @property string $ORIGEN
 * @property string $ID_ORIGEN
 *
 * @property Actor $aCTOR
 */
class Autenticacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autenticacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_ACTOR', 'ORIGEN', 'ID_ORIGEN'], 'required'],
            [['ID_ACTOR'], 'integer'],
            [['ORIGEN', 'ID_ORIGEN'], 'string', 'max' => 255],
            [['ID_ACTOR'], 'exist', 'skipOnError' => true, 'targetClass' => Actor::className(), 'targetAttribute' => ['ID_ACTOR' => 'ID_ACTOR']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ID_ACTOR' => Yii::t('app', 'Id  Actor'),
            'ORIGEN' => Yii::t('app', 'Origen'),
            'ID_ORIGEN' => Yii::t('app', 'Id  Origen'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getaCTOR()
    {
        return $this->hasOne(Actor::className(), ['ID_ACTOR' => 'ID_ACTOR']);
    }

    /**
     * {@inheritdoc}
     * @return AutenticacionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AutenticacionQuery(get_called_class());
    }
}
