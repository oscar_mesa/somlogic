<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[UnidadProducto]].
 *
 * @see UnidadProducto
 */
class UnidadProductoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return UnidadProducto[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UnidadProducto|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
