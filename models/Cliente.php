<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cliente".
 *
 * @property int $ID_CLIENTE
 * @property string $NM_DOCUMENTO_NIT
 * @property int $NM_TIPO_DOCUMENTO_ID
 * @property string $DS_NOMBRES_CLIENTE
 * @property string $DS_APELLIDOS_CLIENTE
 * @property int $NM_TELEFONO
 * @property string $NM_CELULAR
 * @property string $DS_CORREO
 * @property string $DS_DIRECCION
 * @property int $NM_ESTADO_ID
 * @property string $DT_FECHA_CREACION
 * @property string $DF_FECHA_ACTUALIZACION
 *
 * @property EstadoCliente $nMESTADO
 * @property TipoDocumento $nMTIPODOCUMENTO
 */
class Cliente extends \yii\db\ActiveRecord
{
    const CLIENTE_ACTIVO = 1;
    const CLIENTE_INACTIVO = 2;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NM_DOCUMENTO_NIT', 'NM_TIPO_DOCUMENTO_ID', 'NM_TELEFONO', 'NM_ESTADO_ID'], 'integer', 'min' => 0],
            [['DS_NOMBRES_CLIENTE', 'DS_CORREO', 'NM_ESTADO_ID', 'NM_DOCUMENTO_NIT', 'NM_TELEFONO'], 'required'],
            [['DT_FECHA_CREACION', 'DF_FECHA_ACTUALIZACION'], 'safe'],
            [['DS_NOMBRES_CLIENTE', 'DS_APELLIDOS_CLIENTE'], 'string', 'max' => 30],
            [['NM_CELULAR'], 'string', 'max' => 20],
            [['DS_CORREO'], 'string', 'max' => 50],
            [['NM_DOCUMENTO_NIT'], 'match', 'pattern' => '/^[1-9]\d*$/'],
            ['DS_CORREO', 'email'],
            [['DS_DIRECCION'], 'string', 'max' => 200],
            [['NM_DOCUMENTO_NIT', 'DS_CORREO'], 'unique'],
            [['NM_ESTADO_ID'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoCliente::className(), 'targetAttribute' => ['NM_ESTADO_ID' => 'NM_ESTADO_ID']],
            [['NM_TIPO_DOCUMENTO_ID'], 'exist', 'skipOnError' => true, 'targetClass' => TipoDocumento::className(), 'targetAttribute' => ['NM_TIPO_DOCUMENTO_ID' => 'CS_TIPO_DOCUMENTO_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_CLIENTE' => Yii::t('app', 'ID'),
            'NM_DOCUMENTO_NIT' => Yii::t('app', 'Documento o Nit'),
            'NM_TIPO_DOCUMENTO_ID' => Yii::t('app', 'Tipo Documento'),
            'DS_NOMBRES_CLIENTE' => Yii::t('app', 'Nombre o razón social'),
            'DS_APELLIDOS_CLIENTE' => Yii::t('app', 'Apellidos'),
            'NM_TELEFONO' => Yii::t('app', 'Teléfono'),
            'NM_CELULAR' => Yii::t('app', 'Celular'),
            'DS_CORREO' => Yii::t('app', 'Correo'),
            'DS_DIRECCION' => Yii::t('app', 'Dirección'),
            'NM_ESTADO_ID' => Yii::t('app', 'Estado'),
            'DT_FECHA_CREACION' => Yii::t('app', 'Fecha creación'),
            'DF_FECHA_ACTUALIZACION' => Yii::t('app', 'Fecha Actualización'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMESTADO()
    {
        return $this->hasOne(EstadoCliente::className(), ['NM_ESTADO_ID' => 'NM_ESTADO_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMTIPODOCUMENTO()
    {
        return $this->hasOne(TipoDocumento::className(), ['CS_TIPO_DOCUMENTO_ID' => 'NM_TIPO_DOCUMENTO_ID']);
    }

    /**
     * {@inheritdoc}
     * @return ClienteQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClienteQuery(get_called_class());
    }
}
