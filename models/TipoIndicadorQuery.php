<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoIndicador]].
 *
 * @see TipoIndicador
 */
class TipoIndicadorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TipoIndicador[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TipoIndicador|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
