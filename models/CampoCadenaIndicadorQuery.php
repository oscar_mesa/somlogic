<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CampoCadenaIndicador]].
 *
 * @see CampoCadenaIndicador
 */
class CampoCadenaIndicadorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CampoCadenaIndicador[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CampoCadenaIndicador|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
