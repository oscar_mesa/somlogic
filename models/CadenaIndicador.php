<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%cadena_indicador}}".
 *
 * @property int $ID_INDICADOR_CADENA
 * @property int $ID_INDICADOR
 * @property int $ID_CADENA
 * @property double $NM_RESULTADO_INDICADOR
 * @property int $NM_PORCENTAJE_SIGNIFICANCIA
 * @property int $ID_PEDIDO
 * @property int $USUARIO_CREACION
 * @property int $USUARIO_ACTUALIZACION
 * @property double $META
 * @property int $FECHA_CREACION
 * @property int $FECHA_ACTUALIZACION
 *
 * @property Indicador $iNDICADOR
 * @property CadenaEtapa $cADENA
 * @property CampoCadenaIndicador[] $campoCadenaIndicadors
 */
class CadenaIndicador extends \yii\db\ActiveRecord
{
    public $IDCANDEABAS;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cadena_indicador}}';
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            
            if ($this->isNewRecord) {
                $this->USUARIO_CREACION = Yii::$app->user->getIdentity()->ID_ACTOR;
            }else{
                $this->USUARIO_ACTUALIZACION = Yii::$app->user->getIdentity()->ID_ACTOR;
            }
            return true;
        } else {
            return false;
        }
    }

    public function validarPorcentajedesignificanciaPorPedido($attribute) {
        if(!empty($this->ID_PEDIDO)){
            $total_pocentaje = self::find()->joinWith(['cADENAETAPA'])
                        ->where(['ID_PEDIDO'=>$this->ID_PEDIDO, 'cadena_etapa.ID_CADENA' => $this->IDCANDEABAS])
                        ->andFilterWhere(['<>', 'ID_INDICADOR_CADENA', $this->ID_INDICADOR_CADENA])
                        ->sum('NM_PORCENTAJE_SIGNIFICANCIA'); 
            if (($total_pocentaje+$this->NM_PORCENTAJE_SIGNIFICANCIA) > 100) {
                $this->addError($attribute, 'La ponderación para este pedido ya se encuentra en el '.$total_pocentaje.'%, adicionando la de este indicador, ya supera el 100%, ajustelo para que no supere dicho valor.');
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_INDICADOR', 'ID_CADENA', 'NM_PORCENTAJE_SIGNIFICANCIA', 'ID_PEDIDO'], 'integer'],
            [['NM_RESULTADO_INDICADOR', 'META'], 'number'],
            //['ID_PEDIDO', 'validarPorcentajedesignificanciaPorPedido',],
            [['ID_INDICADOR'], 'exist', 'skipOnError' => true, 'targetClass' => Indicador::className(), 'targetAttribute' => ['ID_INDICADOR' => 'ID_INDICADOR']],
            [['ID_CADENA'], 'exist', 'skipOnError' => true, 'targetClass' => CadenaEtapa::className(), 'targetAttribute' => ['ID_CADENA' => 'ID_ETP_CAD']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_INDICADOR_CADENA' => Yii::t('app', 'Id Indicador Cadena'),
            'ID_INDICADOR' => Yii::t('app', 'Id Indicador'),
            'ID_CADENA' => Yii::t('app', 'Id Cadena'),
            'NM_RESULTADO_INDICADOR' => Yii::t('app', 'Nm Resultado Indicador'),
            'NM_PORCENTAJE_SIGNIFICANCIA' => Yii::t('app', 'Ponderación'),
            'ID_PEDIDO' => Yii::t('app', 'Pedido'),
            'META'  => Yii::t('app', 'Meta'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getiNDICADOR()
    {
        return $this->hasOne(Indicador::className(), ['ID_INDICADOR' => 'ID_INDICADOR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getcADENAETAPA()
    {
        return $this->hasOne(CadenaEtapa::className(), ['ID_ETP_CAD' => 'ID_CADENA']);
    }

    /**
    *
    */
    public function getiDPEDIDO()
    {
        return $this->hasOne(Pedido::className(), ['CS_PEDIDO_ID' => 'ID_PEDIDO']);
    }

    /**
    *
    */
    public function getuSUARIOCREACION()
    {
        return $this->hasOne(Actor::className(), ['ID_ACTOR' => 'USUARIO_CREACION']);
    }    

    /**
    *
    */
    public function getuSUARIOACTUALIZACION()
    {
        return $this->hasOne(Actor::className(), ['ID_ACTOR' => 'USUARIO_ACTUALIZACION']);
    }   
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampoCadenaIndicadors()
    {
        return $this->hasMany(CampoCadenaIndicador::className(), ['ID_CADENA_INDICADOR' => 'ID_INDICADOR_CADENA']);
    }

    /**
     * {@inheritdoc}
     * @return CadeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CadeQuery(get_called_class());
    }
}
