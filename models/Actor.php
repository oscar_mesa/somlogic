<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property int $ID_ACTOR
 * @property string $NM_DOCUMENTO_ID
 * @property int $NM_TIPO_DOCUMENTO_ID
 * @property string $DS_NOMBRES_ACTOR
 * @property string $DS_APELLIDOS_ACTOR
 * @property int $NM_TELEFONO
 * @property string $NM_CELULAR
 * @property string $DS_CORREO
 * @property string $DS_DIRECCION
 * @property string $DS_CONTRASENA
 * @property int $NM_TIPO_ACTOR_ID
 * @property int $NM_ESTADO_ID
 * @property int $ID_SEDE_CLIENTE
 * @property string $DT_FECHA_CREACION
 * @property string $DF_FECHA_ACTUALIZACION 
 * @property string $CLAVE_AUTENTICACION
 * @property string $PAGINA_WEB
 * @property string $CONTACTO
 * @property string $CARGO_CONTACTO
 *
 * @property Autenticacion[] $autenticacions
 * @property Factura[] $facturas
 * @property Factura[] $facturas0
 * @property EstadoActor $nMESTADO
 * @property TipoDocumento $nMTIPODOCUMENTO
 * @property TipoActor $nMTipoActor
 */
class Actor extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const SCENARIO_REDES = "redes";
    const SCENARIO_REGIRSTRO = "registro";
    const SCENARIO_CREATE_UPDATE = "creacion_modificacion";
    const SCENARIO_CREATE_UPDATE_NOREQ = "creacion_modificacion_no_requridos";
    const SCENARIO_TARE_CREDENTIALS = "restaurar_credenciales_usuario";
    const SCENARIO_DELETE = "eliminar";
    public $CONFIRMAR_CONTRASENA;
    public $reCaptcha;
    public $username;
   

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'actor';
    }
    
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_REDES] = ['DS_NOMBRES_ACTOR', 'DS_APELLIDOS_ACTOR', 'NM_ESTADO_ID', 'DS_CORREO', 'DS_CONTRASENA'];
        $scenarios[self::SCENARIO_CREATE_UPDATE] = ['NM_DOCUMENTO_ID','NM_TIPO_DOCUMENTO_ID','DS_NOMBRES_ACTOR','DS_APELLIDOS_ACTOR','NM_TELEFONO','NM_CELULAR','DS_CORREO','DS_DIRECCION','DS_CONTRASENA','NM_TIPO_ACTOR_ID','NM_ESTADO_ID','DT_FECHA_CREACION','CONFIRMAR_CONTRASENA','DF_FECHA_ACTUALIZACION', 'PAGINA_WEB', 'CONTACTO', 'CARGO_CONTACTO'];
        $scenarios[self::SCENARIO_CREATE_UPDATE_NOREQ] = ['NM_DOCUMENTO_ID','NM_TIPO_DOCUMENTO_ID','DS_NOMBRES_ACTOR','DS_APELLIDOS_ACTOR','NM_TELEFONO','NM_CELULAR','DS_CORREO','DS_DIRECCION','NM_TIPO_ACTOR_ID','NM_ESTADO_ID','DT_FECHA_CREACION', 'DF_FECHA_ACTUALIZACION', 'PAGINA_WEB', 'CONTACTO', 'CARGO_CONTACTO'];
        $scenarios[self::SCENARIO_DELETE] = ['NM_ESTADO_ID'];
        $scenarios[self::SCENARIO_TARE_CREDENTIALS] = ['DS_CORREO'];
        return $scenarios;
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NM_DOCUMENTO_ID', 'NM_TIPO_DOCUMENTO_ID', 'DS_NOMBRES_ACTOR', 'NM_CELULAR', 'DS_CORREO', 'DS_CONTRASENA', 'NM_ESTADO_ID', 'CONFIRMAR_CONTRASENA'], 'required'],
            [['NM_DOCUMENTO_ID', 'NM_TIPO_DOCUMENTO_ID', 'NM_TIPO_ACTOR_ID', 'NM_ESTADO_ID'], 'integer'],
            [['DT_FECHA_CREACION', 'PAGINA_WEB', 'CONTACTO', 'CARGO_CONTACTO'], 'safe'],
            [['NM_DOCUMENTO_ID'], 'match', 'pattern' => '/^[1-9]\d*$/'],
            ["CONFIRMAR_CONTRASENA", 'compare', 'compareAttribute' => 'DS_CONTRASENA', 'message' => 'Las contraseñas nos son iguales'],
            [['DS_NOMBRES_ACTOR', 'DS_APELLIDOS_ACTOR'], 'string', 'max' => 30],
            [['NM_CELULAR', 'NM_TELEFONO'], 'string', 'max' => 20],
            [['DS_CORREO'], 'string', 'max' => 50],
            ['DS_CORREO', 'validarExistenciaCorreo',],
            ['DS_CORREO', 'email'],
            [['DS_DIRECCION'], 'string', 'max' => 200],
            [['CLAVE_AUTENTICACION'], 'string', 'max' => 60],
            [['DS_CONTRASENA'], 'string', 'max' => 100],
            [['NM_DOCUMENTO_ID', 'DS_CORREO', 'CLAVE_AUTENTICACION'], 'unique'],
            [['NM_ESTADO_ID'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoActor::className(), 'targetAttribute' => ['NM_ESTADO_ID' => 'CS_ESTADO_ID']],
            [['NM_TIPO_DOCUMENTO_ID'], 'exist', 'skipOnError' => true, 'targetClass' => TipoDocumento::className(), 'targetAttribute' => ['NM_TIPO_DOCUMENTO_ID' => 'CS_TIPO_DOCUMENTO_ID']],
            [['NM_TIPO_ACTOR_ID'], 'exist', 'skipOnError' => true, 'targetClass' => TipoActor::className(), 'targetAttribute' => ['NM_TIPO_ACTOR_ID' => 'CS_tipo_actor']],
            ['reCaptcha', \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LcjNXoUAAAAALGCOz8fTKs9JgTXGD8_puSU9kFj',  'uncheckedMessage' => 'Por favor, confirme que no es un robot.'],
        ];
    }
    
    public function validarExistenciaCorreo($attribute) {
//    var_dump(self::find()->where(["DS_CORREO" =>  $this->DS_CORREO])->count());
//        die($this->DS_CORREO);die;
        if (self::SCENARIO_TARE_CREDENTIALS == $this->scenario && self::find()->where(["DS_CORREO" =>  $this->DS_CORREO])->count() == 0)
            $this->addError($attribute, 'Este correo no existe, por favor verifique.');
    }
    
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_ACTOR' => Yii::t('app', 'Id'),
            'NM_DOCUMENTO_ID' => Yii::t('app', 'Documento'),
            'NM_TIPO_DOCUMENTO_ID' => Yii::t('app', 'Tipo documento'),
            'DS_NOMBRES_ACTOR' => Yii::t('app', 'Nombres'),
            'DS_APELLIDOS_ACTOR' => Yii::t('app', 'Apellidos'),
            'NM_TELEFONO' => Yii::t('app', 'Teléfono'),
            'NM_CELULAR' => Yii::t('app', 'Celular'),
            'DS_CORREO' => Yii::t('app', 'Correo'),
            'DS_DIRECCION' => Yii::t('app', 'Dirección'),
            'DS_CONTRASENA' => Yii::t('app', 'Contraseña'),
            'NM_TIPO_ACTOR_ID' => Yii::t('app', 'Tipo actor'),
            'NM_ESTADO_ID' => Yii::t('app', 'Estado'),
            'DT_FECHA_CREACION' => Yii::t('app', 'Fecha creación'),
            'CONFIRMAR_CONTRASENA' => Yii::t('app', 'Confirmar contraseña'),
            'CLAVE_AUTENTICACION' => Yii::t('app', 'Clave de autenticación'),
            'DF_FECHA_ACTUALIZACION' => Yii::t('app', 'Fecha de actualización'),
            'CLAVE_AUTENTICACION' => Yii::t('app', 'Clave de autenticación'),
            'reCaptcha' => '',
            'PAGINA_WEB' => Yii::t('app', 'Página Web'),
            'CONTACTO'  => Yii::t('app', 'Contacto'),
            'CARGO_CONTACTO'  => Yii::t('app', 'Cargo contacto'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMSEDECLIENTE()
    {
        return $this->hasOne(SedeCliente::className(), ['ID_SEDE' => 'NM_SEDE_CLIENTE_ID']);
    }
    
    
    public function beforeValidate() {
        if($this->isNewRecord){
            $this->setClaveDeAutenticacion();
        }
        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturas()
    {
        return $this->hasMany(Factura::className(), ['NM_CLIENTE_ID' => 'ID_ACTOR']);
    }
    
    /**
    * @return \yii\db\ActiveQuery
    */
   public function getAutenticacions()
   {
       return $this->hasMany(Autenticacion::className(), ['ID_ACTOR' => 'ID_ACTOR']);
   }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturas0()
    {
        return $this->hasMany(Factura::className(), ['NM_VENDEDOR_ID' => 'ID_ACTOR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMESTADO()
    {
        return $this->hasOne(EstadoActor::className(), ['CS_ESTADO_ID' => 'NM_ESTADO_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMTIPODOCUMENTO()
    {
        return $this->hasOne(TipoDocumento::className(), ['CS_TIPO_DOCUMENTO_ID' => 'NM_TIPO_DOCUMENTO_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMTipoActor()
    {
        return $this->hasOne(TipoActor::className(), ['CS_tipo_actor' => 'NM_TIPO_ACTOR_ID']);
    }

    /**
     * {@inheritdoc}
     * @return ActorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ActorQuery(get_called_class());
    }
    
    public function validatePassword($password) {
        return \Yii::$app->security->validatePassword($password, $this->DS_CONTRASENA);
    }

    public function getAuthKey(): string {
        return !is_null($this->CLAVE_AUTENTICACION)?$this->CLAVE_AUTENTICACION:"";
    }

    public function getId() {
        return $this->ID_ACTOR;
    }

    public function validateAuthKey($authKey): bool {
        return $this->CLAVE_AUTENTICACION === $authKey;
    }

    public static function findIdentity($id): \yii\web\IdentityInterface {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null): \yii\web\IdentityInterface {
        return null;
    }

    public function setClaveDeAutenticacion() {
        $this->CLAVE_AUTENTICACION = Yii::$app->security->generateRandomString(60);
    }
    
    public function getRols() {
        return Yii::$app->authManager->getRolesByUser($this->ID_ACTOR);
    }

    public function removerAllRols(){
        foreach ($this->getRols() as $rol) {
            Yii::$app->authManager->revoke($rol, $this->ID_ACTOR);
        }
    }

}
