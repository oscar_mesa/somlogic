<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VwIdlPedidos;

/**
 * VwIdlPedidosSearch represents the model behind the search form of `app\models\VwIdlPedidos`.
 */
class VwIdlPedidosSearch extends VwIdlPedidos
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DS_CODIGO_PEDIDO'], 'safe'],
            [['NM_CADENA', 'CS_PEDIDO_ID'], 'integer'],
            [['TOTAL'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VwIdlPedidos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'NM_CADENA' => $this->NM_CADENA,
            'CS_PEDIDO_ID' => $this->CS_PEDIDO_ID,
        ]);

        $query->andFilterWhere(['like', 'DS_CODIGO_PEDIDO', $this->DS_CODIGO_PEDIDO])
                ->andFilterWhere(['like', 'TOTAL', $this->TOTAL]);

        return $dataProvider;
    }
}
