<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%vw_total_indicadores_ca}}".
 *
 * @property string $DS_NOMBRE_INDICADOR
 * @property double $NM_RESULTADO_INDICADOR
 * @property int $NM_PORCENTAJE_SIGNIFICANCIA
 * @property int $ID_INDICADOR
 * @property string $ETAPA
 * @property int $ID_CADENA
 * @property int $ID_INDICADOR_CADENA
 * @property string $DS_FORMULA
 * @property string $DS_NOMENCLATURA
 * @property string $DEFINICION
 * @property string $DS_AREA
 * @property int $ID_ETAPA
 * @property int $UNIDAD
 * @property int $DS_CODIGO_PEDIDO
 * @property double $RESULTADO_SIGNIFICANCIA
 * @property double $META
 * @property int $NM_ID_TIPO_IND
 */
class VwTotalIndicadoresCa extends \yii\db\ActiveRecord
{
    public $PORCENTAJE_DE_CUMPLIMIENTO;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%vw_total_indicadores_ca}}';
    }

    /**
     * @inheritdoc$primaryKey
     */
    public static function primaryKey()
    {
        return ["ID_INDICADOR_CADENA"];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NM_RESULTADO_INDICADOR'], 'number'],
            [['NM_PORCENTAJE_SIGNIFICANCIA', 'ID_INDICADOR', 'ID_CADENA', 'NM_ID_TIPO_IND'], 'integer'],
            [['DEFINICION'], 'string'],
            [['DS_NOMBRE_INDICADOR', 'ETAPA', 'DS_FORMULA', 'DS_NOMENCLATURA', 'DS_AREA'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DS_NOMBRE_INDICADOR' => Yii::t('app', 'Indicador'),
            'NM_RESULTADO_INDICADOR' => Yii::t('app', 'Resultado Indicador'),
            'NM_PORCENTAJE_SIGNIFICANCIA' => Yii::t('app', 'Ponderación'),
            'ID_INDICADOR' => Yii::t('app', 'Id Indicador'),
            'ETAPA' => Yii::t('app', 'Proceso'),
            'ID_CADENA' => Yii::t('app', 'Cadena'),
            'DS_FORMULA' => Yii::t('app', 'Formula'),
            'DS_NOMENCLATURA' => Yii::t('app', 'Nomenclatura'),
            'DEFINICION' => Yii::t('app', 'Definicion'),
            'DS_AREA' => Yii::t('app', 'Area'),
            'UNIDAD' => Yii::t('app', 'Unidad'),
            'ID_INDICADOR_CADENA' => Yii::t('app', 'ID'),
            'ID_ETAPA' => 'ID etapa',
            'RESULTADO_SIGNIFICANCIA' => Yii::t('app', 'Resultado de significancia'),
            'DS_CODIGO_PEDIDO' => Yii::t('app', 'Pedido'),
            'META' => Yii::t('app', 'Meta'),
            'NM_ID_TIPO_IND' => Yii::t('app', 'Id tipo indicador'),
            'PORCENTAJE_DE_CUMPLIMIENTO' => Yii::t('app', 'Porcentaje de cumplimiento'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return VwTotalIndicadoresCaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VwTotalIndicadoresCaQuery(get_called_class());
    }
}
 