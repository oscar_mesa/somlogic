<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Autenticacion]].
 *
 * @see Autenticacion
 */
class AutenticacionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Autenticacion[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Autenticacion|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
