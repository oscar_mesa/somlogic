<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producto".
 *
 * @property int $CS_PRODUCTO_ID
 * @property string $DS_CODIGO_PRODUCTO
 * @property string $DS_NOMBRE_PRODUCTO
 * @property string $DS_DESCRIPCION_PRODUCTO
 * @property int $FK_UNIDAD
 * @property string $DT_FECHA_CREACION
 * @property int $NM_ELIMINADO
 * @property int $IVA
 * @property int $PORCENTAJE_DESCUENTO
 *
 * @property FacturaDetalle[] $facturaDetalles
 * @property InventarioProducto[] $inventarioProductos
 * @property UnidadProducto $fKUNIDAD
 */
class Producto extends \yii\db\ActiveRecord
{
    public $CANTIDAD_INVENTARIO;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'producto';
    }
    
    public function init() {
        $this->IVA = 0;
        $this->PORCENTAJE_DESCUENTO = 0;
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DS_CODIGO_PRODUCTO', 'DS_NOMBRE_PRODUCTO', 'FK_UNIDAD'], 'required'],
            [['FK_UNIDAD', 'NM_ELIMINADO', 'PORCENTAJE_DESCUENTO','IVA'], 'integer', 'min' => 0],
            [['DS_CODIGO_PRODUCTO', 'DS_NOMBRE_PRODUCTO', 'FK_UNIDAD'], 'required'],
            [['FK_UNIDAD', 'NM_ELIMINADO', 'NM_USUARIO_CREADOR', 'IVA', 'PORCENTAJE_DESCUENTO','NM_ID_PROVEEDOR'], 'integer'],
            [['DT_FECHA_CREACION'], 'safe'],
            [['IVA'],'default', 'value' => 0],
            [['DS_CODIGO_PRODUCTO'], 'string', 'max' => 10],
            [['DS_NOMBRE_PRODUCTO'], 'string', 'max' => 50],
            [['DS_DESCRIPCION_PRODUCTO'], 'string', 'max' => 255],
            [['DS_CODIGO_PRODUCTO'], 'unique'],
            [['FK_UNIDAD'], 'exist', 'skipOnError' => true, 'targetClass' => UnidadProducto::className(), 'targetAttribute' => ['FK_UNIDAD' => 'CS_UNIDAD_ID']],
            [['NM_ID_PROVEEDOR'], 'exist', 'skipOnError' => true, 'targetClass' => Actor::className(), 'targetAttribute' => ['NM_ID_PROVEEDOR' => 'ID_ACTOR']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CS_PRODUCTO_ID' => Yii::t('app', 'ID'),
            'DS_CODIGO_PRODUCTO' => Yii::t('app', 'Código'),
            'DS_NOMBRE_PRODUCTO' => Yii::t('app', 'Nombre'),
            'DS_DESCRIPCION_PRODUCTO' => Yii::t('app', 'Descripción'),
            'FK_UNIDAD' => Yii::t('app', 'Unidad'),
            'DT_FECHA_CREACION' => Yii::t('app', 'Fecha creación'),
            'NM_ELIMINADO' => Yii::t('app', 'Eliminado'),
            'NM_USUARIO_CREADOR' => Yii::t('app', 'Creador'),
            'IVA' => Yii::t('app', 'Iva'),
            'PORCENTAJE_DESCUENTO'  => Yii::t('app', '% Descuento'),
            'NM_ID_PROVEEDOR' => Yii::t('app', 'Proveedor'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturaDetalles()
    {
        return $this->hasMany(FacturaDetalle::className(), ['CS_PRODUCTO_ID' => 'CS_PRODUCTO_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getNMIDPROVEEDOR() {
        return $this->hasOne(Actor::className(), ['ID_ACTOR' => 'NM_ID_PROVEEDOR' ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventarioProductos()
    {
        return $this->hasMany(InventarioProducto::className(), ['CS_PRODUCTO_ID' => 'CS_PRODUCTO_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getfKUNIDAD()
    {
        return $this->hasOne(UnidadProducto::className(), ['CS_UNIDAD_ID' => 'FK_UNIDAD']);
    }

    /**
     * {@inheritdoc}
     * @return ProductoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProductoQuery(get_called_class());
    }
}
