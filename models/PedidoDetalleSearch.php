<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PedidoDetalle;

/**
 * PedidoDetalleSearch represents the model behind the search form of `app\models\PedidoDetalle`.
 */
class PedidoDetalleSearch extends PedidoDetalle
{
    public $ESPEDIDO = false;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NM_ID_DETALLE_PEDIDO', 'CS_PEDIDO_ID', 'NM_CANTIDAD_COMPRA', 'CS_PRODUCTO_ID', 'NM_USUARIO_CREADOR', 'PORCENTAJE_IVA', 'PORCENTAJE_DESCUENTO', 'MG', 'FK_UNIDAD'], 'integer'],
            [['NM_PRECIO_TOTAL_PRODUCTO', 'NM_PRECIO_UNITARIO', 'NM_VALOR_IVA', 'NM_SUB_TOTAL', 'VALOR_DESCUENTO', 'FC'], 'number'],
            [['DS_UQ_SESSION', 'DT_FECHA_CREACION'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PedidoDetalle::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        if($this->ESPEDIDO)
            $query->where(["CS_PEDIDO_ID" => null, 'pedido_detalle.NM_USUARIO_CREADOR' => Yii::$app->user->getIdentity()->ID_ACTOR]); 
        $query->andFilterWhere([
            'NM_ID_DETALLE_PEDIDO' => $this->NM_ID_DETALLE_PEDIDO,
            'CS_PEDIDO_ID' => $this->CS_PEDIDO_ID,
            'NM_CANTIDAD_COMPRA' => $this->NM_CANTIDAD_COMPRA,
            'CS_PRODUCTO_ID' => $this->CS_PRODUCTO_ID,
            'NM_PRECIO_TOTAL_PRODUCTO' => $this->NM_PRECIO_TOTAL_PRODUCTO,
            'NM_PRECIO_UNITARIO' => $this->NM_PRECIO_UNITARIO,
            'NM_USUARIO_CREADOR' => $this->NM_USUARIO_CREADOR,
            'PORCENTAJE_IVA' => $this->PORCENTAJE_IVA,
            'PORCENTAJE_DESCUENTO' => $this->PORCENTAJE_DESCUENTO,
            'NM_VALOR_IVA' => $this->NM_VALOR_IVA,
            'NM_SUB_TOTAL' => $this->NM_SUB_TOTAL,
            'VALOR_DESCUENTO' => $this->VALOR_DESCUENTO,
            'DT_FECHA_CREACION' => $this->DT_FECHA_CREACION,
            'FC' => $this->FC,
            'MG' => $this->MG,
            'FK_UNIDAD' => $this->FK_UNIDAD,
        ]);

        $query->andFilterWhere(['like', 'DS_UQ_SESSION', $this->DS_UQ_SESSION]);

        return $dataProvider;
    }
}
