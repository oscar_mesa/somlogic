<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%cadena_etapa}}".
 *
 * @property int $ID_ETP_CAD
 * @property int $ID_ETAPA
 * @property int $ID_CADENA
 * @property int $ID_ACTOR
 * @property double $CUMPLIMIENTO_PROMEDIO
 * @property double $NM_PORCENTAJE_SIGNIFICANCIA
 *
 * @property Actor $aCTOR
 * @property Cadenadeabastecimiento $cADENA
 * @property Etapa $eTAPA
 * @property CadenaIndicador[] $cadenaIndicadors
 */
class CadenaEtapa extends \yii\db\ActiveRecord
{
    public $TIPO_ACTOR;
    public $eTTAPA;
    public $idl;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cadena_etapa}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_ETAPA', 'ID_CADENA', 'ID_ACTOR'], 'integer'],
            [['CUMPLIMIENTO_PROMEDIO'], 'number'],
            [['ID_ACTOR'], 'exist', 'skipOnError' => true, 'targetClass' => Actor::className(), 'targetAttribute' => ['ID_ACTOR' => 'ID_ACTOR']],
            [['ID_CADENA'], 'exist', 'skipOnError' => true, 'targetClass' => Cadenadeabastecimiento::className(), 'targetAttribute' => ['ID_CADENA' => 'NM_CADENA_ID']],
            [['ID_ETAPA'], 'exist', 'skipOnError' => true, 'targetClass' => Etapa::className(), 'targetAttribute' => ['ID_ETAPA' => 'ID_ETAPA']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_ETP_CAD' => Yii::t('app', 'Id Etp Cad'),
            'ID_ETAPA' => Yii::t('app', 'Id Etapa'),
            'ID_CADENA' => Yii::t('app', 'Id Cadena'),
            'ID_ACTOR' => Yii::t('app', 'Nombre'),
            'TIPO_ACTOR' => Yii::t('app', 'Tipo de Actor'),
            'CUMPLIMIENTO_PROMEDIO'  => Yii::t('app', 'Cumplimiento promedio'),
            'NM_PORCENTAJE_SIGNIFICANCIA' => Yii::t('app', 'Ponderación'),
            'eTTAPA' => Yii::t('app', 'Proceso'),
            'idl' => Yii::t('app', 'IDL'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getACTOR()
    {
        return $this->hasOne(Actor::className(), ['ID_ACTOR' => 'ID_ACTOR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getcADENA()
    {
        return $this->hasOne(Cadenadeabastecimiento::className(), ['NM_CADENA_ID' => 'ID_CADENA']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function geteTAPA()
    {
        return $this->hasOne(Etapa::className(), ['ID_ETAPA' => 'ID_ETAPA']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCadenaIndicadors()
    {
        return $this->hasMany(CadenaIndicador::className(), ['ID_CADENA' => 'ID_ETP_CAD']);
    }

    /**
     * {@inheritdoc}
     * @return CadenaEtapaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CadenaEtapaQuery(get_called_class());
    }
}
