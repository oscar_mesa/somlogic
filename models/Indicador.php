<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%indicador}}".
 *
 * @property int $ID_INDICADOR
 * @property string $DS_NOMBRE_INDICADOR
 * @property int $ID_AREA
 * @property string $DS_FORMULA
 * @property string $DS_NOMENCLATURA
 * @property string $DEFINICION
 * @property string $UNIDAD
 * @property int $NM_ELIMINADO
 * @property int $NM_ID_TIPO_IND
 *
 * @property CadenaIndicador[] $cadenaIndicadors
 * @property Area $aREA
 */
class Indicador extends \yii\db\ActiveRecord
{
    public $CHK;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%indicador}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_AREA', 'DS_NOMBRE_INDICADOR', 'DS_FORMULA', 'UNIDAD', 'NM_ID_TIPO_IND'], 'required'],            
            [['ID_AREA', 'NM_ID_TIPO_IND'], 'integer'],
            [['DEFINICION'], 'string'],
            [['CHK', 'UNIDAD'], 'safe'],
            [['DS_NOMBRE_INDICADOR', 'DS_FORMULA', 'DS_NOMENCLATURA'], 'string', 'max' => 255],
            [['ID_AREA'], 'exist', 'skipOnError' => true, 'targetClass' => Area::className(), 'targetAttribute' => ['ID_AREA' => 'ID_AREA']],
            [['NM_ID_TIPO_IND'], 'exist', 'skipOnError' => true, 'targetClass' => TipoIndicador::className(), 'targetAttribute' => ['NM_ID_TIPO_IND' => 'ID_TIPO']], 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_INDICADOR' => Yii::t('app', 'ID'),
            'DS_NOMBRE_INDICADOR' => Yii::t('app', 'Indicador'),
            'ID_AREA' => Yii::t('app', 'Area'),
            'DS_FORMULA' => Yii::t('app', 'Fórmula'),
            'DS_NOMENCLATURA' => Yii::t('app', 'Nomenclatura'),
            'DEFINICION' => Yii::t('app', 'Definición'),
            'CHK' => Yii::t('app', ''),
            'UNIDAD' => Yii::t('app', 'Unidad'),
            'NM_ELIMINADO' => Yii::t('app', 'Eliminado'),
            'NM_ID_TIPO_IND' => Yii::t('app', 'Tipo de indicador'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCadenaIndicadors()
    {
        return $this->hasMany(CadenaIndicador::className(), ['ID_INDICADOR' => 'ID_INDICADOR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getaREA()
    {
        return $this->hasOne(Area::className(), ['ID_AREA' => 'ID_AREA']);
    }

    public function gettIPOINDICADOR()
    {
        return $this->hasOne(TipoIndicador::className(), ['ID_TIPO' => 'NM_ID_TIPO_IND']);
    }

    /**
     * {@inheritdoc}
     * @return IndicadorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IndicadorQuery(get_called_class());
    }
}
