<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Producto;

/**
 * ProductoSearch represents the model behind the search form of `app\models\Producto`.
 */
class ProductoSearch extends Producto
{
    public $CATEGORIA, $REFERENTE, $ESPEDIDO = false;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CS_PRODUCTO_ID', 'FK_UNIDAD', 'NM_ELIMINADO'], 'integer'],
           [['CS_PRODUCTO_ID', 'FK_UNIDAD', 'NM_ELIMINADO', 'NM_USUARIO_CREADOR', 'IVA', 'PORCENTAJE_DESCUENTO','NM_ID_PROVEEDOR'], 'integer'],
            [['DS_CODIGO_PRODUCTO', 'DS_NOMBRE_PRODUCTO', 'DS_DESCRIPCION_PRODUCTO', 'DT_FECHA_CREACION'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Producto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query        
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            return $dataProvider;
        }

            $query->where('0=0');
        // grid filtering conditions
        $query->andFilterWhere([
            'CS_PRODUCTO_ID' => $this->CS_PRODUCTO_ID,
            'FK_UNIDAD' => $this->FK_UNIDAD,
            'DT_FECHA_CREACION' => $this->DT_FECHA_CREACION,
            'DS_CODIGO_PRODUCTO' => $this->DS_CODIGO_PRODUCTO,
            'NM_ELIMINADO' => 0,
            'NM_USUARIO_CREADOR' => $this->NM_USUARIO_CREADOR,
        ]);

        $query->andFilterWhere(['like', 'DS_NOMBRE_PRODUCTO', $this->DS_NOMBRE_PRODUCTO])
            ->andFilterWhere(['like', 'DS_DESCRIPCION_PRODUCTO', $this->DS_DESCRIPCION_PRODUCTO])
            ->andFilterWhere(['like', 'DS_CODIGO_PRODUCTO', $this->DS_CODIGO_PRODUCTO]);
        
        return $dataProvider;
    }
}
