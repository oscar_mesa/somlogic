<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CampoCaculo]].
 *
 * @see CampoCaculo
 */
class CampoCaculoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CampoCaculo[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CampoCaculo|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
