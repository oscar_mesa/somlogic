<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PedidoCadenabastecimiento]].
 *
 * @see PedidoCadenabastecimiento
 */
class PedidoCadenabastecimientoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return PedidoCadenabastecimiento[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return PedidoCadenabastecimiento|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
