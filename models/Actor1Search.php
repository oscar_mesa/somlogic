<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Actor1;

/**
 * Actor1Search represents the model behind the search form of `app\models\Actor1`.
 */
class Actor1Search extends Actor1
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_ACTOR', 'NM_DOCUMENTO_ID', 'NM_TIPO_DOCUMENTO_ID', 'NM_TELEFONO', 'NM_TIPO_ACTOR_ID', 'NM_ESTADO_ID'], 'integer'],
            [['DS_NOMBRES_ACTOR', 'DS_APELLIDOS_ACTOR', 'NM_CELULAR', 'DS_CORREO', 'DS_DIRECCION', 'DS_CONTRASENA', 'DT_FECHA_CREACION', 'DF_FECHA_ACTUALIZACION', 'CLAVE_AUTENTICACION'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Actor1::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID_ACTOR' => $this->ID_ACTOR,
            'NM_DOCUMENTO_ID' => $this->NM_DOCUMENTO_ID,
            'NM_TIPO_DOCUMENTO_ID' => $this->NM_TIPO_DOCUMENTO_ID,
            'NM_TELEFONO' => $this->NM_TELEFONO,
            'NM_TIPO_ACTOR_ID' => $this->NM_TIPO_ACTOR_ID,
            'NM_ESTADO_ID' => EstadoActor::ACTIVO,
            'DT_FECHA_CREACION' => $this->DT_FECHA_CREACION,
            'DF_FECHA_ACTUALIZACION' => $this->DF_FECHA_ACTUALIZACION,
        ]);

        $query->andFilterWhere(['like', 'DS_NOMBRES_ACTOR', $this->DS_NOMBRES_ACTOR])
            ->andFilterWhere(['like', 'DS_APELLIDOS_ACTOR', $this->DS_APELLIDOS_ACTOR])
            ->andFilterWhere(['like', 'NM_CELULAR', $this->NM_CELULAR])
            ->andFilterWhere(['like', 'DS_CORREO', $this->DS_CORREO])
            ->andFilterWhere(['like', 'DS_DIRECCION', $this->DS_DIRECCION])
            ->andFilterWhere(['like', 'DS_CONTRASENA', $this->DS_CONTRASENA])
            ->andFilterWhere(['like', 'CLAVE_AUTENTICACION', $this->CLAVE_AUTENTICACION]);

        return $dataProvider;
    }
}
