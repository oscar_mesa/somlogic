<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_proveedor".
 *
 * @property int $ID_TIPO
 * @property string $NOMBRE_TIPO
 *
 * @property Proveedor[] $proveedors
 */
class TipoProveedor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_proveedor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NOMBRE_TIPO'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_TIPO' => Yii::t('app', 'Id Tipo'),
            'NOMBRE_TIPO' => Yii::t('app', 'Nombre Tipo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProveedors()
    {
        return $this->hasMany(Proveedor::className(), ['ID_TIPO_PROVEEDOR' => 'ID_TIPO']);
    }

    /**
     * {@inheritdoc}
     * @return TipoProveedorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TipoProveedorQuery(get_called_class());
    }
}
