<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use Yii;
/**
 * Description of Mailer
 *
 * @author Oscar
 */

define('NAMEAPP', Yii::$app->name);

class Mailer {
    
    const TYPE_REGISTRATION = 1;
    const TYPE_PASSWORD_RESET = 2;
    const TYPE_RESTAURAR_CONTRASENA = 3;
    private static $form = ["poliaulink@gmail.com" => NAMEAPP];
    private static $to;
    private static $subject;
    private static $renderFile;
    private static $renderParams;
    /**
     * 
     * @param type $type
     * @param Usuario $model
     * @return boolean
     */
    public static function  validate($type, $model){
        switch($type){
            case self::TYPE_REGISTRATION:
                if(empty($model->DS_CORREO) || empty($model->NM_DOCUMENTO_ID)){
                    return false;
                }
                self::$to = $model->DS_CORREO;
                self::$subject = \Yii::t("app", "Su cuenta a sido creada exitosamente");
                self::$renderFile = "registration"; 
                self::$renderParams = ["usuario" => $model ];
                break;
            case self::TYPE_PASSWORD_RESET:
                break;
            case self::TYPE_RESTAURAR_CONTRASENA:
                self::$to = $model->DS_CORREO;
                self::$subject = \Yii::t("app", "Restablecer Credenciales");
                self::$renderFile = "restaurar_credenciales"; 
                self::$renderParams = ["usuario" => $model ];
                break;
            default:
                return false;
        }
        return true;
    }
    
    public static function send($type, $model){
        if(!self::validate($type, $model)){
            return false;
        }
        $message = Yii::$app->mailer->compose(self::$renderFile, self::$renderParams);
        return $message->setFrom(self::$form)->setTo(self::$to)->setSubject(self::$subject)->send();
        
    }
}
