<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "configuracion".
 *
 * @property int $ID
 * @property string $NOMBRE_APLICACION
 * @property int $IVA_FACTURA
 */
class Configuracion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'configuracion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IVA_FACTURA'], 'integer'],
            [['NOMBRE_APLICACION'], 'string', 'max' => 255, 'min' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'NOMBRE_APLICACION' => Yii::t('app', 'Nombre  Aplicacion'),
            'IVA_FACTURA' => Yii::t('app', 'Iva  Factura'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return ConfiguracionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ConfiguracionQuery(get_called_class());
    }
}
