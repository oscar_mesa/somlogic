<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cadenadeabastecimiento".
 *
 * @property int $NM_CADENA_ID
 * @property int $ID_EMPRESA
 * @property string $DS_CADENA_ABASTECIMIENTO
 * @property string $FECHA_PEDIDO
 * @property string $FECHA_FIN
 * @property string $FECHA_CREACION
 */
class Cadenadeabastecimiento extends \yii\db\ActiveRecord
{
    public $PEDIDOS;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cadenadeabastecimiento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['FECHA_PEDIDO', 'FECHA_FIN', 'ID_EMPRESA', 'PEDIDOS'], 'required'],
            [['ID_EMPRESA'], 'integer'],
            [['FECHA_PEDIDO', 'FECHA_FIN', 'FECHA_CREACION'], 'safe'],
            //[['FECHA_PEDIDO', 'FECHA_FIN'], 'date'],
            [['DS_CADENA_ABASTECIMIENTO'], 'string', 'max' => 80],
        ];
    }

    public function afterSave($insert, $changedAttributes) {
        PedidoCadenabastecimiento::deleteAll(['NM_CADENA' => $this->NM_CADENA_ID]);
        foreach ($this->PEDIDOS as $idPedido) {
            $pedidoCadena = new PedidoCadenabastecimiento();
            $pedidoCadena->NM_CADENA = $this->NM_CADENA_ID;
            $pedidoCadena->NM_PEDIDO = $idPedido;
            if(!$pedidoCadena->save()){
                print_r($pedidoCadena->errors);
                die("sd");
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NM_CADENA_ID' => Yii::t('app', 'ID'),
            'ID_EMPRESA' => Yii::t('app', 'Empresa'),
            'DS_CADENA_ABASTECIMIENTO' => Yii::t('app', 'Descripción de la cadena'),
            'FECHA_PEDIDO' => Yii::t('app', 'Fecha de entrada'),
            'FECHA_FIN' => Yii::t('app', 'Fecha de llegada'),
            'PEDIDOS' => Yii::t('app', 'Pedidos'),
            'FECHA_CREACION' => Yii::t('app', 'Fecha de creación')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getpEDIDOS() {
        return $this->hasMany(Pedido::className(), ['CS_PEDIDO_ID' => 'NM_PEDIDO'])
                    ->viaTable('pedido_cadenabastecimiento', ['NM_CADENA' => 'NM_CADENA_ID']);
    }    

    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getiNDICADORES() {
        return $this->hasMany(CadenaIndicador::className(), ['ID_CADENA' => 'NM_CADENA_ID']);
    }  

    public function getiNDICADORESTOTALES()
    {
        return $this->hasMany(VwTotalIndicadoresCa::className(), ['ID_CADENA' => 'NM_CADENA_ID']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getiDEMPRESA() {
        return $this->hasOne(Actor::className(), ['ID_ACTOR' => 'ID_EMPRESA']);
    }


    /**
     * {@inheritdoc}
     * @return CadenadeabastecimientoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CadenadeabastecimientoQuery(get_called_class());
    }
}
