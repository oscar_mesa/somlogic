<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ColorRangoIdl;

/**
 * ColorRangoIdlSearch represents the model behind the search form of `app\models\ColorRangoIdl`.
 */
class ColorRangoIdlSearch extends ColorRangoIdl
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'ID_INDICADOR'], 'integer'],
            [['RANGO1', 'RANGO2'], 'number'],
            [['COLOR'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ColorRangoIdl::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'ID_INDICADOR' => $this->ID_INDICADOR,
            'RANGO1' => $this->RANGO1,
            'RANGO2' => $this->RANGO2,
        ]);

        $query->andFilterWhere(['like', 'COLOR', $this->COLOR]);

        return $dataProvider;
    }
}
