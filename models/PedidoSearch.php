<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pedido;

/**
 * PedidoSearch represents the model behind the search form of `app\models\Pedido`.
 */
class PedidoSearch extends Pedido
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CS_PEDIDO_ID', 'NM_USUARIO_CREADOR_ID', 'NM_EMPRESA', 'ID_ESTADO', 'NM_PORCENTAJE_DESCUENTO', 'PORCENTAJE_IVA'], 'integer'],
            [['DS_CODIGO_PEDIDO', 'DS_NOTAS_PEDIDO', 'DT_FECHA_CREACION', 'DS_CLIENTE', 'GUSTO_PRODUCTOS_CLIENTE'], 'safe'],
            [['NM_PRECIO_SUBTOTAL', 'NM_PRECIO_DESCUENTO', 'NM_PRECIO_TOTAL', 'NM_PRECIO_IVA'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pedido::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'CS_PEDIDO_ID' => $this->CS_PEDIDO_ID,
            'NM_USUARIO_CREADOR_ID' => $this->NM_USUARIO_CREADOR_ID,
            'NM_PRECIO_SUBTOTAL' => $this->NM_PRECIO_SUBTOTAL,
            'NM_PRECIO_DESCUENTO' => $this->NM_PRECIO_DESCUENTO,
            'NM_PRECIO_TOTAL' => $this->NM_PRECIO_TOTAL,
            'NM_PRECIO_IVA' => $this->NM_PRECIO_IVA,
            'DT_FECHA_CREACION' => $this->DT_FECHA_CREACION,
            'NM_EMPRESA' => $this->NM_EMPRESA,
            'ID_ESTADO' => $this->ID_ESTADO,
            'NM_PORCENTAJE_DESCUENTO' => $this->NM_PORCENTAJE_DESCUENTO,
            'PORCENTAJE_IVA' => $this->PORCENTAJE_IVA,
        ]);

        $query->andFilterWhere(['like', 'DS_CODIGO_PEDIDO', $this->DS_CODIGO_PEDIDO])
            ->andFilterWhere(['like', 'DS_NOTAS_PEDIDO', $this->DS_NOTAS_PEDIDO])
            ->andFilterWhere(['like', 'DS_CLIENTE', $this->DS_CLIENTE])
            ->andFilterWhere(['like', 'GUSTO_PRODUCTOS_CLIENTE', $this->GUSTO_PRODUCTOS_CLIENTE]);

        return $dataProvider;
    }
}
