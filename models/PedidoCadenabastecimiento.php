<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pedido_cadenabastecimiento".
 *
 * @property int $ID
 * @property int $NM_CADENA
 * @property int $NM_PEDIDO
 *
 * @property Cadenadeabastecimiento $nMCADENA
 * @property Pedido $nMPEDIDO
 */
class PedidoCadenabastecimiento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedido_cadenabastecimiento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NM_CADENA', 'NM_PEDIDO'], 'required'],
            [['ID', 'NM_CADENA', 'NM_PEDIDO'], 'integer'],
            [['ID'], 'unique'],
            [['NM_CADENA'], 'exist', 'skipOnError' => true, 'targetClass' => Cadenadeabastecimiento::className(), 'targetAttribute' => ['NM_CADENA' => 'NM_CADENA_ID']],
            [['NM_PEDIDO'], 'exist', 'skipOnError' => true, 'targetClass' => Pedido::className(), 'targetAttribute' => ['NM_PEDIDO' => 'CS_PEDIDO_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'NM_CADENA' => Yii::t('app', 'Nm Cadena'),
            'NM_PEDIDO' => Yii::t('app', 'Nm Pedido'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMCADENA()
    {
        return $this->hasOne(Cadenadeabastecimiento::className(), ['NM_CADENA_ID' => 'NM_CADENA']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMPEDIDO()
    {
        return $this->hasOne(Pedido::className(), ['CS_PEDIDO_ID' => 'NM_PEDIDO']);
    }

    /**
     * {@inheritdoc}
     * @return PedidoCadenabastecimientoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PedidoCadenabastecimientoQuery(get_called_class());
    }
}
