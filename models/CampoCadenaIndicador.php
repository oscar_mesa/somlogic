<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%campo_cadena_indicador}}".
 *
 * @property int $ID_INDICADOR_VALOR
 * @property int $ID_CAMPO
 * @property int $ID_CADENA_INDICADOR
 * @property double $VALOR
 *
 * @property CampoCaculo $cAMPO
 * @property CadenaIndicador $cADENAINDICADOR
 */
class CampoCadenaIndicador extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%campo_cadena_indicador}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_CAMPO', 'ID_CADENA_INDICADOR'], 'integer'],
            [['VALOR'], 'number'],
            [['ID_CAMPO'], 'exist', 'skipOnError' => true, 'targetClass' => CampoCaculo::className(), 'targetAttribute' => ['ID_CAMPO' => 'ID_CAMPO']],
            [['ID_CADENA_INDICADOR'], 'exist', 'skipOnError' => true, 'targetClass' => CadenaIndicador::className(), 'targetAttribute' => ['ID_CADENA_INDICADOR' => 'ID_INDICADOR_CADENA']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_INDICADOR_VALOR' => Yii::t('app', 'Id Indicador Valor'),
            'ID_CAMPO' => Yii::t('app', 'Id Campo'),
            'ID_CADENA_INDICADOR' => Yii::t('app', 'Id Cadena Indicador'),
            'VALOR' => Yii::t('app', 'Valor'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCAMPO()
    {
        return $this->hasOne(CampoCaculo::className(), ['ID_CAMPO' => 'ID_CAMPO']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCADENAINDICADOR()
    {
        return $this->hasOne(CadenaIndicador::className(), ['ID_INDICADOR_CADENA' => 'ID_CADENA_INDICADOR']);
    }

    /**
     * {@inheritdoc}
     * @return CampoCadenaIndicadorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CampoCadenaIndicadorQuery(get_called_class());
    }
}
