<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedor".
 *
 * @property int $ID_PROVEEDOR
 * @property string $RAZON_SOCIAL_PROVEEDOR
 * @property string $NIT
 * @property string $DIRECCION
 * @property string $TELEFONO
 * @property string $NOMBRE_CONTACTO
 * @property string $CARGO_CONTACTO
 * @property string $SITIO_WEB
 * @property int $ID_TIPO_PROVEEDOR
 *
 * @property TipoProveedor $tIPOPROVEEDOR
 */
class Proveedor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_TIPO_PROVEEDOR'], 'integer'],
            [['RAZON_SOCIAL_PROVEEDOR', 'NIT', 'DIRECCION', 'SITIO_WEB'], 'string', 'max' => 255],
            [['TELEFONO', 'CARGO_CONTACTO'], 'string', 'max' => 100],
            [['NOMBRE_CONTACTO'], 'string', 'max' => 200],
            [['ID_TIPO_PROVEEDOR'], 'exist', 'skipOnError' => true, 'targetClass' => TipoProveedor::className(), 'targetAttribute' => ['ID_TIPO_PROVEEDOR' => 'ID_TIPO']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_PROVEEDOR' => Yii::t('app', 'Id Proveedor'),
            'RAZON_SOCIAL_PROVEEDOR' => Yii::t('app', 'Razon Social Proveedor'),
            'NIT' => Yii::t('app', 'Nit'),
            'DIRECCION' => Yii::t('app', 'Direccion'),
            'TELEFONO' => Yii::t('app', 'Telefono'),
            'NOMBRE_CONTACTO' => Yii::t('app', 'Nombre Contacto'),
            'CARGO_CONTACTO' => Yii::t('app', 'Cargo Contacto'),
            'SITIO_WEB' => Yii::t('app', 'Sitio Web'),
            'ID_TIPO_PROVEEDOR' => Yii::t('app', 'Tipo Proveedor'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTIPOPROVEEDOR()
    {
        return $this->hasOne(TipoProveedor::className(), ['ID_TIPO' => 'ID_TIPO_PROVEEDOR']);
    }

    /**
     * {@inheritdoc}
     * @return ProveedorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProveedorQuery(get_called_class());
    }
}
