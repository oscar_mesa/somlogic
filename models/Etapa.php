<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%etapa}}".
 *
 * @property int $ID_ETAPA
 * @property string $ETAPA
 *
 * @property CadenaEtapa[] $cadenaEtapas
 */
class Etapa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%etapa}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ETAPA'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_ETAPA' => Yii::t('app', 'Id Proceso'),
            'ETAPA' => Yii::t('app', 'Proceso'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCadenaEtapas()
    {
        return $this->hasMany(CadenaEtapa::className(), ['ID_ETAPA' => 'ID_ETAPA']);
    }

    /**
     * {@inheritdoc}
     * @return EtapaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EtapaQuery(get_called_class());
    }
}
