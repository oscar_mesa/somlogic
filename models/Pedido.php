<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pedido".
 *
 * @property int $CS_PEDIDO_ID
 * @property string $DS_CODIGO_PEDIDO
 * @property int $NM_USUARIO_CREADOR_ID
 * @property string $DS_NOTAS_PEDIDO
 * @property double $NM_PRECIO_SUBTOTAL
 * @property double $NM_PRECIO_DESCUENTO
 * @property double $NM_PRECIO_TOTAL
 * @property double $NM_PRECIO_IVA
 * @property string $DT_FECHA_CREACION
 * @property int $NM_EMPRESA
 * @property string $DS_CLIENTE
 * @property int $ID_ESTADO
 * @property int $NM_PORCENTAJE_DESCUENTO
 * @property int $PORCENTAJE_IVA
 * @property string $GUSTO_PRODUCTOS_CLIENTE
 * @property int ID_CLIENTE_FINAL
 * @property Actor $nMEMPRESA
 * @property Actor $iDCLIENTEFINAL
 * @property EstadoPedido $eSTADO
 * @property Actor $nMACTORCREADOR
 * @property PedidoDetalle[] $pedidoDetalles 
 */
class Pedido extends \yii\db\ActiveRecord {

    public $PRODUCTOS_PEDIDO, $DS_ACTOR, $ACTUALIZAR_GUSTO_PRODUCTOS_CLIENTE;
    public $DATERIME_RANGE, $DATETIME_MIN, $DATETIME_MAX;
    const CREACION_PEDIDO = 'creacion_pedido';
    const ACTUALIZACION_PEDIDO = 'actualizacion_pedido';

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'pedido';
    }

    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::CREACION_PEDIDO] = $scenarios["default"];
        $scenarios[self::ACTUALIZACION_PEDIDO] = $scenarios["default"];
        //echo "<pre>";
        //print_r($scenarios);die;
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NM_USUARIO_CREADOR_ID', 'NM_PRECIO_SUBTOTAL', 'NM_PRECIO_TOTAL', 'NM_EMPRESA', 'ID_CLIENTE_FINAL'], 'required'],
            [['NM_USUARIO_CREADOR_ID', 'ID_CLIENTE_FINAL', 'NM_EMPRESA', 'ID_ESTADO', 'NM_PORCENTAJE_DESCUENTO', 'PORCENTAJE_IVA'], 'integer'],
            [['DS_NOTAS_PEDIDO', 'GUSTO_PRODUCTOS_CLIENTE'], 'string'],
            [['NM_PRECIO_SUBTOTAL', 'NM_PRECIO_DESCUENTO', 'NM_PRECIO_TOTAL', 'NM_PRECIO_IVA'], 'number'],
            [['DT_FECHA_CREACION', 'PRODUCTOS_PEDIDO'], 'safe'],
            ['PRODUCTOS_PEDIDO', 'validarCantidadPeoductosPedido',],
            [['DS_CODIGO_PEDIDO'], 'string', 'max' => 40],
            [['DS_CLIENTE'], 'string', 'max' => 200],
            [['NM_EMPRESA'], 'exist', 'skipOnError' => true, 'targetClass' => Actor::className(), 'targetAttribute' => ['NM_EMPRESA' => 'ID_ACTOR']],
            [['ID_ESTADO'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoPedido::className(), 'targetAttribute' => ['ID_ESTADO' => 'ID_ESTADO']],
            [['NM_USUARIO_CREADOR_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Actor::className(), 'targetAttribute' => ['NM_USUARIO_CREADOR_ID' => 'ID_ACTOR']],
        ];
    }

    public function validarCantidadPeoductosPedido($attribute) {
        if (PedidoDetalle::find()->where(["CS_PEDIDO_ID" => null, 'NM_USUARIO_CREADOR' => Yii::$app->user->getIdentity()->ID_ACTOR])->count() == 0)
            $this->addError($attribute, 'Se deben adicionar productos al pedido');
    }
    
    public function afterSave($insert, $changedAttributes) {
        if(self::CREACION_PEDIDO == $this->scenario){
            \app\models\PedidoDetalle::updateAll(["CS_PEDIDO_ID" => $this->CS_PEDIDO_ID], ["CS_PEDIDO_ID" => null, 'NM_USUARIO_CREADOR' => Yii::$app->user->getIdentity()->ID_ACTOR]);
        }
        parent::afterSave($insert, $changedAttributes);
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'CS_PEDIDO_ID' => Yii::t('app', 'ID'),
            'DS_CODIGO_PEDIDO' => Yii::t('app', 'Código del Pedido'),
            'NM_USUARIO_CREADOR_ID' => Yii::t('app', 'Creador'),
            'DS_NOTAS_PEDIDO' => Yii::t('app', 'Notas del Pedido'),
            'NM_PRECIO_SUBTOTAL' => Yii::t('app', 'Subtotal'),
            'NM_PRECIO_DESCUENTO' => Yii::t('app', 'Descuento'),
            'NM_PRECIO_TOTAL' => Yii::t('app', 'Precio Total'),
            'NM_PRECIO_IVA' => Yii::t('app', '$ Iva'),
            'DT_FECHA_CREACION' => Yii::t('app', 'Fecha Creación'),
            'NM_EMPRESA' => Yii::t('app', 'Empresa'),
            'DS_CLIENTE' => Yii::t('app', 'Cliente'),
            'ID_ESTADO' => Yii::t('app', 'Estado'),
            'NM_PORCENTAJE_DESCUENTO' => Yii::t('app', 'Porcentaje Descuento'),
            'PORCENTAJE_IVA' => Yii::t('app', 'Porcentaje Iva'),
            'GUSTO_PRODUCTOS_CLIENTE' => Yii::t('app', 'Gustos producto cliente'),
            'PRODUCTOS_PEDIDO' => 'Productos del pedido',
            'DS_ACTOR' => 'Creador',
            'ID_CLIENTE_FINAL'  => Yii::t('app', 'Cliente final'),
        ];
    }

    public static function obtenerCodigoPedidoDisponible() {
        $ultimaFactura = self::find()->orderBy(['CS_PEDIDO_ID' => SORT_DESC])->one();
        return 'SOMLOGIC-' . str_pad((is_object($ultimaFactura) ? ($ultimaFactura->CS_PEDIDO_ID + 1) : 1), 12, '0', STR_PAD_LEFT);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getnMEMPRESA()
    {
        return $this->hasOne(Actor::className(), ['ID_ACTOR' => 'NM_EMPRESA']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getiDCLIENTEFINAL()
    {
        return $this->hasOne(Actor::className(), ['ID_ACTOR' => 'ID_CLIENTE_FINAL']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function geteSTADO()
    {
        return $this->hasOne(EstadoPedido::className(), ['ID_ESTADO' => 'ID_ESTADO']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getnMACTORCREADOR(){
        return $this->hasOne(Actor::className(), ['ID_ACTOR' => 'NM_USUARIO_CREADOR_ID']);
    }
    
    /**
     * @return \yii\db\ActiveQuery 
     */
    public function getPedidoDetalles() {
        return $this->hasMany(PedidoDetalle::className(), ['CS_PEDIDO_ID' => 'CS_PEDIDO_ID']);
    }    

    /**
     * {@inheritdoc}
     * @return PedidoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PedidoQuery(get_called_class());
    }
}
