<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\VwTotalIndicadoresCa;

/**
 * VwTotalIndicadoresCaSearch represents the model behind the search form of `app\models\VwTotalIndicadoresCa`.
 */
class VwTotalIndicadoresCaSearch extends VwTotalIndicadoresCa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['DS_NOMBRE_INDICADOR', 'ETAPA', 'DS_FORMULA', 'DS_NOMENCLATURA', 'DEFINICION', 'DS_AREA', 'UNIDAD', 'DS_CODIGO_PEDIDO', 'META', 'FECHA_CREACION', 'FECHA_ACTUALIZACION', 'USUARIO_ACTUALIZACION', 'USUARIO_CREACION'], 'safe'],
            [['NM_RESULTADO_INDICADOR'], 'number'],
            [['NM_PORCENTAJE_SIGNIFICANCIA', 'ID_INDICADOR', 'ID_CADENA', 'ID_INDICADOR_CADENA', 'NM_ID_TIPO_IND'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VwTotalIndicadoresCa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        
            'sort'=> ['defaultOrder' => ['ID_ETAPA'=>SORT_ASC]]

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'NM_RESULTADO_INDICADOR' => $this->NM_RESULTADO_INDICADOR,
            'NM_PORCENTAJE_SIGNIFICANCIA' => $this->NM_PORCENTAJE_SIGNIFICANCIA,
            'ID_INDICADOR' => $this->ID_INDICADOR,
            'ID_CADENA' => $this->ID_CADENA,
            'ID_INDICADOR_CADENA' => $this->ID_INDICADOR_CADENA,
            'NM_ID_TIPO_IND' => $this->NM_ID_TIPO_IND,
        ]);

        $query->andFilterWhere(['like', 'DS_NOMBRE_INDICADOR', $this->DS_NOMBRE_INDICADOR])
            ->andFilterWhere(['like', 'ETAPA', $this->ETAPA])
            ->andFilterWhere(['like', 'DS_FORMULA', $this->DS_FORMULA])
            ->andFilterWhere(['like', 'DS_NOMENCLATURA', $this->DS_NOMENCLATURA])
            ->andFilterWhere(['like', 'DEFINICION', $this->DEFINICION])
            ->andFilterWhere(['like', 'UNIDAD', $this->UNIDAD])
            ->andFilterWhere(['like', 'DS_CODIGO_PEDIDO', $this->DS_CODIGO_PEDIDO])
            ->andFilterWhere(['like', 'META', $this->META])
            ->andFilterWhere(['like', 'FECHA_CREACION', $this->FECHA_CREACION])
            ->andFilterWhere(['like', 'FECHA_ACTUALIZACION', $this->FECHA_ACTUALIZACION])
            ->andFilterWhere(['like', 'USUARIO_ACTUALIZACION', $this->USUARIO_ACTUALIZACION])
            ->andFilterWhere(['like', 'USUARIO_CREACION', $this->USUARIO_CREACION])
            ->andFilterWhere(['like', 'DS_AREA', $this->DS_AREA]);

        return $dataProvider;
    }
}
