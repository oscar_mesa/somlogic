<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Cadenadeabastecimiento]].
 *
 * @see Cadenadeabastecimiento
 */
class CadenadeabastecimientoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Cadenadeabastecimiento[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Cadenadeabastecimiento|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
