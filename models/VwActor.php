<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vw_actor".
 *
 * @property int $id
 * @property string $username
 * @property int $ID_ACTOR
 * @property int $NM_DOCUMENTO_ID
 * @property int $NM_TIPO_DOCUMENTO_ID
 * @property string $DS_NOMBRES_ACTOR
 * @property string $DS_APELLIDOS_ACTOR
 * @property string $NM_TELEFONO
 * @property string $NM_CELULAR
 * @property string $DS_CORREO
 * @property string $DS_DIRECCION
 * @property string $DS_CONTRASENA
 * @property int $NM_TIPO_ACTOR_ID
 * @property int $NM_ESTADO_ID
 * @property string $DT_FECHA_CREACION
 * @property string $DF_FECHA_ACTUALIZACION
 * @property string $CLAVE_AUTENTICACION
 * @property string $PAGINA_WEB
 * @property string $CONTACTO
 * @property string $CARGO_CONTACTO
 */
class VwActor extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vw_actor';
    }

    /**
     * @inheritdoc$primaryKey
     */
    public static function primaryKey()
    {
        return ["ID_ACTOR"];
    }        

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ID_ACTOR', 'NM_DOCUMENTO_ID', 'NM_TIPO_DOCUMENTO_ID', 'NM_TIPO_ACTOR_ID', 'NM_ESTADO_ID'], 'integer'],
            [['username', 'DS_NOMBRES_ACTOR', 'DS_APELLIDOS_ACTOR', 'DS_CORREO', 'DS_CONTRASENA', 'NM_ESTADO_ID'], 'required'],
            [['DT_FECHA_CREACION', 'DF_FECHA_ACTUALIZACION'], 'safe'],
            [['username', 'DS_CORREO'], 'string', 'max' => 50],
            [['DS_NOMBRES_ACTOR', 'DS_APELLIDOS_ACTOR'], 'string', 'max' => 30],
            [['NM_TELEFONO', 'NM_CELULAR'], 'string', 'max' => 20],
            [['DS_DIRECCION'], 'string', 'max' => 200],
            [['DS_CONTRASENA'], 'string', 'max' => 100],
            [['CLAVE_AUTENTICACION'], 'string', 'max' => 60],
            [['PAGINA_WEB', 'CONTACTO', 'CARGO_CONTACTO'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'ID_ACTOR' => Yii::t('app', 'Id Actor'),
            'NM_DOCUMENTO_ID' => Yii::t('app', 'Nm Documento ID'),
            'NM_TIPO_DOCUMENTO_ID' => Yii::t('app', 'Nm Tipo Documento ID'),
            'DS_NOMBRES_ACTOR' => Yii::t('app', 'Ds Nombres Actor'),
            'DS_APELLIDOS_ACTOR' => Yii::t('app', 'Ds Apellidos Actor'),
            'NM_TELEFONO' => Yii::t('app', 'Nm Telefono'),
            'NM_CELULAR' => Yii::t('app', 'Nm Celular'),
            'DS_CORREO' => Yii::t('app', 'Ds Correo'),
            'DS_DIRECCION' => Yii::t('app', 'Ds Direccion'),
            'DS_CONTRASENA' => Yii::t('app', 'Ds Contrasena'),
            'NM_TIPO_ACTOR_ID' => Yii::t('app', 'Nm Tipo Actor ID'),
            'NM_ESTADO_ID' => Yii::t('app', 'Nm Estado ID'),
            'DT_FECHA_CREACION' => Yii::t('app', 'Dt Fecha Creacion'),
            'DF_FECHA_ACTUALIZACION' => Yii::t('app', 'Df Fecha Actualizacion'),
            'CLAVE_AUTENTICACION' => Yii::t('app', 'Clave Autenticacion'),
            'PAGINA_WEB' => Yii::t('app', 'Pagina Web'),
            'CONTACTO' => Yii::t('app', 'Contacto'),
            'CARGO_CONTACTO' => Yii::t('app', 'Cargo Contacto'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMSEDECLIENTE()
    {
        return $this->hasOne(SedeCliente::className(), ['ID_SEDE' => 'NM_SEDE_CLIENTE_ID']);
    }
    
    
    public function beforeValidate() {
        if($this->isNewRecord){
            $this->setClaveDeAutenticacion();
        }
        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturas()
    {
        return $this->hasMany(Factura::className(), ['NM_CLIENTE_ID' => 'ID_ACTOR']);
    }
    
    /**
    * @return \yii\db\ActiveQuery
    */
   public function getAutenticacions()
   {
       return $this->hasMany(Autenticacion::className(), ['ID_ACTOR' => 'ID_ACTOR']);
   }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturas0()
    {
        return $this->hasMany(Factura::className(), ['NM_VENDEDOR_ID' => 'ID_ACTOR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMESTADO()
    {
        return $this->hasOne(EstadoActor::className(), ['CS_ESTADO_ID' => 'NM_ESTADO_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMTIPODOCUMENTO()
    {
        return $this->hasOne(TipoDocumento::className(), ['CS_TIPO_DOCUMENTO_ID' => 'NM_TIPO_DOCUMENTO_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMTipoActor()
    {
        return $this->hasOne(TipoActor::className(), ['CS_tipo_actor' => 'NM_TIPO_ACTOR_ID']);
    }
    
    public function validatePassword($password) {
        return \Yii::$app->security->validatePassword($password, $this->DS_CONTRASENA);
    }

    public function getAuthKey(): string {
        return !is_null($this->CLAVE_AUTENTICACION)?$this->CLAVE_AUTENTICACION:"";
    }

    public function getId() {
        return $this->ID_ACTOR;
    }

    public function validateAuthKey($authKey): bool {
        return $this->CLAVE_AUTENTICACION === $authKey;
    }

    public static function findIdentity($id): \yii\web\IdentityInterface {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null): \yii\web\IdentityInterface {
        return null;
    }

    public function setClaveDeAutenticacion() {
        $this->CLAVE_AUTENTICACION = Yii::$app->security->generateRandomString(60);
    }
    
    public function getRols() {
        return Yii::$app->authManager->getRolesByUser($this->ID_ACTOR);
    }

    public function removerAllRols(){
        foreach ($this->getRols() as $rol) {
            Yii::$app->authManager->revoke($rol, $this->ID_ACTOR);
        }
    }    

    /**
     * {@inheritdoc}
     * @return VwActorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VwActorQuery(get_called_class());
    }
}
