<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoProveedor]].
 *
 * @see TipoProveedor
 */
class TipoProveedorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TipoProveedor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TipoProveedor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
