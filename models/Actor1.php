<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property int $ID_ACTOR
 * @property string $NM_DOCUMENTO_ID
 * @property int $NM_TIPO_DOCUMENTO_ID
 * @property string $DS_NOMBRES_ACTOR
 * @property string $DS_APELLIDOS_ACTOR
 * @property int $NM_TELEFONO
 * @property string $NM_CELULAR
 * @property string $DS_CORREO
 * @property string $DS_DIRECCION
 * @property string $DS_CONTRASENA
 * @property int $NM_TIPO_ACTOR_ID
 * @property int $NM_ESTADO_ID
 * @property string $DT_FECHA_CREACION
 * @property string $DF_FECHA_ACTUALIZACION
 * @property string $CLAVE_AUTENTICACION
 * @property string $PAGINA_WEB
 * @property string $CONTACTO
 * @property string $CARGO_CONTACTO
 * 
 * @property Autenticacion[] $autenticacions
 * @property Factura[] $facturas
 * @property Factura[] $facturas0
 * @property EstadoActor $nMESTADO
 * @property TipoDocumento $nMTIPODOCUMENTO
 * @property TipoActor $nMTipoActor
 */
class Actor1 extends \yii\db\ActiveRecord
{
    const SCENARIO_TARE_CREDENTIALS = "restaurar_credenciales_usuario";
    const SCENARIO_SAVE_TARE_CREDENTIALS = "guardar_restauracion_credenciales_usuario";
    public $reCaptcha;
    public $CONFIRMAR_CONTRASENA;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'actor';
    }
    
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_TARE_CREDENTIALS] = ['DS_CORREO', 'reCaptcha'];
        $scenarios[self::SCENARIO_SAVE_TARE_CREDENTIALS] = ['DS_CONTRASENA', 'CONFIRMAR_CONTRASENA'];
        
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NM_DOCUMENTO_ID', 'NM_TIPO_DOCUMENTO_ID', 'NM_TELEFONO', 'NM_TIPO_ACTOR_ID', 'NM_ESTADO_ID'], 'integer'],
            [['DS_NOMBRES_ACTOR', 'NM_CELULAR', 'DS_CORREO', 'DS_CONTRASENA', 'NM_ESTADO_ID'], 'required'],
            [['DT_FECHA_CREACION', 'DF_FECHA_ACTUALIZACION', 'PAGINA_WEB'], 'safe'],
            [['DS_NOMBRES_ACTOR', 'DS_APELLIDOS_ACTOR'], 'string', 'max' => 30],
            ["CONFIRMAR_CONTRASENA", 'compare', 'compareAttribute' => 'DS_CONTRASENA', 'message' => 'Las contraseñas nos son iguales'],
            [['NM_CELULAR'], 'string', 'max' => 10],
            [['DS_CORREO'], 'string', 'max' => 50],
            ['DS_CORREO', 'validarExistenciaCorreo',],
            [['DS_DIRECCION'], 'string', 'max' => 200],
            [['DS_CONTRASENA'], 'string', 'max' => 100],
            [['CLAVE_AUTENTICACION'], 'string', 'max' => 60],
            [['NM_DOCUMENTO_ID'], 'unique'],
            [['NM_ESTADO_ID'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoActor::className(), 'targetAttribute' => ['NM_ESTADO_ID' => 'CS_ESTADO_ID']],
            [['NM_TIPO_DOCUMENTO_ID'], 'exist', 'skipOnError' => true, 'targetClass' => TipoDocumento::className(), 'targetAttribute' => ['NM_TIPO_DOCUMENTO_ID' => 'CS_TIPO_DOCUMENTO_ID']],
            [['NM_TIPO_ACTOR_ID'], 'exist', 'skipOnError' => true, 'targetClass' => TipoActor::className(), 'targetAttribute' => ['NM_TIPO_ACTOR_ID' => 'CS_TIPO_ACTOR']],
           // ['reCaptcha', \himiklab\yii2\recaptcha\ReCaptchaValidator::className(), 'secret' => '6LcjNXoUAAAAALGCOz8fTKs9JgTXGD8_puSU9kFj',  'uncheckedMessage' => 'Por favor, confirme que no es un robot.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_ACTOR' => Yii::t('app', 'Id'),
            'NM_DOCUMENTO_ID' => Yii::t('app', 'Documento'),
            'NM_TIPO_DOCUMENTO_ID' => Yii::t('app', 'Tipo documento'),
            'DS_NOMBRES_ACTOR' => Yii::t('app', 'Nombres o Razón social'),
            'DS_APELLIDOS_ACTOR' => Yii::t('app', 'Apellidos'),
            'NM_TELEFONO' => Yii::t('app', 'Teléfono'),
            'NM_CELULAR' => Yii::t('app', 'Celular'),
            'DS_CORREO' => Yii::t('app', 'Correo'),
            'DS_DIRECCION' => Yii::t('app', 'Dirección'),
            'DS_CONTRASENA' => Yii::t('app', 'Contraseña'),
            'NM_TIPO_ACTOR_ID' => Yii::t('app', 'Tipo usuario'),
            'NM_ESTADO_ID' => Yii::t('app', 'Estado'),
            'DT_FECHA_CREACION' => Yii::t('app', 'Fecha creación'),
            'CONFIRMAR_CONTRASENA' => Yii::t('app', 'Confirmar contraseña'),
            'CLAVE_AUTENTICACION' => Yii::t('app', 'Clave de autenticación'),
            'DF_FECHA_ACTUALIZACION' => Yii::t('app', 'Fecha de actualización'),
            'CLAVE_AUTENTICACION' => Yii::t('app', 'Clave de autenticación'),
            'reCaptcha' => '',
            'PAGINA_WEB' => Yii::t('app', 'Página Web')
        ];
    }
    
     public function validarExistenciaCorreo($attribute) {
        if (self::SCENARIO_TARE_CREDENTIALS == $this->scenario && ($usuario = self::find()->where(["DS_CORREO" =>  $this->DS_CORREO])->one()) == null)
            $this->addError($attribute, 'Este correo no existe, por favor verifique.');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutenticacions()
    {
        return $this->hasMany(Autenticacion::className(), ['ID_ACTOR' => 'ID_ACTOR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturas()
    {
        return $this->hasMany(Factura::className(), ['NM_CLIENTE_ID' => 'ID_ACTOR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFacturas0()
    {
        return $this->hasMany(Factura::className(), ['NM_VENDEDOR_ID' => 'ID_ACTOR']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMESTADO()
    {
        return $this->hasOne(EstadoActor::className(), ['CS_ESTADO_ID' => 'NM_ESTADO_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMTIPODOCUMENTO()
    {
        return $this->hasOne(TipoDocumento::className(), ['CS_TIPO_DOCUMENTO_ID' => 'NM_TIPO_DOCUMENTO_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNMTipoActor()
    {
        return $this->hasOne(TipoActor::className(), ['CS_TIPO_ACTOR' => 'NM_TIPO_ACTOR_ID']);
    }

    /**
     * {@inheritdoc}
     * @return Actor1Query the active query used by this AR class.
     */
    public static function find()
    {
        return new Actor1Query(get_called_class());
    }
}
