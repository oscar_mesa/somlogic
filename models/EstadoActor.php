<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estado_actor".
 *
 * @property int $CS_ESTADO_ID
 * @property string $DS_NOMBRE_ESTADO
 * @property string $DS_DESCRIPCION_ESTADO
 *
 * @property actor[] $actors
 */
class EstadoActor extends \yii\db\ActiveRecord
{
    const ACTIVO = 1;
    const INNACTIVO = 2;
    const PENDIENTE_RESTAURAR_CONTRASENA = 3;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estado_actor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DS_NOMBRE_ESTADO'], 'required'],
            [['DS_NOMBRE_ESTADO'], 'string', 'max' => 50],
            [['DS_DESCRIPCION_ESTADO'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CS_ESTADO_ID' => Yii::t('app', 'Cs  Estado  ID'),
            'DS_NOMBRE_ESTADO' => Yii::t('app', 'Ds  Nombre  Estado'),
            'DS_DESCRIPCION_ESTADO' => Yii::t('app', 'Ds  Descripcion  Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActores()
    {
        return $this->hasMany(Actor::className(), ['NM_ESTADO_ID' => 'CS_ESTADO_ID']);
    }

    /**
     * {@inheritdoc}
     * @return EstadoActorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EstadoActorQuery(get_called_class());
    }
}
