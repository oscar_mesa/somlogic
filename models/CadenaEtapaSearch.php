<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CadenaEtapa;

/**
 * CadenaEtapaSearch represents the model behind the search form of `app\models\CadenaEtapa`.
 */
class CadenaEtapaSearch extends CadenaEtapa
{
    public $NOMBREACTORETAPA;
    public $NOMBREDELAETAPA;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_ETP_CAD', 'ID_ETAPA', 'ID_CADENA', 'ID_ACTOR'], 'integer'],
            [['CUMPLIMIENTO_PROMEDIO', 'NM_PORCENTAJE_SIGNIFICANCIA'], 'number'],
            [['NOMBREACTORETAPA', 'NOMBREDELAETAPA'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CadenaEtapa::find();


        // add conditions that should always apply here

         $query->joinWith(['aCTOR', 'eTAPA']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        
        ]);

        $dataProvider->sort->attributes['NOMBREACTORETAPA'] = [
            'asc' => ['actor.DS_NOMBRES_ACTOR' => SORT_ASC],
            'desc' => ['actor.DS_NOMBRES_ACTOR' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['NOMBREDELAETAPA'] = [
            'asc' => ['etapa.ETAPA' => SORT_ASC],
            'desc' => ['etapa.ETAPA' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID_ETP_CAD' => $this->ID_ETP_CAD,
            'ID_ETAPA' => $this->ID_ETAPA,
            'ID_CADENA' => $this->ID_CADENA,
            'ID_ACTOR' => $this->ID_ACTOR,
            'CUMPLIMIENTO_PROMEDIO' => $this->CUMPLIMIENTO_PROMEDIO,
            'NM_PORCENTAJE_SIGNIFICANCIA' => $this->NM_PORCENTAJE_SIGNIFICANCIA,
        ])
        ->andFilterWhere(['like', 'actor.DS_NOMBRES_ACTOR', $this->NOMBREACTORETAPA])
        ->andFilterWhere(['like', 'etapa.ETAPA', $this->NOMBREDELAETAPA])
       // ->andFilterWhere(['like', 'actor.DS_APELLIDOS_ACTOR', $this->aCTOR]);
       // ->andFilterWhere(['like', 'actor.DS_APELLIDOS_ACTOR', $this->actor])
        ;

        return $dataProvider;
    }
}
