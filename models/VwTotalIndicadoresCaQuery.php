<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwTotalIndicadoresCa]].
 *
 * @see VwTotalIndicadoresCa
 */
class VwTotalIndicadoresCaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return VwTotalIndicadoresCa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return VwTotalIndicadoresCa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
