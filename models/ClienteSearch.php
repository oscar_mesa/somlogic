<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cliente;

/**
 * ClienteSearch represents the model behind the search form of `app\models\Cliente`.
 */
class ClienteSearch extends Cliente
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_CLIENTE', 'NM_DOCUMENTO_NIT', 'NM_TIPO_DOCUMENTO_ID', 'NM_TELEFONO', 'NM_ESTADO_ID'], 'integer'],
            [['DS_NOMBRES_CLIENTE', 'DS_APELLIDOS_CLIENTE', 'NM_CELULAR', 'DS_CORREO', 'DS_DIRECCION', 'DT_FECHA_CREACION', 'DF_FECHA_ACTUALIZACION'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cliente::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID_CLIENTE' => $this->ID_CLIENTE,
            'NM_DOCUMENTO_NIT' => $this->NM_DOCUMENTO_NIT,
            'NM_TIPO_DOCUMENTO_ID' => $this->NM_TIPO_DOCUMENTO_ID,
            'NM_TELEFONO' => $this->NM_TELEFONO,
            'NM_ESTADO_ID' => $this->NM_ESTADO_ID,
            'DT_FECHA_CREACION' => $this->DT_FECHA_CREACION,
            'DF_FECHA_ACTUALIZACION' => $this->DF_FECHA_ACTUALIZACION,
        ]);

        $query->andFilterWhere(['like', 'DS_NOMBRES_CLIENTE', $this->DS_NOMBRES_CLIENTE])
            ->andFilterWhere(['like', 'DS_APELLIDOS_CLIENTE', $this->DS_APELLIDOS_CLIENTE])
            ->andFilterWhere(['like', 'NM_CELULAR', $this->NM_CELULAR])
            ->andFilterWhere(['like', 'DS_CORREO', $this->DS_CORREO])
            ->andFilterWhere(['like', 'DS_DIRECCION', $this->DS_DIRECCION]);

        return $dataProvider;
    }
}
