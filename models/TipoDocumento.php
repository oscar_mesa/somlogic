<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_documento".
 *
 * @property int $CS_TIPO_DOCUMENTO_ID
 * @property string $DS_NOMBRE_TIPO_DOCUMENTO
 * @property string $DS_DESCRIPCION_TIPO_DOCUMENTO
 *
 * @property Usuario[] $usuarios
 */
class TipoDocumento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_documento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DS_NOMBRE_TIPO_DOCUMENTO'], 'required'],
            [['DS_NOMBRE_TIPO_DOCUMENTO'], 'string', 'max' => 50],
            [['DS_DESCRIPCION_TIPO_DOCUMENTO'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CS_TIPO_DOCUMENTO_ID' => Yii::t('app', 'Cs  Tipo  Documento  ID'),
            'DS_NOMBRE_TIPO_DOCUMENTO' => Yii::t('app', 'Ds  Nombre  Tipo  Documento'),
            'DS_DESCRIPCION_TIPO_DOCUMENTO' => Yii::t('app', 'Ds  Descripcion  Tipo  Documento'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['NM_TIPO_DOCUMENTO_ID' => 'CS_TIPO_DOCUMENTO_ID']);
    }

    /**
     * {@inheritdoc}
     * @return TipoDocumentoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TipoDocumentoQuery(get_called_class());
    }
}
