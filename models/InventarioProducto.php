<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inventario_producto".
 *
 * @property int $CS_INVENTARIO_ID
 * @property int $CS_PRODUCTO_ID
 * @property int $NM_CANTIDAD_INVENTARIO
 * @property int $CS_VENDEDOR_PRODUCTO_ID
 * @property string $DT_FECHA_CREACION
 * @property double $NM_PRECIO_UNITARIO_COMPRA
 *
 * @property Producto $cSPRODUCTO
 * @property VendedorProducto $cSVENDEDORPRODUCTO
 */
class InventarioProducto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inventario_producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CS_PRODUCTO_ID', 'NM_CANTIDAD_INVENTARIO', 'CS_VENDEDOR_PRODUCTO_ID', 'NM_PRECIO_UNITARIO_COMPRA'], 'required'],
            [['CS_PRODUCTO_ID', 'NM_CANTIDAD_INVENTARIO', 'CS_VENDEDOR_PRODUCTO_ID', 'NM_PRECIO_UNITARIO_COMPRA'], 'number', 'min' => 0],
            [['DT_FECHA_CREACION'], 'safe'],
            [['NM_CANTIDAD_INVENTARIO', 'NM_PRECIO_UNITARIO_COMPRA'], 'match', 'pattern' => '/^[1-9]\d*$/'],
            [['CS_PRODUCTO_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['CS_PRODUCTO_ID' => 'CS_PRODUCTO_ID']],
            [['CS_VENDEDOR_PRODUCTO_ID'], 'exist', 'skipOnError' => true, 'targetClass' => VendedorProducto::className(), 'targetAttribute' => ['CS_VENDEDOR_PRODUCTO_ID' => 'CS_VENDEDOR_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CS_INVENTARIO_ID' => Yii::t('app', 'ID'),
            'CS_PRODUCTO_ID' => Yii::t('app', 'Producto'),
            'NM_CANTIDAD_INVENTARIO' => Yii::t('app', 'Cantidad producto'),
            'CS_VENDEDOR_PRODUCTO_ID' => Yii::t('app', 'Proveedor'),
            'DT_FECHA_CREACION' => Yii::t('app', 'Fecha  creacion'),
            'NM_PRECIO_UNITARIO_COMPRA' => Yii::t('app', 'Precio unitario compra'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCSPRODUCTO()
    {
        return $this->hasOne(Producto::className(), ['CS_PRODUCTO_ID' => 'CS_PRODUCTO_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCSVENDEDORPRODUCTO()
    {
        return $this->hasOne(VendedorProducto::className(), ['CS_VENDEDOR_ID' => 'CS_VENDEDOR_PRODUCTO_ID']);
    }

    /**
     * {@inheritdoc}
     * @return InventarioProductoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new InventarioProductoQuery(get_called_class());
    }
}
