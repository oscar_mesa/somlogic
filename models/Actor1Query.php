<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Actor1]].
 *
 * @see Actor1
 */
class Actor1Query extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Actor1[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Actor1|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
