<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estado_pedido".
 *
 * @property int $ID_ESTADO
 * @property string $DES_ESTADO
 *
 * @property Pedido[] $pedidos
 */
class EstadoPedido extends \yii\db\ActiveRecord
{
    const RECIBIDO = 1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estado_pedido';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DES_ESTADO'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_ESTADO' => Yii::t('app', 'Id Estado'),
            'DES_ESTADO' => Yii::t('app', 'Des Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPedidos()
    {
        return $this->hasMany(Pedido::className(), ['ID_ESTADO' => 'ID_ESTADO']);
    }

    /**
     * {@inheritdoc}
     * @return EstadoPedidoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new EstadoPedidoQuery(get_called_class());
    }
}
