<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%color_rango_idl}}".
 *
 * @property int $ID
 * @property int $ID_INDICADOR
 * @property double $RANGO1
 * @property double $RANGO2
 * @property string $COLOR
 *
 * @property Indicador $iNDICADOR
 */
class ColorRangoIdl extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%color_rango_idl}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_INDICADOR'], 'integer'],
            [['RANGO1', 'RANGO2','ID_INDICADOR', 'COLOR'], 'required'],    
            [['RANGO1', 'RANGO2'], 'number'],
            [['COLOR'], 'string'],
            [['ID_INDICADOR'], 'exist', 'skipOnError' => true, 'targetClass' => Indicador::className(), 'targetAttribute' => ['ID_INDICADOR' => 'ID_INDICADOR']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'ID_INDICADOR' => Yii::t('app', 'Indicador'),
            'RANGO1' => Yii::t('app', 'Rango 1'),
            'RANGO2' => Yii::t('app', 'Rango 2'),
            'COLOR' => Yii::t('app', 'Color'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getINDICADOR()
    {
        return $this->hasOne(Indicador::className(), ['ID_INDICADOR' => 'ID_INDICADOR']);
    }

    /**
     * {@inheritdoc}
     * @return ColorRangoIdlQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ColorRangoIdlQuery(get_called_class());
    }
}
