<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UnidadProducto;

/**
 * UnidadProductoSearch represents the model behind the search form of `app\models\UnidadProducto`.
 */
class UnidadProductoSearch extends UnidadProducto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CS_UNIDAD_ID'], 'integer'],
            [['DS_NOMBRE_UNIDAD', 'DS_DESCRIPCION_UNIDAD', 'ESTADO'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UnidadProducto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'CS_UNIDAD_ID' => $this->CS_UNIDAD_ID,
            'ESTADO' => UnidadProducto::UND_ACTIVO
        ]);

        $query->andFilterWhere(['like', 'DS_NOMBRE_UNIDAD', $this->DS_NOMBRE_UNIDAD])
            ->andFilterWhere(['like', 'DS_DESCRIPCION_UNIDAD', $this->DS_DESCRIPCION_UNIDAD]);

        return $dataProvider;
    }
}
