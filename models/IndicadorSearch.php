<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Indicador;

/**
 * IndicadorSearch represents the model behind the search form of `app\models\Indicador`.
 */
class IndicadorSearch extends Indicador
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID_INDICADOR', 'ID_AREA', 'NM_ID_TIPO_IND'], 'integer'],
            [['DS_NOMBRE_INDICADOR', 'DS_FORMULA', 'DS_NOMENCLATURA', 'DEFINICION', 'NM_ELIMINADO', 'UNIDAD'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Indicador::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID_INDICADOR' => $this->ID_INDICADOR,
            'ID_AREA' => $this->ID_AREA,
            'NM_ID_TIPO_IND' => $this->NM_ID_TIPO_IND,
            'NM_ELIMINADO' => $this->NM_ELIMINADO
        ]);

        $query->andFilterWhere(['like', 'DS_NOMBRE_INDICADOR', $this->DS_NOMBRE_INDICADOR])
            ->andFilterWhere(['like', 'DS_FORMULA', $this->DS_FORMULA])
            ->andFilterWhere(['like', 'DS_NOMENCLATURA', $this->DS_NOMENCLATURA])
            ->andFilterWhere(['like', 'UNIDAD', $this->UNIDAD])
            ->andFilterWhere(['like', 'DEFINICION', $this->DEFINICION]);

        return $dataProvider;
    }
}
