<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $email;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // email and password are both required
            [['email', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }
    
    public function attributeLabels() {
        return [
            "rememberMe" => "Recordarme",
            "email" => "Correo",
            "password" => "Contraseña"
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if(empty($user)){
                $this->addError($attribute, 'Usuario o contraseña incorrecto.');
            }elseif (!$user->validatePassword($this->password)) {
                 $this->addError($attribute, 'Usuario o contraseña incorrecto.');
            } elseif ($user->NM_ESTADO_ID == \app\models\EstadoActor::INNACTIVO || $user->NM_ESTADO_ID == \app\models\EstadoActor::PENDIENTE_RESTAURAR_CONTRASENA) {
                $this->addError($attribute, 'El usuario se encuentra inactivo o tiene un proceso para restaurar contraseña.');
            }
        }
    }

    /**
     * Logs in a user using the provided email and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        }else{
           // die("no valido");
        }
        return false;
    }

    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Actor::findOne(["DS_CORREO"=>$this->email]);
        }

        return $this->_user;
    }

    public function getUsuario()
    {
       return $this->_user;
    }
}
