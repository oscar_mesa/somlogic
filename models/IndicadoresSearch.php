<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cadenadeabastecimiento;

/**
 * IndicadoresSearch represents the model behind the search form of `app\models\Cadenadeabastecimiento`.
 */
class IndicadoresSearch extends Cadenadeabastecimiento
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NM_CADENA_ID', 'ID_EMPRESA'], 'integer'],
            [['DS_PEDIDO', 'FECHA_PEDIDO', 'FECHA_FIN'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cadenadeabastecimiento::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'NM_CADENA_ID' => $this->NM_CADENA_ID,
            'ID_EMPRESA' => $this->ID_EMPRESA,
            'FECHA_PEDIDO' => $this->FECHA_PEDIDO,
            'FECHA_FIN' => $this->FECHA_FIN,
        ]);

        $query->andFilterWhere(['like', 'DS_CADENA_ABASTECIMIENTO', $this->DS_CADENA_ABASTECIMIENTO]);

        return $dataProvider;
    }
}
