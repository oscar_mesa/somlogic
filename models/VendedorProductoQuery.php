<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VendedorProducto]].
 *
 * @see VendedorProducto
 */
class VendedorProductoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return VendedorProducto[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return VendedorProducto|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
