<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%area}}".
 *
 * @property int $ID_AREA
 * @property string $DS_AREA
 *
 * @property Indicador[] $indicadors
 */
class Area extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%area}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DS_AREA'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_AREA' => Yii::t('app', 'Id Area'),
            'DS_AREA' => Yii::t('app', 'Ds Area'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicadors()
    {
        return $this->hasMany(Indicador::className(), ['ID_AREA' => 'ID_AREA']);
    }

    /**
     * {@inheritdoc}
     * @return AreaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AreaQuery(get_called_class());
    }
}
