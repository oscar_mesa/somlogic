<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "vendedor_producto".
 *
 * @property int $CS_VENDEDOR_ID
 * @property string $DS_NOMBRE_VENDEDOR
 * @property string $DS_DESCRIPCION_VENDEDOR
 * @property enum $ESTADO
 *
 * @property InventarioProducto[] $inventarioProductos
 */
class VendedorProducto extends \yii\db\ActiveRecord
{
    const VENDEDOR_ACTIVO = 'activo';
    const VENDEDOR_ELIMINADO = 'eliminado';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vendedor_producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DS_NOMBRE_VENDEDOR'], 'required'],
            [['DS_NOMBRE_VENDEDOR'], 'string', 'max' => 30],
            [['DS_DESCRIPCION_VENDEDOR'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CS_VENDEDOR_ID' => Yii::t('app', 'ID'),
            'DS_NOMBRE_VENDEDOR' => Yii::t('app', 'Nombre Vendedor'),
            'DS_DESCRIPCION_VENDEDOR' => Yii::t('app', 'Descripción Vendedor'),
            'ESTADO' => Yii::t('app', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInventarioProductos()
    {
        return $this->hasMany(InventarioProducto::className(), ['CS_VENDEDOR_PRODUCTO_ID' => 'CS_VENDEDOR_ID']);
    }

    /**
     * {@inheritdoc}
     * @return VendedorProductoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VendedorProductoQuery(get_called_class());
    }
}
