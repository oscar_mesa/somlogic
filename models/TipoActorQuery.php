<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[TipoActor]].
 *
 * @see TipoActor
 */
class TipoActorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return TipoActor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return TipoActor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
