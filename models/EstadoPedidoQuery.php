<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[EstadoPedido]].
 *
 * @see EstadoPedido
 */
class EstadoPedidoQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return EstadoPedido[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return EstadoPedido|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
