<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[VwActor]].
 *
 * @see VwActor
 */
class VwActorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return VwActor[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return VwActor|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
