<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[CadenaEtapa]].
 *
 * @see CadenaEtapa
 */
class CadenaEtapaQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return CadenaEtapa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return CadenaEtapa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
