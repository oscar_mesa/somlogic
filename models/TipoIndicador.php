<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%tipo_indicador}}".
 *
 * @property int $ID_TIPO
 * @property string $DS_TIPO_IND
 *
 * @property Indicador[] $indicadors
 */
class TipoIndicador extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tipo_indicador}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DS_TIPO_IND'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID_TIPO' => Yii::t('app', 'Id Tipo'),
            'DS_TIPO_IND' => Yii::t('app', 'Ds Tipo Ind'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndicadors()
    {
        return $this->hasMany(Indicador::className(), ['NM_ID_TIPO_IND' => 'ID_TIPO']);
    }

    /**
     * {@inheritdoc}
     * @return TipoIndicadorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TipoIndicadorQuery(get_called_class());
    }
}
