<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_actor".
 *
 * @property int $CS_TIPO_ACTOR
 * @property string $DS_NOMBRE_TIPO_ACTOR
 *
 * @property Usuario[] $usuarios
 */
class TipoActor extends \yii\db\ActiveRecord
{
    CONST ADMINISTRADOR = 1;
    CONST VENDEDOR = 3;
    CONST ACTOR_NORMAL = 2;
    CONST PROVEEDOR = 4;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_actor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DS_NOMBRE_TIPO_ACTOR', 'NM_PORCENTAJE_INCENTIVO'], 'required'],
            [['NM_PORCENTAJE_INCENTIVO'], 'number'],
            [['DS_NOMBRE_TIPO_ACTOR'], 'string', 'max' => 15],
            [['DS_DESCRIPCION_tipo_actor'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CS_TIPO_ACTOR' => Yii::t('app', 'Cs  Tipo  Usuario'),
            'DS_NOMBRE_TIPO_ACTOR' => Yii::t('app', 'Ds  Nombre  Tipo  Usuario'),
            'DS_DESCRIPCION_tipo_actor' => Yii::t('app', 'Ds  Descripcion  Tipo  Usuario'),
            'NM_PORCENTAJE_INCENTIVO' => Yii::t('app', 'Nm  Porcentaje  Incentivo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['NM_TIPO_ACTOR_ID' => 'CS_TIPO_ACTOR']);
    }

    /**
     * {@inheritdoc}
     * @return PerfilQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PerfilQuery(get_called_class());
    }
}
