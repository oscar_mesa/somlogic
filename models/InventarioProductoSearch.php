<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\InventarioProducto;

/**
 * InventarioProductoSearch represents the model behind the search form of `app\models\InventarioProducto`.
 */
class InventarioProductoSearch extends InventarioProducto
{
    public $cSPRODUCTO;
    public $cSVENDEDORPRODUCTO;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CS_INVENTARIO_ID', 'CS_PRODUCTO_ID', 'NM_CANTIDAD_INVENTARIO', 'CS_VENDEDOR_PRODUCTO_ID'], 'integer'],
            [['DT_FECHA_CREACION', 'cSPRODUCTO', 'cSVENDEDORPRODUCTO'], 'safe'],
            [['NM_PRECIO_UNITARIO_COMPRA'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InventarioProducto::find();
        
         $query->joinWith(['cSPRODUCTO', 'cSVENDEDORPRODUCTO']);
         
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,        
        ]);
        
         // Important: here is how we set up the sorting
        // The key is the attribute name on our "TourSearch" instance
        $dataProvider->sort->attributes['cSPRODUCTO'] = [
            'asc' => ['producto.DS_NOMBRE_PRODUCTO' => SORT_ASC],
            'desc' => ['producto.DS_NOMBRE_PRODUCTO' => SORT_DESC],
        ];
        // Lets do the same with country now
        $dataProvider->sort->attributes['cSVENDEDORPRODUCTO'] = [
            'asc' => ['vendedor_producto.DS_NOMBRE_VENDEDOR' => SORT_ASC],
            'desc' => ['vendedor_producto.DS_NOMBRE_VENDEDOR' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'CS_INVENTARIO_ID' => $this->CS_INVENTARIO_ID,
            'CS_PRODUCTO_ID' => $this->CS_PRODUCTO_ID,
            'NM_CANTIDAD_INVENTARIO' => $this->NM_CANTIDAD_INVENTARIO,
            'CS_VENDEDOR_PRODUCTO_ID' => $this->CS_VENDEDOR_PRODUCTO_ID,
            'DT_FECHA_CREACION' => $this->DT_FECHA_CREACION,
            'NM_PRECIO_UNITARIO_COMPRA' => $this->NM_PRECIO_UNITARIO_COMPRA,
        ])
        ->andFilterWhere(['like', 'producto.DS_NOMBRE_PRODUCTO', $this->cSPRODUCTO])
        ->andFilterWhere(['like', 'vendedor_producto.DS_NOMBRE_VENDEDOR', $this->cSVENDEDORPRODUCTO])        ;

        return $dataProvider;
    }
}
