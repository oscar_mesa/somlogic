<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pedido_detalle".
 *
 * @property int $NM_ID_DETALLE_PEDIDO
 * @property int $CS_PEDIDO_ID
 * @property int $NM_CANTIDAD_COMPRA
 * @property int $CS_PRODUCTO_ID
 * @property double $NM_PRECIO_TOTAL_PRODUCTO
 * @property double $NM_PRECIO_UNITARIO
 * @property string $DS_UQ_SESSION
 * @property int $NM_USUARIO_CREADOR
 * @property int $PORCENTAJE_IVA
 * @property int $PORCENTAJE_DESCUENTO
 * @property double $NM_VALOR_IVA
 * @property double $NM_SUB_TOTAL
 * @property double $VALOR_DESCUENTO
 * @property string $DT_FECHA_CREACION
 * @property string $FC_FECHA_MODIFICACION
 * @property int $NM_USUARIO_MODIFICA
 * @property string $FC
 * @property int $MG
 * @property int $FK_UNIDAD
 * @property string $DS_OBSERVACION
 *
 * @property Pedido $cSPEDIDO
 * @property Producto $cSPRODUCTO
 */
class PedidoDetalle extends \yii\db\ActiveRecord
{
    public $DS_PRODUCTO, $CALCULO_MANUAL, $SUM_NM_CANTIDAD_COMPRA;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pedido_detalle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CS_PEDIDO_ID', 'NM_CANTIDAD_COMPRA', 'CS_PRODUCTO_ID', 'NM_USUARIO_CREADOR', 'PORCENTAJE_IVA', 'PORCENTAJE_DESCUENTO', 'MG', 'FK_UNIDAD'], 'integer'],
            [['NM_CANTIDAD_COMPRA', 'CS_PRODUCTO_ID', 'NM_PRECIO_TOTAL_PRODUCTO', 'FK_UNIDAD'], 'required'],
            [['NM_PRECIO_TOTAL_PRODUCTO', 'NM_PRECIO_UNITARIO', 'NM_VALOR_IVA', 'NM_SUB_TOTAL', 'VALOR_DESCUENTO', 'FC'], 'number'],
            [['DT_FECHA_CREACION', 'DS_OBSERVACION', 'NM_USUARIO_MODIFICA', 'DT_FECHA_CREACION'], 'safe'],
            [['DS_UQ_SESSION'], 'string', 'max' => 255],
            [['CS_PEDIDO_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Pedido::className(), 'targetAttribute' => ['CS_PEDIDO_ID' => 'CS_PEDIDO_ID']],
            [['CS_PRODUCTO_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['CS_PRODUCTO_ID' => 'CS_PRODUCTO_ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NM_ID_DETALLE_PEDIDO' => Yii::t('app', 'ID'),
            'CS_PEDIDO_ID' => Yii::t('app', 'ID Pedido'),
            'NM_CANTIDAD_COMPRA' => Yii::t('app', 'Cantidad Compra'),
            'CS_PRODUCTO_ID' => Yii::t('app', 'ID Producto'),
            'DS_PRODUCTO' => Yii::t('app', 'Producto'),
            'NM_PRECIO_TOTAL_PRODUCTO' => Yii::t('app', 'Precio Total Producto'),
            'NM_PRECIO_UNITARIO' => Yii::t('app', 'Precio Unitario'),
            'DS_UQ_SESSION' => Yii::t('app', 'UQ Session'),
            'NM_USUARIO_CREADOR' => Yii::t('app', 'Usuario Creador'),
            'PORCENTAJE_IVA' => Yii::t('app', 'Porcentaje Iva'),
            'PORCENTAJE_DESCUENTO' => Yii::t('app', '% Descuento'),
            'NM_VALOR_IVA' => Yii::t('app', 'Valor Iva'),
            'NM_SUB_TOTAL' => Yii::t('app', 'Sub Total'),
            'VALOR_DESCUENTO' => Yii::t('app', 'Valor Descuento'),
            'DT_FECHA_CREACION' => Yii::t('app', 'Fecha Creacion'),
            'FC' => Yii::t('app', 'Fc'),
            'MG' => Yii::t('app', 'Mg'),
            'FK_UNIDAD' => Yii::t('app', 'Unidad'),
            'DS_OBSERVACION' => Yii::t('app', 'Observación'),
            'CALCULO_MANUAL' => Yii::t('app', 'Calcular manualmente'),
            'FC_FECHA_MODIFICACION'=> Yii::t('app', 'Fecha de modificación'),
            'NM_USUARIO_MODIFICA'=> Yii::t('app', 'Usuario de modificador'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCSPEDIDO()
    {
        return $this->hasOne(Pedido::className(), ['CS_PEDIDO_ID' => 'CS_PEDIDO_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getcSPRODUCTO()
    {
        return $this->hasOne(Producto::className(), ['CS_PRODUCTO_ID' => 'CS_PRODUCTO_ID']);
    }
    
        /**
     * @return \yii\db\ActiveQuery
     */
    public function getFKUNIDAD() {
        return $this->hasOne(UnidadProducto::className(), ['CS_UNIDAD_ID' => 'FK_UNIDAD']);
    }

    public function getNMUSUARIOMODIFICA()
   {
       return $this->hasOne(Actor::className(), ['ID_ACTOR' => 'NM_USUARIO_MODIFICA']);
   }
   /**
    * @return \yii\db\ActiveQuery
    */
   public function getNMUSUARIOCREADOR()
   {
       return $this->hasOne(Actor::className(), ['ID_ACTOR' => 'NM_USUARIO_CREADOR']);
   }
    /**
     * {@inheritdoc}
     * @return PedidoDetalleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PedidoDetalleQuery(get_called_class());
    }
}
