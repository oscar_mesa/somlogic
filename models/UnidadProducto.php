<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unidad_producto".
 *
 * @property int $CS_UNIDAD_ID
 * @property string $DS_NOMBRE_UNIDAD
 * @property string $DS_DESCRIPCION_UNIDAD
 * @property enum $ESTADO
 *
 * @property Producto[] $productos
 */
class UnidadProducto extends \yii\db\ActiveRecord
{
    const UND_ACTIVO = 'activo';
    const UND_ELIMINADO = 'eliminado';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unidad_producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DS_NOMBRE_UNIDAD'], 'required'],
            [['DS_NOMBRE_UNIDAD'], 'string', 'max' => 30],
            [['DS_DESCRIPCION_UNIDAD'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'CS_UNIDAD_ID' => Yii::t('app', 'ID'),
            'DS_NOMBRE_UNIDAD' => Yii::t('app', 'Nombre'),
            'DS_DESCRIPCION_UNIDAD' => Yii::t('app', 'Descripción'),
            'ESTADO' => Yii::t('app', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductos()
    {
        return $this->hasMany(Producto::className(), ['FK_UNIDAD' => 'CS_UNIDAD_ID']);
    }

    /**
     * {@inheritdoc}
     * @return UnidadProductoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UnidadProductoQuery(get_called_class());
    }
}
