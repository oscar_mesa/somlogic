<?php

use yii\db\Migration;

/**
 * Class m181113_102848_create_news_autenticacion
 */
class m181113_102848_create_news_autenticacion extends Migration {

    /**
     * {@inheritdoc}
     */
    public function safeUp() {
        $this->createTable('autenticacion', [
            'ID' => $this->primaryKey(),
            'ID_USUARIO' => $this->integer(5)->notNull(),
            'ORIGEN' => $this->string()->notNull(),
            'ID_ORIGEN' => $this->string()->notNull(),
        ]);

        $this->addForeignKey('FK_AUTENTICACION_USUARIO_ID', 'autenticacion', 'ID_USUARIO', 'usuario', 'ID_USUARIO', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown() {
        $this->dropForeignKey("FK_AUTENTICACION_USUARIO_ID", "autenticacion");
        $this->dropTable('autenticacion');
    }

}
