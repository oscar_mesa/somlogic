<?php

namespace app\controllers;

use Yii;
use app\models\Pedido;
use app\models\PedidoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use Web3\Web3;
use Web3\Providers\HttpProvider;
use Web3\Eth;

/**
 * PedidoController implements the CRUD actions for Pedido model.
 */
class PedidoController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii2mod\rbac\filters\AccessControl::class,
                'allowActions' => [
                ]
            ],
        ];
    }

    /**
     * Lists all Pedido models.
     * @return mixed
     */
    public function actionIndex() {
       // $web3 = new Web3('http://192.168.0.11:8501');

        /*$eth = new Eth('http://192.168.0.11:8501');

        $eth->batch(true);
        $eth->protocolVersion();
        $eth->syncing(); 

        $eth->provider->execute(function ($err, $data) {
            if ($err !== null) {
                echo "<pre>";
                print_r($err);die;
                // do something
                return;
            }
            // do something
        });

        */

        $searchModel = new PedidoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pedido model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pedido model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Pedido();
        $model->scenario = Pedido::CREACION_PEDIDO;
        if (!isset(Yii::$app->request->post()["_csrf"]) && !isset(Yii::$app->request->get()["_pjax"]))
            \app\models\PedidoDetalle::deleteAll(["CS_PEDIDO_ID" => null]);
        
        $model->PRODUCTOS_PEDIDO = "productos";

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            //var_dump(Yii::$app->request->post());die;
            Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                'duration' => 5000,
                'icon' => 'fa fa-users',
                'message' => Yii::t('app', \yii\bootstrap\Html::encode('El pedido se realizo correctamente.')),
                'title' => Yii::t('app', \yii\bootstrap\Html::encode('Pedido realizado')),]);
            $model->DT_FECHA_CREACION = date("Y-m-d");
            $model->ID_ESTADO = \app\models\EstadoPedido::RECIBIDO; 
            if ($model->save()) {
            return $this->redirect(['view', 'id' => $model->CS_PEDIDO_ID]);
             }else{
                 print_r($model->errors);
                 die();
             }
        } else {
            
            $model->DS_CODIGO_PEDIDO = Pedido::obtenerCodigoPedidoDisponible();
            $model->DS_ACTOR = ucwords(Yii::$app->user->getIdentity()->DS_NOMBRES_ACTOR . " " . Yii::$app->user->getIdentity()->DS_APELLIDOS_ACTOR);
            $model->DT_FECHA_CREACION = date("Y-m-d");
            $model->ID_ESTADO = \app\models\EstadoPedido::RECIBIDO;
            $model->NM_USUARIO_CREADOR_ID = Yii::$app->user->getIdentity()->ID_ACTOR;
            $searchModelPrd = new \app\models\ProductoSearch();
            $dataProviderPrd = $searchModelPrd->search(Yii::$app->request->queryParams);
            $dataProviderPrd->pagination = ['pageSize' => 10,];


            $searchModelPedidoDetalle = new \app\models\PedidoDetalleSearch();
            $searchModelPedidoDetalle->ESPEDIDO = true;
            $dataProviderPedidoDetalle = $searchModelPedidoDetalle->search(Yii::$app->request->queryParams);
            $dataProviderPedidoDetalle->pagination = ['pageSize' => 10,];

            return $this->render('create', [
                        'model' => $model,
                        'configuracion' => \app\models\Configuracion::findOne(1),
                        'searchModelPrd' => $searchModelPrd,
                        'dataProviderPrd' => $dataProviderPrd,
                        'dataProviderPedidoDetalle' => $dataProviderPedidoDetalle,
                        'searchModelPedidoDetalle' => $searchModelPedidoDetalle
            ]);
        }
    }

    public function actionObtenertotalespedido() {
        if (Yii::$app->request->isAjax) {
            if (empty(Yii::$app->request->get("CS_PEDIDO_ID"))) {
                return \yii\helpers\Json::encode(\app\models\PedidoDetalle::find()->where(["CS_PEDIDO_ID" => null, 'NM_USUARIO_CREADOR' => Yii::$app->user->getIdentity()->ID_ACTOR])->all());
            }else{
                return \yii\helpers\Json::encode(\app\models\PedidoDetalle::find()->where(["CS_PEDIDO_ID" => Yii::$app->request->get("CS_PEDIDO_ID")])->all());
            }
        } else {
            throw new \yii\base\Exception('Acci�n no disponible.');
        }
    }

    /**
     * Updates an existing Pedido model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $model->scenario = Pedido::ACTUALIZACION_PEDIDO;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->CS_PEDIDO_ID]);
        } else {
            $model->DS_ACTOR = $model->nMACTORCREADOR->DS_NOMBRES_ACTOR;
            $searchModelPrd = new \app\models\ProductoSearch();
            $dataProviderPrd = $searchModelPrd->search(Yii::$app->request->queryParams);
            $dataProviderPrd->pagination = ['pageSize' => 10,];

            $searchModelPedidoDetalle = new \app\models\PedidoDetalleSearch();
            $searchModelPedidoDetalle->CS_PEDIDO_ID = $model->CS_PEDIDO_ID;
            $searchModelPedidoDetalle->ESPEDIDO = false;
            $dataProviderPedidoDetalle = $searchModelPedidoDetalle->search(Yii::$app->request->queryParams);
            $dataProviderPedidoDetalle->pagination = ['pageSize' => 10,];          
            return $this->render('update', [
                        'model' => $model,
                    'configuracion' => \app\models\Configuracion::findOne(1),
                    'dataProviderPedidoDetalle' => $dataProviderPedidoDetalle,
                    'searchModelPedidoDetalle' => $searchModelPedidoDetalle,
                    'dataProviderPrd' => $dataProviderPrd,
                    'searchModelPrd' => $searchModelPrd
            ]);
        }
    }

    public function actionObtenerpedidosselect2()
    {
         $q = Yii::$app->request->get("q");
         $d = [];
         if(!empty(Yii::$app->request->get("idempresa"))){
            $query = \app\models\Pedido::find();
                                   $query->where(['NM_EMPRESA' => Yii::$app->request->get("idempresa")])
                                   ->andWhere(['<>','ID_ESTADO', 5])
                                   ->andWhere([
                                    'OR',
                                    ['like', 'DS_CODIGO_PEDIDO', '%' . $q . '%', false]
                                ]);
                                
            $query->limit = 10;
            $d = [];
            foreach ($query->all() as $pedido) {
                $d[] = ["id" => $pedido->CS_PEDIDO_ID, "text" => $pedido->DS_CODIGO_PEDIDO, 'modelo' => $pedido->attributes];
            }
        }
        return \yii\helpers\BaseJson::encode(["items" => $d]);
    }

    /**
     * Deletes an existing Pedido model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        //\app\models\PedidoDetalle::deleteAll(['CS_PEDIDO_ID' => $id]);
        //$model->delete();
        $model->ID_ESTADO = 5;
        $model->update();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Pedido model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pedido the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Pedido::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
