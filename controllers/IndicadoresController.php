<?php

namespace app\controllers;

use Yii;
use app\models\Cadenadeabastecimiento;
use app\models\IndicadoresSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * IndicadoresController implements the CRUD actions for Cadenadeabastecimiento model.
 */
class IndicadoresController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii2mod\rbac\filters\AccessControl::class,
                'allowActions' => [
                ]
            ],
        ];
    }

    /**
     * Lists all Cadenadeabastecimiento models.
     * @return mixed
     */
    public function actionTiempo()
    {
        return $this->render('tiempo', [
        ]);
    }

    /**
     * Lists all Cadenadeabastecimiento models.
     * @return mixed
     */
    public function actionCosto()
    {
        return $this->render('costo', [
        ]);
    }

    /**
     * Lists all Cadenadeabastecimiento models.
     * @return mixed
     */
    public function actionTransporte()
    {
        return $this->render('trasnporte', [
        ]);
    }


    /**
     * Lists all Cadenadeabastecimiento models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IndicadoresSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cadenadeabastecimiento model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Cadenadeabastecimiento model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cadenadeabastecimiento();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->NM_CADENA_ID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Cadenadeabastecimiento model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->NM_CADENA_ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cadenadeabastecimiento model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cadenadeabastecimiento model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cadenadeabastecimiento the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cadenadeabastecimiento::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
