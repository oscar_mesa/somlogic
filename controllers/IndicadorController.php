<?php

namespace app\controllers;

use Yii;
use app\models\Indicador;
use app\models\IndicadorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * IndicadorController implements the CRUD actions for Indicador model.
 */
class IndicadorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii2mod\rbac\filters\AccessControl::class,
                'allowActions' => [
                ]
            ],
        ];
    }

    public function actionAlmacenarinformacioncumplimientoproceso()
    {
         Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
         try{
             $CadenaEtapa = Yii::$app->request->post("CadenaEtapa");
             foreach ($CadenaEtapa['NM_PORCENTAJE_PONDERACION'] as $ID_ETP_CAD => $NM_PORCENTAJE_PONDERACION) {
                $r = \app\models\CadenaEtapa::findOne($ID_ETP_CAD);
                $r->NM_PORCENTAJE_SIGNIFICANCIA = $NM_PORCENTAJE_PONDERACION;
                $r->save();
             }
             return ['respuesta' => true, 'mensaje' => 'Los datos fueron almacenados correctamente.'];
         }catch(Exception $e){
            return ['respuesta' => false, 'mensaje' => 'Se presento un error.'];
         }
    }

    public function actionCalcularcumplimientopromedio()
    {
        //\yii\helpers\BaseJson:
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $N = 0;
        $S  = 0;
        foreach (\app\models\CadenaIndicador::find()->where(['ID_CADENA' => Yii::$app->request->post("ID_ETP_CAD")])->all() as $row) {
            if($row->iNDICADOR->NM_ID_TIPO_IND == 2 || $row->iNDICADOR->NM_ID_TIPO_IND == 3){
                $S += $row->NM_RESULTADO_INDICADOR/((empty($row->META) || $row->META == 0)?1:$row->META);
            }else{
                $S += $row->NM_RESULTADO_INDICADOR;
            }
            $N++;
        }
        $r = \app\models\CadenaEtapa::findOne(Yii::$app->request->post("ID_ETP_CAD"));
        $r->CUMPLIMIENTO_PROMEDIO = $N>0?(($S/$N)):0;
        $r->save();
        return round($r->CUMPLIMIENTO_PROMEDIO*100,3);
    }

    public function actionEliminarindicadoretapaca()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        \app\models\CampoCadenaIndicador::deleteAll(["ID_CADENA_INDICADOR" => Yii::$app->request->post()["CadenaIndicador"]["ID_INDICADOR_CADENA"]]);

        \app\models\CadenaIndicador::deleteAll(['ID_INDICADOR_CADENA' => Yii::$app->request->post()["CadenaIndicador"]["ID_INDICADOR_CADENA"]]);
        return ['respuesta' => true, 'mensaje' => 'Indicador fue eliminado exitosamente.'];
    }

    public function actionGuardarindicadoretapaca()
    {
        try{
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //print_r(Yii::$app->request->post()["CampoCadenaIndicador"]);
            \app\models\CampoCadenaIndicador::deleteAll(["ID_CADENA_INDICADOR" => Yii::$app->request->post()["CadenaIndicador"]["ID_INDICADOR_CADENA"]]);
            foreach (Yii::$app->request->post()["CampoCadenaIndicador"] as $CAMPOID => $VALOR) {
                $modelo = new \app\models\CampoCadenaIndicador();
                $modelo->ID_CAMPO = $CAMPOID;
                $modelo->VALOR = $VALOR;
                $modelo->ID_CADENA_INDICADOR = Yii::$app->request->post()["CadenaIndicador"]["ID_INDICADOR_CADENA"];
                $modelo->save();
            }

            $indicador = \app\models\CadenaIndicador::findOne(Yii::$app->request->post()["CadenaIndicador"]["ID_INDICADOR_CADENA"]);
            $indicador->NM_RESULTADO_INDICADOR = Yii::$app->request->post()["CadenaIndicador"]["NM_RESULTADO_INDICADOR"];
            $indicador->NM_PORCENTAJE_SIGNIFICANCIA = Yii::$app->request->post()["CadenaIndicador"]["NM_PORCENTAJE_SIGNIFICANCIA"];
            $indicador->ID_PEDIDO = Yii::$app->request->post()["CadenaIndicador"]["ID_PEDIDO"];
            $indicador->META = Yii::$app->request->post()["CadenaIndicador"]["META"];
            $indicador->IDCANDEABAS = Yii::$app->request->post()["CadenaIndicador"]["IDCANDEABAS"];
            if($indicador->save())
                return ['respuesta' => true, 'mensaje' => 'El indicador se actualizo correctamente.'];
            else
                return ['errors' => $indicador->errors, 'respuesta' => false, 'mensaje' => 'Se presentaron algunos errores durante la validación de los datos.'];
        }catch(Exception $e){
            return ['respuesta' => false, 'mensaje' => $e->getMessage()];
        }    
            
    }

    /**
     * Lists all Indicador models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IndicadorSearch();
        $searchModel->NM_ELIMINADO = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Indicador model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Indicador model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Indicador();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-line-chart',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('El indicador fue creado correctamente.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Creación')),]);
            return $this->redirect(['view', 'id' => $model->ID_INDICADOR]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Indicador model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-line-chart',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('El indicador fue actualizado correctamente.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Actualización')),]);
            return $this->redirect(['view', 'id' => $model->ID_INDICADOR]);
        } else {
            $searchModel = new \app\models\ColorRangoIdlSearch();
            $searchModel->ID_INDICADOR = $id;
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('update', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionGuardarindicadores()
    { 
        try{
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            if(!is_null(Yii::$app->request->post('Indicador'))){
                foreach(Yii::$app->request->post('Indicador') as $ID_INDICADOR) {
                    $modelo = new \app\models\CadenaIndicador();
                    $modelo->ID_INDICADOR = $ID_INDICADOR;
                    $modelo->ID_CADENA = Yii::$app->request->post('ID_ETP_CAD');
                    $modelo->save();
                }
            }
            return ['respuesta' => true, 'mensaje' => 'Se adicionaron los indicadores correctamente.'];
        }catch(Exception $e){
            return ['respuesta' => false, 'mensaje' => $e->getMessage()];
        }
    }

    public function actionIndexmodal() {
        $searchModel = new IndicadorSearch();
        $searchModel->NM_ELIMINADO = 0;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = false;

        return $this->renderAjax('indexmodal', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'ID_ETP_CAD' => Yii::$app->request->get('ID_ETP_CAD'),
        ]);
    }

    /**
     * Deletes an existing Indicador model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->NM_ELIMINADO = 1;
        $model->save();
        Yii::$app->getSession()->setFlash('success', ['type' => 'danger',
                        'duration' => 5000,
                        'icon' => 'fa fa-line-chart',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('El indicador fue eliminado correctamente.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Eliminación')),]);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Indicador model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Indicador the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Indicador::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
