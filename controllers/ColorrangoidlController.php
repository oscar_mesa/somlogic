<?php

namespace app\controllers;

use Yii;
use app\models\ColorRangoIdl;
use app\models\ColorRangoIdlSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ColorrangoIdlController implements the CRUD actions for ColorRangoIdl model.
 */
class ColorrangoidlController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii2mod\rbac\filters\AccessControl::class,
                'allowActions' => [
                ]
            ],
        ];
    }
    /**
     * Lists all ColorRangoIdl models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ColorRangoIdlSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ColorRangoIdl model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if (Yii::$app->request->get('modal')) {
            
            return $this->renderAjax('view', [
                        'model' => $this->findModel($id),
            ]);
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new ColorRangoIdl model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ColorRangoIdl();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdatemodal($id) {
        $model = $this->findModel($id);
        //$model->scenario = SedeCliente::SCENARIO_CREAR_SEDE_MODAL;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax) {
                // JSON response is expected in case of successful save
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                return ['success' => true, 'message' => Yii::t('app', 'El color de rango fue atualizada exitosamente para este indicador.')];
            }
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('update', [
                            'model' => $model,
                            'createmodel' => true,
                ]);
            } else {
                return $this->render('update', [
                            'model' => $model,
                            'createmodel' => true,
                ]);
            }
        }
    }

    public function actionCreatemodal() {
        $model = new ColorRangoIdl();
        //$model->scenario = ColorRangoIdl::SCENARIO_CREAR_SEDE_MODAL;
        $model->ID_INDICADOR = Yii::$app->request->get("id_indicador");
        if ($model->load(Yii::$app->request->post())) {
            //$model->NM_ELIMINADO = 0;
            if ($model->save()) {
                if (Yii::$app->request->isAjax) {
                    // JSON response is expected in case of successful save
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['success' => true, 'message' => Yii::t('app', 'El color de rango fue creada exitosamente para este indicador.')];
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }else{
               // var_dump($model->getErrors());
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
                'createmodel' => true
            ]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'createmodel' => true
            ]);
        }
    }    

    /**
     * Updates an existing ColorRangoIdl model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ColorRangoIdl model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return \yii\helpers\BaseJson::encode(["mensaje" => "El color fue eliminado exitosamente."]);
    }

    /**
     * Finds the ColorRangoIdl model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ColorRangoIdl the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ColorRangoIdl::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
