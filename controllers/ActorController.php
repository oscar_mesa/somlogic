<?php

namespace app\controllers;

use Yii;
use app\models\Actor1;
use app\models\Actor1Search;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ActorController implements the CRUD actions for Actor model.
 */
class ActorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii2mod\rbac\filters\AccessControl::class,
                'allowActions' => [
                ]
            ],
        ];
    }
    
    public function actionObtenerActorselect2()
    {
        $q = Yii::$app->request->get("q");
        $query = Actor1::find();
                               $query->where(["NM_ESTADO_ID" => 1]);
                            
        $query->limit = 10;
        $d = [];
        foreach ($query->all() as $Actor) {
            $d[] = ["id" => $Actor->ID_ACTOR, "text" => $Actor->DS_NOMBRES_ACTOR." ".$Actor->DS_APELLIDOS_ACTOR, 'modelo' => $Actor->attributes];
        }
        echo \yii\helpers\BaseJson::encode(["items" => $d]);die;
    }

    public function actionObteneractortiposelect2()
    {
        $q = Yii::$app->request->get("q");
        $query = Actor1::find();
        $query->where(["NM_ESTADO_ID" => 1, 'NM_TIPO_ACTOR_ID' => Yii::$app->request->get("tipo")]);
        $query->andWhere([
            'OR',
            ['like', 'DS_NOMBRES_ACTOR', '%' . $q . '%', false],
            ['like', 'DS_APELLIDOS_ACTOR', '%' . $q. '%', false],
            ['like', 'NM_DOCUMENTO_ID', '%' . $q. '%', false]
        ]);
                            
        $query->limit = 10;
        $d = [];
        foreach ($query->all() as $Actor) {
            $d[] = ["id" => $Actor->ID_ACTOR, "text" => $Actor->DS_NOMBRES_ACTOR." ".$Actor->DS_APELLIDOS_ACTOR, 'modelo' => $Actor->attributes];
        }
        echo \yii\helpers\BaseJson::encode(["items" => $d]);die;
    }    

    /**
     * Lists all Actor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new Actor1Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Actor model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $modelo = $this->findModel($id);
        return $this->render('view', [
            'model' => $modelo,
        ]);
    }

    /**
     * Creates a new Actor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new \app\models\Actor(['scenario' => \app\models\Actor::SCENARIO_CREATE_UPDATE]);
        
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\bootstrap\ActiveForm::validate($model);
        } 
        
        if ($model->load(Yii::$app->request->post())) {
            $model->NM_ESTADO_ID = \app\models\EstadoActor::ACTIVO;
            $contrasena = $model->DS_CONTRASENA;
            $model->DS_CONTRASENA = $model->CONFIRMAR_CONTRASENA = Yii::$app->getSecurity()->generatePasswordHash($model->DS_CONTRASENA);
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-users',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('El actor fue creado exitosamente.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Creación')),]);
             return $this->redirect(['view', 'id' => $model->ID_ACTOR]);   
            }else{
                $model->DS_CONTRASENA = $model->CONFIRMAR_CONTRASENA = $contrasena;
                return $this->render('create', [
                'model' => $model,
            ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Actor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = \app\models\Actor::SCENARIO_CREATE_UPDATE_NOREQ;
        
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\bootstrap\ActiveForm::validate($model);
        } 
        
        
        if ($model->load(Yii::$app->request->post())) {
            if(!empty(Yii::$app->request->post()['Actor']['DS_CONTRASENA'])){
               // die("entro!!");
                $model->DS_CONTRASENA = Yii::$app->request->post()['Actor']['DS_CONTRASENA'];
                $model->CONFIRMAR_CONTRASENA = Yii::$app->request->post()['Actor']['CONFIRMAR_CONTRASENA'];
                $model->scenario = \app\models\Actor::SCENARIO_CREATE_UPDATE;
                if($model->validate()){
                   $model->DS_CONTRASENA = $model->CONFIRMAR_CONTRASENA = Yii::$app->getSecurity()->generatePasswordHash($model->DS_CONTRASENA);
                }else{
                    $model->scenario = \app\models\Actor::SCENARIO_CREATE_UPDATE_NOREQ;
                    return $this->render('update', [
                        'model' => $model,
                    ]);
                }
               
            }
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-users',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('El actor fue actualizaddo exitosamente.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Actualizar')),]);
                return $this->redirect(['view', 'id' => $model->ID_ACTOR]);
            }else {
                $model->scenario = \app\models\Actor::SCENARIO_CREATE_UPDATE_NOREQ;
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            $model->DS_CONTRASENA = "";  
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Actor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $Actor = $this->findModel($id);
        $Actor->NM_ESTADO_ID = \app\models\EstadoActor::INNACTIVO;
        $Actor->scenario = \app\models\Actor::SCENARIO_DELETE;
        if($Actor->save()){
            Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-users',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('El actor fue eliminado exitosamente.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Eliminar')),]);
        } else {
            Yii::$app->getSession()->setFlash('danger', ['type' => 'danger',
                        'duration' => 5000,
                        'icon' => 'fa fa-users',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('Se genero un error durante el eliminado. '.json_encode($Actor->errors))),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Eliminar')),]);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Actor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Actor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = \app\models\Actor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
