<?php

namespace app\controllers;

use Yii;
use app\models\CampoCaculo;
use app\models\CampoCaculoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CampocaculoController implements the CRUD actions for CampoCaculo model.
 */
class CampocaculoController extends Controller
{

    /**
     * Lists all CampoCaculo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CampoCaculoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CampoCaculo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CampoCaculo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CampoCaculo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-outdent',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('El campo fue creado correctamente.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Creación')),]);
            return $this->redirect(['view', 'id' => $model->ID_CAMPO]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CampoCaculo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-outdent',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('El campo fue actualizado correctamente.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Actualización')),]);
            return $this->redirect(['view', 'id' => $model->ID_CAMPO]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }


    /**
     * Finds the CampoCaculo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CampoCaculo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CampoCaculo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
