<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Mailer as AcmeMailer;
use app\components\AuthHandler;

class SiteController extends Controller {

    public function actionBackup() {
        /** @var \demi\backup\Component $backup */
        $backup = \Yii::$app->backup;

        $file = $backup->create();

        return "Se creo el backup";
    }

    public function actionAcercade() {
        return $this->render('acercade');
    }

    public function actionAyuda()
    {
        return $this->render('ayuda');
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'onAuthSuccess'],
            ],
        ];
    }

    public function onAuthSuccess($client) {
        (new AuthHandler($client))->handle();
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
        //$l = new \yii\caching\FileCache();
        //$l->flushValues();

        //Yii::$app->cache->flush();

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    public function actionRecuperarPassword() {
        $Actor = new MathUser('registerwcaptcha');
        if (isset($_POST['MathUser'])) {
            $Actor->attributes = $_POST['MathUser'];
            //  print_r($_POST);die;
            $this->performAjaxValidation($Actor);
            //print_r($_POST);
            $u = $Actor->find('email=?', array($Actor->email));
            $this->enviarMailRecuperacionActor($u);
            $u->state = CRUGEUSERSTATE_RECOVERPASSWORD;
            $u->update();
            $user = Yii::app()->getComponent('user');
            Yii::app()->user->setFlash(
                    'success', "<strong>Exito!</strong> Se ha enviado un correo con la informacón para realizar este cambío."
            );
            $this->redirect(Yii::app()->getBaseUrl(true));
        }
    }

    public function actionRestaurarcredenciales() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new \app\models\Actor1(['scenario' => \app\models\Actor1::SCENARIO_TARE_CREDENTIALS]);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return \yii\bootstrap\ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
//            if (true) {
            if ($model->validate()) {
                $model = \app\models\Actor1::find()->where(["DS_CORREO" => $model->DS_CORREO])->one();
                if (AcmeMailer::send(AcmeMailer::TYPE_RESTAURAR_CONTRASENA, $model)) {
                    $model->NM_ESTADO_ID = \app\models\EstadoActor::PENDIENTE_RESTAURAR_CONTRASENA;
                    \app\models\Actor1::updateAll(['NM_ESTADO_ID' => \app\models\EstadoActor::PENDIENTE_RESTAURAR_CONTRASENA], ["ID_ACTOR" => $model->ID_ACTOR]);
                    Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-users',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('Se a enviado un correo para iniciar el proceso de recuperación de contraseña, mientras no se restaure no podrá iniciar sesión.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Envío de correo exitosa')),]);

                    return $this->redirect(["site/login"]);
                }
            } else {
                return $this->render('recuperar_contrasena', [
                            'model' => $model,
                ]);
            }
        } else {
            return $this->render('recuperar_contrasena', [
                        'model' => $model,
            ]);
        }
    }

    public function actionRegitrar() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new \app\models\Actor();

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return \yii\bootstrap\ActiveForm::validate($model);
        }


        if ($model->load(Yii::$app->request->post())) {
            $model->setAttribute("NM_ESTADO_ID", \app\models\EstadoActor::ACTIVO);
            $model->DT_FECHA_CREACION = date("Y-m-d");
            $model->NM_TIPO_ACTOR_ID = \app\models\TipoActor::ACTOR_NORMAL;
            //var_dump($model->attributes);
            if ($model->validate()) {
                $model->DS_CONTRASENA = $model->CONFIRMAR_CONTRASENA = Yii::$app->getSecurity()->generatePasswordHash($model->DS_CONTRASENA);
                if ($model->save() && AcmeMailer::send(AcmeMailer::TYPE_REGISTRATION, $model)) {
                    Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-users',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('El Actor fue registrado exitosamente.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Actor registrado')),]);

                    return $this->redirect(["site/login"]);
                }
            } else {
                return $this->render('registro', [
                            'model' => $model,
                ]);
            }
        } else {
            $model->NM_TIPO_DOCUMENTO_ID = "1";
            $model->NM_TIPO_ACTOR_ID = "2";
            return $this->render('registro', [
                        'model' => $model,
            ]);
        }
    }

    public function actionCambiocontrasena() {
//        print_r($_REQUEST);die;
        $model = new \app\models\Actor1();
        $model->scenario = \app\models\Actor1::SCENARIO_SAVE_TARE_CREDENTIALS;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return \yii\bootstrap\ActiveForm::validate($model);
        } else if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                 
                $model->NM_ESTADO_ID = \app\models\EstadoActor::ACTIVO;
                $model->ID_ACTOR = $_REQUEST["id"];
                $model->DS_CONTRASENA = $model->CONFIRMAR_CONTRASENA = Yii::$app->getSecurity()->generatePasswordHash($model->DS_CONTRASENA);
                if (\app\models\Actor1::updateAll(['DS_CONTRASENA' => $model->DS_CONTRASENA, 'NM_ESTADO_ID' => \app\models\EstadoActor::ACTIVO], ["ID_ACTOR" => $model->ID_ACTOR])) {
                    Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-users',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('Los datos de la cuenta fueron actualizados. El Actor fue activado y puede ingresar nuevamente al sistema sin problemas.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Contraseña restablecida')),]);

                    return $this->redirect(["site/login"]);
                } else {
                    return $this->render("nueva_contrasena", array('model' => $model));
                }
            } else {
                return $this->render("nueva_contrasena", array('model' => $model));
            }
        } else if (isset($_GET['hash']) && isset($_GET['id']) && sha1('logisticapolitecnicojic') == $_GET['hash']) {
            $Actor = \app\models\Actor1::findOne(["ID_ACTOR" => $_GET['id'], "NM_ESTADO_ID" => \app\models\EstadoActor::PENDIENTE_RESTAURAR_CONTRASENA]);
            if (!is_null($Actor)) {
                $Actor->scenario = \app\models\Actor1::SCENARIO_SAVE_TARE_CREDENTIALS;
                $Actor->DS_CONTRASENA = "";
                return $this->render("nueva_contrasena", array('model' => $Actor));
            } else {
                Yii::$app->getSession()->setFlash('error', ['type' => 'error',
                    'duration' => 5000,
                    'icon' => 'fa fa-users',
                    'message' => Yii::t('app', \yii\bootstrap\Html::encode('Este Actor no tiene solicitudes para cambio de contraseña o posiblemente no exista.')),
                    'title' => Yii::t('app', \yii\bootstrap\Html::encode('Error')),]);

                return $this->redirect(["site/login"]);
            }
        } else {
            Yii::$app->getSession()->setFlash('error', ['type' => 'error',
                'duration' => 5000,
                'icon' => 'fa fa-users',
                'message' => Yii::t('app', \yii\bootstrap\Html::encode('Usted no se encuentra autorizado para realizar esta acción.')),
                'title' => Yii::t('app', \yii\bootstrap\Html::encode('Error')),]);

            return $this->redirect(["site/login"]);
            $this->render('application.views.site.error');
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    public function getToken($token) {
        $model = Users::model()->findByAttributes(array('token' => $token));
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function actionVerToken($token) {
        $model = $this->getToken($token);
        if (isset($_POST['Ganti'])) {
            if ($model->token == $_POST['Ganti']['tokenhid']) {
                $model->password = md5($_POST['Ganti']['password']);
                $model->token = "null";
                $model->save();
                Yii::app()->user->setFlash('ganti', '<b>Password has been successfully changed! please login</b>');
                $this->redirect('?r=site/login');
                $this->refresh();
            }
        }
        $this->render('verifikasi', array(
            'model' => $model,
        ));
    }

    public function actionForgot() {

        if (isset($_POST['Lupa'])) {
            $getEmail = $_POST['Lupa']['email'];
            $getModel = Users::model()->findByAttributes(array('email' => $getEmail));
            $getToken = rand(0, 99999);
            $getTime = date("H:i:s");
            $getModel->token = md5($getToken . $getTime);
            $namaPengirim = "Owner Jsource Indonesia";
            $emailadmin = "fahmi.j@programmer.net";
            $subjek = "Reset Password";
            $setpesan = "you have successfully reset your password<br/>
                    <a href='http://yourdomain.com/index.php?r=site/vertoken/view&token=" . $getModel->token . "'>Click Here to Reset Password</a>";
            if ($getModel->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($namaPengirim) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($subjek) . '?=';
                $headers = "From: $name <{$emailadmin}>\r\n" .
                        "Reply-To: {$emailadmin}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-type: text/html; charset=UTF-8";
                $getModel->save();
                Yii::app()->user->setFlash('forgot', 'link to reset your password has been sent to your email');
                mail($getEmail, $subject, $setpesan, $headers);
                $this->refresh();
            }
        }
        $this->render('forgot');
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout() {
        return $this->render('about');
    }

}
