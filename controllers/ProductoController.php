<?php

namespace app\controllers;

use Yii;
use app\models\Producto;
use app\models\ProductoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ProductoController implements the CRUD actions for Producto model.
 */
class ProductoController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii2mod\rbac\filters\AccessControl::class,
                'allowActions' => [
                ]
            ],
        ];
    }

    /**
     * Lists all Producto models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ProductoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Producto model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Producto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Producto();
        
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\bootstrap\ActiveForm::validate($model);
        } 
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-users',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('El producto fue creado exitosamente.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Actualizar')),]);
            return $this->redirect(['view', 'id' => $model->CS_PRODUCTO_ID]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Producto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        
        if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())){
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\bootstrap\ActiveForm::validate($model);
        } 
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-users',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('El producto fue actualizado exitosamente.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Actualizar')),]);
            return $this->redirect(['view', 'id' => $model->CS_PRODUCTO_ID]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionObtenerproductoselect2() {
        $q = Yii::$app->request->get("q");
        $query = Producto::find();
        $query->where(["NM_ELIMINADO" => 0]);
        $query->andFilterWhere(['LIKE',"DS_CODIGO_PRODUCTO", $q]);
        $query->orFilterWhere(['LIKE',"DS_NOMBRE_PRODUCTO", $q]);
        $query->limit = 10;
        $d = [];
        foreach ($query->all() as $producto) {
            $d[] = ["id" => $producto->CS_PRODUCTO_ID, "text" => $producto->DS_CODIGO_PRODUCTO." - ".$producto->DS_NOMBRE_PRODUCTO];
        }
        echo \yii\helpers\BaseJson::encode(["items" => $d]);die;
    }

    /**
     * Deletes an existing Producto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $p = $this->findModel($id);
        
        $p->NM_ELIMINADO = 1;
        $p->update();
        Yii::$app->getSession()->setFlash('danger', ['type' => 'danger',
                        'duration' => 5000,
                        'icon' => 'fa fa-users',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('El producto fue eliminado correctamente.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Actualizar')),]);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Producto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Producto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Producto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
