<?php

namespace app\controllers;

use Yii;
use app\models\UnidadProducto;
use app\models\UnidadProductoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * UnidadProductoController implements the CRUD actions for UnidadProducto model.
 */
    class UnidadController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii2mod\rbac\filters\AccessControl::class,
                'allowActions' => [
                ]
            ],
        ];
    }
    
    public function actionObtenerunidadesselect2()
    {
        $q = Yii::$app->request->get("q");
        $query = UnidadProducto::find();
        $query->where = "DS_NOMBRE_UNIDAD LIKE '%".$q."%'";                   
        $query->limit = 100;
        $d = [];
        foreach ($query->all() as $unidad) {
            $d[] = ["id" => $unidad->CS_UNIDAD_ID, "text" => $unidad->DS_NOMBRE_UNIDAD."(".$unidad->DS_DESCRIPCION_UNIDAD.")", 'modelo' => $unidad->attributes];
        }
        echo \yii\helpers\BaseJson::encode(["items" => $d]);die;
    }

    /**
     * Lists all UnidadProducto models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new UnidadProductoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UnidadProducto model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UnidadProducto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new UnidadProducto();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->CS_UNIDAD_ID]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionCreatemodal() {
        $model = new UnidadProducto();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if (Yii::$app->request->isAjax) {
                    // JSON response is expected in case of successful save
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['success' => true];
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                        'model' => $model,
            ]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing UnidadProducto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->CS_UNIDAD_ID]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing UnidadProducto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $modelo = $this->findModel($id);
        $modelo->ESTADO = UnidadProducto::UND_ELIMINADO;
        if($modelo->update()){
            Yii::$app->getSession()->setFlash('danger', ['type' => 'danger',
                            'duration' => 5000,
                            'icon' => 'fa fa-warning',
                            'message' => Yii::t('app', \yii\bootstrap\Html::encode('La unidad fue eliminada exitosamente.')),
                            'title' => Yii::t('app', \yii\bootstrap\Html::encode('Eliminación')),]);
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the UnidadProducto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UnidadProducto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = UnidadProducto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
