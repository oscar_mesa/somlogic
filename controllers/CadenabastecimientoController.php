<?php

namespace app\controllers;

use Yii;
use app\models\Cadenadeabastecimiento;
use app\models\VwTotalIndicadoresCa;
use app\models\VwTotalIndicadoresCaSearch;
use app\models\VwIdlPedidosSearch;

class CadenabastecimientoController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii2mod\rbac\filters\AccessControl::class,
                'allowActions' => [
                ]
            ],
        ];
    }
    
    public function actionEficienciadespachoaduanero()
    {
        return $this->render('eficienciadespachoaduanero');
    }

    public function actionEficienciaempresatransporte()
    {
        return $this->render('eficienciaempresatransporte');
    }

    public function actionEficienciapuertos()
    {
        return $this->render('eficienciapuertos');
    }

    public function actionEficienciazonafranca()
    {
        return $this->render('eficienciazonafranca');
    }

    public function actionTiempoca()
    {
        if(Yii::$app->request->get('idca')){
            return $this->render('tiempoca');
        }else{
            throw new Exception("El id de la cadena de abastecimiento se debe enviar, para su carga.", 1);
        }
    }

    public function actionGestionarcadena($id)
    {
        //Yii::$app->cache->flush();

        $cadena = $this->findModel($id);
        $etapas = [];
        //print_r($e);die;
        if( Yii::$app->request->get('secccion') == 'actualizar_cadena' ) {
            array_unshift($etapas, [
                'label' => 'Cadena',
                'content' => $this->renderPartial('actualizar', [
                            'model' => $cadena,
                            ]),
                'active' => true
            ]);
        }else{
            array_unshift($etapas, [
                'label' => 'Cadena',
                'url' => \yii\helpers\Url::to(['cadenabastecimiento/gestionarcadena', 'id'=>$id, 'secccion' => 'actualizar_cadena'])
            ]);
        }

        foreach (\app\models\Etapa::find()->all() as $e) {
             $k = [
                'label' => $e->ETAPA,
                'url' => \yii\helpers\Url::to(['cadenabastecimiento/gestionarcadena', 'id'=>$id, 'etapa' => $e->ID_ETAPA, 'secccion' => 'proceso']),
            ];
            if(Yii::$app->request->get('etapa')==$e->ID_ETAPA){
                $k["active"] = true;
                if( is_null($cadenaEtapa = \app\models\CadenaEtapa::find()->where(['ID_ETAPA' => $e->ID_ETAPA, 'ID_CADENA' =>  $id])->one()) ){
                    $cadenaEtapa = new \app\models\CadenaEtapa();
                    $cadenaEtapa->ID_ETAPA = $e->ID_ETAPA;
                    $cadenaEtapa->ID_CADENA = $id;
                    $cadenaEtapa->save();
                }
                $dataProvider = new \yii\data\ActiveDataProvider([
                    'query' => \app\models\CadenaIndicador::find()->where(['ID_CADENA' => $cadenaEtapa->ID_ETP_CAD]),
                    'pagination' => [
                        'pageSize' => 20,
                    ],
                ]);
                $k["content"] = $this->renderPartial('etapa_cadena', [
                                    'model' => $cadenaEtapa,
                                    'cadena' => $cadena,
                                    'dataProvider' => $dataProvider
                                ]);

            }
            $etapas[] = $k;
        }

        if(Yii::$app->request->get('secccion') == 'resultado_indicadores') {
            $searchModel = new VwTotalIndicadoresCaSearch();
            $searchModel->ID_CADENA = Yii::$app->request->get('id');
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $etapas[] = [
                    'label' => 'Resultado de indicadores',
                    'content' => $this->renderPartial('total_indicadores_ca', [
                                'model' => $cadena,
                                'searchModel' => $searchModel,
                                'dataProvider' => $dataProvider,
                                ]),
                    'active' => true
                ];
        }else{
            $etapas[] = [
                'label' => 'Resultado de indicadores',
                'url' => \yii\helpers\Url::to(['cadenabastecimiento/gestionarcadena', 'id'=>$id, 'secccion' => 'resultado_indicadores'])
            ];
        }

        if(Yii::$app->request->get('secccion') == 'idl_pedidos') {
            //$searchModel = new \app\models\CadenaEtapaSearch();
            //$searchModel->ID_CADENA = Yii::$app->request->get('id');
            //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $Metapas = \app\models\Etapa::find()->all();
            $cadena = $this->findModel(Yii::$app->request->get('id'));
            $arr = [];
            $i = 0;
            foreach ($cadena->pEDIDOS as $row) {
               $arr[] = [
                    'DS_CODIGO_PEDIDO' => $row->DS_CODIGO_PEDIDO
               ];
               foreach ($Metapas as $etapa) {
                    $MCadenaEtapa = \app\models\CadenaEtapa::find()->with(['cadenaIndicadors'=>function($m) use ($row){
                        $m->where(['ID_PEDIDO' => $row->CS_PEDIDO_ID]);
                    }])->where(['ID_ETAPA' => $etapa->ID_ETAPA, 'ID_CADENA' => Yii::$app->request->get('id')])->one();
                    //echo "<pre>"; print_r($MCadenaEtapa);die;
                    $N = 0;
                    $S  = 0;
                    /*if (!is_object($MCadenaEtapa)) {
                        echo $row->CS_PEDIDO_ID;
                        echo "<br/>".$etapa->ID_ETAPA;die;
                    } */
                    if (is_object($MCadenaEtapa)) {
                        foreach ($MCadenaEtapa->cadenaIndicadors as $r) {
                            if($r->iNDICADOR->NM_ID_TIPO_IND == 2 || $r->iNDICADOR->NM_ID_TIPO_IND == 3){
                                $S += $r->NM_RESULTADO_INDICADOR/((empty($r->META) || $r->META == 0)?1:$r->META);
                            }else{
                                $S += $r->NM_RESULTADO_INDICADOR;
                            }
                            $N++;
                        }
                        # code...
                    }
                    $arr[$i][trim($etapa->ETAPA.$etapa->ID_ETAPA)] = round(($N>0?(($S/$N)):0)*100, 3)."%";
                   //round($model->CUMPLIMIENTO_PROMEDIO*100, 3)
               }
               $i++;
            }

            $dataProvider=new \yii\data\ArrayDataProvider([
                'key' => 'DS_CODIGO_PEDIDO',
                'sort'=>['attributes'=>['DS_CODIGO_PEDIDO'],],
                'allModels' => $arr,
                'pagination'=>['pageSize'=>1000]
                ]
            );

        //    echo "<pre>";print_r($arr);die;

            $etapas[] = [
                    'label' => 'Cumplimiento por pedido',
                    'content' => $this->renderPartial('idl_pedidos_ca', [
                                'model' => $cadena,
                                'etapas' => $Metapas, 
                                //'searchModel' => $searchModel,
                                'dataProvider' => $dataProvider,
                                ]),
                    'active' => true
                ];
        }else{
            $etapas[] = [
                'label' => 'Cumplimiento por pedido',
                'url' => \yii\helpers\Url::to(['cadenabastecimiento/gestionarcadena', 'id'=>$id, 'secccion' => 'idl_pedidos'])
            ];
        }

        if(Yii::$app->request->get('secccion') == 'complimiento_proceso') {
            $searchModel = new \app\models\CadenaEtapaSearch();
            $searchModel->ID_CADENA = Yii::$app->request->get('id');
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $etapas[] = [
                    'label' => 'IDL del Proceso',
                    'content' => $this->renderPartial('cumplimiento_proceso', [
                                'model' => $cadena,
                                'searchModel' => $searchModel,
                                'dataProvider' => $dataProvider,
                                ]),
                    'active' => true
                ];
        }else{
            $etapas[] = [
                'label' => 'IDL del Proceso',
                'url' => \yii\helpers\Url::to(['cadenabastecimiento/gestionarcadena', 'id'=>$id, 'secccion' => 'complimiento_proceso'])
            ];
        }
        //echo "<pre>";print_r($etapas);die;
        if ($cadena->load(Yii::$app->request->post()) && $cadena->save()) {
            Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-users',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('La cadena fue actualizada correctamente.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Creación')),]);
                return $this->redirect(['gestionarcadena', 'id' => $cadena->NM_CADENA_ID, 'secccion' => 'actualizar_cadena']);
        }
         return $this->render('gestrionar_cadena', [
            'cadena' => $cadena,
            'etapas' => $etapas
         ]);
    }

    public function actionConsultarcadenacontrato($id)
    {
        $cadena = $this->findModel($id);
        return $this->render('consultarcadenacontrato', [
            'cadena' => $cadena,
         ]);
    }

    public function actionEscribircadenacontrato($id)
    {
        $cadena = $this->findModel($id);
        return $this->render('escribircadenacontrato', [
            'cadena' => $cadena,
         ]);
    }

    public function actionVerca()
    {
        $searchModel = new \app\models\CadenadeabastecimientoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('verca', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIngresoca()
    {
        $modelo = new Cadenadeabastecimiento();
        $modelo->FECHA_CREACION = date("Y-m-d H:i:s");
        if ($modelo->load(Yii::$app->request->post()) && $modelo->save()) {
            Yii::$app->getSession()->setFlash('success', ['type' => 'success',
                        'duration' => 5000,
                        'icon' => 'fa fa-users',
                        'message' => Yii::t('app', \yii\bootstrap\Html::encode('La empresa fue creado exitosamente.')),
                        'title' => Yii::t('app', \yii\bootstrap\Html::encode('Creación')),]);
                return $this->redirect(['verca', 'idca' => $modelo->NM_CADENA_ID]);
        }else{
            return $this->render('ingresoca', ['model' => $modelo]);
        }
    }

    /**
     * Finds the Cliente model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cliente the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cadenadeabastecimiento::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
