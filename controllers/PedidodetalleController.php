<?php

namespace app\controllers;

use Yii;
use app\models\PedidoDetalle;
use app\models\PedidoDetalleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/** 
 * PedidoDetalleController implements the CRUD actions for PedidoDetalle model.
 */
class PedidodetalleController extends Controller
{ 
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii2mod\rbac\filters\AccessControl::class,
                'allowActions' => [
                ]
            ],
        ];
    }

    /**
     * Lists all PedidoDetalle models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PedidoDetalleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PedidoDetalle model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PedidoDetalle model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PedidoDetalle();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->NM_ID_DETALLE_PEDIDO]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionCreatemodal(){
        $model = new PedidoDetalle();
        $model->DS_UQ_SESSION = Yii::$app->request->get("DS_UQ_SESSION");

        if(!empty(Yii::$app->request->get("CS_PEDIDO_ID"))){
            $model->CS_PEDIDO_ID = Yii::$app->request->get("CS_PEDIDO_ID");   
        }

        $sede =  \app\models\Actor::findOne(["ID_ACTOR" => Yii::$app->request->get("NM_EMPRESA")]);  
        
         if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && !$model->validate()) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\bootstrap\ActiveForm::validate($model);
        }
        
        if ($model->load(Yii::$app->request->post())) {
            
            $model->NM_USUARIO_CREADOR = Yii::$app->user->getIdentity()->ID_ACTOR;
            if ($model->save()) {
                if (Yii::$app->request->isAjax) {
                    // JSON response is expected in case of successful save
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['success' => true, 'message' => 'El producto fue agregado correctamente.'];
                }
                return $this->redirect(['view', 'id' => $model->CS_PEDIDO_ID]);
            }else{
                $producto = \app\models\Producto::findOne(Yii::$app->request->get("NM_PRODUCTO_ID"));
                $model->setAttributes($producto->getAttributes()); 
            }
        }else{
            $producto = \app\models\Producto::findOne(Yii::$app->request->get("NM_PRODUCTO_ID"));
            $model->setAttributes($producto->getAttributes());
        }
        
        if (Yii::$app->request->isAjax) {
            
            return $this->renderAjax('create', [
                        'model' => $model,
                        'producto' => $producto,
                ]);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionProcesar($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post()) && !$model->validate()) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\bootstrap\ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            $model->NM_USUARIO_MODIFICA = Yii::$app->user->getIdentity()->ID_ACTOR;
            if ($model->save()) {
                if (Yii::$app->request->isAjax) {
                    // JSON response is expected in case of successful save
                    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['rpt' => true, 'message' => 'El producto fue actualizado.'];
                }
                return $this->redirect(['view', 'id' => $model->NM_ID_DETALLE_PEDIDO]);
            }else{
                return ['rpt' => false, 'message' => 'Se presentaron algunos errores.', 'errors' => $model->errors];
            }
        } else {  
            $precioLista = \app\models\ListaPreciosCliente::findOne(["ID_CLIENTE" => $model->cSPEDIDO->nMSEDECLIENTE->ID_CLIENTE ,"ID_PRODUCTO" => $model->CS_PRODUCTO_ID]);

            return $this->renderAjax('update', [
                'model' => $model,
                'producto' => $model->cSPRODUCTO,
                'precioLista' => $precioLista
            ]);
        }
    }

    /**
     * Updates an existing PedidoDetalle model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->NM_USUARIO_MODIFICA = Yii::$app->user->getIdentity()->ID_ACTOR;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->NM_ID_DETALLE_PEDIDO]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PedidoDetalle model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return \yii\helpers\Json::encode(["mensaje" => "Produco eliminado"]);
    }

    /**
     * Finds the PedidoDetalle model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PedidoDetalle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PedidoDetalle::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
