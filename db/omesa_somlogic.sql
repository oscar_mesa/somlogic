/*
 Navicat Premium Data Transfer

 Source Server         : TG
 Source Server Type    : MySQL
 Source Server Version : 100141
 Source Host           : 167.71.244.15:3306
 Source Schema         : omesa_somlogic

 Target Server Type    : MySQL
 Target Server Version : 100141
 File Encoding         : 65001

 Date: 07/10/2019 13:14:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for actor
-- ----------------------------
DROP TABLE IF EXISTS `actor`;
CREATE TABLE `actor`  (
  `ID_ACTOR` int(5) NOT NULL AUTO_INCREMENT,
  `NM_DOCUMENTO_ID` bigint(20) NULL DEFAULT NULL,
  `NM_TIPO_DOCUMENTO_ID` int(4) NULL DEFAULT NULL,
  `DS_NOMBRES_ACTOR` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DS_APELLIDOS_ACTOR` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NM_TELEFONO` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NM_CELULAR` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DS_CORREO` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DS_DIRECCION` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DS_CONTRASENA` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NM_TIPO_ACTOR_ID` int(4) NULL DEFAULT NULL,
  `NM_ESTADO_ID` int(4) NOT NULL,
  `DT_FECHA_CREACION` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DF_FECHA_ACTUALIZACION` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `CLAVE_AUTENTICACION` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PAGINA_WEB` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CONTACTO` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CARGO_CONTACTO` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_ACTOR`) USING BTREE,
  UNIQUE INDEX `UQ_DOCUMENTO`(`NM_DOCUMENTO_ID`) USING BTREE,
  INDEX `FK_TIPO_USUARIO`(`NM_TIPO_ACTOR_ID`) USING BTREE,
  INDEX `FK_ESTADO`(`NM_ESTADO_ID`) USING BTREE,
  INDEX `FK_TIPO_DOCUMENTO`(`NM_TIPO_DOCUMENTO_ID`) USING BTREE,
  CONSTRAINT `actor_ibfk_1` FOREIGN KEY (`NM_ESTADO_ID`) REFERENCES `estado_actor` (`CS_ESTADO_ID`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `actor_ibfk_2` FOREIGN KEY (`NM_TIPO_DOCUMENTO_ID`) REFERENCES `tipo_documento` (`CS_TIPO_DOCUMENTO_ID`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `actor_ibfk_3` FOREIGN KEY (`NM_TIPO_ACTOR_ID`) REFERENCES `tipo_actor` (`CS_TIPO_ACTOR`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 67 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of actor
-- ----------------------------
INSERT INTO `actor` VALUES (1, 12453, 2, 'pepito', 'perez', '3234423', '3243532423', 'juan@del.com', 'calle 2334', '$2y$10$cL0IiTQ4AqOljUh6X/I8cuvw/FPJ.SMResdlQOYFwWvMD8hJp5Gg.', 2, 2, '2018-11-06 12:55:22', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (2, 35611553, 1, 'jairo', 'ortiz', '1200000', '8452530125', 'jairo@gmail.com', NULL, '$2y$10$DUbb22PcZE3R0wvwv/SZzO9ef9PIBsg6W8b10YQ5O.OjBAEZnJU2.', 2, 2, '2018-09-06 12:00:14', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (3, 36610553, 1, 'jhon', 'valencia', '7896325', '8965635632', 'jhon@gmail.com', NULL, '$2y$10$6sZZ9FGEIaWYF10ZLeB40eR838NfAPd2fMzvMMgzyMve4yKRiBPWe', 2, 2, '2018-09-06 13:43:35', '2018-11-25 18:11:23', NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (4, 43101104, 1, 'nidia', 'valencia', '3127899', '3127852212', 'oscar_moooesa24092@elpoli.edu.co', '', '$2y$10$Mo3BAL/l6V3aH9UxdupiY.0jRJ1dWrpwaFRAlBG8FSif2VA56xQl.', 2, 2, '2018-08-25 11:15:21', '2019-01-24 21:01:39', NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (5, 95501690, 1, 'juan', 'acevedo', '7896366', '1521521255', 'juan@gmail.com', NULL, '$2y$10$IUQ0n8aZ4WmdByRAiF9HJec4kTxkHP26mXgyar0cBf4RW0xOzXNMq', 2, 2, '2018-09-06 15:03:01', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (6, 123456789, 2, 'ana', 'herrera', '4545632', '2563254896', 'ana@gmail.com', NULL, '$2y$10$nuKTZucQxy7mbMvoQt2TtOwwc9vRNZ8NjmqMIpjSMV1mfdzIOBwom', 2, 2, '2018-09-06 03:50:32', '2019-02-01 17:20:36', NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (7, 323432423, 2, 'pepe', 'juarea', '3242343', '3243242342', 'oscarmesa.elpsssoli@gmail.com', 'calle 123', '$2y$10$KGuITl2u4.UkbatVJ6lxse7/BvWTBPW54X9LNgSzb1nSqs0Ew9sk.', 2, 2, '2018-11-06 12:58:41', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (8, 324343333, 1, 'pepe', 'dfsadf', '2342343', '3232432234', 'ososcar@fasdf.com', NULL, '$2y$10$Gj8Zas7zys..mmXtvvTzMua6eOeg8zjcIIIFNNFGoEUb1Kn0vrVm6', 2, 2, '2018-09-06 13:41:14', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (9, 356115453, 1, 'carlos bab ', 'pepito perez', '3242342', '3243243243', 'jairwwwo@gmail.com', 'calle 13', '$2y$10$70JYvGcKTCb225I0uhl5f..A.u220BJgurFBFS6gonJZcINqT0Nnm', 2, 2, '2018-10-16 14:04:30', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (10, 356115533, 1, 'calos ', 'pedrada', '3242343', '3242342342', 'jairo3333@gmail.com', 'calle 1234', '$2y$10$FUvZ5r4zk1fowrozpLf7WeK5pTrruxbo/wL8vH4wUUbIl2AKbHsA6', 1, 2, '2018-10-17 02:37:57', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (11, 789623778, 1, 'pedro', 'perez', '12369', '789632', 'pedro@gmail.com', NULL, '$2y$10$29aWCUIkJ5mb8KKOevJLXuBiqiL5I8KhdGREpxJDkPOOOswp5eE7C', 2, 2, '2018-09-06 18:15:44', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (12, 11521888632, 1, 'Oscar', 'Mesa', '5804661', '3012280744', 'oscarmesa@gmail.com', '', '$2y$10$3sTTtL17ShNLWimjwm5f6ehtn3YcfJW5BRC3aT0hSWQd4fdfWEFGi', 3, 1, '2018-09-10 19:01:51', '2019-02-03 18:25:17', NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (13, 1152204758, 1, 'santiago', 'betancur|', '6666666', '6666666666', 'poliaulink@gmail.com', '', '$2y$10$32AbUlynEMogC8aG/NEk6OtxD6hlD.QUjkZRWnv.C8ye3zCt87PGG', 2, 1, '2018-08-25 13:59:36', '2019-03-05 09:07:47', NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (14, 2147483647, 1, 'Diego', 'Mejia', '5555555', '5555555555', '555@gmail.com', NULL, '$2y$10$7QNQvUzb7xZmPnmqWOmL5..evrhHRzuBEc4zgchHJyOrjIqA9ZpzG', 2, 2, '2018-09-06 16:14:52', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (15, 5435843958, 3, 'asdfasdf', 'sadfsdfasdf', '2313333', '3432423333', 'oscar@gmailc.om', NULL, '$2y$10$sJqVbpfYBnNs482CdCgJYeRDiRXxI5mjkDP7ZjAyvwVFnPK5hOQK.', 1, 2, '2018-09-07 16:10:45', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (16, 23123213123, 1, 'calitos', 'pepo', '2131232', '1232131233', 'carlos234@gmail.com', 'calle 123', '$2y$10$NNxohVe2CN8H2K3q/YV0/OjiRQjEHcQNWhiI0/h8kR7u3h3DzGx26', 2, 2, '2018-09-25 15:52:06', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (17, 564353453454, 4, 'pepito', 'perez', '2342342', '3242343243', 'pepoooo@gmail.com', 'callle 324324', '$2y$10$8QR9XCFgNWawPScSAxu1Du5TL1GNgeR3ENbbzIFa/ygKTedztVFkC', 2, 2, '2018-10-11 15:03:20', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (18, 111111111111111, 1, 'ana', 'mesa', '2314324', '2342342342', 'anamesa@gmail.com', 'calle 23123', '$2y$10$hjFasakxrStDbXiIxLFWSeH0ztgbEnZFDYeva19JuOQ9kVNEoNclW', 2, 2, '2018-09-25 15:54:48', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (19, 234324234234234, 1, 'pepito', 'asdfasdf', '3242342', '2342342343', 'casd@ksdfasdf.com', 'afsdfasdf', '$2y$10$1VUVzKMgemAJCyJ7Q1gM3urBp2yy5b1R5a2ZdzSHgSa6sVXcJlW3e', 2, 2, '2018-10-11 15:05:04', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (20, 324123412341234, 1, 'carlos', 'mesa', '3242342', '3242234234', 'pepito@gmaill.com', 'calere', '$2y$10$jHabhAQwKyDOeeMzqvYMce9JxPDzGWXmEwMuXHsA5EEQ2Ns5c1gCG', 2, 2, '2018-10-11 14:59:11', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (21, 324234234324234, 1, 'carlos', 'landa', '2343243', '2342343243', 'jaime12321@gmail.com', '', '$2y$10$8VExBHrLnMWQAxRdcCNpZ.napTbLqxh.psPj6ziJshwuMQGe9DcXG', 1, 2, '2018-09-24 20:16:13', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (22, 324333233233333, 2, 'ds', 'werasdfadsf', '3334444', '2333333333', 'pepeepepe@gmail.com', 'calle 234', '$2y$10$LOqFa1B5fFRWVILwfWzy7unsimGuJjwOUSP0YPYbQq1QCvjU3XrvC', 1, 2, '2018-11-05 23:19:03', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (23, 345345435345345, 1, 'sdafasfdasdasdf', 'sadfsdfasdfasdfasdf', '2342342', '3423432423', '234324@fmaol.com', 'cale', '$2y$10$FtlayZDyNC8YFTamZR4A6umMlOgG41BJZBoFS/spKWhIOeVggNogK', 2, 2, '2018-10-11 15:08:17', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (24, 432423432423423, 3, 'ewsadf', 'asdf', '2321312', '1231232342', 'pepe@gmail.com', 'calle 123', '$2y$10$0VWFYnJ4J4N.64HJ4exALej1.VfzNV66PhY3.WfvyEs1AQTlYEmoq', 2, 2, '2018-10-11 14:55:17', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (25, 452345243523452, 1, 'Daniel', 'Mesa', '3232231', '2343243432', 'oscarmesja.elpoli@gmail.com', 'calle 2234', '$2y$10$.XREgbeKAACNeqgLXDjPC.t0CmV7rYpxNsS6SeIHndOYPoLOBN/Me', 3, 1, '2018-10-17 01:56:17', '2019-08-20 04:10:18', NULL, '', '', '');
INSERT INTO `actor` VALUES (27, 877777777777777, 1, 'sdfasdf', 'adfgdgsdfg', '5555555', '5555555555', 'asdfasdf@gmail.com', NULL, '$2y$10$lHEjTLDk4BoEshMS7ebJHuvQDeDCpwRd5E3HXMOdWRR8ycIc3maZW', 2, 2, '2018-09-07 16:44:43', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (28, 996954695469456, 2, 'dfgksdfgksdkfgsdfkgs', 'dskjfskf', '3432423', '4234234234', '4535345@gmail.com', NULL, '$2y$10$f0VW.aYOb2.nMcEkAoZVteW7MYUTWSoJr8tTEcKnFE6Ct2CdPwDZ6', 2, 2, '2018-09-07 16:13:32', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (29, 999999999999999, 1, 'Diego', 'Mejia', '5555555', '5555555555', '555@gmail.com', 'calle 1234', '$2y$10$hxmzv4AJN4G7pJQntHvEdOtIqRwj0/JIoFRaXjZY5I6HO5K27az7W', 2, 2, '2018-09-07 16:35:21', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (39, 1152188863, 1, 'Oscar', 'Mesa', '34234', '324324', 'oscarmesa.elpoli@gmail.com', 'carrera 95 # 48D - 58', '$2y$13$8F6vf3Pq4.7J6qZ//criOeDFlNIxXTls1tLxMsBHpU0.crdavLhiS', 1, 1, '2018-11-13 00:00:00', '2019-08-25 13:02:34', 'ctoH5aV3gwQrWmkSTBGQIgiQspj2pwMQhEUZV1TdqKPJxjaY6JXAW6WpOkpJ', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (48, 898989898, 1, 'Oscar', 'Mesa', NULL, 'kjjkkjkj', 'deosmega@gmail.com', '', 'yqaXFrQHELBc', 2, 1, '2018-11-13 17:25:55', '2019-01-15 05:23:41', 'Uv3FIQsL8wn3gMuBdFN1lzz0DUY9dmUnojVDcKAtEeMN95ZPGKr5L3wU3wgB', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (50, 431011042, 1, 'pepe ', 'grillo', '21474', '32323', 'jaime121@gmail.com', 'calle 123', '$2y$13$JQcmgGcqvAJXDnmXITZXTeQmiETA8kDOnuwnrPx6cjYNF5J9YrpJS', 3, 2, '2018-11-25 18:52:57', '2019-05-21 06:57:28', 'P92QE7yeaFXthQG8VB72WliBy1DUeNL1L6yiWtbKIxZyffT-1uZppmcizNPN', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (51, 435322452345, 1, 'Jorge Andres', 'Sepulveda Arredondo', '2147483647', '94595349539459349593', 'jj.asepulveda@gmail.com', '', '$2y$13$GziuuumVgQPW3iD.Z77//ecyeegKyn6YMnZzRJYgtUTETqitLfo2i', 3, 1, '2019-01-04 19:13:08', '2019-01-04 19:50:34', 'OWud1mxwDA67qvqikvPpIcCB-eDYFr8s9J89xEqSLVJNQBamflcyTI5vpEsF', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (52, 21223, 1, 'Camilo', 'Gaviria', '2147483647', '3249234234', 'carlosga@gmial.com', 'cra 95 # 48D 58', '$2y$13$Ja.0KHN0g9Gz2NVB.hTXTO8G.wTFo7XARW3AWxcPrkHkmcbTQHiO6', 4, 1, '2019-01-15 00:00:00', '2019-08-13 19:10:57', 'o7XSRWIt6AF6twz6ERRWgFTf00KI8k_ZoSn5cuWPeSr-njOMKdbd21RSPj3s', '', '', '');
INSERT INTO `actor` VALUES (53, 234123545, 1, 'oscar', 'mesa', '2147483647', '4444', 'oscarmhhhesa.elpoli@gmail.com', 'cra 95 # 48D 58', '$2y$13$4CAWGl.oNa.4sKrtFpBCVu1Cndupv1kD.bodlQigl83c/7eMQPChu', 2, 1, '2019-01-15 00:00:00', '2019-01-15 12:28:37', 'tGluXwaKtnzDFdTykd620mFeUIbe_C58YEYoYttfliKx23jEm5N7gzoHAwbF', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (54, 1152188864, 1, 'Calificador', 'Politécnico', '2147483647', '555555', 'oscarmesa.elpoli@gamil.com', 'cra 95 # 48D 58', '$2y$13$k036P/6NqFpnenBB.LGsquZRety8bev.ZRaofCSIoxC0e35v7xiES', 1, 1, '2019-01-15 00:00:00', '2019-02-19 14:57:43', 'y7P4h6jsKFt2Y418IP2A58ipApQ61lxfvLelQwGRf5ahPo28hrmKl1DiZbwi', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (55, 9999, 4, 'dfasdfaksdf', 'sadfkasdf', '23312', 'asdfasdf', 'asdfasd2f@gmail.com', 'sadfasdf', '$2y$13$QIfuMVqMVwSh4gNYYaJRtuhhVWN45C5t9njGkycsVeNhwTFw1Sok2', 3, 2, '2019-02-01 17:19:48', '2019-02-01 17:20:07', '5I8KrhHr2i9tetWb46uBfW1v-V1A8e0n3vq5TIOeKgivnXppkgEXg34A108s', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (56, 2334324123, 3, 'DANIEL', 'PEREZ', '34234234', 'ddsf32432432', 'oscarmesa.elpoli@gm2ail.com', 'asdfasdf', '$2y$13$ouQu8JttvjJ4hEnB2NzoiOh7UxzBSu7vrL3B2IfSPduc3UfmBETYi', 8, 1, '2019-02-01 18:15:59', '2019-08-13 17:34:32', '7RFvZwQ64lTy433Vz4X7BZsYx6EwXJveRKnCMMDCAjCNlkC3ASlV1oeOaPn3', '', '', '');
INSERT INTO `actor` VALUES (57, 2132132, 4, 'asdfasdf', 'asdfasdf', 'asd21', '32324', 'sdfklasdjf@coorreo.com', 'adfkasdf', '$2y$13$Jt0SqEMwXmPWUdMXcR7HX.HkN7IFSDjGzfF5BUkcQN2MaBEijw7.S', 3, 1, '2019-02-03 18:17:47', NULL, 'fbC4_4PrBiJIbGBdZRBw9_3Nj4w6XvJ7fsftSAA0Ub0lk9vyz6-gMqEii1E0', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (58, 8767876, 1, 'pepe', 'landa', '243234', '234234324', 'oscarmesa.elpoli@gmdddail.com', 'asdaf', '$2y$13$rI92VHKQ.M0IBdL89bX8IOWCVYrYyUjQCedVzj/Sppda9GCWkWOce', 3, 1, '2019-02-13 17:18:56', NULL, 'vhI-ZRHUWZ0ePJ8EyR8ubaVCjR7vsMbo2ZWcI80Yxnl-JZuKBG1GxAq44CWT', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (59, 9837438383, 1, 'Laura', 'Ospina Garcia', '93838', '324234234234', 'laura_ospina82122@elpoli.edu.co', '', '$2y$13$LVM9K0WVYEfvqezIMihHrOfYYEoCD3oo/dCzVxjmFCK3csjdsVz6W', 1, 1, '2019-03-05 09:07:19', '2019-03-05 09:54:37', 'OSdNstw9bmKrCJct-nySD9kNEfPCxPcZi9yJ_e2hY4A9G7uFiq56L2dTTIi7', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (60, 342123444, 5, 'MAKRO', '', '39999333', '234324', 'makro@gmail.com', '', '$2y$13$i4Wahn4Bp.cMRT3zlJYHeeT3Et/swvoQG1bFlmXNZhgxrC47ueeNu', 11, 1, '2019-08-12 14:20:08', '2019-08-13 17:05:30', 'fHWni3T8ueFVgkHItTVzmtXNRXQLU5wslLgynpuKS1TRX0Nsn9JAuycdhXk5', 'http://www.ggol.com', 'carlos andrade', 'saludo');
INSERT INTO `actor` VALUES (61, 999999, 1, 'Isabel Cristina', 'Arango Palacio ', '', '310 8955195', 'isabelarango@elpoli.edu.co', '', '$2y$13$kiG7baVtXo60rfSY.wrUFeyfRB4qvTw/UGA4C6cGgKUAX.EkahXr2', 1, 1, '2019-08-22 02:35:44', NULL, 'MqErWMMuG8x_PXS13eWTC05wtN-i40kCKriqo5dPmYBEyazmpliJoaxf7HRD', '', '', '');
INSERT INTO `actor` VALUES (62, 111111, 5, 'Grupo Exito ', '', '2222222', '6666666', 'grupoexito@exito.com', 'asdfasdf', '$2y$13$rzOMRrajxLVna/ZjC1fIXOCncQUUUVtROaooCTXEq3K87NyGj4aXu', 11, 1, '2019-08-26 16:08:04', '2019-08-26 16:09:54', 'V7_zKwLedRXH8f8ZbJTcWMfcrRbhfFvsZiFhgNhG2kCzaz9KZ3PAs3r_qef2', 'http://www.exito.com', 'dfasdf', 'asdfasdf');
INSERT INTO `actor` VALUES (63, 55555, 1, 'JIAME S.A.S', '', '3224344', '234', 'jaimesas.e@gmail.com', 'calle 2234', '$2y$13$wF7VpU20yQII7.7v9FDxCufMpf7IMLGvZbzT3M6u7LW4jlksoAUsK', 4, 1, '2019-08-28 01:43:34', NULL, 'c0AW3U7K0LE1YF47qEwPw1gwepY9-3UNQKXcOmftKKCE7k8yeLxeA1FVO6sA', '', '', '');
INSERT INTO `actor` VALUES (64, NULL, NULL, 'Joseph', 'Ortiz Laverde', NULL, NULL, 'joseph_ortiz23103@elpoli.edu.co', NULL, 'eJ0qwbZSF3Na', 12, 1, '2019-08-28 16:11:15', NULL, 'E5obyXq_VM2RfjI7u2VKBjzPLWN4t04ahVfs89iwP9a8GXB8kXso6F0D2pOB', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (65, 45645758, 1, 'Isabel Cristina', 'Palacio', '456456', '456456', 'isa@asdf.com', 'asdfasdf', '$2y$13$AJr3TjMjmA/ibAiRPVnzAuxdnJXDik8e/KQKF8RhXTmjPSB1uazUa', 9, 1, '2019-09-27 19:34:15', '2019-09-27 19:35:47', 'ARGUwmroSjxDYuERLpnkVMlZ38DMa7AG9wqRRYSLZ12cOtR1bof9c4nTDeo_', 'http://www.asdfasdf.com', '', '');
INSERT INTO `actor` VALUES (66, 45242, 1, 'José', 'Palacio', '45645', '545245', 'isa@asdfe.com', 'asdfasdfasdf', '$2y$13$2q2V7FSAAZzxnwdMWkERIe0uCpryTjpcyT.8yVqjPfwRz.Q8g1hgS', 7, 1, '2019-09-27 20:04:09', NULL, 'X9N5jE67rr-83KdMB_-Fh_DQXhP9nmeH6VHTvRqdagKB445745vj4sYEFCQ1', 'http://www.asdfasdf.com', 'Jose', 'Conductor');

-- ----------------------------
-- Table structure for area
-- ----------------------------
DROP TABLE IF EXISTS `area`;
CREATE TABLE `area`  (
  `ID_AREA` int(5) NOT NULL AUTO_INCREMENT,
  `DS_AREA` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_AREA`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of area
-- ----------------------------
INSERT INTO `area` VALUES (1, 'Almacenamiento');
INSERT INTO `area` VALUES (2, 'Compras');
INSERT INTO `area` VALUES (3, 'Distribución');
INSERT INTO `area` VALUES (4, 'Planeación');

-- ----------------------------
-- Table structure for autenticacion
-- ----------------------------
DROP TABLE IF EXISTS `autenticacion`;
CREATE TABLE `autenticacion`  (
  `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ID_ACTOR` int(5) NOT NULL,
  `ORIGEN` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ID_ORIGEN` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `FK_AUTENTICACION_USUARIO_ID1`(`ID_ACTOR`) USING BTREE,
  CONSTRAINT `autenticacion_ibfk_1` FOREIGN KEY (`ID_ACTOR`) REFERENCES `actor` (`ID_ACTOR`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of autenticacion
-- ----------------------------
INSERT INTO `autenticacion` VALUES (8, 39, 'google', '102463048375185432372');
INSERT INTO `autenticacion` VALUES (9, 48, 'facebookb', '10218031264559067');
INSERT INTO `autenticacion` VALUES (11, 51, 'google', '106996163223176927221');
INSERT INTO `autenticacion` VALUES (12, 59, 'google', '111399809301416643925');
INSERT INTO `autenticacion` VALUES (13, 64, 'google', '105299741119989169740');
INSERT INTO `autenticacion` VALUES (14, 61, 'google', '103157348548898685555');

-- ----------------------------
-- Table structure for auth_assignment
-- ----------------------------
DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment`  (
  `item_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`item_name`, `user_id`) USING BTREE,
  INDEX `idx-auth_assignment-user_id`(`user_id`) USING BTREE,
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of auth_assignment
-- ----------------------------
INSERT INTO `auth_assignment` VALUES ('Administrador', '1', 1566369040);
INSERT INTO `auth_assignment` VALUES ('Administrador', '39', 1566369059);
INSERT INTO `auth_assignment` VALUES ('Administrador', '61', 1566834340);
INSERT INTO `auth_assignment` VALUES ('Administrador', '64', 1567008716);
INSERT INTO `auth_assignment` VALUES ('Proveedor', '66', 1569622666);

-- ----------------------------
-- Table structure for auth_item
-- ----------------------------
DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item`  (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `rule_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `data` blob NULL,
  `created_at` int(11) NULL DEFAULT NULL,
  `updated_at` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE,
  INDEX `rule_name`(`rule_name`) USING BTREE,
  INDEX `idx-auth_item-type`(`type`) USING BTREE,
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of auth_item
-- ----------------------------
INSERT INTO `auth_item` VALUES ('/*', 2, NULL, NULL, NULL, 1566368990, 1566368990);
INSERT INTO `auth_item` VALUES ('Administrador', 1, 'Administrador', NULL, NULL, 1566368920, 1566368920);
INSERT INTO `auth_item` VALUES ('Proveedor', 1, 'Empresa Lolita', NULL, NULL, 1569622590, 1569622590);

-- ----------------------------
-- Table structure for auth_item_child
-- ----------------------------
DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child`  (
  `parent` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`, `child`) USING BTREE,
  INDEX `child`(`child`) USING BTREE,
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of auth_item_child
-- ----------------------------
INSERT INTO `auth_item_child` VALUES ('Administrador', '/*');
INSERT INTO `auth_item_child` VALUES ('Proveedor', '/*');

-- ----------------------------
-- Table structure for auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule`  (
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data` blob NULL,
  `created_at` int(11) NULL DEFAULT NULL,
  `updated_at` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cadena_etapa
-- ----------------------------
DROP TABLE IF EXISTS `cadena_etapa`;
CREATE TABLE `cadena_etapa`  (
  `ID_ETP_CAD` int(5) NOT NULL AUTO_INCREMENT,
  `ID_ETAPA` int(5) NULL DEFAULT NULL,
  `ID_CADENA` int(5) NULL DEFAULT NULL,
  `ID_ACTOR` int(5) NULL DEFAULT NULL,
  `CUMPLIMIENTO_PROMEDIO` double NULL DEFAULT 0,
  `NM_PORCENTAJE_SIGNIFICANCIA` double NULL DEFAULT NULL,
  PRIMARY KEY (`ID_ETP_CAD`) USING BTREE,
  INDEX `FK_CADENA_CADENA`(`ID_CADENA`) USING BTREE,
  INDEX `FK_CADENA_ETAPA`(`ID_ETAPA`) USING BTREE,
  INDEX `FK_CADENA_ACTOR`(`ID_ACTOR`) USING BTREE,
  CONSTRAINT `FK_CADENA_ACTOR` FOREIGN KEY (`ID_ACTOR`) REFERENCES `actor` (`ID_ACTOR`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_CADENA_CADENA` FOREIGN KEY (`ID_CADENA`) REFERENCES `cadenadeabastecimiento` (`NM_CADENA_ID`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_CADENA_ETAPA` FOREIGN KEY (`ID_ETAPA`) REFERENCES `etapa` (`ID_ETAPA`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cadena_etapa
-- ----------------------------
INSERT INTO `cadena_etapa` VALUES (1, 1, 13, 25, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (2, 2, 13, 52, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (3, 3, 13, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (4, 4, 13, 60, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (5, 5, 13, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (6, 6, 13, 51, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (7, 1, 14, 48, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (8, 2, 14, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (9, 3, 14, 52, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (10, 4, 14, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (11, 5, 14, 51, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (12, 1, 16, 52, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (13, 2, 16, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (14, 1, 15, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (15, 3, 16, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (16, 4, 16, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (17, 5, 16, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (18, 6, 16, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (19, 1, 17, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (20, 2, 17, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (21, 3, 17, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (22, 4, 17, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (23, 5, 17, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (24, 6, 17, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (25, 6, 15, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (26, 4, 15, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (27, 3, 15, 52, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (28, 2, 15, 63, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (29, 1, 18, 48, 0.37254901960784, 5);
INSERT INTO `cadena_etapa` VALUES (30, 6, 18, NULL, 0, 20);
INSERT INTO `cadena_etapa` VALUES (31, 2, 18, NULL, 0.53333333333333, 15);
INSERT INTO `cadena_etapa` VALUES (32, 4, 18, NULL, 0, 30);
INSERT INTO `cadena_etapa` VALUES (33, 3, 18, NULL, 0, 20);
INSERT INTO `cadena_etapa` VALUES (34, 5, 18, 52, 0.825, 10);
INSERT INTO `cadena_etapa` VALUES (35, 2, 1, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (36, 1, 1, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (37, 1, 19, 63, 0.8, 20);
INSERT INTO `cadena_etapa` VALUES (38, 2, 19, NULL, 0.61904761904762, 70);
INSERT INTO `cadena_etapa` VALUES (39, 3, 19, NULL, 1.2254901960784, 10);
INSERT INTO `cadena_etapa` VALUES (40, 4, 19, NULL, 0, 0);
INSERT INTO `cadena_etapa` VALUES (41, 5, 19, NULL, 0, 0);
INSERT INTO `cadena_etapa` VALUES (42, 6, 19, NULL, 0, 0);
INSERT INTO `cadena_etapa` VALUES (43, 1, 20, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (44, 2, 20, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (45, 3, 20, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (46, 4, 20, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (47, 5, 20, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (48, 6, 20, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (49, 1, 21, 52, 0.175, 33);
INSERT INTO `cadena_etapa` VALUES (50, 2, 21, 65, 0.9375, 33);
INSERT INTO `cadena_etapa` VALUES (51, 3, 21, 66, 1.202380952381, 34);
INSERT INTO `cadena_etapa` VALUES (52, 4, 21, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (53, 5, 21, NULL, 0, NULL);
INSERT INTO `cadena_etapa` VALUES (54, 6, 21, NULL, 0, NULL);

-- ----------------------------
-- Table structure for cadena_indicador
-- ----------------------------
DROP TABLE IF EXISTS `cadena_indicador`;
CREATE TABLE `cadena_indicador`  (
  `ID_INDICADOR_CADENA` int(5) NOT NULL AUTO_INCREMENT,
  `ID_INDICADOR` int(5) NULL DEFAULT NULL,
  `ID_CADENA` int(5) NULL DEFAULT NULL,
  `NM_RESULTADO_INDICADOR` double NULL DEFAULT NULL,
  `NM_PORCENTAJE_SIGNIFICANCIA` int(3) NULL DEFAULT NULL,
  `ID_PEDIDO` int(5) NULL DEFAULT NULL,
  `META` double NULL DEFAULT NULL,
  `FECHA_CREACION` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `USUARIO_CREACION` int(5) NULL DEFAULT NULL,
  `FECHA_ACTUALIZACION` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `USUARIO_ACTUALIZACION` int(5) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_INDICADOR_CADENA`) USING BTREE,
  INDEX `FK_INDICADOR_CADENA_1`(`ID_INDICADOR`) USING BTREE,
  INDEX `FK_INDICADOR_CADENA_2`(`ID_CADENA`) USING BTREE,
  INDEX `FK_INDICADOR_CADENA_3`(`ID_PEDIDO`) USING BTREE,
  INDEX `FK_INDICADOR_CADENA_4`(`USUARIO_CREACION`) USING BTREE,
  INDEX `FK_INDICADOR_CADENA_5`(`USUARIO_ACTUALIZACION`) USING BTREE,
  CONSTRAINT `FK_INDICADOR_CADENA_1` FOREIGN KEY (`ID_INDICADOR`) REFERENCES `indicador` (`ID_INDICADOR`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_INDICADOR_CADENA_2` FOREIGN KEY (`ID_CADENA`) REFERENCES `cadena_etapa` (`ID_ETP_CAD`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_INDICADOR_CADENA_3` FOREIGN KEY (`ID_PEDIDO`) REFERENCES `pedido` (`CS_PEDIDO_ID`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_INDICADOR_CADENA_4` FOREIGN KEY (`USUARIO_CREACION`) REFERENCES `actor` (`ID_ACTOR`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_INDICADOR_CADENA_5` FOREIGN KEY (`USUARIO_ACTUALIZACION`) REFERENCES `actor` (`ID_ACTOR`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 73 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cadena_indicador
-- ----------------------------
INSERT INTO `cadena_indicador` VALUES (8, 1, 1, 0.66666666666667, 20, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (9, 1, 1, 1.25, 45, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (11, 20, 2, NULL, NULL, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (12, 9, 3, NULL, NULL, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (13, 10, 3, NULL, NULL, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (14, 11, 3, NULL, NULL, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (15, 2, 5, NULL, NULL, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (16, 27, 5, NULL, NULL, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (17, 1, 7, 2.6666666666667, 10, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (18, 3, 7, 0.32584269662921, 20, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (20, 25, 11, NULL, NULL, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (21, 26, 11, NULL, NULL, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (22, 2, 7, NULL, NULL, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (23, 3, 7, NULL, NULL, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (24, 1, 9, NULL, NULL, NULL, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (25, 1, 12, 0.75, 10, 12, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (26, 3, 12, 1.4, 2, 2, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (27, 5, 12, 4, 2, 2, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (28, 1, 13, 0.01, 5, 2, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (29, 12, 15, 250, 10, 2, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (30, 1, 16, 0.5, 10, 2, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (31, 2, 16, 10000000, 10, 2, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (32, 12, 17, 500, 10, 2, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (33, 13, 17, 5000, 10, 2, NULL, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (34, 15, 19, 0.71875, 3, 15, 10000, '2019-08-28 02:09:57', NULL, '2019-08-28 08:46:03', 39);
INSERT INTO `cadena_indicador` VALUES (35, 16, 19, 0.66666666666667, 10, 15, 100, '2019-08-28 02:09:57', NULL, '2019-08-28 08:46:16', 39);
INSERT INTO `cadena_indicador` VALUES (36, 7, 20, 0.66666666666667, 10, 15, 100, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (37, 12, 21, 200, 10, 15, 200, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (38, 13, 21, 520000, 20, 15, 450000, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (39, 2, 22, 180000, 10, 15, 17000, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (40, 6, 23, 50, 10, 15, 50, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (41, 1, 24, 0.66666666666667, 10, 15, 100, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (42, 27, 27, 2, 10, 5, 3, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (43, 5, 27, 20, 90, 10, 20, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (44, 22, 24, 0.4, 0, 15, 1, '2019-08-28 02:09:57', NULL, NULL, NULL);
INSERT INTO `cadena_indicador` VALUES (45, 1, 28, 0.66666666666667, 100, 5, 0.8, '2019-08-28 05:50:06', NULL, '2019-08-28 05:51:41', NULL);
INSERT INTO `cadena_indicador` VALUES (46, 1, 29, 0.33333333333333, 10, 15, 100, '2019-08-28 10:54:29', 39, '2019-09-20 13:39:06', 61);
INSERT INTO `cadena_indicador` VALUES (48, 3, 29, 0.11764705882353, 50, 15, 100, '2019-08-28 13:32:14', 39, '2019-09-20 13:39:06', 61);
INSERT INTO `cadena_indicador` VALUES (49, 21, 29, 0.66666666666667, 4, 15, 100, '2019-09-10 21:00:25', 61, '2019-09-20 13:39:06', 61);
INSERT INTO `cadena_indicador` VALUES (52, 15, 31, 0.66666666666667, 6, 15, 100, '2019-09-18 23:59:24', 61, '2019-09-20 13:39:06', 61);
INSERT INTO `cadena_indicador` VALUES (53, 16, 31, 0.4, 30, 15, 0, '2019-09-18 23:59:24', 61, '2019-09-20 13:39:06', 61);
INSERT INTO `cadena_indicador` VALUES (57, 22, 37, 0.8, 20, 13, 100, '2019-09-20 13:26:27', 61, '2019-09-20 18:53:18', 61);
INSERT INTO `cadena_indicador` VALUES (59, 1, 38, 0.66666666666667, 50, 13, 100, '2019-09-20 13:35:23', 61, '2019-09-20 18:53:18', 61);
INSERT INTO `cadena_indicador` VALUES (60, 14, 38, 2000, 10, 13, 3500, '2019-09-20 13:36:05', 61, '2019-09-20 18:53:18', 61);
INSERT INTO `cadena_indicador` VALUES (61, 12, 39, 980392.15686275, 20, 13, 800000, '2019-09-20 18:51:42', 61, '2019-09-20 19:07:09', 61);
INSERT INTO `cadena_indicador` VALUES (62, 2, 34, 247.5, 0, NULL, 300, '2019-09-20 20:42:33', 61, '2019-09-20 20:43:13', 61);
INSERT INTO `cadena_indicador` VALUES (63, 6, 49, 0.6, 0, 13, 4, '2019-09-27 19:28:25', 61, '2019-09-27 19:51:56', 39);
INSERT INTO `cadena_indicador` VALUES (64, 16, 50, 0.66666666666667, 0, 13, 100, '2019-09-27 19:36:28', 61, '2019-09-27 20:00:50', 61);
INSERT INTO `cadena_indicador` VALUES (65, 18, 50, 0.83333333333333, 0, 13, 100, '2019-09-27 19:36:28', 61, '2019-09-27 19:57:00', 61);
INSERT INTO `cadena_indicador` VALUES (66, 6, 49, 0.8, 0, 14, 4, '2019-09-27 19:53:53', 61, '2019-09-27 19:54:10', 61);
INSERT INTO `cadena_indicador` VALUES (67, 16, 50, 1, 0, 14, 100, '2019-09-27 19:55:53', 61, '2019-09-27 20:01:06', 61);
INSERT INTO `cadena_indicador` VALUES (68, 18, 50, 1.25, 0, 14, 100, '2019-09-27 19:55:53', 61, '2019-09-27 19:57:10', 61);
INSERT INTO `cadena_indicador` VALUES (69, 12, 51, 37.5, 0, 13, 35, '2019-09-27 19:58:10', 61, '2019-09-27 19:58:59', 61);
INSERT INTO `cadena_indicador` VALUES (70, 12, 51, 46.666666666667, 0, 14, 35, '2019-09-27 19:59:20', 61, '2019-09-27 19:59:42', 61);

-- ----------------------------
-- Table structure for cadenadeabastecimiento
-- ----------------------------
DROP TABLE IF EXISTS `cadenadeabastecimiento`;
CREATE TABLE `cadenadeabastecimiento`  (
  `NM_CADENA_ID` int(5) NOT NULL AUTO_INCREMENT,
  `ID_EMPRESA` int(5) NULL DEFAULT NULL,
  `DS_CADENA_ABASTECIMIENTO` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FECHA_PEDIDO` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `FECHA_FIN` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `FECHA_CREACION` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`NM_CADENA_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cadenadeabastecimiento
-- ----------------------------
INSERT INTO `cadenadeabastecimiento` VALUES (1, 2, '998784', '2019-03-13 09:03:00', '2019-03-07 10:03:00', '2019-08-17 12:16:20');
INSERT INTO `cadenadeabastecimiento` VALUES (2, 2, '998784', '2019-03-13 09:03:00', '2019-03-07 10:03:00', '2019-08-17 12:16:20');
INSERT INTO `cadenadeabastecimiento` VALUES (3, 10, '4534534', '2019-03-06 20:03:00', '2019-03-14 10:03:00', '2019-08-17 12:16:20');
INSERT INTO `cadenadeabastecimiento` VALUES (4, 10, '998784', '2019-03-05 05:03:00', '2019-03-16 15:03:00', '2019-08-17 12:16:20');
INSERT INTO `cadenadeabastecimiento` VALUES (5, 60, 'prueba', '2019-08-01 13:08:00', '2019-08-23 21:08:00', '2019-08-17 12:16:20');
INSERT INTO `cadenadeabastecimiento` VALUES (6, 60, '', '2019-08-01 10:08:00', '2019-08-08 22:08:00', '2019-08-17 12:16:20');
INSERT INTO `cadenadeabastecimiento` VALUES (7, 60, '', '2019-08-01 10:08:00', '2019-08-08 22:08:00', '2019-08-17 12:16:20');
INSERT INTO `cadenadeabastecimiento` VALUES (8, 60, '', '2019-08-14 02:08:00', '2019-08-31 02:08:00', '2019-08-17 12:16:20');
INSERT INTO `cadenadeabastecimiento` VALUES (9, 60, '', '2019-08-14 02:08:00', '2019-08-31 02:08:00', '2019-08-17 12:16:20');
INSERT INTO `cadenadeabastecimiento` VALUES (10, 60, '', '2019-08-14 02:08:00', '2019-08-31 02:08:00', '2019-08-17 12:16:20');
INSERT INTO `cadenadeabastecimiento` VALUES (11, 60, 'esta es una preuba', '2019-08-01 02:08:00', '2019-08-29 02:08:00', '2019-08-17 12:16:20');
INSERT INTO `cadenadeabastecimiento` VALUES (12, 60, 'Esta es una prueba!!!!!', '2019-08-07 12:08:00', '2019-08-29 13:08:00', '2019-08-17 12:37:10');
INSERT INTO `cadenadeabastecimiento` VALUES (13, 60, 'prueba tales tales', '2019-08-17 17:18:32', '2019-08-17 17:18:32', '2019-08-17 16:41:27');
INSERT INTO `cadenadeabastecimiento` VALUES (14, 60, 'Prueba', '2019-08-01 04:08:00', '2019-08-31 22:08:00', '2019-08-20 04:31:09');
INSERT INTO `cadenadeabastecimiento` VALUES (15, 60, '', '2019-08-01 18:08:00', '2019-09-07 05:09:00', '2019-08-20 05:04:50');
INSERT INTO `cadenadeabastecimiento` VALUES (16, 60, 'pruebba', '2019-08-20 13:08:00', '2019-09-07 17:09:00', '2019-08-20 16:46:22');
INSERT INTO `cadenadeabastecimiento` VALUES (17, 62, 'pedido para prueba', '2019-08-26 09:08:00', '2019-09-06 11:09:00', '2019-08-26 11:22:34');
INSERT INTO `cadenadeabastecimiento` VALUES (18, 62, 'Prueba otra cadena de suministro', '2019-08-07 18:08:00', '2019-08-01 19:08:00', '2019-08-28 05:21:44');
INSERT INTO `cadenadeabastecimiento` VALUES (19, 60, 'prueba', '2019-09-20 08:09:00', '2019-10-04 18:10:00', '2019-09-20 08:23:25');
INSERT INTO `cadenadeabastecimiento` VALUES (20, 60, 'pruebab', '2019-09-03 09:09:00', '2019-10-01 13:10:00', '2019-09-24 05:15:35');
INSERT INTO `cadenadeabastecimiento` VALUES (21, 60, 'Cadena de distribución de PT de las tiendas Makro de Medellín', '2019-09-27 14:09:00', '2019-10-04 14:10:00', '2019-09-27 14:27:39');

-- ----------------------------
-- Table structure for campo_caculo
-- ----------------------------
DROP TABLE IF EXISTS `campo_caculo`;
CREATE TABLE `campo_caculo`  (
  `ID_CAMPO` int(5) NOT NULL AUTO_INCREMENT,
  `DS_CAMPO_CALCULO` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_CAMPO`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 52 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of campo_caculo
-- ----------------------------
INSERT INTO `campo_caculo` VALUES (1, 'suma de tiempo en ser transportado un material a la bodega o un producto terminado al lugar de consumo');
INSERT INTO `campo_caculo` VALUES (2, 'mide el costo de transportar la mercancia');
INSERT INTO `campo_caculo` VALUES (3, 'cantidad de pedidos transportados');
INSERT INTO `campo_caculo` VALUES (4, 'costo de material de empaque por pedido');
INSERT INTO `campo_caculo` VALUES (5, 'Material recuperado');
INSERT INTO `campo_caculo` VALUES (6, 'total de Material usado');
INSERT INTO `campo_caculo` VALUES (7, 'costo del material de empaque recuperado');
INSERT INTO `campo_caculo` VALUES (8, 'Unidades entregadas por proceso');
INSERT INTO `campo_caculo` VALUES (9, 'Unidades planeadas por proceso');
INSERT INTO `campo_caculo` VALUES (10, 'recursos usados');
INSERT INTO `campo_caculo` VALUES (11, 'total recursos planeados por proceso');
INSERT INTO `campo_caculo` VALUES (12, 'Costo real por proceso');
INSERT INTO `campo_caculo` VALUES (13, 'Costo planeado por proceso');
INSERT INTO `campo_caculo` VALUES (14, 'unidades entregadas');
INSERT INTO `campo_caculo` VALUES (15, 'unidades pronosticadas');
INSERT INTO `campo_caculo` VALUES (16, 'Unidades vendidas a tiempo y completas');
INSERT INTO `campo_caculo` VALUES (17, 'Unidades pedidas ');
INSERT INTO `campo_caculo` VALUES (18, ' Unidades transportadas a tiempo y completas');
INSERT INTO `campo_caculo` VALUES (19, 'unidades pedidas ');
INSERT INTO `campo_caculo` VALUES (20, 'SUMA de los Requerimientos con Respuesta Efectiva');
INSERT INTO `campo_caculo` VALUES (21, '100% de las solicitudes');
INSERT INTO `campo_caculo` VALUES (22, 'No unidades almacenadas');
INSERT INTO `campo_caculo` VALUES (23, 'No días en el almacen');
INSERT INTO `campo_caculo` VALUES (24, 'costo diario de almacenamiento');
INSERT INTO `campo_caculo` VALUES (25, 'total unidades');
INSERT INTO `campo_caculo` VALUES (26, 'suma de producto oMP averiado');
INSERT INTO `campo_caculo` VALUES (27, 'total de inventario disponible');
INSERT INTO `campo_caculo` VALUES (28, 'Venta del Periodo');
INSERT INTO `campo_caculo` VALUES (29, 'Stock Promedio');
INSERT INTO `campo_caculo` VALUES (30, 'Tiempo desde que el pedido es realizado, hasta que el pedido es cargado en el transporte asignado');
INSERT INTO `campo_caculo` VALUES (31, 'Tiempo en que tarda un carro en ser cargado');
INSERT INTO `campo_caculo` VALUES (32, 'Total pedidos');
INSERT INTO `campo_caculo` VALUES (33, 'Compras realizadas');
INSERT INTO `campo_caculo` VALUES (34, 'compras solicitadas');
INSERT INTO `campo_caculo` VALUES (35, 'inventario solicitado');
INSERT INTO `campo_caculo` VALUES (36, 'inventario despachado');
INSERT INTO `campo_caculo` VALUES (37, 'Compras perfectas de proveedores certificados');
INSERT INTO `campo_caculo` VALUES (38, 'total de compras');
INSERT INTO `campo_caculo` VALUES (39, 'tiempo promedio de reposición de las compras');
INSERT INTO `campo_caculo` VALUES (40, 'Unidades despachadas a tiempo y completas');
INSERT INTO `campo_caculo` VALUES (41, 'unidades pedidas ');
INSERT INTO `campo_caculo` VALUES (42, 'Número de requerimientos con agotados de Inventario');
INSERT INTO `campo_caculo` VALUES (43, 'total de los requerimientos de esos productos');
INSERT INTO `campo_caculo` VALUES (44, 'Ingreso Neto');
INSERT INTO `campo_caculo` VALUES (45, 'Capital de los accionistas Promedio');
INSERT INTO `campo_caculo` VALUES (46, 'Costos de los productos vendidos');
INSERT INTO `campo_caculo` VALUES (47, 'Cuentas por pagar');
INSERT INTO `campo_caculo` VALUES (48, 'Ingresos por Ventas');
INSERT INTO `campo_caculo` VALUES (49, 'Cuentas por Cobrar');
INSERT INTO `campo_caculo` VALUES (50, 'Ingresos por Ventas');
INSERT INTO `campo_caculo` VALUES (51, 'PPET');

-- ----------------------------
-- Table structure for campo_cadena_indicador
-- ----------------------------
DROP TABLE IF EXISTS `campo_cadena_indicador`;
CREATE TABLE `campo_cadena_indicador`  (
  `ID_INDICADOR_VALOR` int(5) NOT NULL AUTO_INCREMENT,
  `ID_CAMPO` int(5) NULL DEFAULT NULL,
  `ID_CADENA_INDICADOR` int(5) NULL DEFAULT NULL,
  `VALOR` double NULL DEFAULT NULL,
  PRIMARY KEY (`ID_INDICADOR_VALOR`) USING BTREE,
  INDEX `FK_CADENA_INDICADOR_CAMPO_1`(`ID_CAMPO`) USING BTREE,
  INDEX `FK_CADENA_INDICADOR_CAMPO_2`(`ID_CADENA_INDICADOR`) USING BTREE,
  CONSTRAINT `FK_CADENA_INDICADOR_CAMPO_1` FOREIGN KEY (`ID_CAMPO`) REFERENCES `campo_caculo` (`ID_CAMPO`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_CADENA_INDICADOR_CAMPO_2` FOREIGN KEY (`ID_CADENA_INDICADOR`) REFERENCES `cadena_indicador` (`ID_INDICADOR_CADENA`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 220 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of campo_cadena_indicador
-- ----------------------------
INSERT INTO `campo_cadena_indicador` VALUES (1, 20, 8, 20);
INSERT INTO `campo_cadena_indicador` VALUES (2, 21, 8, 30);
INSERT INTO `campo_cadena_indicador` VALUES (3, 20, 9, 40);
INSERT INTO `campo_cadena_indicador` VALUES (4, 21, 9, 32);
INSERT INTO `campo_cadena_indicador` VALUES (5, 20, 17, 32);
INSERT INTO `campo_cadena_indicador` VALUES (6, 21, 17, 12);
INSERT INTO `campo_cadena_indicador` VALUES (7, 26, 18, 29);
INSERT INTO `campo_cadena_indicador` VALUES (8, 27, 18, 89);
INSERT INTO `campo_cadena_indicador` VALUES (21, 20, 25, 3);
INSERT INTO `campo_cadena_indicador` VALUES (22, 21, 25, 4);
INSERT INTO `campo_cadena_indicador` VALUES (25, 30, 27, 4);
INSERT INTO `campo_cadena_indicador` VALUES (26, 26, 26, 7);
INSERT INTO `campo_cadena_indicador` VALUES (27, 27, 26, 5);
INSERT INTO `campo_cadena_indicador` VALUES (28, 20, 28, 10);
INSERT INTO `campo_cadena_indicador` VALUES (29, 21, 28, 1000);
INSERT INTO `campo_cadena_indicador` VALUES (30, 2, 29, 5000);
INSERT INTO `campo_cadena_indicador` VALUES (31, 3, 29, 20);
INSERT INTO `campo_cadena_indicador` VALUES (32, 22, 31, 1000);
INSERT INTO `campo_cadena_indicador` VALUES (33, 23, 31, 10);
INSERT INTO `campo_cadena_indicador` VALUES (34, 24, 31, 50000);
INSERT INTO `campo_cadena_indicador` VALUES (35, 25, 31, 50);
INSERT INTO `campo_cadena_indicador` VALUES (36, 20, 30, 10);
INSERT INTO `campo_cadena_indicador` VALUES (37, 21, 30, 20);
INSERT INTO `campo_cadena_indicador` VALUES (38, 2, 32, 5000);
INSERT INTO `campo_cadena_indicador` VALUES (39, 3, 32, 10);
INSERT INTO `campo_cadena_indicador` VALUES (40, 4, 33, 5000);
INSERT INTO `campo_cadena_indicador` VALUES (63, 5, 42, 20);
INSERT INTO `campo_cadena_indicador` VALUES (64, 6, 42, 10);
INSERT INTO `campo_cadena_indicador` VALUES (65, 30, 43, 20);
INSERT INTO `campo_cadena_indicador` VALUES (70, 33, 36, 10);
INSERT INTO `campo_cadena_indicador` VALUES (71, 34, 36, 15);
INSERT INTO `campo_cadena_indicador` VALUES (72, 2, 37, 20000);
INSERT INTO `campo_cadena_indicador` VALUES (73, 3, 37, 100);
INSERT INTO `campo_cadena_indicador` VALUES (74, 4, 38, 520000);
INSERT INTO `campo_cadena_indicador` VALUES (75, 22, 39, 1000);
INSERT INTO `campo_cadena_indicador` VALUES (76, 23, 39, 30);
INSERT INTO `campo_cadena_indicador` VALUES (77, 24, 39, 6000);
INSERT INTO `campo_cadena_indicador` VALUES (78, 25, 39, 1000);
INSERT INTO `campo_cadena_indicador` VALUES (79, 31, 40, 50);
INSERT INTO `campo_cadena_indicador` VALUES (80, 32, 40, 1);
INSERT INTO `campo_cadena_indicador` VALUES (81, 20, 41, 100);
INSERT INTO `campo_cadena_indicador` VALUES (82, 21, 41, 150);
INSERT INTO `campo_cadena_indicador` VALUES (83, 42, 44, 4);
INSERT INTO `campo_cadena_indicador` VALUES (84, 43, 44, 10);
INSERT INTO `campo_cadena_indicador` VALUES (91, 20, 45, 200);
INSERT INTO `campo_cadena_indicador` VALUES (92, 21, 45, 300);
INSERT INTO `campo_cadena_indicador` VALUES (95, 8, 34, 23);
INSERT INTO `campo_cadena_indicador` VALUES (96, 9, 34, 32);
INSERT INTO `campo_cadena_indicador` VALUES (99, 10, 35, NULL);
INSERT INTO `campo_cadena_indicador` VALUES (100, 11, 35, NULL);
INSERT INTO `campo_cadena_indicador` VALUES (143, 20, 46, 3);
INSERT INTO `campo_cadena_indicador` VALUES (144, 21, 46, 9);
INSERT INTO `campo_cadena_indicador` VALUES (145, 26, 48, 4);
INSERT INTO `campo_cadena_indicador` VALUES (146, 27, 48, 34);
INSERT INTO `campo_cadena_indicador` VALUES (149, 40, 49, 1000);
INSERT INTO `campo_cadena_indicador` VALUES (150, 41, 49, 1500);
INSERT INTO `campo_cadena_indicador` VALUES (161, 8, 52, 1000);
INSERT INTO `campo_cadena_indicador` VALUES (162, 9, 52, 1500);
INSERT INTO `campo_cadena_indicador` VALUES (171, 42, 57, 20);
INSERT INTO `campo_cadena_indicador` VALUES (172, 43, 57, 25);
INSERT INTO `campo_cadena_indicador` VALUES (175, 10, 53, 10);
INSERT INTO `campo_cadena_indicador` VALUES (176, 11, 53, 25);
INSERT INTO `campo_cadena_indicador` VALUES (177, 20, 59, 1000);
INSERT INTO `campo_cadena_indicador` VALUES (178, 21, 59, 1500);
INSERT INTO `campo_cadena_indicador` VALUES (179, 7, 60, 2000);
INSERT INTO `campo_cadena_indicador` VALUES (184, 2, 61, 50000000);
INSERT INTO `campo_cadena_indicador` VALUES (185, 3, 61, 51);
INSERT INTO `campo_cadena_indicador` VALUES (186, 22, 62, 5);
INSERT INTO `campo_cadena_indicador` VALUES (187, 23, 62, 3);
INSERT INTO `campo_cadena_indicador` VALUES (188, 24, 62, 33);
INSERT INTO `campo_cadena_indicador` VALUES (189, 25, 62, 2);
INSERT INTO `campo_cadena_indicador` VALUES (198, 31, 63, 3);
INSERT INTO `campo_cadena_indicador` VALUES (199, 32, 63, 5);
INSERT INTO `campo_cadena_indicador` VALUES (200, 31, 66, 4);
INSERT INTO `campo_cadena_indicador` VALUES (201, 32, 66, 5);
INSERT INTO `campo_cadena_indicador` VALUES (204, 12, 65, 1000);
INSERT INTO `campo_cadena_indicador` VALUES (205, 13, 65, 1200);
INSERT INTO `campo_cadena_indicador` VALUES (206, 12, 68, 1500);
INSERT INTO `campo_cadena_indicador` VALUES (207, 13, 68, 1200);
INSERT INTO `campo_cadena_indicador` VALUES (212, 10, 64, 10);
INSERT INTO `campo_cadena_indicador` VALUES (213, 11, 64, 15);
INSERT INTO `campo_cadena_indicador` VALUES (214, 10, 67, 15);
INSERT INTO `campo_cadena_indicador` VALUES (215, 11, 67, 15);
INSERT INTO `campo_cadena_indicador` VALUES (216, 2, 70, 1400);
INSERT INTO `campo_cadena_indicador` VALUES (217, 3, 70, 30);
INSERT INTO `campo_cadena_indicador` VALUES (218, 2, 69, 1500);
INSERT INTO `campo_cadena_indicador` VALUES (219, 3, 69, 40);

-- ----------------------------
-- Table structure for color_rango_idl
-- ----------------------------
DROP TABLE IF EXISTS `color_rango_idl`;
CREATE TABLE `color_rango_idl`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ID_INDICADOR` int(5) NULL DEFAULT NULL,
  `RANGO1` double NULL DEFAULT NULL,
  `RANGO2` double NULL DEFAULT NULL,
  `COLOR` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `FK_ID_INDICADOR_COLOR`(`ID_INDICADOR`) USING BTREE,
  CONSTRAINT `FK_ID_INDICADOR_COLOR` FOREIGN KEY (`ID_INDICADOR`) REFERENCES `indicador` (`ID_INDICADOR`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of color_rango_idl
-- ----------------------------
INSERT INTO `color_rango_idl` VALUES (2, 1, 0, 33, '#ff0000');
INSERT INTO `color_rango_idl` VALUES (3, 1, 33, 66, '#f1c232');
INSERT INTO `color_rango_idl` VALUES (4, 1, 66, 100, '#00ff00');
INSERT INTO `color_rango_idl` VALUES (5, 1, 100, 500, '#000000');

-- ----------------------------
-- Table structure for configuracion
-- ----------------------------
DROP TABLE IF EXISTS `configuracion`;
CREATE TABLE `configuracion`  (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `NOMBRE_APLICACION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `IVA_FACTURA` int(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of configuracion
-- ----------------------------
INSERT INTO `configuracion` VALUES (1, 'Somlogic', 16);

-- ----------------------------
-- Table structure for estado_actor
-- ----------------------------
DROP TABLE IF EXISTS `estado_actor`;
CREATE TABLE `estado_actor`  (
  `CS_ESTADO_ID` int(4) NOT NULL AUTO_INCREMENT,
  `DS_NOMBRE_ESTADO` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DS_DESCRIPCION_ESTADO` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`CS_ESTADO_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of estado_actor
-- ----------------------------
INSERT INTO `estado_actor` VALUES (1, 'Activo', 'Activo');
INSERT INTO `estado_actor` VALUES (2, 'Inactivo', 'Inactivo');
INSERT INTO `estado_actor` VALUES (3, 'Pendiente de restaurar contraseña', 'El usuario solicito restaurar su contraseña');

-- ----------------------------
-- Table structure for estado_pedido
-- ----------------------------
DROP TABLE IF EXISTS `estado_pedido`;
CREATE TABLE `estado_pedido`  (
  `ID_ESTADO` int(5) NOT NULL AUTO_INCREMENT,
  `DES_ESTADO` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_ESTADO`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of estado_pedido
-- ----------------------------
INSERT INTO `estado_pedido` VALUES (1, 'Recibido');
INSERT INTO `estado_pedido` VALUES (2, 'En proceso de compra');
INSERT INTO `estado_pedido` VALUES (3, 'En producción');
INSERT INTO `estado_pedido` VALUES (4, 'Despachado');
INSERT INTO `estado_pedido` VALUES (5, 'Anulado');

-- ----------------------------
-- Table structure for etapa
-- ----------------------------
DROP TABLE IF EXISTS `etapa`;
CREATE TABLE `etapa`  (
  `ID_ETAPA` int(5) NOT NULL AUTO_INCREMENT,
  `ETAPA` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_ETAPA`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of etapa
-- ----------------------------
INSERT INTO `etapa` VALUES (1, 'Abastecimiento');
INSERT INTO `etapa` VALUES (2, 'Producción');
INSERT INTO `etapa` VALUES (3, 'Transporte');
INSERT INTO `etapa` VALUES (4, 'Almacenamiento');
INSERT INTO `etapa` VALUES (5, 'Distribución');
INSERT INTO `etapa` VALUES (6, 'Agentes');

-- ----------------------------
-- Table structure for indicador
-- ----------------------------
DROP TABLE IF EXISTS `indicador`;
CREATE TABLE `indicador`  (
  `ID_INDICADOR` int(5) NOT NULL AUTO_INCREMENT,
  `DS_NOMBRE_INDICADOR` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ID_AREA` int(5) NULL DEFAULT NULL,
  `DS_FORMULA` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DS_NOMENCLATURA` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DEFINICION` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `UNIDAD` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NM_ELIMINADO` int(1) NULL DEFAULT 0,
  `NM_ID_TIPO_IND` int(5) NULL DEFAULT 1,
  PRIMARY KEY (`ID_INDICADOR`) USING BTREE,
  INDEX `FK_AREA_INDICADOR`(`ID_AREA`) USING BTREE,
  INDEX `FK_NM_ID_TIPO_INDICADOR`(`NM_ID_TIPO_IND`) USING BTREE,
  CONSTRAINT `FK_AREA_INDICADOR` FOREIGN KEY (`ID_AREA`) REFERENCES `area` (`ID_AREA`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_NM_ID_TIPO_INDICADOR` FOREIGN KEY (`NM_ID_TIPO_IND`) REFERENCES `tipo_indicador` (`ID_TIPO`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of indicador
-- ----------------------------
INSERT INTO `indicador` VALUES (1, 'Nivel servicio de almacenamiento', 1, 'N.S.A. = (%20%) / %21%\r\n', 'N.S.A', 'Mide el grado de cumplimiento en la respuesta del almacenamiento a los requerimientos del sistema', '% nivel de servicio', 0, 1);
INSERT INTO `indicador` VALUES (2, 'Costo de Almacenamiento (por unidad promedio)', 1, 'C.P.A = (%22% * %23% *  %24%)/ %25%', 'C.P.A', 'Se refiere al costo promedio de almacenar una unidad en una bodega o almacen. Puede hacerse por categorias', 'costo por unidad', 0, 2);
INSERT INTO `indicador` VALUES (3, 'Nivel de mermas en inventario', 1, 'N.M.I = (%26%) / %27%', 'N.M.I', 'Mide el grado de deterioro de productos o materias primas almacenadas', '% de mermas', 0, 1);
INSERT INTO `indicador` VALUES (4, 'Rotación de inventario', 1, 'Rotación = %28% / %29%\r\n', 'R.I', 'Mide la rotación del inventario', 'dias de rotación', 0, 1);
INSERT INTO `indicador` VALUES (5, 'Tiempo de alistamientio de pedidos', 1, 'T.A = %30%', 'T.A', 'Mide el tiempo que tarda alistar un pedido (puede ser solicitado por producción o solicitado por el cliente)', 'tiempo alistamiento', 0, 3);
INSERT INTO `indicador` VALUES (6, 'Tiempo de carga de pedidos', 1, 'T.A = %31% / %32%', 'T.C.', 'Mide el tiempo en que es cargado un carro promedio por pedido', 'tiempo carga', 0, 3);
INSERT INTO `indicador` VALUES (7, 'Nivel de servicio de la compra', 2, 'N.S.C = %33% / %34%\r\n', 'N.S.C', 'Mide el grado de cumplimiento de las compras generadas, con respecto a la solicitud del sistema', '%  compras', 0, 1);
INSERT INTO `indicador` VALUES (8, 'Disponibilidad de inventario', 2, 'D.I = %35% / %36%', 'D.I.', 'Mide el grado de disponibilidad del inventario para atender los requerimientos', '% disponibilidad', 0, 1);
INSERT INTO `indicador` VALUES (9, 'Calidad de la compra', 2, 'C.C= %37% / %38%\r\n', 'C.C.', 'Mide la cantidad de compras realizadas a proveedores certificados en comparación con el total de las compras, ayuda al desarrollo de proveedores ', '% compras perfectas', 0, 1);
INSERT INTO `indicador` VALUES (10, 'Tiempo reposición de compra', 2, 'T.R.C = %39%', 'T.R.C', 'Mide el tiempo que se demora un material para estar disponible en un almacen, se mide desde que es puesta la orden de compra, hasta el momento en que esta disponible el material en el almacen', 'Tiempo de reposición', 0, 3);
INSERT INTO `indicador` VALUES (11, 'Tiempo de distribución de productos', 3, 'T.D = %1%', 'T.D.', 'Mide el tiempo en que se demora un producto terminado o una materia prima en ser transportado de un lugar de suministro a un lugar de consumo', 'Tiempo de distribución', 0, 3);
INSERT INTO `indicador` VALUES (12, 'Costo de transporte', 3, 'C.T= %2% / %3%', 'C.T.', 'Mide el costo por pedido del transporte', 'Costo de transporte+I13', 0, 2);
INSERT INTO `indicador` VALUES (13, 'Costo de empaque y embalaje', 3, 'C.M.E.E = %4%', 'C.M.E.E', 'Mide el costo del material de empaque y embalaje por pedido', 'Costo de empaque', 0, 2);
INSERT INTO `indicador` VALUES (14, 'Costo material recuperado', 3, 'C.M.R = %7%', 'C.M.R', 'Mide el costo del material de empaque y embalaje recuperado desde el punto de consumo o cliente', 'Costo material recuperado', 0, 2);
INSERT INTO `indicador` VALUES (15, '% Cumplimiento de lo planeado', 4, 'A.U.P = %8% / %9%', 'A.U.P', 'Mide el grado de asertividad de lo planeado con respecto a lo entregado, sirve para validar la utilización de recursos planeados', '% Cumplimiento de lo planeado', 0, 1);
INSERT INTO `indicador` VALUES (16, '% utilización de recursos', 4, 'A.R.P = %10% / %11%', 'A.R.P', 'Mide el grado de asertividad de los recursos (humanos y físicos)  planeados con respecto a los recursos usados', '% utilización de recursos', 0, 1);
INSERT INTO `indicador` VALUES (17, 'Asertividad en el pronostico', 4, 'AP = %14% / %15%', 'AP', 'Mide el grado de cumplimiento en el pronostico por item', 'Asertividad en el pronostico', 0, 1);
INSERT INTO `indicador` VALUES (18, 'Cumplimiento de costos', 4, 'A.C.P = %12% / %13%', 'A.C.P', 'Mide el grado de cumplimiento en el pronostico por item', 'Asertividad en el pronostico', 0, 1);
INSERT INTO `indicador` VALUES (19, 'Nivel de servicio del sistema', 4, 'N.S.S = %16% / %17%', 'N.S.S', 'Mide el grado de cumplimiento del servicio  del sistema hacia el cliente', '% Nivel de servicio del sistema', 0, 1);
INSERT INTO `indicador` VALUES (20, 'Nivel de servicio del transporte', 4, 'N.S.T= %18% / %19%', 'N.S.T', 'Mide el grado de cumplimiento del servicio  de transporte', '% Nivel de servicio transporte', 0, 1);
INSERT INTO `indicador` VALUES (21, 'Nivel de servicio de despacho', 4, 'N.S.D= %40% / %41%', 'N.S.D', 'Mide el grado de cumplimiento del despacho', '% Nivel de servicio despacho', 0, 1);
INSERT INTO `indicador` VALUES (22, 'Nivel de Agotados ', 4, 'N.A = %42% / %43%', 'N.A', 'Mide el indicador de agotados al momento de ser requeridos por un usuario', '%  Agotados', 0, 1);
INSERT INTO `indicador` VALUES (23, 'Rendimiento sobre la inversion. Return on Equity', 4, 'ROE = (%44%)/(%45%)', 'R.O.E', 'Mide el rendimiento sobre la inversión realizada por los accionistas de la empresa. ', '% Rendimiento de la inversión', 0, 1);
INSERT INTO `indicador` VALUES (24, 'Rotación de cuentas por pagar\r\nAccounts Payable Turnover', 4, 'APT = (%46%)/(%47%)', 'A.P.T', 'Mide la rotación de cuentas por pagar', 'días cuentas por pagar', 0, 1);
INSERT INTO `indicador` VALUES (25, 'Rotación de las cuentas por cobrar\r\nAccounts Receivable Turnove', 4, 'ART=(%48%)/(%49%)', 'A.R.T', 'Mide la rotación de las cuentas por cobrar', 'días cuentas por cobrar', 0, 1);
INSERT INTO `indicador` VALUES (26, 'Rotación propiedad, planta y equipo\r\nProperty, Plant and Equipment Turnover', 4, 'PPET=(%50%)/%51%', 'P.P.E.T', 'Mide la rotación de la propiedad, planta y equipo', 'Rotación propiedad, planta', 0, 1);
INSERT INTO `indicador` VALUES (27, 'Material de empaque recuperado', 3, 'M.E.R = %5% / %6%', 'M.E.R', 'Mide la cantidad de empaque que es recuperado desde el punto final de la cadena de suministro', 'Material de empaque', 0, 1);

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration`  (
  `version` varchar(180) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `apply_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m180523_151638_rbac_updates_indexes_without_prefix', 1566368562);
INSERT INTO `migration` VALUES ('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1566368562);
INSERT INTO `migration` VALUES ('m140506_102106_rbac_init', 1566368562);

-- ----------------------------
-- Table structure for pedido
-- ----------------------------
DROP TABLE IF EXISTS `pedido`;
CREATE TABLE `pedido`  (
  `CS_PEDIDO_ID` int(6) NOT NULL AUTO_INCREMENT,
  `DS_CODIGO_PEDIDO` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NM_USUARIO_CREADOR_ID` int(5) NOT NULL,
  `DS_NOTAS_PEDIDO` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `NM_PRECIO_SUBTOTAL` double NOT NULL,
  `NM_PRECIO_DESCUENTO` double NULL DEFAULT 0,
  `NM_PRECIO_TOTAL` double NOT NULL,
  `NM_PRECIO_IVA` double NULL DEFAULT NULL,
  `DT_FECHA_CREACION` date NULL DEFAULT NULL,
  `NM_EMPRESA` int(5) NULL DEFAULT NULL,
  `DS_CLIENTE` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `ID_ESTADO` int(5) NOT NULL DEFAULT 1,
  `NM_PORCENTAJE_DESCUENTO` int(3) NULL DEFAULT 0,
  `PORCENTAJE_IVA` int(3) NULL DEFAULT 0,
  `GUSTO_PRODUCTOS_CLIENTE` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `FECHA_ENTREGA` date NULL DEFAULT NULL,
  `ID_HORARIO_ENTREGA` int(2) NULL DEFAULT NULL,
  `ID_CLIENTE_FINAL` int(5) NULL DEFAULT NULL,
  PRIMARY KEY (`CS_PEDIDO_ID`) USING BTREE,
  INDEX `FK_CLIENTE_PEDIDO`(`NM_EMPRESA`) USING BTREE,
  INDEX `FK_USUARIO_CREADOR_PEDIDO`(`NM_USUARIO_CREADOR_ID`) USING BTREE,
  INDEX `FK_ESTADO_FAC`(`ID_ESTADO`) USING BTREE,
  INDEX `pedido_ibfk_4`(`ID_HORARIO_ENTREGA`) USING BTREE,
  INDEX `pedido_ibfk_5`(`ID_CLIENTE_FINAL`) USING BTREE,
  CONSTRAINT `pedido_ibfk_1` FOREIGN KEY (`NM_EMPRESA`) REFERENCES `actor` (`ID_ACTOR`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `pedido_ibfk_2` FOREIGN KEY (`ID_ESTADO`) REFERENCES `estado_pedido` (`ID_ESTADO`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `pedido_ibfk_3` FOREIGN KEY (`NM_USUARIO_CREADOR_ID`) REFERENCES `actor` (`ID_ACTOR`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `pedido_ibfk_5` FOREIGN KEY (`ID_CLIENTE_FINAL`) REFERENCES `actor` (`ID_ACTOR`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pedido
-- ----------------------------
INSERT INTO `pedido` VALUES (1, 'SOMLOGIC-000000000001', 39, 'PRUEBA', 180754, 36150.8, 144603.2, 3615.08, '2019-08-25', 60, NULL, 5, 20, 2, '<p>PRUEBA</p>', NULL, NULL, 56);
INSERT INTO `pedido` VALUES (2, 'SOMLOGIC-000000000002', 39, 'prueba', 313000, 31300, 281700, 50080, '2019-07-17', 60, NULL, 5, 10, 16, '<p>papaya maradol verde mediana</p>', NULL, 3, 56);
INSERT INTO `pedido` VALUES (3, 'SOMLOGIC-000000000003', 39, '', 183000, 5490, 177510, 0, '2019-07-17', 60, NULL, 5, 3, 0, '', NULL, 3, NULL);
INSERT INTO `pedido` VALUES (4, 'SOMLOGIC-000000000004', 39, '', 9000, 450, 8550, 0, '2019-07-17', 60, NULL, 4, 5, 0, '', '2019-07-17', 2, NULL);
INSERT INTO `pedido` VALUES (5, 'SOMLOGIC-000000000005', 39, 'Otra prueba', 27000, 0, 27000, 4320, '2019-08-25', 60, NULL, 2, 0, 16, '<p>prueba</p>', '2019-07-20', 2, 56);
INSERT INTO `pedido` VALUES (6, 'SOMLOGIC-000000000006', 39, 'Veamos a ver que pasa acá !!!', 8400, 0, 8400, 0, '2019-08-25', 60, NULL, 2, 0, 16, '<p>prueba</p>', '2019-07-23', 2, 56);
INSERT INTO `pedido` VALUES (7, 'SOMLOGIC-000000000007', 39, '', 0, 0, 0, 0, '2019-08-13', NULL, NULL, 1, 0, 16, NULL, NULL, NULL, NULL);
INSERT INTO `pedido` VALUES (8, 'SOMLOGIC-000000000008', 39, 'prueba nota pedido', 2999, 0, 2999, 0, '2019-08-25', 60, NULL, 1, 0, 16, NULL, NULL, NULL, 56);
INSERT INTO `pedido` VALUES (9, 'SOMLOGIC-000000000009', 39, 'adfadfasdfasdf', 4600, 460, 4140, 736, '2019-08-25', 60, NULL, 5, 10, 16, NULL, NULL, NULL, 56);
INSERT INTO `pedido` VALUES (10, 'SOMLOGIC-000000000010', 39, '', 400, 12, 388, 64, '2019-08-20', 60, NULL, 1, 3, 16, NULL, NULL, NULL, 56);
INSERT INTO `pedido` VALUES (11, 'SOMLOGIC-000000000011', 39, 'Prueba', 200, 20, 180, 32, '2019-08-20', 60, NULL, 1, 10, 16, NULL, NULL, NULL, 56);
INSERT INTO `pedido` VALUES (12, 'SOMLOGIC-000000000012', 39, 'prueba', 200000, 10000, 190000, 38000, '2019-08-20', 60, NULL, 1, 5, 16, NULL, NULL, NULL, 56);
INSERT INTO `pedido` VALUES (13, 'SOMLOGIC-000000000013', 39, 'prueba', 2400, 240, 2160, 288, '2019-08-25', 60, NULL, 1, 10, 12, NULL, NULL, NULL, 56);
INSERT INTO `pedido` VALUES (14, 'SOMLOGIC-000000000014', 39, 'pepe', 20.9, 0.209, 20.691, 1.045, '2019-08-24', 60, NULL, 1, 1, 5, NULL, NULL, NULL, 56);
INSERT INTO `pedido` VALUES (15, 'SOMLOGIC-000000000015', 61, 'Solo atiende los viernes', 3000000, 0, 3000000, 480000, '2019-08-26', 62, NULL, 1, 0, 16, NULL, NULL, NULL, 56);
INSERT INTO `pedido` VALUES (16, 'SOMLOGIC-000000000016', 61, 'pruea', 10989, 0, 10989, 1758.24, '2019-09-24', 62, NULL, 1, 0, 16, NULL, NULL, NULL, 56);

-- ----------------------------
-- Table structure for pedido_cadenabastecimiento
-- ----------------------------
DROP TABLE IF EXISTS `pedido_cadenabastecimiento`;
CREATE TABLE `pedido_cadenabastecimiento`  (
  `ID` int(5) NOT NULL AUTO_INCREMENT,
  `NM_CADENA` int(5) NULL DEFAULT NULL,
  `NM_PEDIDO` int(5) NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `FK_PEDIDO_ID`(`NM_PEDIDO`) USING BTREE,
  INDEX `FK_CADENA_ABASTECIMIENTO`(`NM_CADENA`) USING BTREE,
  CONSTRAINT `FK_CADENA_ABASTECIMIENTO` FOREIGN KEY (`NM_CADENA`) REFERENCES `cadenadeabastecimiento` (`NM_CADENA_ID`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `FK_PEDIDO_ID` FOREIGN KEY (`NM_PEDIDO`) REFERENCES `pedido` (`CS_PEDIDO_ID`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pedido_cadenabastecimiento
-- ----------------------------
INSERT INTO `pedido_cadenabastecimiento` VALUES (1, 10, 5);
INSERT INTO `pedido_cadenabastecimiento` VALUES (2, 11, 2);
INSERT INTO `pedido_cadenabastecimiento` VALUES (3, 11, 6);
INSERT INTO `pedido_cadenabastecimiento` VALUES (4, 11, 8);
INSERT INTO `pedido_cadenabastecimiento` VALUES (5, 12, 2);
INSERT INTO `pedido_cadenabastecimiento` VALUES (9, 13, 4);
INSERT INTO `pedido_cadenabastecimiento` VALUES (10, 13, 8);
INSERT INTO `pedido_cadenabastecimiento` VALUES (11, 13, 6);
INSERT INTO `pedido_cadenabastecimiento` VALUES (12, 13, 5);
INSERT INTO `pedido_cadenabastecimiento` VALUES (13, 14, 10);
INSERT INTO `pedido_cadenabastecimiento` VALUES (14, 14, 5);
INSERT INTO `pedido_cadenabastecimiento` VALUES (15, 15, 5);
INSERT INTO `pedido_cadenabastecimiento` VALUES (16, 15, 10);
INSERT INTO `pedido_cadenabastecimiento` VALUES (17, 16, 6);
INSERT INTO `pedido_cadenabastecimiento` VALUES (18, 16, 2);
INSERT INTO `pedido_cadenabastecimiento` VALUES (19, 16, 12);
INSERT INTO `pedido_cadenabastecimiento` VALUES (20, 17, 15);
INSERT INTO `pedido_cadenabastecimiento` VALUES (22, 18, 15);
INSERT INTO `pedido_cadenabastecimiento` VALUES (23, 19, 13);
INSERT INTO `pedido_cadenabastecimiento` VALUES (24, 20, 8);
INSERT INTO `pedido_cadenabastecimiento` VALUES (25, 20, 5);
INSERT INTO `pedido_cadenabastecimiento` VALUES (26, 21, 14);
INSERT INTO `pedido_cadenabastecimiento` VALUES (27, 21, 13);

-- ----------------------------
-- Table structure for pedido_detalle
-- ----------------------------
DROP TABLE IF EXISTS `pedido_detalle`;
CREATE TABLE `pedido_detalle`  (
  `NM_ID_DETALLE_PEDIDO` int(5) NOT NULL AUTO_INCREMENT,
  `CS_PEDIDO_ID` int(6) NULL DEFAULT NULL,
  `NM_CANTIDAD_COMPRA` int(4) NOT NULL,
  `CS_PRODUCTO_ID` int(6) NOT NULL,
  `NM_PRECIO_TOTAL_PRODUCTO` double NOT NULL,
  `NM_PRECIO_UNITARIO` double NULL DEFAULT NULL,
  `DS_UQ_SESSION` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NM_USUARIO_CREADOR` int(5) NULL DEFAULT NULL,
  `PORCENTAJE_IVA` int(3) NULL DEFAULT 0,
  `PORCENTAJE_DESCUENTO` int(3) NULL DEFAULT 0,
  `NM_VALOR_IVA` double NULL DEFAULT NULL,
  `NM_SUB_TOTAL` double NULL DEFAULT NULL,
  `VALOR_DESCUENTO` double NULL DEFAULT 0,
  `DT_FECHA_CREACION` date NULL DEFAULT NULL,
  `FC` decimal(5, 3) NULL DEFAULT NULL,
  `MG` int(3) NULL DEFAULT NULL,
  `FK_UNIDAD` int(6) NOT NULL,
  `DS_OBSERVACION` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `NM_USUARIO_MODIFICA` int(11) NULL DEFAULT NULL,
  `FC_FECHA_MODIFICACION` date NULL DEFAULT NULL,
  `SE_PUDO_COMPRAR` enum('no','si') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `OBSERVACIONES_COMPRA` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`NM_ID_DETALLE_PEDIDO`) USING BTREE,
  INDEX `FK_FACTURA`(`CS_PEDIDO_ID`) USING BTREE,
  INDEX `FK_PRODUTO`(`CS_PRODUCTO_ID`) USING BTREE,
  INDEX `pedido_detalle_ibfk_3`(`NM_USUARIO_MODIFICA`) USING BTREE,
  INDEX `pedido_detalle_ibfk_4`(`NM_USUARIO_CREADOR`) USING BTREE,
  CONSTRAINT `pedido_detalle_fk1` FOREIGN KEY (`NM_USUARIO_CREADOR`) REFERENCES `actor` (`ID_ACTOR`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `pedido_detalle_fk2` FOREIGN KEY (`NM_USUARIO_MODIFICA`) REFERENCES `actor` (`ID_ACTOR`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 41 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pedido_detalle
-- ----------------------------
INSERT INTO `pedido_detalle` VALUES (10, 2, 15, 203, 135000, 9000, 'PJtoOMJ5ms6ZameWDirIIY1LNZEqupw1YlIxZbzmuWh78z5t70H3r7RfVuZicKVvxnN5p0nw0X0YPQlV1IXVHQ==', 39, NULL, NULL, NULL, NULL, NULL, '2019-07-17', NULL, NULL, 1, '', 39, '2019-08-14', NULL, '');
INSERT INTO `pedido_detalle` VALUES (11, 2, 70, 202, 133000, 1900, 'kqGE6nDyr99akaiaVu_mWNTR72I6-K4Un3VQmalQy9nVydK_XcrCvnekmeo6tYsWn-mjVFmy41zlGmipwTOnrA==', 39, NULL, NULL, NULL, NULL, NULL, '2019-07-17', 0.000, NULL, 3, '<p><img src=\"/uploads/LOGO AGRO.jpg\" style=\"width: 142px;\" class=\"fr-fic fr-dib\"></p>', 39, '2019-08-14', NULL, '');
INSERT INTO `pedido_detalle` VALUES (12, 2, 15, 158, 45000, 3000, 'd86Z1HsCf_7CiE5led6pJRa-CufmISKyoAn-2FsAaC4wps-BVjoSn--9fxUVhMRrXYZG0YVrb_raZsboM2MEWw==', 39, NULL, 0, NULL, NULL, NULL, '2019-07-17', NULL, NULL, 1, '', 39, '2019-08-14', NULL, '');
INSERT INTO `pedido_detalle` VALUES (13, 3, 21, 204, 126000, 6000, 'bWLnAxRdbchHdJKhda8aMzLCWIiqY1u0jcGRCQixs7wqCrFWOWUAqWpBo9EZ9Xd9efoUvskpFvz3rqk5YNLfyQ==', 39, NULL, NULL, NULL, NULL, NULL, '2019-07-17', NULL, NULL, 1, '', 39, '2019-08-14', NULL, '');
INSERT INTO `pedido_detalle` VALUES (14, 3, 30, 202, 57000, 1900, 'CdR7oVWTUDRGR6JaScnAFDuMRubgSBLPox2RIWXOlspOvC30eKs9VWtykyolk61acLQK0IMCX4fZcqkRDa36vw==', 39, NULL, NULL, NULL, NULL, NULL, '2019-07-17', 0.000, NULL, 3, '', 39, '2019-08-14', NULL, '');
INSERT INTO `pedido_detalle` VALUES (16, 4, 9, 134, 9000, 1000, '2XA7eawTSIxLTEqV701BzB5fnHHuFG0bZigOY4uq3R28Hw8pwVl4_CNhP8aNAQe1awrxR6RgKSJRe307xZuJbg==', 39, NULL, 0, NULL, NULL, NULL, '2019-07-17', NULL, NULL, 1, '', 39, '2019-08-14', 'si', '<p>esta prueba&nbsp;</p>');
INSERT INTO `pedido_detalle` VALUES (17, 5, 9, 134, 27000, 3000, 'pRdRXMGrzOfBwNR9zZchoScG0reJWt9ti2zlopm2zOTocAI6pNy91LuS4wqczVnCVjGAx-0Iq13oHonlro_-gA==', 39, NULL, 0, NULL, NULL, NULL, '2019-07-17', NULL, NULL, 1, '<p>dfgakdfvgdsf</p>', 39, '2019-08-14', 'no', '<p>adsfasdfasdf</p>');
INSERT INTO `pedido_detalle` VALUES (18, 5, 90, 55, 0, NULL, '719wgNlP8ggAQJ0eRqBev4yjWEqcvXDBEDwCmLxHiqWiOCPmvDiDO3oSqmkX-ibc_ZQKOvjvBPFzTm7fi364wQ==', 39, NULL, 0, NULL, NULL, NULL, '2019-07-17', NULL, NULL, 1, '<p>asdfkasdkfkajsdfj<img src=\"/uploads/googlelogo_color_272x92dp.png\" style=\"width: 300px;\" class=\"fr-fic fr-dib\"></p>', 39, '2019-08-14', NULL, '');
INSERT INTO `pedido_detalle` VALUES (19, 6, 6, 55, 1800, 300, 'KZIEZH00bO1A3s0WUpPvpCpN7F0CmJvDZBYbxpQDD5Vk2kMsN2M03B-mqSIgvpjFUAvbEnOgyIFcI3-h4zt7oA==', 39, NULL, 0, NULL, NULL, NULL, '2019-07-30', 8.000, 99, 1, '<p>otra prueba</p>', 39, '2019-07-30', 'si', '<p><u>jjjjuu</u></p>');
INSERT INTO `pedido_detalle` VALUES (22, 6, 3, 54, 600, 200, 'NcGsMJXvX-_M0VpIXyxz6LJdVbdmL4fC3d3sM1uyIF4MnvxA2aoG3pTjHxcTWkeKnw9gxghO9Yycm58BN-dTOA==', 39, NULL, 0, NULL, NULL, NULL, '2019-08-14', NULL, NULL, 34, 'prueba', NULL, NULL, NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (23, 6, 3, 59, 6000, 2000, 'BxMmftlRoMrB_EpXSUNW-9FYKVd82GDgkbUCpghFtO8-THYOlRT5-5nODwgFNWKZ_AocJhK5Eq7Q83GUZBDHiQ==', 39, NULL, 0, NULL, NULL, NULL, '2019-08-14', NULL, NULL, 34, 'esta es otra prueba\r\n', NULL, NULL, NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (26, 8, 2999, 54, 2999, 1, 'GBYP2YD0g1LiGnWx_H0XU37U1mzdCSRVH4y1X_6nQ9chSV-pzLHaY7ooMO6wCyMxU4bjHbNoVhteysZtkvIwsQ==', 39, NULL, 0, NULL, NULL, NULL, '2019-08-14', NULL, NULL, 34, 'pruebaaaaaaaaa', NULL, NULL, NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (27, 9, 300, 54, 600, 2, 'Ik32-HAIgJoqJo5NNEaqfEw7E0FTXQ6nWAG-lnvAYvIbEqaIPE3Zq3IUyxJ4MJ4eYWkmMD08fOkZR82kF5URlA==', 39, NULL, 0, NULL, NULL, NULL, '2019-08-14', NULL, NULL, 34, 'prueba que guarde', NULL, '2019-08-14', NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (28, 9, 2, 63, 4000, 2000, 'Ik32-HAIgJoqJo5NNEaqfEw7E0FTXQ6nWAG-lnvAYvIbEqaIPE3Zq3IUyxJ4MJ4eYWkmMD08fOkZR82kF5URlA==', 39, NULL, 0, NULL, NULL, NULL, '2019-08-14', NULL, NULL, 34, 'tales', NULL, '2019-08-14', NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (29, 10, 2, 54, 400, 200, '73NpjQAFHKf5dEXbn5BrrANCWSLwIQV17kAlg0LZjn-GMgL8NFVTwY0CJ4PI0SbLWS0eRpZudTu6dFz7DI3DMA==', 39, NULL, 0, NULL, NULL, NULL, '2019-08-20', NULL, NULL, 34, '', NULL, '2019-08-20', NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (30, 11, 1, 54, 200, 200, '9Nn-2yLakre7kTjPgWnahOpQzuvGQQzCa9wNokxLC6mY7YSBcumjwNmgdYjeOpXijw-bu7QZeYtYmkrsGipB_g==', 39, NULL, 0, NULL, NULL, NULL, '2019-08-20', NULL, NULL, 34, 'prueba', NULL, '2019-08-20', NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (31, 12, 1, 54, 200000, 200000, 'CDnWU3MM5OOTOv-xhIw09pVsdDRL5OXY9x0Prlpdo6s7bPsKBGeQkKZRr-6x4QyHwggZBACJoIjBdVf7bxj14w==', 39, NULL, 0, NULL, NULL, NULL, '2019-08-20', NULL, NULL, 34, 'prueba', NULL, '2019-08-20', NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (32, 13, 2, 54, 2400, 1400, 'MskWR51ftjbVhxpdkKP1uDVCPmjUFWv2bu3qzpwFZ05K-2cUwjD1QKPtciig5rL-YzhnIZJMGJReg4O9_lwJJA==', 39, NULL, 0, NULL, NULL, NULL, '2019-08-23', NULL, NULL, 34, 'prueba', NULL, '2019-08-23', NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (33, 1, 3, 54, 4800, 2000, 'TJLc_v-7nCzjjCT7HL6Txis1msx5RMauhNVrc8NHwuoDv6WNsfjQeavhFrhD8MWSGHrYoS8PsPqwpT0yhCS2og==', 39, NULL, 20, NULL, NULL, NULL, '2019-08-24', NULL, NULL, 34, 'prueba', NULL, NULL, NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (34, 1, 23, 55, 175954, 9000, 'c9iGItxk5DIYWwbZaT-ziVertdpYgtCTF3uw_SeG_KY89f9RkieoZ1A2NJo2ceXdZOT3tw7JpscjC-a8YOWI7g==', 39, NULL, 15, NULL, NULL, NULL, '2019-08-24', NULL, NULL, 34, 'prueasadfasdf', NULL, NULL, NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (35, 14, 22, 55, 20.9, 1, 'C4ogsRrXeWJSaB_aXMSzrCQidIp0rrJIuEd7FSTtd3hEp1nCVJQ1NxoFLZkDiuX4F2025yLlxByMNy1UY44DMA==', 39, NULL, 5, NULL, NULL, NULL, '2019-08-24', NULL, NULL, 1, 'fasdf', NULL, '2019-08-24', NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (36, 15, 2000, 203, 3000000, 1500, 'lyaz0iI6qAGt6VacaLxd3g5W0b3rvuEUhwJjF248yrXdb-niRWjYc-iRAvQj6D6yWGW8zpqMlHDgMxlAW3by2w==', 61, NULL, 0, NULL, NULL, NULL, '2019-08-26', NULL, NULL, 3, '', NULL, NULL, NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (37, 1, 2, 54, 1800, 1000, 'Y_YKC1HnkAz00N6dzK5MorVIgUoXGy1Xq0a47HJueoEhokdaFpD3dcDnqrCk4Cfw33z2K0JDHzPgAOC_GA883g==', 61, NULL, 10, NULL, NULL, NULL, '2019-09-24', NULL, NULL, 34, 'prueba', NULL, NULL, NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (38, 16, 33, 63, 10989, 333, 'w9h2jldKjO96502JiEmeXy9jECbcCEA4kMCpTfB7IGqBjDvfED3rlk7QOaTgB_UNRVdnR4lQclzbhvEemhpmNQ==', 61, NULL, 0, NULL, NULL, NULL, '2019-09-24', NULL, NULL, 1, 'eqrqwer', NULL, NULL, NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (39, NULL, 100, 55, 250000, 2500, 'SL3twq_YeNWdhSncmhoUsiNRWuYUdX8VQVhlYf2Tnh0404-2gr9AhdfGUJLRalDDFjQS300dFHQYAAIHycb0VA==', 61, NULL, 0, NULL, NULL, NULL, '2019-09-30', NULL, NULL, 1, '', NULL, NULL, NULL, NULL);
INSERT INTO `pedido_detalle` VALUES (40, NULL, 10, 57, 10000, 1000, 'SL3twq_YeNWdhSncmhoUsiNRWuYUdX8VQVhlYf2Tnh0404-2gr9AhdfGUJLRalDDFjQS300dFHQYAAIHycb0VA==', 61, NULL, 0, NULL, NULL, NULL, '2019-09-30', NULL, NULL, 1, '', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for producto
-- ----------------------------
DROP TABLE IF EXISTS `producto`;
CREATE TABLE `producto`  (
  `CS_PRODUCTO_ID` int(6) NOT NULL AUTO_INCREMENT,
  `DS_CODIGO_PRODUCTO` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DS_NOMBRE_PRODUCTO` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DS_DESCRIPCION_PRODUCTO` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FK_UNIDAD` int(6) NOT NULL,
  `DT_FECHA_CREACION` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `NM_ELIMINADO` int(1) NULL DEFAULT 0,
  `NM_USUARIO_CREADOR` int(5) NOT NULL DEFAULT 1,
  `IVA` int(3) NULL DEFAULT 0,
  `PORCENTAJE_DESCUENTO` int(3) NULL DEFAULT 0,
  `FC` decimal(5, 3) NULL DEFAULT NULL,
  `MG` int(3) NULL DEFAULT NULL,
  `NM_ID_PROVEEDOR` int(5) NULL DEFAULT NULL,
  `NM_ID_ESTADO_MADURACION` int(5) NULL DEFAULT NULL,
  `NM_ID_TAMANO` int(5) NULL DEFAULT NULL,
  `NM_ID_TIPO_PRODUCTO` int(5) NULL DEFAULT NULL,
  `NM_ID_CATEGORIA_PRODUCTO` int(5) NULL DEFAULT NULL,
  PRIMARY KEY (`CS_PRODUCTO_ID`) USING BTREE,
  UNIQUE INDEX `UQ_CODIGO_PRODUCTO`(`DS_CODIGO_PRODUCTO`) USING BTREE,
  INDEX `FK_UNIDAD`(`FK_UNIDAD`) USING BTREE,
  INDEX `proveedor_fk_id`(`NM_ID_PROVEEDOR`) USING BTREE,
  INDEX `estado_maduracion_fk_id`(`NM_ID_ESTADO_MADURACION`) USING BTREE,
  INDEX `tamanod_fk_id_tam`(`NM_ID_TAMANO`) USING BTREE,
  INDEX `tipo_producto_fk_id`(`NM_ID_TIPO_PRODUCTO`) USING BTREE,
  INDEX `categoria_producto_fk_id`(`NM_ID_CATEGORIA_PRODUCTO`) USING BTREE,
  CONSTRAINT `producto_ibfk_1` FOREIGN KEY (`FK_UNIDAD`) REFERENCES `unidad_producto` (`CS_UNIDAD_ID`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `proveedor_fk_id` FOREIGN KEY (`NM_ID_PROVEEDOR`) REFERENCES `actor` (`ID_ACTOR`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 204 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of producto
-- ----------------------------
INSERT INTO `producto` VALUES (54, 'AW1', 'ACELGA', 'Producto de tipo servicio', 34, '2019-05-14 21:37:59', 0, 1, 19, 10, NULL, NULL, 52, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (55, '2', 'AGRAZ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (56, '3', 'AGUACATE CHOQUETTE', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (57, '4', 'AGUACATE HASS ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (58, '5', 'AGUACATE PAPELILLO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (59, '7', 'AHUYAMA  LIMPIA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (60, '8', 'AJI DULCE ', NULL, 1, '2019-05-14 21:37:59', 1, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (61, '9', 'AJI PIQUE', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (62, '10', 'AJO IMPORTADO ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (63, '11', 'ALBAHACA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (64, '12', 'APIO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (65, '13', 'ARRACACHA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (66, '14', 'ARVEJA DESGRANADA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (67, '15', 'ARVEJA EN VAINA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (68, '16', 'BANANO MADURO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (69, '17', 'BANANO PINTON', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (70, '18', 'BERENJENA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (71, '19', 'BREVAS', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (72, '20', 'BROCOLI', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (73, '21', 'CALABACIN AMARILLO ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (74, '22', 'CALABACIN VERDE ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (75, '23', 'CARAMBOLO  KILO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (76, '24', 'CEBOLLA CABEZONA BLANCA PELADA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (77, '25', 'CEBOLLA CABEZONA BLANCA SUCIA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (78, '26', 'CEBOLLA CABEZONA ROJA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (79, '27', 'CEBOLLA JUNCA  PELADA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (80, '28', 'CEBOLLA JUNCA  SUCIA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (81, '29', 'CEBOLLA PUERRO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (82, '30', 'CEBOLLA ROJA PELADA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (83, '31', 'CEBOLLIN', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (84, '32', 'CHAMPIÑON ENTERO PORTOB', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (85, '33', 'CHAMPIÑON ENTERO AGRANEL', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (86, '34', 'CHAMPIÑON  CRIMINI', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (87, '35', 'CHAMPIÑON AGRANEL SHIITAKE ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (88, '36', 'CHOCOLO DESGRANADO ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (89, '37', 'CHOCOLO SIN CAPACHO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (90, '38', 'CIDRA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (91, '39', 'CILANTRO ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (92, '40', 'CIRUELA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (93, '41', 'COCO KILO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (94, '42', 'COCO UNIDAD', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (95, '43', 'COL CHINA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (96, '44', 'COLES ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (97, '45', 'COLIFLOR', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (98, '46', 'COMINO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (99, '47', 'CURUBA LARGA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (100, '48', 'DURAZNO NECTARIN', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (101, '49', 'ESPARRAGOS ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (102, '50', 'ESPINACA BOGOTANA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (103, '51', 'ESPINACA REGIONAL', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (104, '52', 'FRESA CORRIENTE', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (105, '53', 'FRESA EXTRA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (106, '54', 'FRESA EXTRA JUMBO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (107, '55', 'FRIJOL DESGRANADO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (108, '56', 'FRIJOL VERDE', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (109, '57', 'GRANADILLA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (110, '58', 'GUANABANA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (111, '59', 'GUASCA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (112, '60', 'GUAYABA MANZANA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (113, '61', 'GUAYABA PERA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (114, '62', 'GUINEO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (115, '63', 'HABICHUELA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (116, '64', 'HIERBABUENA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (117, '65', 'HINOJO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (118, '66', 'HOJAS VIAHO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (119, '67', 'JENGIBRE ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (120, '68', 'KIWI', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (121, '69', 'LAUREL', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (122, '70', 'LECHUGA BATAVIA KILO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (123, '71', 'LECHUGA CRESPA X UNIDAD', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (124, '72', 'LECHUGA CRESPA X KILO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (125, '73', 'LECHUGA MORADA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (126, '74', 'LECHUGA ROMANA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (127, '75', 'LIMON MANDARINO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (128, '76', 'LIMON TAHITI', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (129, '77', 'LIMONCILLO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (130, '78', 'LULO ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (131, '79', 'MAIZ DULCE', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (132, '80', 'MANDARINA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (133, '81', 'MANDARINA SEGUNDA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (134, '82', 'MANGO  FARCHY', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (135, '83', 'MANGO CRIOLLO ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (136, '84', 'MANGO TOMY ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (137, '85', 'MANZANA GALA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (138, '86', 'MANZANA ROJA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (139, '87', 'MANZANA VERDE', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (140, '88', 'MARACUYA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (141, '89', 'MAZORCA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (142, '90', 'MELON ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (143, '91', 'MENTA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (144, '92', 'MORA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (145, '93', 'NARANJA TANGELO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (146, '94', 'NARANJA VALENCIA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (147, '95', 'OREGANO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (148, '96', 'PAPA CAPIRA  BUENA 1ERA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (149, '97', 'PAPA CAPIRA 2DA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (150, '98', 'PAPA MEDIO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (151, '99', 'PAPA TRONCO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (152, '100', 'PAPA BALIN O  EMPANADA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (153, '101', 'PAPA CRIOLLA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (154, '102', 'PAPA NEVADA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (155, '103', 'PAPAYA MARADOL ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (156, '104', 'PAPAYA MELONA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (157, '105', 'PAPAYUELA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (158, '106', 'PATILLA BABY ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (159, '107', 'PATILLA-SANDIA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (160, '108', 'PEPINO COHOMBRO ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (161, '109', 'PEPINO RELLENAR ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (162, '110', 'PERA IMPORTADA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (163, '111', 'PEREJIL CRESPO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (164, '112', 'PEREJIL LISO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (165, '113', 'PIMENTON ROJO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (166, '114', 'PIMENTON VERDE', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (167, '115', 'PINA MANZANA KILO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (168, '116', 'PIÑA ORO MIEL KILO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (169, '117', 'PITAHAYA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (170, '118', 'PLATANO MADURO DOMINICO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (171, '119', 'PLATANO MADURO HARTON', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (172, '120', 'PLATANO VERDE DOMINICO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (173, '121', 'PLATANO VERDE HARTON', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (174, '122', 'RABANO ROJO ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (175, '123', 'RAIZ CHINA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (176, '124', 'REMOLACHA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (177, '125', 'REPOLLO BLANCO ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (178, '126', 'REPOLLO MORADO ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (179, '127', 'ROMERO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (180, '128', 'RUGULA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (181, '129', 'TOMATE ALIÑO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (182, '130', 'TOMATE CHERRY ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (183, '131', 'TOMATE CHONTO  ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (184, '132', 'TOMATE DE ARBOL', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (185, '133', 'TOMATE DE ARBOL  TAMARILLO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (186, '134', 'TOMATE LARGA VIDA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (187, '135', 'TOMATE RIÑON ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (188, '136', 'TOMILLO', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (189, '137', 'TORONJIL', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (190, '138', 'UCHUVA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (191, '139', 'UVA IMPORTADA', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (192, '140', 'UVA ISABELA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (193, '141', 'UVA RED GLOBE NACIONAL', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (194, '142', 'VITORIA  ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (195, '143', 'YACON ', 'prueba ', 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (196, '144', 'YUCA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (197, '145', 'ZANAHORIA ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 19, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (198, '146', 'ZANAHORIA  BABY 250 GR ', NULL, 1, '2019-05-14 21:37:59', 0, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 1, NULL);
INSERT INTO `producto` VALUES (199, '147', 'ZANAHRIA BABY X 454 GR', NULL, 1, '2019-05-14 21:37:59', 0, 1, 0, 0, 1.000, 1, NULL, NULL, NULL, 1, 1);
INSERT INTO `producto` VALUES (200, '0', '1', '', 1, '2019-05-14 21:43:03', 0, 1, 19, 0, 1.000, 1, NULL, NULL, NULL, 1, 1);
INSERT INTO `producto` VALUES (201, '41985', 'LECHUGA BATAVIA UNIDAD', '', 3, '2019-06-19 10:58:31', 0, 1, 0, NULL, 1.000, 20, 6, NULL, 1, 1, 1);
INSERT INTO `producto` VALUES (202, '687570', 'BANANO BOLSA 1.5', '', 3, '2019-06-19 10:59:01', 0, 1, 0, NULL, 0.000, NULL, 11, 1, 1, 1, 22);
INSERT INTO `producto` VALUES (203, '4556566', 'Piña', 'Oro miel', 3, '2019-08-26 16:11:26', 0, 1, 19, 0, NULL, NULL, 52, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tipo_actor
-- ----------------------------
DROP TABLE IF EXISTS `tipo_actor`;
CREATE TABLE `tipo_actor`  (
  `CS_TIPO_ACTOR` int(4) NOT NULL AUTO_INCREMENT,
  `DS_NOMBRE_TIPO_ACTOR` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`CS_TIPO_ACTOR`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tipo_actor
-- ----------------------------
INSERT INTO `tipo_actor` VALUES (1, 'Administrador');
INSERT INTO `tipo_actor` VALUES (2, 'Distribuidor');
INSERT INTO `tipo_actor` VALUES (3, 'Agente aduanero');
INSERT INTO `tipo_actor` VALUES (4, 'Proveedor');
INSERT INTO `tipo_actor` VALUES (5, 'Transportador maritimo');
INSERT INTO `tipo_actor` VALUES (6, 'Transportador areo');
INSERT INTO `tipo_actor` VALUES (7, 'Transporte terrestre');
INSERT INTO `tipo_actor` VALUES (8, 'Cliente final');
INSERT INTO `tipo_actor` VALUES (9, 'Fabricante');
INSERT INTO `tipo_actor` VALUES (10, 'Almacenamiento');
INSERT INTO `tipo_actor` VALUES (11, 'Empresa');
INSERT INTO `tipo_actor` VALUES (12, 'Visitante');

-- ----------------------------
-- Table structure for tipo_documento
-- ----------------------------
DROP TABLE IF EXISTS `tipo_documento`;
CREATE TABLE `tipo_documento`  (
  `CS_TIPO_DOCUMENTO_ID` int(4) NOT NULL AUTO_INCREMENT,
  `DS_NOMBRE_TIPO_DOCUMENTO` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DS_DESCRIPCION_TIPO_DOCUMENTO` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`CS_TIPO_DOCUMENTO_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tipo_documento
-- ----------------------------
INSERT INTO `tipo_documento` VALUES (1, 'Cédula Ciudadanía', 'Cédula Ciudadanía');
INSERT INTO `tipo_documento` VALUES (2, 'Registro civil', 'Registro civil');
INSERT INTO `tipo_documento` VALUES (3, 'Pasaporte', 'Pasaporte');
INSERT INTO `tipo_documento` VALUES (4, 'Cédula Extranjera ', 'Cédula Extranjera');
INSERT INTO `tipo_documento` VALUES (5, 'NIT', 'NIT');

-- ----------------------------
-- Table structure for tipo_indicador
-- ----------------------------
DROP TABLE IF EXISTS `tipo_indicador`;
CREATE TABLE `tipo_indicador`  (
  `ID_TIPO` int(5) NOT NULL AUTO_INCREMENT,
  `DS_TIPO_IND` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_TIPO`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tipo_indicador
-- ----------------------------
INSERT INTO `tipo_indicador` VALUES (1, 'Porcentual');
INSERT INTO `tipo_indicador` VALUES (2, 'Costo');
INSERT INTO `tipo_indicador` VALUES (3, 'Tiempo');

-- ----------------------------
-- Table structure for unidad_producto
-- ----------------------------
DROP TABLE IF EXISTS `unidad_producto`;
CREATE TABLE `unidad_producto`  (
  `CS_UNIDAD_ID` int(6) NOT NULL AUTO_INCREMENT,
  `DS_NOMBRE_UNIDAD` varchar(30) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `DS_DESCRIPCION_UNIDAD` varchar(255) CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL DEFAULT NULL,
  `ESTADO` enum('activo','eliminado') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'activo',
  PRIMARY KEY (`CS_UNIDAD_ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of unidad_producto
-- ----------------------------
INSERT INTO `unidad_producto` VALUES (1, 'Kilos', 'Kl', 'activo');
INSERT INTO `unidad_producto` VALUES (2, 'Paquetes', 'Paquete', 'activo');
INSERT INTO `unidad_producto` VALUES (3, 'Unidades', 'Und', 'activo');
INSERT INTO `unidad_producto` VALUES (4, 'Sobre', 'Sobre', 'activo');
INSERT INTO `unidad_producto` VALUES (5, 'Caja', 'Caja', 'activo');
INSERT INTO `unidad_producto` VALUES (33, 'Gramos', 'gr', 'activo');
INSERT INTO `unidad_producto` VALUES (34, 'Servicio', 'srv', 'activo');

-- ----------------------------
-- View structure for vw_actor
-- ----------------------------
DROP VIEW IF EXISTS `vw_actor`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `vw_actor` AS select `actor`.`ID_ACTOR` AS `id`,concat(`actor`.`DS_CORREO`,' - ',`actor`.`DS_NOMBRES_ACTOR`) AS `username`,`actor`.`ID_ACTOR` AS `ID_ACTOR`,`actor`.`NM_DOCUMENTO_ID` AS `NM_DOCUMENTO_ID`,`actor`.`NM_TIPO_DOCUMENTO_ID` AS `NM_TIPO_DOCUMENTO_ID`,`actor`.`DS_NOMBRES_ACTOR` AS `DS_NOMBRES_ACTOR`,`actor`.`DS_APELLIDOS_ACTOR` AS `DS_APELLIDOS_ACTOR`,`actor`.`NM_TELEFONO` AS `NM_TELEFONO`,`actor`.`NM_CELULAR` AS `NM_CELULAR`,`actor`.`DS_CORREO` AS `DS_CORREO`,`actor`.`DS_DIRECCION` AS `DS_DIRECCION`,`actor`.`DS_CONTRASENA` AS `DS_CONTRASENA`,`actor`.`NM_TIPO_ACTOR_ID` AS `NM_TIPO_ACTOR_ID`,`actor`.`NM_ESTADO_ID` AS `NM_ESTADO_ID`,`actor`.`DT_FECHA_CREACION` AS `DT_FECHA_CREACION`,`actor`.`DF_FECHA_ACTUALIZACION` AS `DF_FECHA_ACTUALIZACION`,`actor`.`CLAVE_AUTENTICACION` AS `CLAVE_AUTENTICACION`,`actor`.`PAGINA_WEB` AS `PAGINA_WEB`,`actor`.`CONTACTO` AS `CONTACTO`,`actor`.`CARGO_CONTACTO` AS `CARGO_CONTACTO` from `actor`;

-- ----------------------------
-- View structure for vw_idl
-- ----------------------------
DROP VIEW IF EXISTS `vw_idl`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `vw_idl` AS select `t1`.`ID_ETP_CAD` AS `ID_ETP_CAD`,`t1`.`ID_ETAPA` AS `ID_ETAPA`,`t1`.`ID_CADENA` AS `ID_CADENA`,`t1`.`ID_ACTOR` AS `ID_ACTOR`,`t1`.`CUMPLIMIENTO_PROMEDIO` AS `CUMPLIMIENTO_PROMEDIO`,`t1`.`NM_PORCENTAJE_SIGNIFICANCIA` AS `NM_PORCENTAJE_SIGNIFICANCIA`,(`t1`.`CUMPLIMIENTO_PROMEDIO` * (`t1`.`NM_PORCENTAJE_SIGNIFICANCIA` / 100)) AS `IDL` from `cadena_etapa` `t1`;

-- ----------------------------
-- View structure for vw_idl_pedidos
-- ----------------------------
DROP VIEW IF EXISTS `vw_idl_pedidos`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `vw_idl_pedidos` AS select `t1`.`DS_CODIGO_PEDIDO` AS `DS_CODIGO_PEDIDO`,sum((`t3`.`NM_RESULTADO_INDICADOR` * `t3`.`NM_PORCENTAJE_SIGNIFICANCIA`)) AS `TOTAL`,`t1`.`CS_PEDIDO_ID` AS `CS_PEDIDO_ID`,`cadena_etapa`.`ID_CADENA` AS `NM_CADENA` from ((`pedido` `t1` join `cadena_indicador` `t3` on((`t3`.`ID_PEDIDO` = `t1`.`CS_PEDIDO_ID`))) join `cadena_etapa` on((`t3`.`ID_CADENA` = `cadena_etapa`.`ID_ETP_CAD`))) group by `t1`.`CS_PEDIDO_ID`;

-- ----------------------------
-- View structure for vw_total_indicadores_ca
-- ----------------------------
DROP VIEW IF EXISTS `vw_total_indicadores_ca`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `vw_total_indicadores_ca` AS select `indicador`.`DS_NOMBRE_INDICADOR` AS `DS_NOMBRE_INDICADOR`,`cadena_indicador`.`NM_RESULTADO_INDICADOR` AS `NM_RESULTADO_INDICADOR`,`cadena_indicador`.`NM_PORCENTAJE_SIGNIFICANCIA` AS `NM_PORCENTAJE_SIGNIFICANCIA`,((`cadena_indicador`.`NM_PORCENTAJE_SIGNIFICANCIA` / 100) * `cadena_indicador`.`NM_RESULTADO_INDICADOR`) AS `RESULTADO_SIGNIFICANCIA`,`indicador`.`ID_INDICADOR` AS `ID_INDICADOR`,`etapa`.`ETAPA` AS `ETAPA`,`indicador`.`DS_FORMULA` AS `DS_FORMULA`,`indicador`.`DS_NOMENCLATURA` AS `DS_NOMENCLATURA`,`indicador`.`DEFINICION` AS `DEFINICION`,`area`.`DS_AREA` AS `DS_AREA`,`cadena_indicador`.`ID_INDICADOR_CADENA` AS `ID_INDICADOR_CADENA`,`cadena_etapa`.`ID_CADENA` AS `ID_CADENA`,`etapa`.`ID_ETAPA` AS `ID_ETAPA`,`indicador`.`UNIDAD` AS `UNIDAD`,`pedido`.`DS_CODIGO_PEDIDO` AS `DS_CODIGO_PEDIDO`,`cadena_indicador`.`META` AS `META`,`cadena_indicador`.`FECHA_CREACION` AS `FECHA_CREACION`,`creador`.`DS_NOMBRES_ACTOR` AS `USUARIO_CREACION`,`actualizador`.`DS_NOMBRES_ACTOR` AS `USUARIO_ACTUALIZACION`,`cadena_indicador`.`FECHA_ACTUALIZACION` AS `FECHA_ACTUALIZACION`,`indicador`.`NM_ID_TIPO_IND` AS `NM_ID_TIPO_IND` from (((((((`cadena_indicador` join `indicador` on((`cadena_indicador`.`ID_INDICADOR` = `indicador`.`ID_INDICADOR`))) join `cadena_etapa` on((`cadena_indicador`.`ID_CADENA` = `cadena_etapa`.`ID_ETP_CAD`))) join `etapa` on((`cadena_etapa`.`ID_ETAPA` = `etapa`.`ID_ETAPA`))) join `area` on((`indicador`.`ID_AREA` = `area`.`ID_AREA`))) left join `pedido` on((`cadena_indicador`.`ID_PEDIDO` = `pedido`.`CS_PEDIDO_ID`))) left join `actor` `creador` on((`creador`.`ID_ACTOR` = `cadena_indicador`.`USUARIO_CREACION`))) left join `actor` `actualizador` on((`actualizador`.`ID_ACTOR` = `cadena_indicador`.`USUARIO_ACTUALIZACION`)));

SET FOREIGN_KEY_CHECKS = 1;
