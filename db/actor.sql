/*
 Navicat Premium Data Transfer

 Source Server         : TG
 Source Server Type    : MySQL
 Source Server Version : 100141
 Source Host           : 167.71.244.15:3306
 Source Schema         : omesa_somlogic

 Target Server Type    : MySQL
 Target Server Version : 100141
 File Encoding         : 65001

 Date: 25/08/2019 03:37:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for actor
-- ----------------------------
DROP TABLE IF EXISTS `actor`;
CREATE TABLE `actor`  (
  `ID_ACTOR` int(5) NOT NULL AUTO_INCREMENT,
  `NM_DOCUMENTO_ID` bigint(20) NULL DEFAULT NULL,
  `NM_TIPO_DOCUMENTO_ID` int(4) NULL DEFAULT NULL,
  `DS_NOMBRES_ACTOR` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DS_APELLIDOS_ACTOR` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NM_TELEFONO` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `NM_CELULAR` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DS_CORREO` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `DS_DIRECCION` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `DS_CONTRASENA` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `NM_TIPO_ACTOR_ID` int(4) NULL DEFAULT NULL,
  `NM_ESTADO_ID` int(4) NOT NULL,
  `DT_FECHA_CREACION` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `DF_FECHA_ACTUALIZACION` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `CLAVE_AUTENTICACION` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `PAGINA_WEB` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CONTACTO` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `CARGO_CONTACTO` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID_ACTOR`) USING BTREE,
  UNIQUE INDEX `UQ_DOCUMENTO`(`NM_DOCUMENTO_ID`) USING BTREE,
  INDEX `FK_TIPO_USUARIO`(`NM_TIPO_ACTOR_ID`) USING BTREE,
  INDEX `FK_ESTADO`(`NM_ESTADO_ID`) USING BTREE,
  INDEX `FK_TIPO_DOCUMENTO`(`NM_TIPO_DOCUMENTO_ID`) USING BTREE,
  CONSTRAINT `actor_ibfk_1` FOREIGN KEY (`NM_ESTADO_ID`) REFERENCES `estado_actor` (`CS_ESTADO_ID`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `actor_ibfk_2` FOREIGN KEY (`NM_TIPO_DOCUMENTO_ID`) REFERENCES `tipo_documento` (`CS_TIPO_DOCUMENTO_ID`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `actor_ibfk_3` FOREIGN KEY (`NM_TIPO_ACTOR_ID`) REFERENCES `tipo_actor` (`CS_TIPO_ACTOR`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 62 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of actor
-- ----------------------------
INSERT INTO `actor` VALUES (1, 12453, 2, 'pepito', 'perez', '3234423', '3243532423', 'juan@del.com', 'calle 2334', '$2y$10$cL0IiTQ4AqOljUh6X/I8cuvw/FPJ.SMResdlQOYFwWvMD8hJp5Gg.', 2, 2, '2018-11-06 12:55:22', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (2, 35611553, 1, 'jairo', 'ortiz', '1200000', '8452530125', 'jairo@gmail.com', NULL, '$2y$10$DUbb22PcZE3R0wvwv/SZzO9ef9PIBsg6W8b10YQ5O.OjBAEZnJU2.', 2, 2, '2018-09-06 12:00:14', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (3, 36610553, 1, 'jhon', 'valencia', '7896325', '8965635632', 'jhon@gmail.com', NULL, '$2y$10$6sZZ9FGEIaWYF10ZLeB40eR838NfAPd2fMzvMMgzyMve4yKRiBPWe', 2, 2, '2018-09-06 13:43:35', '2018-11-25 18:11:23', NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (4, 43101104, 1, 'nidia', 'valencia', '3127899', '3127852212', 'oscar_moooesa24092@elpoli.edu.co', '', '$2y$10$Mo3BAL/l6V3aH9UxdupiY.0jRJ1dWrpwaFRAlBG8FSif2VA56xQl.', 2, 2, '2018-08-25 11:15:21', '2019-01-24 21:01:39', NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (5, 95501690, 1, 'juan', 'acevedo', '7896366', '1521521255', 'juan@gmail.com', NULL, '$2y$10$IUQ0n8aZ4WmdByRAiF9HJec4kTxkHP26mXgyar0cBf4RW0xOzXNMq', 2, 2, '2018-09-06 15:03:01', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (6, 123456789, 2, 'ana', 'herrera', '4545632', '2563254896', 'ana@gmail.com', NULL, '$2y$10$nuKTZucQxy7mbMvoQt2TtOwwc9vRNZ8NjmqMIpjSMV1mfdzIOBwom', 2, 2, '2018-09-06 03:50:32', '2019-02-01 17:20:36', NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (7, 323432423, 2, 'pepe', 'juarea', '3242343', '3243242342', 'oscarmesa.elpsssoli@gmail.com', 'calle 123', '$2y$10$KGuITl2u4.UkbatVJ6lxse7/BvWTBPW54X9LNgSzb1nSqs0Ew9sk.', 2, 2, '2018-11-06 12:58:41', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (8, 324343333, 1, 'pepe', 'dfsadf', '2342343', '3232432234', 'ososcar@fasdf.com', NULL, '$2y$10$Gj8Zas7zys..mmXtvvTzMua6eOeg8zjcIIIFNNFGoEUb1Kn0vrVm6', 2, 2, '2018-09-06 13:41:14', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (9, 356115453, 1, 'carlos bab ', 'pepito perez', '3242342', '3243243243', 'jairwwwo@gmail.com', 'calle 13', '$2y$10$70JYvGcKTCb225I0uhl5f..A.u220BJgurFBFS6gonJZcINqT0Nnm', 2, 2, '2018-10-16 14:04:30', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (10, 356115533, 1, 'calos ', 'pedrada', '3242343', '3242342342', 'jairo3333@gmail.com', 'calle 1234', '$2y$10$FUvZ5r4zk1fowrozpLf7WeK5pTrruxbo/wL8vH4wUUbIl2AKbHsA6', 1, 2, '2018-10-17 02:37:57', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (11, 789623778, 1, 'pedro', 'perez', '12369', '789632', 'pedro@gmail.com', NULL, '$2y$10$29aWCUIkJ5mb8KKOevJLXuBiqiL5I8KhdGREpxJDkPOOOswp5eE7C', 2, 2, '2018-09-06 18:15:44', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (12, 11521888632, 1, 'Oscar', 'Mesa', '5804661', '3012280744', 'oscarmesa@gmail.com', '', '$2y$10$3sTTtL17ShNLWimjwm5f6ehtn3YcfJW5BRC3aT0hSWQd4fdfWEFGi', 3, 1, '2018-09-10 19:01:51', '2019-02-03 18:25:17', NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (13, 1152204758, 1, 'santiago', 'betancur|', '6666666', '6666666666', 'poliaulink@gmail.com', '', '$2y$10$32AbUlynEMogC8aG/NEk6OtxD6hlD.QUjkZRWnv.C8ye3zCt87PGG', 2, 1, '2018-08-25 13:59:36', '2019-03-05 09:07:47', NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (14, 2147483647, 1, 'Diego', 'Mejia', '5555555', '5555555555', '555@gmail.com', NULL, '$2y$10$7QNQvUzb7xZmPnmqWOmL5..evrhHRzuBEc4zgchHJyOrjIqA9ZpzG', 2, 2, '2018-09-06 16:14:52', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (15, 5435843958, 3, 'asdfasdf', 'sadfsdfasdf', '2313333', '3432423333', 'oscar@gmailc.om', NULL, '$2y$10$sJqVbpfYBnNs482CdCgJYeRDiRXxI5mjkDP7ZjAyvwVFnPK5hOQK.', 1, 2, '2018-09-07 16:10:45', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (16, 23123213123, 1, 'calitos', 'pepo', '2131232', '1232131233', 'carlos234@gmail.com', 'calle 123', '$2y$10$NNxohVe2CN8H2K3q/YV0/OjiRQjEHcQNWhiI0/h8kR7u3h3DzGx26', 2, 2, '2018-09-25 15:52:06', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (17, 564353453454, 4, 'pepito', 'perez', '2342342', '3242343243', 'pepoooo@gmail.com', 'callle 324324', '$2y$10$8QR9XCFgNWawPScSAxu1Du5TL1GNgeR3ENbbzIFa/ygKTedztVFkC', 2, 2, '2018-10-11 15:03:20', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (18, 111111111111111, 1, 'ana', 'mesa', '2314324', '2342342342', 'anamesa@gmail.com', 'calle 23123', '$2y$10$hjFasakxrStDbXiIxLFWSeH0ztgbEnZFDYeva19JuOQ9kVNEoNclW', 2, 2, '2018-09-25 15:54:48', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (19, 234324234234234, 1, 'pepito', 'asdfasdf', '3242342', '2342342343', 'casd@ksdfasdf.com', 'afsdfasdf', '$2y$10$1VUVzKMgemAJCyJ7Q1gM3urBp2yy5b1R5a2ZdzSHgSa6sVXcJlW3e', 2, 2, '2018-10-11 15:05:04', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (20, 324123412341234, 1, 'carlos', 'mesa', '3242342', '3242234234', 'pepito@gmaill.com', 'calere', '$2y$10$jHabhAQwKyDOeeMzqvYMce9JxPDzGWXmEwMuXHsA5EEQ2Ns5c1gCG', 2, 2, '2018-10-11 14:59:11', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (21, 324234234324234, 1, 'carlos', 'landa', '2343243', '2342343243', 'jaime12321@gmail.com', '', '$2y$10$8VExBHrLnMWQAxRdcCNpZ.napTbLqxh.psPj6ziJshwuMQGe9DcXG', 1, 2, '2018-09-24 20:16:13', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (22, 324333233233333, 2, 'ds', 'werasdfadsf', '3334444', '2333333333', 'pepeepepe@gmail.com', 'calle 234', '$2y$10$LOqFa1B5fFRWVILwfWzy7unsimGuJjwOUSP0YPYbQq1QCvjU3XrvC', 1, 2, '2018-11-05 23:19:03', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (23, 345345435345345, 1, 'sdafasfdasdasdf', 'sadfsdfasdfasdfasdf', '2342342', '3423432423', '234324@fmaol.com', 'cale', '$2y$10$FtlayZDyNC8YFTamZR4A6umMlOgG41BJZBoFS/spKWhIOeVggNogK', 2, 2, '2018-10-11 15:08:17', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (24, 432423432423423, 3, 'ewsadf', 'asdf', '2321312', '1231232342', 'pepe@gmail.com', 'calle 123', '$2y$10$0VWFYnJ4J4N.64HJ4exALej1.VfzNV66PhY3.WfvyEs1AQTlYEmoq', 2, 2, '2018-10-11 14:55:17', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (25, 452345243523452, 1, 'Daniel', 'Mesa', '3232231', '2343243432', 'oscarmesja.elpoli@gmail.com', 'calle 2234', '$2y$10$.XREgbeKAACNeqgLXDjPC.t0CmV7rYpxNsS6SeIHndOYPoLOBN/Me', 3, 1, '2018-10-17 01:56:17', '2019-08-20 04:10:18', NULL, '', '', '');
INSERT INTO `actor` VALUES (27, 877777777777777, 1, 'sdfasdf', 'adfgdgsdfg', '5555555', '5555555555', 'asdfasdf@gmail.com', NULL, '$2y$10$lHEjTLDk4BoEshMS7ebJHuvQDeDCpwRd5E3HXMOdWRR8ycIc3maZW', 2, 2, '2018-09-07 16:44:43', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (28, 996954695469456, 2, 'dfgksdfgksdkfgsdfkgs', 'dskjfskf', '3432423', '4234234234', '4535345@gmail.com', NULL, '$2y$10$f0VW.aYOb2.nMcEkAoZVteW7MYUTWSoJr8tTEcKnFE6Ct2CdPwDZ6', 2, 2, '2018-09-07 16:13:32', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (29, 999999999999999, 1, 'Diego', 'Mejia', '5555555', '5555555555', '555@gmail.com', 'calle 1234', '$2y$10$hxmzv4AJN4G7pJQntHvEdOtIqRwj0/JIoFRaXjZY5I6HO5K27az7W', 2, 2, '2018-09-07 16:35:21', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `actor` VALUES (39, 1152188863, 1, 'Oscar', 'Mesa', '34234', '324324', 'oscarmesa.elpoli@gmail.com', 'carrera 95 # 48D - 58', '$2y$13$TGIup1JMQVja5MlQSrgU2eVpqMY4FZfjG9wTZlyX7j2qPYAE/h6Gm', 1, 3, '2018-11-13 00:00:00', '2019-05-17 13:38:11', 'ctoH5aV3gwQrWmkSTBGQIgiQspj2pwMQhEUZV1TdqKPJxjaY6JXAW6WpOkpJ', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (48, 898989898, 1, 'Oscar', 'Mesa', NULL, 'kjjkkjkj', 'deosmega@gmail.com', '', 'yqaXFrQHELBc', 2, 1, '2018-11-13 17:25:55', '2019-01-15 05:23:41', 'Uv3FIQsL8wn3gMuBdFN1lzz0DUY9dmUnojVDcKAtEeMN95ZPGKr5L3wU3wgB', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (49, 1152183338863, 1, 'Oscar', 'Mesa', NULL, '2312332423', 'oscar_mesa24092@elpoli.edu.co', '', 'rhdfoMaHZqos', 3, 1, '2018-11-13 17:26:28', '2018-11-26 13:28:21', '1FEyIiRg1qGXSiUg3RV_P1lSzjN-JNwvrMl_cSOify7nmuYfZngSn0KHiJRi', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (50, 431011042, 1, 'pepe ', 'grillo', '21474', '32323', 'jaime121@gmail.com', 'calle 123', '$2y$13$JQcmgGcqvAJXDnmXITZXTeQmiETA8kDOnuwnrPx6cjYNF5J9YrpJS', 3, 2, '2018-11-25 18:52:57', '2019-05-21 06:57:28', 'P92QE7yeaFXthQG8VB72WliBy1DUeNL1L6yiWtbKIxZyffT-1uZppmcizNPN', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (51, 435322452345, 1, 'Jorge Andres', 'Sepulveda Arredondo', '2147483647', '94595349539459349593', 'jj.asepulveda@gmail.com', '', '$2y$13$GziuuumVgQPW3iD.Z77//ecyeegKyn6YMnZzRJYgtUTETqitLfo2i', 3, 1, '2019-01-04 19:13:08', '2019-01-04 19:50:34', 'OWud1mxwDA67qvqikvPpIcCB-eDYFr8s9J89xEqSLVJNQBamflcyTI5vpEsF', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (52, 21223, 1, 'Camilo', 'Gaviria', '2147483647', '3249234234', 'carlosga@gmial.com', 'cra 95 # 48D 58', '$2y$13$Ja.0KHN0g9Gz2NVB.hTXTO8G.wTFo7XARW3AWxcPrkHkmcbTQHiO6', 4, 1, '2019-01-15 00:00:00', '2019-08-13 19:10:57', 'o7XSRWIt6AF6twz6ERRWgFTf00KI8k_ZoSn5cuWPeSr-njOMKdbd21RSPj3s', '', '', '');
INSERT INTO `actor` VALUES (53, 234123545, 1, 'oscar', 'mesa', '2147483647', '4444', 'oscarmhhhesa.elpoli@gmail.com', 'cra 95 # 48D 58', '$2y$13$4CAWGl.oNa.4sKrtFpBCVu1Cndupv1kD.bodlQigl83c/7eMQPChu', 2, 1, '2019-01-15 00:00:00', '2019-01-15 12:28:37', 'tGluXwaKtnzDFdTykd620mFeUIbe_C58YEYoYttfliKx23jEm5N7gzoHAwbF', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (54, 1152188864, 1, 'Calificador', 'Politécnico', '2147483647', '555555', 'oscarmesa.elpoli@gamil.com', 'cra 95 # 48D 58', '$2y$13$k036P/6NqFpnenBB.LGsquZRety8bev.ZRaofCSIoxC0e35v7xiES', 1, 1, '2019-01-15 00:00:00', '2019-02-19 14:57:43', 'y7P4h6jsKFt2Y418IP2A58ipApQ61lxfvLelQwGRf5ahPo28hrmKl1DiZbwi', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (55, 9999, 4, 'dfasdfaksdf', 'sadfkasdf', '23312', 'asdfasdf', 'asdfasd2f@gmail.com', 'sadfasdf', '$2y$13$QIfuMVqMVwSh4gNYYaJRtuhhVWN45C5t9njGkycsVeNhwTFw1Sok2', 3, 2, '2019-02-01 17:19:48', '2019-02-01 17:20:07', '5I8KrhHr2i9tetWb46uBfW1v-V1A8e0n3vq5TIOeKgivnXppkgEXg34A108s', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (56, 2334324123, 3, 'DANIEL', 'PEREZ', '34234234', 'ddsf32432432', 'oscarmesa.elpoli@gm2ail.com', 'asdfasdf', '$2y$13$ouQu8JttvjJ4hEnB2NzoiOh7UxzBSu7vrL3B2IfSPduc3UfmBETYi', 8, 1, '2019-02-01 18:15:59', '2019-08-13 17:34:32', '7RFvZwQ64lTy433Vz4X7BZsYx6EwXJveRKnCMMDCAjCNlkC3ASlV1oeOaPn3', '', '', '');
INSERT INTO `actor` VALUES (57, 2132132, 4, 'asdfasdf', 'asdfasdf', 'asd21', '32324', 'sdfklasdjf@coorreo.com', 'adfkasdf', '$2y$13$Jt0SqEMwXmPWUdMXcR7HX.HkN7IFSDjGzfF5BUkcQN2MaBEijw7.S', 3, 1, '2019-02-03 18:17:47', NULL, 'fbC4_4PrBiJIbGBdZRBw9_3Nj4w6XvJ7fsftSAA0Ub0lk9vyz6-gMqEii1E0', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (58, 8767876, 1, 'pepe', 'landa', '243234', '234234324', 'oscarmesa.elpoli@gmdddail.com', 'asdaf', '$2y$13$rI92VHKQ.M0IBdL89bX8IOWCVYrYyUjQCedVzj/Sppda9GCWkWOce', 3, 1, '2019-02-13 17:18:56', NULL, 'vhI-ZRHUWZ0ePJ8EyR8ubaVCjR7vsMbo2ZWcI80Yxnl-JZuKBG1GxAq44CWT', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (59, 9837438383, 1, 'Laura', 'Ospina Garcia', '93838', '324234234234', 'laura_ospina82122@elpoli.edu.co', '', '$2y$13$LVM9K0WVYEfvqezIMihHrOfYYEoCD3oo/dCzVxjmFCK3csjdsVz6W', 1, 1, '2019-03-05 09:07:19', '2019-03-05 09:54:37', 'OSdNstw9bmKrCJct-nySD9kNEfPCxPcZi9yJ_e2hY4A9G7uFiq56L2dTTIi7', NULL, NULL, NULL);
INSERT INTO `actor` VALUES (60, 342123444, 5, 'MAKRO', '', '39999333', '234324', 'makro@gmail.com', '', '$2y$13$i4Wahn4Bp.cMRT3zlJYHeeT3Et/swvoQG1bFlmXNZhgxrC47ueeNu', 11, 1, '2019-08-12 14:20:08', '2019-08-13 17:05:30', 'fHWni3T8ueFVgkHItTVzmtXNRXQLU5wslLgynpuKS1TRX0Nsn9JAuycdhXk5', 'http://www.ggol.com', 'carlos andrade', 'saludo');
INSERT INTO `actor` VALUES (61, 999999, 1, 'Isabel Cristina', 'Arango Palacio ', '', '310 8955195', 'isabelarango@elpoli.edu.co', '', '$2y$13$kiG7baVtXo60rfSY.wrUFeyfRB4qvTw/UGA4C6cGgKUAX.EkahXr2', 1, 1, '2019-08-22 02:35:44', NULL, 'MqErWMMuG8x_PXS13eWTC05wtN-i40kCKriqo5dPmYBEyazmpliJoaxf7HRD', '', '', '');

SET FOREIGN_KEY_CHECKS = 1;
