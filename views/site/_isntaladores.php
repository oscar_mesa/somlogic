<h2>Nodo Cliente para Windows</h2>
<p>Este archivo descarga la carpeta donde esta almacenado la información del nodo que sincroniza la BlockChain.</p>
<p>Al descargar el archio "Nodo Windows.rar" se visualizarán los siguientes archivos: </p>
<ol>
	<li>En la carpeta "nodo1" se encuentra almacenada la BlockChain.</li>
	<li>El archivo "genesis.json" se encuentra la configuración del primer bloque de la cadena.</li>
	<li>El ejecutable "geth.exe" es el software del servicio de la BlockChain.</li>
	<li>El ejecutable "Nodo instalación.bat", es el software encargado de instalar el nodo inicial "solo se ejecuta una sola vez".</li>
	<li>El ejecutable "Nodo ejecución.bat", es el software encargado de ejecutar el nodo.</li>
</ol>
<a href="documentos/Nodo Windows.rar">Descarga Nodo Cliente</a>
<br/>

<h2>Interfa gráfica MIST para la conexión del nodo principal</h2>

<p>MIST es un software con entorno gráfico que visualiza lo correspondiente al manejo de una BlockChain en Ethereum. Este programa es opcional para visualizar nuestro nodo privado.</p>

<p>Al descargar el archio "MIST Nodo.rar" se visualizarán los siguientes archivos: </p>
<ol>
	<li>El ejecutable "MIST Nodo.bat", es el software encargado de conectarse al nodo principal de forma gráfica.
</li>
</ol>
<ul>
<li><a href="https://github.com/ethereum/mist/releases/download/v0.11.1/Mist-installer-0-11-1.exe">Descarga MIST</a></li>
<li><a href="https://gethstore.blob.core.windows.net/builds/geth-windows-amd64-1.8.23-c9427004.exe">Descarga Geth</a></li>
<li><a href="documentos/MIST Nodo.rar">Descarga MIST Nodo</a></li>
</ul>