<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Restaurar Credenciales');
?>
<div class="login-box" style="width: auto;margin: 7% auto;padding-left: 15%;padding-right: 15%;">
    <h3>Hola <b><?= $model->DS_NOMBRES_ACTOR; ?></b>, por favor realiza el cambio de la contraseña. </h3>
    <div class="usuario-form box box-primary">
        <?php $form = yii\bootstrap\ActiveForm::begin(); ?>
        <div class="box-body table-responsive" style="overflow: hidden;">
            <div class="form-row">
                <div class="row">
                    <div class="col col-md-6">
                        <?= $form->field($model, 'DS_CONTRASENA')->passwordInput() ?>
                    </div>
                    <div class="col col-md-6">
                        <?= $form->field($model, 'CONFIRMAR_CONTRASENA')->passwordInput() ?>
                    </div>
                    <div style="display:none" class="col col-md-6">
                    <?= $form->field($model, 'ID_ACTOR')->hiddenInput() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php yii\bootstrap\ActiveForm::end(); ?>
</div>