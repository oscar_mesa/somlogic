<?php
use yii\helpers\Html;
use conquer\select2\Select2Widget;
use yii\helpers\ArrayHelper;
use yii\widgets\Breadcrumbs;
/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', 'Registro usuario');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Iniciar Sesión'), 'url' => ['site/login']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-box" style="width: auto;margin: 7% auto;padding-left: 15%;padding-right: 15%;">
    <section class="content-header" style="padding-left: 0px;">
    <h1>Registro de usuario</h1>

    <?=
        Breadcrumbs::widget(
            [
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]
        ) ?>
    </section>
    <div class="usuario-form box box-primary">
        <?php $form = yii\bootstrap\ActiveForm::begin(); ?>
        <div class="box-body table-responsive" style="overflow: hidden;">
            <div class="form-row">
                <div class="row">
                    <div class="col col-md-6">
                        <?= $form->field($model, 'NM_DOCUMENTO_ID')->textInput() ?>
                    </div>
                    <div class="col col-md-6">
                        <?=
                        $form->field($model, 'NM_TIPO_DOCUMENTO_ID')->widget(
                                Select2Widget::className(), [
                            'items' => ArrayHelper::map(\app\models\TipoDocumento::find()->all(), 'CS_TIPO_DOCUMENTO_ID', 'DS_NOMBRE_TIPO_DOCUMENTO'),
                            'options' => [
                                'style' => 'width:98%',
                            ]
                                ]
                        )
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <?= $form->field($model, 'DS_NOMBRES_ACTOR')->textInput(['maxlength' => true]) ?>
                    </div> 
                    <div class="col col-md-6">
                        <?= $form->field($model, 'DS_APELLIDOS_ACTOR')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row"> 
                    <div class="col col-md-6">
                        <?= $form->field($model, 'NM_TELEFONO')->textInput() ?>
                    </div>
                    <div class="col col-md-6">
                        <?= $form->field($model, 'NM_CELULAR')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <?= $form->field($model, 'DS_CORREO')->input("email",['maxlength' => true]) ?>
                    </div>
                    <div class="col col-md-6">
                        <?= $form->field($model, 'DS_DIRECCION')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <?= $form->field($model, 'DS_CONTRASENA')->passwordInput() ?>
                    </div>
                    <div class="col col-md-6">
                        <?= $form->field($model, 'CONFIRMAR_CONTRASENA')->passwordInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <?= $form->field($model, 'reCaptcha')->widget(\himiklab\yii2\recaptcha\ReCaptcha::className(),
                                ['siteKey' => '6LcP37MUAAAAAIQnX0VmzUagiqmGLQTZ_oIQggJt']) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success btn-flat']) ?>
        </div>
        <?php yii\bootstrap\ActiveForm::end(); ?>
    </div>
</div>