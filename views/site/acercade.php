<?php

$this->title = Yii::t('app', 'Acerca se');

echo yii\bootstrap\Tabs::widget([
    'items' => [
        [
            'label' => 'Manual',
            'content' => '<iframe src="https://docs.google.com/viewer?srcid=1X9lWag9Y43F1ndwn8MVdyhya591VdBAa&pid=explorer&efh=false&a=v&chrome=false&embedded=true" style="width: 100%; height: 650px"></iframe>',
            'active' => true
        ],
        [
            'label' => 'Video',
            'content' => '<div style="text-align: center;padding-top:30px;"><iframe width="560" height="315" src="https://www.youtube.com/embed/eNLr9kRW7rY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>',
        ],
    
    ],
]);
?>
