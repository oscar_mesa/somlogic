<?php

/* @var $this yii\web\View */

$this->title = 'Presentación';
?>
<style>

</style>
<div class="site-index">

    <!--<div class="jumbotron">
        <h1>Bienvenido!</h1>

        <p class="lead">Esta aplicación será util para la administración de productos, su venta y almacenamiento en la tienda.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div> -->

    <div class="body-content">

        <?php
use easyrider7522\yii2btswidget\Carousel;
?>
<div class="site-index">
    <div>
<?= Carousel::widget([
    'options'               => [                            // Widget container attributes.
        'id'                    => 'bootstrap-touch-slider',    // Slider (carousel) id.
        'class'                 => [                            // Array keys are used for possibility to override all the classes present by default.
            'bootstrap'             => 'carousel',                  // Bootstrap carousel style. Should not be changed.
            'widget'                => 'bs-slider',                 // Widget style.
            'effect'                => 'fade',                      // Slide effect.
            'controls'              => 'control-round',             // Left/Right controls styling.
            'indicators'            => 'indicators-line',           // Slide indicator styling.
        ],                                                      // Only the values are rendered (`class="carousel bs-slider fade control-round indicators-line"`).
        'data'                  => [                            // Data attibutes for Bootstrap JS.
            'ride'                  => 'carousel',                  // Animation starts at page load.
            'pause'                 => 'hover',                     // Cycling stops on hovering mouse.
            'interval'              => 'false',                     // Cycling delay.
        ],                                                      // For full specification see https://getbootstrap.com/docs/3.3/javascript/#carousel .
    ],
    'indicatorOptions'      => [                            // array|false Options of the wrapper of the slide indicators.
        'class'                 => 'carousel-indicators',
    ],
    'controlOptions'        => [                            // array|true|false Settings for left/right controls.
        'class'                 => 'carousel-control',          // Wrapper class.
        'leftArrow'             => 'angle-left',                // Font Awesome symbol for the left arrow.
        'rightArrow'            => 'angle-right',               // Font Awesome symbol for the right arrow.
    ],
    'slideWrapperOptions'   => [                            // Slide wrapper <div> options
        'class'                 => 'carousel-inner',
        'role'                  => 'listbox',
    ],
    'startSlide'            => 0,                           // The slide the animation starts with (index in the `slides` array).
    'slideOverlayOptions'   => [                            // The overlay <div> options
        'class'                 => 'bs-slider-overlay',         // Class of the overlay
    ],
    'slides'                => [                            // Array containing slides data.
        '@web/images/img70.jpg?v=1'=> [                           // Image web path. Aliases can be used, e.g. `@web/images/slider/slide1.jpeg`.
           // 'alt'                   => 'A carousel slide image 1',  // `alt` attribute contents of the <img> tag.
           /* 'textArea'              => [                            // array Settings for the overlaying text area.
                'align'                 => 'left',                      // string left|center|right Horizontal alignment of the text area.
                'header'                => 'Bootstrap Carousel',        // Header contents.
                'headerAnimation'       => 'zoomInRight',               // Header animation (see https://daneden.github.io/animate.css/ for variants).
                'text'                  => 'Bootstrap carousel now touch enabled slide.',   // Text paragraph contents.
                'textAnimation'         => 'fadeInLeft',                // Text paragraph animation.
                'buttons'               => [                            // array|false Some buttons below the text paragraph can be placed.
                    'select one'            => [                            // array Button caption (inner contents of the <a> tag).
                        'href'                  => '#',                         // Where the link points (`href` attribute of the <a> tag).
                        'type'                  => 'default',                   // In this case its class will become `btn btn-default` (see Bootstrap variants or define one).
                        'animation'             => 'fadeInLeft',                // Buttons can be also animated at appearance.
                        'target'                => '_blank',                    // Other acceptable <a> tag attributes can be added as done here.
                    ],
                    'select two'            => [
                        'href'                  => '#',
                        'type'                  => 'primary',
                        'animation'             => 'fadeInRight',
                        'target'                => '_blank',
                    ],
                ],
            ], */
        ],
        '@web/images/img40.jpg?v=1'=> [ ],
        //'@web/images/img42.jpg'=> [ ],
        '@web/images/img50.jpg?v=1'=> [ ],
        '@web/images/slide7.PNG?v=1'=> [ ],
      //  '@web/images/slide8.PNG'=> [ ],
       /* '@web/images/slide2.jpeg'=> [
            'alt'                   => 'A carousel slide image 2',
            'textArea'              => [
                'align'                 => 'center',
                'header'                => 'Bootstrap touch slider',
                'headerAnimation'       => 'flipInX',
                'text'                  => 'Make Bootstrap Better together.',
                'textAnimation'         => 'lightSpeedIn',
                'buttons'               => [
                    'select three'          => [
                        'url'                   => '#',
                        'type'                  => 'default',
                        'animation'             => 'fadeInUp',
                        'target'                => '_blank',
                    ],
                    'select four'           => [
                        'url'                   => '#',
                        'type'                  => 'primary',
                        'animation'             => 'fadeInDown',
                        'target'                => '_blank',
                    ],
                ],
            ],
        ],
        '@web/images/slide3.jpeg'=> [
            'alt'                   => 'A carousel slide image 3',
            'textArea'              => [
                'align'                 => 'right',
                'header'                => 'Beautiful Animations',
                'headerAnimation'       => 'zoomInLeft',
                'text'                  => 'Lots of css3 Animations to make slide beautiful.',
                'textAnimation'         => 'fadeInRight',
                'buttons'               => [
                    'select five'           => [
                        'url'                   => '#',
                        'type'                  => 'default',
                        'animation'             => 'fadeInLeft',
                        'target'                => '_blank',
                    ],
                    'select six'            => [
                        'url'                   => '#',
                        'type'                  => 'primary',
                        'animation'             => 'fadeInRight',
                        'target'                => '_blank',
                    ],
                ],
            ],
        ],*/
    ],
]);?>
    </div>
</div>

    </div>
</div>
