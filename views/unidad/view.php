<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UnidadProducto */

$this->title = $model->CS_UNIDAD_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Unidad Productos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unidad-producto-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->CS_UNIDAD_ID], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->CS_UNIDAD_ID], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'CS_UNIDAD_ID',
                'DS_NOMBRE_UNIDAD',
                'DS_DESCRIPCION_UNIDAD',
            ],
        ]) ?>
    </div>
</div>
