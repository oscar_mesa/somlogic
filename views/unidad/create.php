<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UnidadProducto */

$this->title = Yii::t('app', 'Unidad de Producto');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Unidad de Productos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unidad-producto-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
