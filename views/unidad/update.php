<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UnidadProducto */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Unidad Producto',
]) . $model->CS_UNIDAD_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Unidad Productos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->CS_UNIDAD_ID, 'url' => ['view', 'id' => $model->CS_UNIDAD_ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="unidad-producto-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
