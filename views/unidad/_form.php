<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UnidadProducto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="unidad-producto-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'DS_NOMBRE_UNIDAD')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'DS_DESCRIPCION_UNIDAD')->textInput(['maxlength' => true]) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat', "id" => 'btn_guardar_und']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
