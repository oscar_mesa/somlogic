<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\VwTotalIndicadoresCaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Vw Total Indicadores Cas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vw-total-indicadores-ca-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Vw Total Indicadores Ca'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'DS_NOMBRE_INDICADOR',
                'NM_RESULTADO_INDICADOR',
                'NM_PORCENTAJE_SIGNIFICANCIA',
                'ID_INDICADOR',
                'ETAPA',
                // 'ID_CADENA',
                // 'DS_FORMULA',
                // 'DS_NOMENCLATURA',
                // 'DEFINICION:ntext',
                // 'DS_AREA',
                // 'ID_INDICADOR_CADENA',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
