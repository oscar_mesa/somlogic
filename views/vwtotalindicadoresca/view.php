<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\VwTotalIndicadoresCa */

$this->title = $model->ID_INDICADOR_CADENA;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vw Total Indicadores Cas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vw-total-indicadores-ca-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID_INDICADOR_CADENA], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID_INDICADOR_CADENA], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'DS_NOMBRE_INDICADOR',
                'NM_RESULTADO_INDICADOR',
                'NM_PORCENTAJE_SIGNIFICANCIA',
                'ID_INDICADOR',
                'ETAPA',
                'ID_CADENA',
                'DS_FORMULA',
                'DS_NOMENCLATURA',
                'DEFINICION:ntext',
                'DS_AREA',
                'ID_INDICADOR_CADENA',
            ],
        ]) ?>
    </div>
</div>
