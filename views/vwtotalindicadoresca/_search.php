<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\VwTotalIndicadoresCaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vw-total-indicadores-ca-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'DS_NOMBRE_INDICADOR') ?>

    <?= $form->field($model, 'NM_RESULTADO_INDICADOR') ?>

    <?= $form->field($model, 'NM_PORCENTAJE_SIGNIFICANCIA') ?>

    <?= $form->field($model, 'ID_INDICADOR') ?>

    <?= $form->field($model, 'ETAPA') ?>

    <?php // echo $form->field($model, 'ID_CADENA') ?>

    <?php // echo $form->field($model, 'DS_FORMULA') ?>

    <?php // echo $form->field($model, 'DS_NOMENCLATURA') ?>

    <?php // echo $form->field($model, 'DEFINICION') ?>

    <?php // echo $form->field($model, 'DS_AREA') ?>

    <?php // echo $form->field($model, 'ID_INDICADOR_CADENA') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
