<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CampoCaculo */

$this->title = $model->ID_CAMPO;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Campos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campo-caculo-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID_CAMPO], ['class' => 'btn btn-primary btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ID_CAMPO',
                'DS_CAMPO_CALCULO',
            ],
        ]) ?>
    </div>
</div>
