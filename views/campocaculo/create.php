<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CampoCaculo */

$this->title = Yii::t('app', 'Crear Campo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Campo Caculos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campo-caculo-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
