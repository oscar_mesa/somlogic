<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CampoCaculo */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Campo Calculo',
]) . $model->ID_CAMPO;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Campo Caculos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_CAMPO, 'url' => ['view', 'id' => $model->ID_CAMPO]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="campo-caculo-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
