<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CampoCaculoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cálculos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campo-caculo-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                'ID_CAMPO',
                'DS_CAMPO_CALCULO',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}{update}',
                ],
            ],
            'exportConfig' => [
                 GridView::PDF => [],
                  GridView::EXCEL => [],
            ],
            'pjax' => false,
            'bordered' => true,
            'striped' => false,
            'responsive' => true,
            'floatHeaderOptions' => ['scrollingTop' => '50'],
            'panel' => [
                'type' => GridView::TYPE_INFO,
                'before'=> Html::a(Yii::t('app', 'Crear Campo'), ['create'], ['class' => 'btn btn-success btn-flat']) ,

            ], 
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
