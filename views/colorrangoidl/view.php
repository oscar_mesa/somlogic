<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ColorRangoIdl */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Color Rango Idls'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="color-rango-idl-view box box-primary">
    <div class="box-header">
        <?php if (is_null(Yii::$app->request->get('modal'))){ ?>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?php } ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ID',
                'iNDICADOR.DS_NOMBRE_INDICADOR',
                'RANGO1',
                'RANGO2',
                'COLOR',
            ],
        ]) ?>
    </div>
</div>
