<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ColorRangoIdl */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Color Rango IDL',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Color'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="color-rango-idl-update">

    <?php 
    	if(isset($createmodel)){

	    	echo $this->render('_form', [
		        'model' => $model,
		        'createmodel' => true
		    ]);
	    }else{
	    	echo $this->render('_form', [
		        'model' => $model,
		    ]);
	    }
		?>

</div>
