<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\color\ColorInput;
use conquer\select2\Select2Widget;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\ColorRangoIdl */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="color-rango-idl-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">
        <?php 
            if(isset($createmodel)){
                echo $form->field($model, 'ID_INDICADOR')->hiddenInput()->label(false);
            }else{
                echo $form->field($model, 'ID_INDICADOR')->widget(
                        Select2Widget::className(), [
                    'items' => ArrayHelper::map(\app\models\Indicador::find()->all(), 'ID_INDICADOR', 'DS_NOMBRE_INDICADOR'),
                    'options' => [
                        'style' => 'width:98%',
                    ]
                        ]
                );
            }
            ?>


        <?= $form->field($model, 'RANGO1')->textInput(["type"=>"number", "step"=>"any"]) ?>

        <?= $form->field($model, 'RANGO2')->textInput(["type"=>"number", "step"=>"any"]) ?>

        <?= $form->field($model, 'COLOR')->widget(ColorInput::classname(), [
                'options' => ['placeholder' => 'Seleccione color ...'],
            ]);

         ?>


    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
