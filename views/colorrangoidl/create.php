<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ColorRangoIdl */

$this->title = Yii::t('app', 'Crear Color');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Colores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="color-rango-idl-create">
	<?php if(isset($createmodel)){
    	echo $this->render('_form', [
    		'model' => $model,
    		'createmodel' => true
   		 ]);
    }else{
    	echo $this->render('_form', [
    		'model' => $model,
   		 ]);
    }
    ?>

</div>
