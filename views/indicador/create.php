<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Indicador */

$this->title = Yii::t('app', 'Crear Indicador');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Indicadores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicador-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
