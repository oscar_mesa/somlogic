<?php
use yii\web\JsExpression;
use yii\widgets\Pjax;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IndicadorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}

JS;

$this->registerJs("$(document).ready(function(e){
    $(document).on('click', '#btn_guardar_indicadores', function(e){
        $(this).css('pointer-events', 'none');
        $(this).parent().append(\"<img id='img_preload_modal' src='/dist/img/loading-plugin.gif'/>\");
    }); 
});");
?>
<div class="indicador-index box box-primary">
    <?= \yii\helpers\Html::beginForm(\yii\helpers\Url::to(['indicador/guardarindicadores']), 'post') ?>
    <div class="box-body table-responsive no-padding">

        <?= \kartik\grid\GridView::widget([
    'id'=>'indicadoresgrid',
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    /*'pjax' => true,
    'pjaxSettings' => [
        'options' => [
            'clientOptions' => [
                'enablePushState' => false,
                'enableReplaceState' => false,
                'timeout' => false
            ]
        ],
    ],*/

  //  'filterUrl'          => yii\helpers\Url::to(["indicador/indexmodal"]),
    'layout'=> "{summary}\n{items}\n{pager}",
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'CHK',
            'format' => 'raw',
            'contentOptions' => ['style'=>'text-align: center;vertical-align: middle;'],
            'value'=>function($row){
                return \yii\helpers\Html::checkBox('Indicador[]',false, ['value' => $row->ID_INDICADOR]); 
            },
        ],
        [
            'attribute' => 'ID_AREA',
            'filter'=>false,
            //'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
            'value' => function($data) {
                return is_object($data->aREA) ? $data->aREA->DS_AREA : '';
            },
           /* 'filter'=>\yii\helpers\ArrayHelper::map(app\models\Area::find()->all(),'ID_AREA','DS_AREA'),
            'filterWidgetOptions' => [
                'options' => ['prompt' => 'Buscar area'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
//                            'templateResult' => new JsExpression('formatRepo'),
//                            'templateSelection' => new JsExpression('formatRepoSelection'),
                ],
            ],  */
        ],
        [
            'attribute' => 'DS_NOMBRE_INDICADOR',
            'filter'=>false,
        ],
        [
            'attribute' => 'DS_FORMULA',
            'filter'=>false,
            'value' => function($row){
                foreach (explode('%', $row->DS_FORMULA) as $val) {
                    if (is_numeric($val)) {
                        $campo = \app\models\CampoCaculo::find()->where(['ID_CAMPO' => $val])->one();
                        if($campo){
                            $row->DS_FORMULA = str_replace('%'.$val.'%', $campo->DS_CAMPO_CALCULO, $row->DS_FORMULA);
                        }
                    }
                }
                return $row->DS_FORMULA;
            }
        ],
        [
            'attribute' => 'UNIDAD',
            'filter'=>false,
        ],
        [
            'attribute' => 'NM_ID_TIPO_IND',
            'filter'=>false,
            'value' => function($row){
                return $row->tIPOINDICADOR->DS_TIPO_IND;
            }
        ],
        [
            'attribute' => 'DS_NOMENCLATURA',
            'filter'=>false,
        ],
    ],
    'responsive' =>true,
    'pjax' =>true,
    'pjaxSettings' =>[
    'neverTimeout' => true,
    // 'beforeGrid' => 'My fancy content before.',
    // 'afterGrid' => 'My fancy content after.',
    ],
    'floatHeaderOptions'=>['scrollingTop'=>'50'],
    // 'showPageSummary' => true,
]);

?>
    <?= \yii\helpers\Html::hiddenInput('ID_ETP_CAD', $ID_ETP_CAD) ?>
    </div>
    <div class="box-footer">
        <?= \yii\helpers\Html::submitButton(Yii::t('app', 'Save'), ['id'=>'btn_guardar_indicadores', 'class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?= \yii\helpers\Html::endForm() ?>


</div>
