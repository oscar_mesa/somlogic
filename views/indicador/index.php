<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\IndicadorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Indicadores');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicador-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'ID_INDICADOR',
                'DS_NOMBRE_INDICADOR',
                [
                    'attribute' => 'ID_AREA',
                    'filter'=>\yii\helpers\ArrayHelper::map(app\models\Area::find()->all(),'ID_AREA','DS_AREA'),
                    'value' => function($row){
                        return $row->aREA->DS_AREA;
                    }
                ],
                'UNIDAD',
                [
                    'attribute' => 'NM_ID_TIPO_IND',
                    'filter'=>\yii\helpers\ArrayHelper::map(app\models\TipoIndicador::find()->all(),'ID_TIPO','DS_TIPO_IND'),
                    'value' => function($row){
                        return $row->tIPOINDICADOR->DS_TIPO_IND;
                    }
                ],
                [
                    'attribute' => 'DS_FORMULA',
                    'value' => function($row){
                        foreach (explode('%', $row->DS_FORMULA) as $val) {
                            if (is_numeric($val)) {
                                $campo = \app\models\CampoCaculo::find()->where(['ID_CAMPO' => $val])->one();
                                if($campo){
                                    $row->DS_FORMULA = str_replace('%'.$val.'%', $campo->DS_CAMPO_CALCULO, $row->DS_FORMULA);
                                }
                            }
                        }
                        return $row->DS_FORMULA;
                    }
                ],
                'DS_NOMENCLATURA',
                'DEFINICION:ntext',

                ['class' => 'yii\grid\ActionColumn'],
            ],
            'exportConfig' => [
                 GridView::PDF => [],
                  GridView::EXCEL => [],
            ],
            'pjax' => false,
            'bordered' => true,
            'striped' => false,
            'responsive' => true,
            'floatHeaderOptions' => ['scrollingTop' => '50'],
            'panel' => [
                'type' => GridView::TYPE_INFO,
                'before'=> Html::a(Yii::t('app', 'Crear Indicador'), ['create'], ['class' => 'btn btn-success btn-flat']) ,

            ], 
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
