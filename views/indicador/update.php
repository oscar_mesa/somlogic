<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Indicador */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Indicador',
]) . $model->DS_NOMBRE_INDICADOR;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Indicadores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_INDICADOR, 'url' => ['view', 'id' => $model->ID_INDICADOR]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="indicador-update">

    <?= $this->render('_form', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
