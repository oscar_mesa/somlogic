<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use lo\widgets\modal\ModalAjax;

/* @var $this yii\web\View */
/* @var $model app\models\Indicador */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs("$(function(){
       $(document).on('click', '.eliminarColorRango', function() {
        var deleteUrl = $(this).attr('delete-url');
        var pjaxContainer = $(this).attr('pjax-container');
        
        bootbox.confirm({
                title: 'Eliminar sede', 
                locale: 'es', 
                message: 'Realmente desea eliminar este color de rango?',
                callback: function(result) {                
                    if(result){
                       $.ajax({
                            url: deleteUrl,
                            type: 'post',
                            dataType: 'json',
                            error: function(xhr, status, error) {
                                alert('There was an error with your request.' + xhr.responseText);
                            }
                        }).done(function(data) {    
                            $.notify(data.mensaje, { type: 'success' });
                           $.pjax.reload({container: '#grid_colores', async:false});
                        });
                    }    
                },
                buttons: {
                    confirm: {
                        label: 'Eliminar'
                    },
                    cancel: {
                        label: 'Cancelar',
                    }
                },
        });
    });
  });  ");
?>

<div class="indicador-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'DS_NOMBRE_INDICADOR')->textInput(['maxlength' => true]) ?>


        <?= $form->field($model, 'ID_AREA')->dropDownList(\yii\helpers\ArrayHelper::map(app\models\Area::find()->all(),'ID_AREA','DS_AREA'), ['prompt' => 'Seleccione']) ?>

        <?= $form->field($model, 'NM_ID_TIPO_IND')->dropDownList(\yii\helpers\ArrayHelper::map(app\models\TipoIndicador::find()->all(),'ID_TIPO','DS_TIPO_IND'), ['prompt' => 'Seleccione']) ?>

        <?= $form->field($model, 'DS_FORMULA')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'UNIDAD')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'DS_NOMENCLATURA')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'DEFINICION')->textarea(['rows' => 6]) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php if (!$model->isNewRecord) { ?>
    <div class="cliente-form box box-primary">
         <div class="box-header with-border">
            <h4 style="text-align: center; display: initial;">Rango de colores</h4>
             <?php
                    echo ModalAjax::widget([
                        'id' => 'crearColorIndicador',
                        'header' => 'Nuevo Color',
                        'toggleButton' => [
                            'label' => 'Crear color',
                            'class' => 'btn btn-primary',
                            'style' => 'float: right;'
                        ],
                        'url' => \yii\helpers\Url::to(['colorrangoidl/createmodal', 'id_indicador' => $model->ID_INDICADOR]), // Ajax view with form to load
                        'ajaxSubmit' => true,
                        'size' => ModalAjax::SIZE_LARGE,
                        'events' => [
                            ModalAjax::EVENT_MODAL_SUBMIT => new \yii\web\JsExpression(' 
                                function(event, data, status, xhr, selector) {
                                     try{
                                        if(data.success == true){
                                            $.notify({"message":data.message,"icon":"fa fa-cubes","url":"","target":"_blank"}, {
                                                type: "success",
                                                align: "right",
                                                from: "top"
                                            });
                                            $("#crearColorIndicador").modal("toggle");
                                            $.pjax.reload({container: "#grid_colores", async:false});
                                        }
                                    }catch(e){
                                        console.log("No es json");
                                    }
                                }
                           '),
                        ]
                    ]);
                    ?>
        </div>
        <div class="box-body table-responsive">

            <div class="row">
                <?php
                    echo ModalAjax::widget([
                        'id' => 'actualizarColorIndicador',
                        'selector' => 'a.btnSede', // all buttons in grid view with href attribute
                        'options' => ['class' => 'header-primary'],
                        'pjaxContainer' => '#grid_sedes',
                        'events' => [
                            ModalAjax::EVENT_MODAL_SUBMIT => new \yii\web\JsExpression(' 
                            function(event, data, status, xhr, selector) {
                                try{
                                        if(data.success == true){
                                            $.notify({"message":data.message,"icon":"fa fa-cubes","url":"","target":"_blank"}, {
                                                type: "success",
                                                align: "right",
                                                from: "top"
                                            });
                                            $("#actualizarColorIndicador").modal("toggle");
                                            $.pjax.reload({container: "#grid_colores", async:false});
                                        }
                                    }catch(e){
                                        console.log("No es json");
                                    }
                            }
                        ')
                        ]
                    ]);
                    yii\widgets\Pjax::begin([
                        'id' => 'grid_colores',
                        'timeout' => 5000,
                    ]);
                ?>
                <div class="col col-md-12">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'layout' => "{items}\n{summary}\n{pager}",
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'ID',
                            'RANGO1',
                            'RANGO2',
                            'COLOR',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{eliminar}{editar}{ver}',
                                'buttons' => [
                                    'eliminar' => function ($url, $model) {

                                        return Html::a('<span  class="glyphicon glyphicon-trash"></span>', false, ['class' => 'eliminarColorRango', 'delete-url' => \yii\helpers\Url::to(['colorrangoidl/delete', 'id' => $model->ID]), 'pjax-container' => 'pjax-list', 'title' => Yii::t('app', 'Delete')]);
                                    },
                                            'editar' => function ($url, $model) {
                                              //  echo "<pre>";print_r($model);die;
                                        return Html::a('<span  class="glyphicon glyphicon-pencil"></span>', \yii\helpers\Url::to(['colorrangoidl/updatemodal', 'id' => $model->ID]), ['style' => 'margin-left: 10px;', 'class' => 'btnSede editarSede', 'title' => Yii::t('app', 'Editar')]);
                                    },
                                            'ver' => function ($url, $model) {

                                        return Html::a('<span  class="glyphicon glyphicon-eye-open"></span>', \yii\helpers\Url::to(['colorrangoidl/view', 'id' => $model->ID, 'modal' => true]), ['style' => 'margin-left: 10px;', 'class' => 'btnSede editarSede', 'title' => Yii::t('app', 'Ver sede')]);
                                    }
                                        ]
                            ]
                        ],
                        'pjax' => true,
                        'bordered' => true,
                        'striped' => false,
                        'condensed' => false,
                        'responsive' => true,
                        'hover' => true,
                        //'floatHeader' => true,
                        'floatHeaderOptions' => ['scrollingTop' => '50'],
                        'showPageSummary' => true,
                         'panel' => [
                           'type' => GridView::TYPE_INFO,
                        ], 
                    ]); ?>
                </div>
                <?php Pjax::end(); ?>

                </div>
            </div>
        </div>
    </div>

<?php } ?>