<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IndicadorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="indicador-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ID_INDICADOR') ?>

    <?= $form->field($model, 'DS_NOMBRE_INDICADOR') ?>

    <?= $form->field($model, 'ID_AREA') ?>

    <?= $form->field($model, 'DS_FORMULA') ?>

    <?= $form->field($model, 'DS_NOMENCLATURA') ?>

    <?php // echo $form->field($model, 'DEFINICION') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
