<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Indicador */

$this->title = $model->DS_NOMBRE_INDICADOR;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Indicadores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indicador-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID_INDICADOR], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID_INDICADOR], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ID_INDICADOR',
                'DS_NOMBRE_INDICADOR',
                'aREA.DS_AREA',
                [
                    'attribute' => 'DS_FORMULA',
                    'value' => function($row){
                        foreach (explode('%', $row->DS_FORMULA) as $val) {
                            if (is_numeric($val)) {
                                $campo = \app\models\CampoCaculo::find()->where(['ID_CAMPO' => $val])->one();
                                if($campo){
                                    $row->DS_FORMULA = str_replace('%'.$val.'%', $campo->DS_CAMPO_CALCULO, $row->DS_FORMULA);
                                }
                            }
                        }
                        return $row->DS_FORMULA;
                    }
                ],
                'DS_NOMENCLATURA',
                'UNIDAD',
                [
                    'attribute' => 'NM_ID_TIPO_IND',
                    'value' => function($row){
                        return $row->tIPOINDICADOR->DS_TIPO_IND;
                    }
                ],
                'DEFINICION:ntext',
                [
                    'attribute' => 'NM_ELIMINADO',
                    'value' => function($row){
                        return $row->NM_ELIMINADO==0?'No':'Si';
                    }
                ],
            ],
        ]) ?>
    </div>
</div>
