<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PedidoProductoTerminado */

$this->title = Yii::t('app', 'Create Pedido Producto Terminado');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pedido Producto Terminados'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedido-producto-terminado-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
