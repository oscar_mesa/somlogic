<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PedidoProductoTerminado */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Pedido Producto Terminado',
]) . $model->ID_PEDIDO;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pedido Producto Terminados'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_PEDIDO, 'url' => ['view', 'id' => $model->ID_PEDIDO]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pedido-producto-terminado-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
