<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PedidoProductoTerminadoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pedido-producto-terminado-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ID_PEDIDO') ?>

    <?= $form->field($model, 'ID_EMPRESA') ?>

    <?= $form->field($model, 'ID_PRODUCTO') ?>

    <?= $form->field($model, 'CANTIDAD') ?>

    <?= $form->field($model, 'PRECIO') ?>

    <?php // echo $form->field($model, 'SUB_TOTAL') ?>

    <?php // echo $form->field($model, 'IVA') ?>

    <?php // echo $form->field($model, 'TOTAL') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
