<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use conquer\select2\Select2Widget;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\PedidoProductoTerminado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pedido-producto-terminado-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'ID_PEDIDO')->textInput() ?>


        <?=
            $form->field($model, 'ID_EMPRESA')->widget(
                    Select2Widget::className(), [
                'items' => ArrayHelper::map(\app\models\Empresa::find()->all(), 'ID_EMPRESA', 'DS_NOMBRES_REPRESENTANTE'),
                'options' => [
                    'style' => 'width:98%',
                ]
                    ]
            )
            ?>


        <?=
            $form->field($model, 'ID_PRODUCTO')->widget(
                    Select2Widget::className(), [
                'items' => ArrayHelper::map(\app\models\Producto::find()->all(), 'CS_PRODUCTO_ID', 'DS_NOMBRE_PRODUCTO'),
                'options' => [
                    'style' => 'width:98%',
                ]
                    ]
            )
            ?>
        <?= $form->field($model, 'CANTIDAD')->textInput() ?>

        <?= $form->field($model, 'PRECIO')->textInput() ?>

        <?= $form->field($model, 'SUB_TOTAL')->textInput() ?>

        <?= $form->field($model, 'IVA')->textInput() ?>

        <?= $form->field($model, 'TOTAL')->textInput() ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
