<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PedidoDetalle */

$this->title = Yii::t('app', 'Create Pedido Detalle');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pedido Detalles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedido-detalle-create">

    <?= $this->render('_form', [
        'producto' => $producto,
        'model' => $model,
    ]) ?>

</div>
