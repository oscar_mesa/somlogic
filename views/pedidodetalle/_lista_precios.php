<?php

use yii\web\JsExpression;

$resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;

$dataProviderListaPrecios->pagination = false;
?>

<?=

\kartik\grid\GridView::widget([
    'id' => 'grid_lista_precios',
    'dataProvider' => $dataProviderListaPrecios,
    'filterModel' => $searchModelListaPrecios,
    'layout' => "{items}\n{summary}\n{pager}",
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
//                            'ID_LISTA',
//                            'ID_CLIENTE',
//                            'ID_PRODUCTO',
        [
            'attribute' => 'ID_PRODUCTO',
            'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
            'value' => function($data) {
                return is_object($data->pRODUCTO) ? $data->pRODUCTO->DS_NOMBRE_PRODUCTO : '';
            },
            'filter' => false,
            'enableSorting' => false
        ],
//      [                      'FECHA_CREACION',
        [
            'attribute' => 'PRECIO',
            'filter' => false,
            'enableSorting' => false
        ]
    ],
    'pjax' => true,
    'pjaxSettings' => [
        'options' => [
            'clientOptions' => [
                'enablePushState' => false,
            ]
        ],
    ]
]);
