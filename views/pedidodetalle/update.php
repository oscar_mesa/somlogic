<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PedidoDetalle */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Pedido Detalle',
]) . $model->NM_ID_DETALLE_PEDIDO;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pedido Detalles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NM_ID_DETALLE_PEDIDO, 'url' => ['view', 'id' => $model->NM_ID_DETALLE_PEDIDO]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pedido-detalle-update">

    <?= $this->render('_form', [
        'model' => $model,
        'producto' => $producto,
        'precioLista' => $precioLista
    ]) ?>

</div>
