<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PedidoDetalleSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pedido-detalle-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'NM_ID_DETALLE_PEDIDO') ?>

    <?= $form->field($model, 'CS_PEDIDO_ID') ?>

    <?= $form->field($model, 'NM_CANTIDAD_COMPRA') ?>

    <?= $form->field($model, 'CS_PRODUCTO_ID') ?>

    <?= $form->field($model, 'NM_PRECIO_TOTAL_PRODUCTO') ?>

    <?php // echo $form->field($model, 'NM_PRECIO_UNITARIO') ?>

    <?php // echo $form->field($model, 'DS_UQ_SESSION') ?>

    <?php // echo $form->field($model, 'NM_USUARIO_CREADOR') ?>

    <?php // echo $form->field($model, 'PORCENTAJE_IVA') ?>

    <?php // echo $form->field($model, 'PORCENTAJE_DESCUENTO') ?>

    <?php // echo $form->field($model, 'NM_VALOR_IVA') ?>

    <?php // echo $form->field($model, 'NM_SUB_TOTAL') ?>

    <?php // echo $form->field($model, 'VALOR_DESCUENTO') ?>

    <?php // echo $form->field($model, 'DT_FECHA_CREACION') ?>

    <?php // echo $form->field($model, 'FC') ?>

    <?php // echo $form->field($model, 'MG') ?>

    <?php // echo $form->field($model, 'FK_UNIDAD') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
