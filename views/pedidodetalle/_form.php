<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\PedidoDetalle */
/* @var $form yii\widgets\ActiveForm */
$this->registerJs('$("#producto_pedido").on("afterValidate", function (event, messages) {
            var attributes = $(this).data().attributes; // to get the list of attributes that has been passed in attributes property
            var settings = $(this).data().settings; // to get the settings
            $("#img_preload_modal").remove();
            $(".btn_guardar_modal").css("pointer-events", "auto");
});
$("#pedidodetalle-nm_cantidad_compra").change(function(){
    if($(this).val().length > 0 && $("#pedidodetalle-nm_precio_unitario").val().length > 0){
        calcular_pedido_detalle();
    }
});

$("#pedidodetalle-porcentaje_descuento").change(function(){
    if($(this).val().length > 0 && $("#pedidodetalle-nm_precio_unitario").val().length > 0){
        calcular_pedido_detalle();
    }
});

$("#pedidodetalle-nm_precio_unitario").change(function(){
    if($(this).val().length > 0 && $("#pedidodetalle-nm_cantidad_compra").val().length > 0){
        calcular_pedido_detalle();
    }
});

function calcular_pedido_detalle(){
    var total = 0, subtotal=0;
    subtotal = parseFloat($("#pedidodetalle-nm_precio_unitario").val()) * parseFloat($("#pedidodetalle-nm_cantidad_compra").val());
    total = subtotal - (subtotal*$("#pedidodetalle-porcentaje_descuento").val()/100);
    $("#pedidodetalle-nm_precio_total_producto").val(total);
}

');
$format = <<< SCRIPT

function format(item) {
    if(item.id != undefined && item.id.length > 0){
        return item.text;
    }else{
        return item.text;
    }

}
    
SCRIPT;

$resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;
?>

<div class="pedido-detalle-form box box-primary">

    <?php
    $form = ActiveForm::begin([
                'id' => 'producto_pedido',
//                'enableAjaxValidation' => true,
                'enableClientValidation' => true,
               
    ]);
    ?>
    <div class="box-body table-responsive">


    <?= $form->field($model, 'CS_PEDIDO_ID')->hiddenInput()->label(false) ?>


        <?= $form->field($model, 'DS_PRODUCTO')->textInput(['readonly' => true, 'value' => $producto->DS_NOMBRE_PRODUCTO]) ?>
        <?= $form->field($model, 'CS_PRODUCTO_ID')->hiddenInput()->label(false) ?>

        <?=
        $form->field($model, 'FK_UNIDAD')->widget(Select2::classname(), [
            'initValueText' => is_object($model->fKUNIDAD) ? $model->fKUNIDAD->DS_NOMBRE_UNIDAD : "",
            //'data' => \yii\helpers\ArrayHelper::map(app\models\Producto::find()->all(),'CS_PRODUCTO_ID','DS_NOMBRE_PRODUCTO'),                        
            'options' => ['placeholder' => 'Seleccione unidad'],
            'pluginOptions' => [
                'allowClear' => true,
                'ajax' => [
                    'url' => yii\helpers\Url::to(['unidad/obtenerunidadesselect2']),
                    'dataType' => 'json',
                    'delay' => 250,
                    'data' => new yii\web\JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                    'processResults' => new yii\web\JsExpression($resultsJs),
                    'cache' => true
                ],
                'escapeMarkup' => new yii\web\JsExpression('function (markup) { return markup; }'),
//                            'templateResult' => new JsExpression('formatRepo'),
//                            'templateSelection' => new JsExpression('formatRepoSelection'),
            ],
        ]);
        ?>

        <?= $form->field($model, 'NM_CANTIDAD_COMPRA')->textInput(['type' => 'number']) ?>
        
        <div style="display: <?= Yii::$app->user->can('Cliente')?'none':'block' ?>">
        <?php if($model->isNewRecord){ ?>
            <?= $form->field($model, 'NM_PRECIO_UNITARIO')->textInput(['type' => 'number']) ?>
        <?php }else{ ?>
            <?= $form->field($model, 'NM_PRECIO_UNITARIO')->textInput(['type' => 'number', 'value' => (is_null($model->NM_PRECIO_UNITARIO) || $model->NM_PRECIO_UNITARIO == 0)?(!is_null($precioLista)?$precioLista->PRECIO:''):$model->NM_PRECIO_UNITARIO]) ?>
        <?php } ?>
        </div>

        <?= $form->field($model, 'PORCENTAJE_DESCUENTO')->textInput(['type' => 'number']) ?>

         <div class="row" style="display: <?= Yii::$app->user->can('Cliente')?'none':'block' ?>">
            <div class="col col-md-9">
        <?= $form->field($model, 'NM_PRECIO_TOTAL_PRODUCTO')->textInput(['value' => empty($model->NM_PRECIO_TOTAL_PRODUCTO) ? 0 : $model->NM_PRECIO_TOTAL_PRODUCTO, 'readonly' => true]) ?>
            </div>
            <div class="col col-md-3"><br/>
                  <?= $form->field($model, 'CALCULO_MANUAL')->checkboxList([$model->getAttributeLabel('CALCULO_MANUAL')], ['onchange' => new yii\web\JsExpression(' '
                          . 'if($("[name=\'PedidoDetalle[CALCULO_MANUAL][]\']").prop("checked")){'
                          . '   $("#pedidodetalle-nm_precio_total_producto").attr("readonly",false);'
                          . '}else{'
                          . '   $("#pedidodetalle-nm_precio_total_producto").attr("readonly",true);'
                          . '}'
                          . ' '),])->label(false) ?>
             </div>
         </div>
        <?= $form->field($model, 'DS_UQ_SESSION')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'PORCENTAJE_IVA')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'NM_VALOR_IVA')->textInput(["style" => "display:none"])->label(false) ?>

        <?= $form->field($model, 'NM_SUB_TOTAL')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'VALOR_DESCUENTO')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'DT_FECHA_CREACION')->hiddenInput(['value' => date('Y-m-d')])->label(false) ?>


        <?= $form->field($model, 'DS_OBSERVACION')->textArea(['maxlength' => true]) ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat btn_guardar_modal']) ?>
    </div>
<?php ActiveForm::end(); ?>
</div>
