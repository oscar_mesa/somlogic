<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PedidoDetalleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pedido Detalles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedido-detalle-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Pedido Detalle'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'NM_ID_DETALLE_PEDIDO',
                'CS_PEDIDO_ID',
                'NM_CANTIDAD_COMPRA',
                'CS_PRODUCTO_ID',
                'NM_PRECIO_TOTAL_PRODUCTO',
                // 'NM_PRECIO_UNITARIO',
                // 'DS_UQ_SESSION',
                // 'NM_USUARIO_CREADOR',
                // 'PORCENTAJE_IVA',
                // 'PORCENTAJE_DESCUENTO',
                // 'NM_VALOR_IVA',
                // 'NM_SUB_TOTAL',
                // 'VALOR_DESCUENTO',
                // 'DT_FECHA_CREACION',
                // 'FC',
                // 'MG',
                // 'FK_UNIDAD',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
