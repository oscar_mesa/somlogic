<?php
use kartik\tabs\TabsX;

echo TabsX::widget([
    'position' => TabsX::POS_ABOVE,
    'align' => TabsX::ALIGN_LEFT,
    'items' => [
        [
            'label' => 'Producto',
            'content' => $this->render('_form', [
                'producto' => $producto,
                'model' => $model,
            ]),
            'active' => true,
        ],
        [
            'label' => 'Lista de precios',
            'content' => $this->render('_lista_precios', [
                        'searchModelListaPrecios' => $searchModelListaPrecios,
                        'dataProviderListaPrecios' => $dataProviderListaPrecios,
            ]),
            'headerOptions' => ['style' => 'font-weight:bold'],
            'options' => ['id' => 'productos_agregados'],
        ],
    ],
    'bordered' => true,
]);
