<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\PedidoDetalle */

$this->title = $model->NM_ID_DETALLE_PEDIDO;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pedido Detalles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedido-detalle-view box box-primary">
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'CS_PRODUCTO_ID',
                    'value' => !is_null($model->cSPRODUCTO)?$model->cSPRODUCTO->DS_NOMBRE_PRODUCTO:''
                ],
                'NM_CANTIDAD_COMPRA',
                [
                    'attribute' =>'NM_PRECIO_TOTAL_PRODUCTO',
                    'visible' => !Yii::$app->user->can('Cliente')
                ],
                [ 
                    'attribute' => 'PORCENTAJE_DESCUENTO',
                    'value' => function($row){
                        $formatter = \Yii::$app->formatter;
                        return $formatter->asPercent(($row->PORCENTAJE_DESCUENTO/100), 2);
                    }
                ],
                [
                    'attribute' =>'NM_PRECIO_UNITARIO',
                    'visible' => !Yii::$app->user->can('Cliente')
                ],
                'DT_FECHA_CREACION',
                [
                    'attribute' => 'FK_UNIDAD',
                    'value' => $model->fKUNIDAD->DS_NOMBRE_UNIDAD
                ],
                'DS_OBSERVACION',
               /* [
                    'attribute' => 'NM_USUARIO_MODIFICA',
                    'value' => is_object($model->nMUSUARIOMODIFICA)?$model->nMUSUARIOMODIFICA->DS_NOMBRES_ACTOR:''
                ], */
                'FC_FECHA_MODIFICACION',
            ],
        ]) ?>
    </div>
</div>