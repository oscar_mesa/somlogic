<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\TipoActor;

/* @var $this yii\web\View */
/* @var $model app\models\Producto */

$this->title = $model->CS_PRODUCTO_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Productos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->CS_PRODUCTO_ID], ['class' => 'btn btn-primary btn-flat']) ?>
       <?php if(Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR){ ?>
         <?= Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->CS_PRODUCTO_ID], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('app', '¿Estás seguro de que quieres eliminar este elemento?'),
                'method' => 'post',
            ],
        ]) ?>
       <?php }?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'CS_PRODUCTO_ID',
                'DS_CODIGO_PRODUCTO',
                'DS_NOMBRE_PRODUCTO',
                [
                    'attribute' => 'FK_UNIDAD',
                    'value' => $model->fKUNIDAD->DS_DESCRIPCION_UNIDAD."(".$model->fKUNIDAD->DS_NOMBRE_UNIDAD.")"
                ],
                'DT_FECHA_CREACION',
                'IVA',
                'PORCENTAJE_DESCUENTO',
                [
                  'attribute' => 'NM_ID_PROVEEDOR',
                  'value' => is_object($model->nMIDPROVEEDOR)?$model->nMIDPROVEEDOR->DS_NOMBRES_ACTOR:""
                ],
                'DS_DESCRIPCION_PRODUCTO',
                //'NM_ELIMINADO',
            ],
        ]) ?>
    </div>
</div>
