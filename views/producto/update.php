<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Producto */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Producto',
]) . $model->CS_PRODUCTO_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Productos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->CS_PRODUCTO_ID, 'url' => ['view', 'id' => $model->CS_PRODUCTO_ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="producto-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
