<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Producto */
/* @var $form yii\widgets\ActiveForm */


$format = <<< SCRIPT

function format(item) {
    if(item.id != undefined && item.id.length > 0){
        return item.text;
    }else{
        return item.text;
    }

}
    
SCRIPT;

$resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;

$this->registerJs($format, yii\web\View::POS_HEAD);
$unidades = [];

foreach (\app\models\UnidadProducto::find(['ESTADO' => 'activo'])->all() as $unidad) {
    $unidades[$unidad->CS_UNIDAD_ID] = $unidad->DS_DESCRIPCION_UNIDAD . "(" . $unidad->DS_NOMBRE_UNIDAD . ")";
}
?>

<div class="producto-form box box-primary">
    <?php
    $form = ActiveForm::begin([
//        'enableAjaxValidation' => true,
    ]);
    ?>
    <div class="box-body table-responsive">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'DS_CODIGO_PRODUCTO', ['enableAjaxValidation' => true])->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'DS_NOMBRE_PRODUCTO')->textInput(['maxlength' => true]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <?=
                $form->field($model, 'FK_UNIDAD')->widget(Select2::classname(), [
                    'initValueText' => is_object($model->fKUNIDAD) ? $model->fKUNIDAD->DS_NOMBRE_UNIDAD : "",
                    //'data' => \yii\helpers\ArrayHelper::map(app\models\Producto::find()->all(),'CS_PRODUCTO_ID','DS_NOMBRE_PRODUCTO'),                        
                    'options' => ['placeholder' => 'Seleccione unidad'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'ajax' => [
                            'url' => yii\helpers\Url::to(['unidad/obtenerunidadesselect2']),
                            'dataType' => 'json',
                            'delay' => 250,
                            'data' => new yii\web\JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
                            'processResults' => new yii\web\JsExpression($resultsJs),
                            'cache' => true
                        ],
                        'escapeMarkup' => new yii\web\JsExpression('function (markup) { return markup; }'),
//                            'templateResult' => new JsExpression('formatRepo'),
//                            'templateSelection' => new JsExpression('formatRepoSelection'),
                    ],
                ]);
                ?> 

            </div>
            <div class="col-md-2" style="margin-top: 22px;">                
                <?php
                echo lo\widgets\modal\ModalAjax::widget([
                    'id' => 'createUnidad',
                    'header' => 'Nueva Unidad',
                    'toggleButton' => [
                        'label' => 'Crear unidad',
                        'class' => 'btn btn-primary pull-right'
                    ],
                    'url' => \yii\helpers\Url::to(['unidad/createmodal']), // Ajax view with form to load
                    'ajaxSubmit' => true, // Submit the contained form as ajax, true by default
                        // ... any other yii2 bootstrap modal option you need
                ]);
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?=
                $form->field($model, 'NM_ID_PROVEEDOR')->widget(Select2::classname(), [
                    'initValueText' => is_object($model->nMIDPROVEEDOR) ? $model->nMIDPROVEEDOR->DS_NOMBRES_ACTOR : "",
                    //'data' => \yii\helpers\ArrayHelper::map(app\models\Producto::find()->all(),'CS_PRODUCTO_ID','DS_NOMBRE_PRODUCTO'),                        
                    'options' => ['placeholder' => 'Seleccione el proveedor'],
                    'pluginEvents' => [
                      
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'ajax' => [
                            'url' => yii\helpers\Url::to(['actor/obteneractortiposelect2']),
                            'dataType' => 'json',
                            'delay' => 250,
                            'data' => new yii\web\JsExpression('function(params) { return {q:params.term, page: params.page, tipo: 4}; }'),
                            'processResults' => new yii\web\JsExpression($resultsJs),
                            'cache' => true
                        ],
                        'escapeMarkup' => new yii\web\JsExpression('function (markup) { return markup; }'),
//                            'templateResult' => new JsExpression('formatRepo'),
//                            'templateSelection' => new JsExpression('formatRepoSelection'),
                    ],
                ]);
                ?>
                
                <?= \yii\bootstrap4\Html::hiddenInput('NOMBRE_PREOVEEDOR', (is_object($model->nMIDPROVEEDOR) ? $model->nMIDPROVEEDOR->DS_NOMBRES_ACTOR : ""), ['id' => 'NOMBRE_PREOVEEDOR']) ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'IVA')->textInput(['type' => 'number']) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'PORCENTAJE_DESCUENTO')->textInput(['type' => 'number']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'DS_DESCRIPCION_PRODUCTO')->textarea(['maxlength' => true]) ?>
            </div>
        </div>
    </div>
    <div class="box-footer"> 
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
