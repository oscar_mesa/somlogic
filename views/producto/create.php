<?php

use yii\helpers\Html;
use app\models\TipoUsuario;

/* @var $this yii\web\View */
/* @var $model app\models\Producto */

$this->title = Yii::t('app', 'Crear Producto');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Productos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
