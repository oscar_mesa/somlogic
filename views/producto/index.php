<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use app\models\TipoUsuario;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Productos');
$this->params['breadcrumbs'][] = $this->title;
$template = "";
$template = '{view}{update}{delete}';
?>
<div class="producto-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'DS_CODIGO_PRODUCTO',
                'DS_NOMBRE_PRODUCTO',
                'DS_DESCRIPCION_PRODUCTO',
//                'REFERENTE',
                /*[
                    'attribute' => 'IVA',
                    'value' => function($data) {
                        return $data->IVA . "%";
                    }
                ],*/
                [ 
                    'attribute'=>'FK_UNIDAD',
                    'filter'=>\yii\helpers\ArrayHelper::map(app\models\UnidadProducto::find()->all(),'CS_UNIDAD_ID','DS_NOMBRE_UNIDAD'),

                    'value'=>function($row){
                        return $row->fKUNIDAD->DS_NOMBRE_UNIDAD;
                    }
                ],
                // 'DT_FECHA_CREACION',
                // 'NM_ELIMINADO',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => $template,
                ],
            ],
            'exportConfig' => [
                 GridView::PDF => [],
                  GridView::EXCEL => [],
            ],
            'pjax' => false,
            'bordered' => true,
            'striped' => false,
            'responsive' => true,
            'floatHeaderOptions' => ['scrollingTop' => '50'],
            'panel' => [
                'type' => GridView::TYPE_INFO,
                'before'=> Html::a(Yii::t('app', 'Crear Producto'), ['create'], ['class' => 'btn btn-success btn-flat']) ,

            ], 
        ]);
        ?>
    </div>
    <?php Pjax::end(); ?>
</div>
