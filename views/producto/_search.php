<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="producto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'CS_PRODUCTO_ID') ?>

    <?= $form->field($model, 'DS_CODIGO_PRODUCTO') ?>

    <?= $form->field($model, 'DS_NOMBRE_PRODUCTO') ?>

    <?= $form->field($model, 'DS_DESCRIPCION_PRODUCTO') ?>

    <?php // echo $form->field($model, 'FK_UNIDAD') ?>

    <?php // echo $form->field($model, 'DT_FECHA_CREACION') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
