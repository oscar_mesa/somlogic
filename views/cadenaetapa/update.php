<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CadenaEtapa */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Cadena Etapa',
]) . $model->ID_ETP_CAD;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cadena Etapas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_ETP_CAD, 'url' => ['view', 'id' => $model->ID_ETP_CAD]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cadena-etapa-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
