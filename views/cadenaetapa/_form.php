<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CadenaEtapa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cadena-etapa-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'ID_ETAPA')->textInput() ?>

        <?= $form->field($model, 'ID_CADENA')->textInput() ?>

        <?= $form->field($model, 'ID_ACTOR')->textInput() ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
