<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CadenaEtapa */

$this->title = $model->ID_ETP_CAD;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cadena Etapas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cadena-etapa-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID_ETP_CAD], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID_ETP_CAD], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ID_ETP_CAD',
                'ID_ETAPA',
                'ID_CADENA',
                'ID_ACTOR',
            ],
        ]) ?>
    </div>
</div>
