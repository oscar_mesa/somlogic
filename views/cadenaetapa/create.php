<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CadenaEtapa */

$this->title = Yii::t('app', 'Create Cadena Etapa');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cadena Etapas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cadena-etapa-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
