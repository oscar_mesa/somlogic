<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CadenaEtapaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cadena-etapa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ID_ETP_CAD') ?>

    <?= $form->field($model, 'ID_ETAPA') ?>

    <?= $form->field($model, 'ID_CADENA') ?>

    <?= $form->field($model, 'ID_ACTOR') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
