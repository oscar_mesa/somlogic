<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use lo\widgets\modal\ModalAjax;
/* @var $this yii\web\View */
/* @var $model app\models\Pedido */

$this->title = "Pedido ".$model->CS_PEDIDO_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pedidos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedido-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->CS_PEDIDO_ID], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('app', 'Anular'), ['delete', 'id' => $model->CS_PEDIDO_ID], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('app', 'Esta seguro que desea anular este pedido?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'CS_PEDIDO_ID',
                'DS_CODIGO_PEDIDO',
                [
                    'attribute' => 'NM_USUARIO_CREADOR_ID',
                    'value' => is_object($model->nMACTORCREADOR)?$model->nMACTORCREADOR->DS_NOMBRES_ACTOR.' '.$model->nMACTORCREADOR->DS_APELLIDOS_ACTOR:'',
                ],
                'DS_NOTAS_PEDIDO:ntext',
                'NM_PRECIO_SUBTOTAL',
                'NM_PRECIO_DESCUENTO',
                'NM_PRECIO_TOTAL',
                'NM_PRECIO_IVA',
                'DT_FECHA_CREACION',
                [
                    'attribute' => 'NM_EMPRESA',
                    'value' => is_object($model->nMEMPRESA)?$model->nMEMPRESA->DS_NOMBRES_ACTOR.' '.$model->nMEMPRESA->DS_APELLIDOS_ACTOR:'',
                ],
                [
                    'attribute' => 'ID_CLIENTE_FINAL',
                    'value' => is_object($model->iDCLIENTEFINAL)?$model->iDCLIENTEFINAL->DS_NOMBRES_ACTOR.' '.$model->iDCLIENTEFINAL->DS_APELLIDOS_ACTOR:'',
                ],
                [
                    'attribute' => 'ID_ESTADO',
                    'value' => $model->eSTADO->DES_ESTADO,
                ],
                [ 
                    'attribute' => 'NM_PORCENTAJE_DESCUENTO',
                    'value' => function($row){
                        $formatter = \Yii::$app->formatter;
                        return $formatter->asPercent(($row->NM_PORCENTAJE_DESCUENTO/100), 2);
                    }
                ],
                [
                    'attribute' => 'PORCENTAJE_IVA',
                    'value' => function($row){
                        $formatter = \Yii::$app->formatter;
                        return $formatter->asPercent(($row->PORCENTAJE_IVA/100), 2);
                    }
                ]
            ],
        ]) ?>
    </div>
</div>
<div class="pedido-view box box-primary">
    <div class="box-header"><h4>Productos del pedido</h4></div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-responsive table-striped table-bordered" cellspacing="0" style="width: 100%; text-align: left; font-size: 10pt;">
            <thead>
        <tr>
            <th style="width: 10%;text-align:center" class='midnight-blue'>C&Oacute;DIGO</th>
            <th style="width: 15%" class='midnight-blue'>DESCRIPCION</th>
            <th style="width: 15%;text-align:center" class='midnight-blue'>CANT.</th>
            <th style="width: 15%;text-align:center" class='midnight-blue'>Ver</th>
        </tr>
        </thead>
        <tbody><pre>
            <?php 
           // print_r($model->pedidoDetalles);
                 foreach ($model->pedidoDetalles as $detalle) {
                   // var_dump($detalle);die;
            ?>
                <tr>
                    <td style="width: 10%; text-align: center"><?= !is_null($detalle->cSPRODUCTO)?$detalle->cSPRODUCTO->DS_CODIGO_PRODUCTO:''; ?></td>
                    <td style="width: 15%; text-align: left"><?= !is_null($detalle->cSPRODUCTO)?$detalle->cSPRODUCTO->DS_NOMBRE_PRODUCTO:''; ?></td>
                    
                    <td style="width: 5%; text-align: center"><?= $detalle->NM_CANTIDAD_COMPRA; ?></td>
                    <td style="width: 5%; text-align: center">
                           <?= Html::a('<span  class="glyphicon glyphicon-eye-open"></span>', \yii\helpers\Url::to(['pedidodetalle/view', 'id' => $detalle->NM_ID_DETALLE_PEDIDO]), ['title' => Yii::t('app', 'Ver'), 'target' => '', 'url-anulacion' => \yii\helpers\Url::to(['pedidodetalle/view', 'id' => $detalle->NM_ID_DETALLE_PEDIDO]), 'class' => 'btn btn-default verproducto', 'style' => 'display: inline;']);
                   ?>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    </div>
    <?php 
        echo ModalAjax::widget([
                'id' => 'modalListaPrecios',
                'selector' => 'a.verproducto', // all buttons in grid view with href attribute
                'toggleButton' => [
                    'label' => 'Ver producto',
                    'class' => 'btn btn-primary pull-left',
                    'style' => 'display:none'
                ],
                'options' => ['class' => 'header-primary'],
                'size' => ModalAjax::SIZE_LARGE,
            ]);

    ?>
</div>