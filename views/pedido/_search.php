<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PedidoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pedido-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'CS_PEDIDO_ID') ?>

    <?= $form->field($model, 'DS_CODIGO_PEDIDO') ?>

    <?= $form->field($model, 'NM_USUARIO_CREADOR_ID') ?>

    <?= $form->field($model, 'DS_NOTAS_PEDIDO') ?>

    <?= $form->field($model, 'NM_PRECIO_SUBTOTAL') ?>

    <?php // echo $form->field($model, 'NM_PRECIO_DESCUENTO') ?>

    <?php // echo $form->field($model, 'NM_PRECIO_TOTAL') ?>

    <?php // echo $form->field($model, 'NM_PRECIO_IVA') ?>

    <?php // echo $form->field($model, 'DT_FECHA_CREACION') ?>

    <?php // echo $form->field($model, 'NM_SEDE_CLIENTE_ID') ?>

    <?php // echo $form->field($model, 'DS_CLIENTE') ?>

    <?php // echo $form->field($model, 'ID_ESTADO') ?>

    <?php // echo $form->field($model, 'NM_PORCENTAJE_DESCUENTO') ?>

    <?php // echo $form->field($model, 'PORCENTAJE_IVA') ?>

    <?php // echo $form->field($model, 'GUSTO_PRODUCTOS_CLIENTE') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
