<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pedido */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Pedido',
]) . $model->CS_PEDIDO_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pedidos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->CS_PEDIDO_ID, 'url' => ['view', 'id' => $model->CS_PEDIDO_ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

$model->DT_FECHA_CREACION = date("Y-m-d", strtotime($model->DT_FECHA_CREACION));
?>
<div class="pedido-update">

    <?= $this->render('_form', [
        'model' => $model,
        'dataProviderPedidoDetalle' => $dataProviderPedidoDetalle,
        'searchModelPedidoDetalle' => $searchModelPedidoDetalle,
        'dataProviderPrd' => $dataProviderPrd,
        'searchModelPrd' => $searchModelPrd,
        'configuracion' => $configuracion
    ]) ?>

</div>
