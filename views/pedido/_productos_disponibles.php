<?php

use yii\helpers\Html;
use lo\widgets\modal\ModalAjax;



$this->registerJs('
      $(document).on("click", ".pjax-agregar-producto", function(e){
        e.preventDefault();
        if($("#pedido-nm_empresa").select2("data")[0].id != ""){
                e.preventDefault();
                //$(this).attr("data-toggle", "modal");
                //$(this).attr("data-target", "#adicionarProductoPedido");
                
                var bs_url = $(this).attr("href") + "&NM_EMPRESA="+$("#pedido-nm_empresa").select2("data")[0].id;
              
                jQuery("#adicionarProductoPedido").kbModalAjax({
                  url: bs_url,
                  size:"sm",
                  ajaxSubmit: true,
                  show:true
                });

                jQuery("#adicionarProductoPedido").modal({"show":true});

        }else{
            $.notify({ message: "Debe selecionar una empresa."  },{ type: "danger" });
        }

      })

  ');
?>
<div class="box-body table-responsive">
    <?=
        \kartik\grid\GridView::widget([
        'id' => 'grid_disponibles',
        'dataProvider' => $dataProviderPrd,
        'filterModel' => $searchModelPrd,
        'layout' => "{items}\n{summary}\n{pager}",
        'columns' => [
            'CS_PRODUCTO_ID' => [
                'visible' => 0,
            ],
            [
                'headerOptions' => ['width' => '60'],
                'attribute' => 'DS_CODIGO_PRODUCTO',
                'format' => 'raw',
                'value' => 'DS_CODIGO_PRODUCTO'
            ],
            [
                'attribute' => 'FK_UNIDAD',
                'filter'=>yii\helpers\ArrayHelper::map(\app\models\UnidadProducto::find()->asArray()->all(), 'CS_UNIDAD_ID', 'DS_NOMBRE_UNIDAD'),
                'value' => function($data) {
                    return is_object($data->fKUNIDAD)?$data->fKUNIDAD->DS_NOMBRE_UNIDAD:'';
                }
            ],
            [
                'attribute' => 'DS_NOMBRE_PRODUCTO',
                'value' => function($data) {
                    return $data->DS_NOMBRE_PRODUCTO;
                }
            ],
            [ 
                    'attribute' => 'PORCENTAJE_DESCUENTO',
                    'value' => function($row){
                        return $row->PORCENTAJE_DESCUENTO.'%';
                    }
                ],
            'DS_DESCRIPCION_PRODUCTO',
//                'REFERENTE',
            [
                //  'label' => '<span class="glyphicon glyphicon-plus"></span>',
                'format' => 'raw',
                'value' => function($data) use ($modelP){
          
                    return "<div style='text-align: center;'>" . Html::img('/dist/img/loading-plugin.gif', ['style' => 'display:none']) . Html::a('<span class="glyphicon glyphicon-plus"></span>', \yii\helpers\Url::to(['pedidodetalle/createmodal', 'DS_UQ_SESSION' => Yii::$app->request->csrfToken, 'NM_PRODUCTO_ID' => $data->CS_PRODUCTO_ID, 'CS_PEDIDO_ID' => $modelP->CS_PEDIDO_ID]), ['class' => 'btn btn-success btn-sm pjax-agregar-producto',
                                'title' => "Producto",
                                'pjax-container' => 'my_pjax',
                                'data-ID_PRODUCT' => $data->CS_PRODUCTO_ID,
                                'data-CANTIDAD_INVENTARIO' => $data->CANTIDAD_INVENTARIO
                            ]) . "</div>";
                }
                    ],
                
                // 'DT_FECHA_CREACION',
                // 'NM_ELIMINADO',
                //['class' => 'yii\grid\ActionColumn'],
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'clientOptions' => [
                            'enablePushState' => false,
                        ]
                    ],
                ]
            ]);
            ?>
        </div>
              <?php
               echo ModalAjax::widget([
                    'id' => 'adicionarProductoPedido',
                    'header' => 'Adicionar producto',
                  //  'selector' => 'a.pjax-agregar-producto',
                    'url' => '#', // Ajax view with form to load
                    'ajaxSubmit' => true, // Submit the contained form as ajax, true by default
                    'size' => ModalAjax::SIZE_LARGE,
                    'events'=>[
                        ModalAjax::EVENT_MODAL_SUBMIT => new \yii\web\JsExpression('
                            function(event, data, status, xhr, selector) {
                                try{
                                    if(data.success == true){
                                        $.notify({"message":data.message,"icon":"fa fa-users","url":"","target":"_blank"}, {
                                            type: "success",
                                            align: "right",
                                            from: "top"
                                        });
                                        $("#adicionarProductoPedido").modal("toggle");
                                        $.pjax.reload({container:"#grid_agregados-pjax"});
                                        calcularTotales();
                                    }else{
                                       $("#img_preload_modal").remove();
                                    }
                                }catch(e){
                                    console.log("No es json");
                                }
                            }
                       '),
                    ],
//                    'data' => ['mesa' => 'oscar']
                    // ... any other yii2 bootstrap modal option you need
                ]);
                ?>


