<?php

use yii\helpers\Html;
use lo\widgets\modal\ModalAjax;

$this->registerJs("$(function(){
       $(document).on('click', '.eliminarProductoPedido', function() {
            var deleteUrl = $(this).attr('delete-url');
            var pjaxContainer = $(this).attr('pjax-container');
        
            bootbox.confirm({
                    title: 'Eliminar producto', 
                    locale: 'es', 
                    message: 'Realmente desea eliminar este producto del pedido?',
                    callback: function(result) {                
                        if(result){
                           $.ajax({
                                url: deleteUrl,
                                type: 'post',
                                dataType: 'json',
                                error: function(xhr, status, error) {
                                    alert('Se genero un error en el servidor. ' + xhr.responseText);
                                }
                            }).done(function(data) {    
                               $.notify(data.mensaje, { type: 'success' });
                               $.pjax.reload({container:'#grid_agregados-pjax'});
                               calcularTotales()
                            });
                        }    
                    },
                    buttons: {
                        confirm: {
                            label: 'Eliminar'
                        },
                        cancel: {
                            label: 'Cancelar',
                        }
                    },
            });
        });
    });");

?>

<div class="box-body table-responsive">

    <?=
    \kartik\grid\GridView::widget([
        'id' => 'grid_agregados',
        'dataProvider' => $dataProviderPedidoDetalle,
        'filterModel' => $searchModelPedidoDetalle,
        'layout' => "{items}\n{summary}\n{pager}",
        'columns' => [
                'CS_PRODUCTO_ID' => [
                    'visible' => 0
                ],
                'cSPRODUCTO.DS_CODIGO_PRODUCTO',
                'cSPRODUCTO.DS_NOMBRE_PRODUCTO',
                'NM_CANTIDAD_COMPRA',
                [ 
                    'attribute' => 'PORCENTAJE_DESCUENTO',
                    'value' => function($row){
                        return $row->PORCENTAJE_DESCUENTO.'%';
                    }
                ],
    //            [
    //                'attribute' => 'PORCENTAJE_DESCUENTO',
    //                'label' => '% Dto',
    //                'value' => function($data) {
    //                    return $data->PORCENTAJE_DESCUENTO . "%";
    //                }
    //            ],
    //            [
    //                'attribute' => 'VALOR_DESCUENTO',
    //                'label' => '$ Dto',
    //                'value' => function($data) {
    //                    return $data->VALOR_DESCUENTO;
    //                }
    //            ],
                [
                    'attribute' => 'NM_PRECIO_UNITARIO',
                    'visible' => Yii::$app->user->can('Cliente')?false:true
                ],
    //          'NM_SUB_TOTAL',
                [
                    'attribute' => 'NM_PRECIO_TOTAL_PRODUCTO',
                    'visible' => Yii::$app->user->can('Cliente')?false:true
                ],
                [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}{verproducto}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('<span  class="glyphicon glyphicon-trash "></span>', false, ['class' => 'eliminarProductoPedido', 'delete-url' => \yii\helpers\Url::to(['pedidodetalle/delete', 'id' => $model->NM_ID_DETALLE_PEDIDO]), 'pjax-container' => 'pjax-list', 'title' => Yii::t('app', 'Eliminar')]);
                    },
                    'verproducto' => function ($url, $model) {
                        return Html::a('<span  class="glyphicon glyphicon-eye-open"></span>', \yii\helpers\Url::to(['pedidodetalle/view', 'id' => $model->NM_ID_DETALLE_PEDIDO]), ['title' => Yii::t('app', 'Ver'), 'target' => '', 'url-anulacion' => \yii\helpers\Url::to(['pedidodetalle/view', 'id' => $model->NM_ID_DETALLE_PEDIDO]), 'class' => ' verproducto', 'style' => 'display: inline;']);
                    }
                        ],
                    ],
                ],
                'pjax' => true,
                'pjaxSettings' => [
                    'options' => [
                        'clientOptions' => [
                            'enablePushState' => false,
                        ]
                    ],
                ]
            ]);
            ?>
        </div>
    <?php 
        echo ModalAjax::widget([
                'id' => 'modalListaPrecios',
                'selector' => 'a.verproducto', // all buttons in grid view with href attribute
                'toggleButton' => [
                    'label' => 'Ver producto',
                    'class' => 'btn btn-primary pull-left',
                    'style' => 'display:none'
                ],
                'options' => ['class' => 'header-primary'],
                'size' => ModalAjax::SIZE_LARGE,
            ]);

    ?>