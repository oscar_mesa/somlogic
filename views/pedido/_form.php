<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\web\JsExpression;
use yii\widgets\Pjax;
use kartik\tabs\TabsX;
use \lo\widgets\modal\ModalAjax;
/* @var $this yii\web\View */
/* @var $model app\models\Pedido */
/* @var $form yii\widgets\ActiveForm */

$resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;
$this->registerCss("#btn_abrir_cliente{display: none;}");
$this->registerJs("
//    window.onload = function(){
//	$('.fr-wrapper.show-placeholder').children().hide();
//        $('.fr-element.fr-view').show();
//    }
//    $(document).on('click', '.pjax-agregar-producto', function(e) {
//        e.preventDefault();
//        if($(this).parent().parent().parent().find('.precio').val().length > 0){
//            if($(this).parent().parent().parent().find('.cantidad').val().length > 0){
//                if(parseInt($(this).data('cantidad_inventario')) >= parseInt($(this).parent().parent().parent().find('.cantidad').val()) ){
//                    $(this).hide();
//                    $(this).parent().find('img').show();
//                    var url = $(this).attr('href');
//                    var pjaxContainer = $(this).attr('pjax-container');
//                    var porcentaje_descuento = $(this).parent().parent().parent().find('.aplicar_desceunto');
//                    var valor_porcentaje_descuento = (porcentaje_descuento.prop( 'checked' )?porcentaje_descuento.attr('porcentaje_desceunto'):0);
//                    //return;
//                    var data = {porcentaje_descuento:valor_porcentaje_descuento,porcentaje_iva:iva, id_product:$(this).data('id_product'),precio:$(this).parent().parent().parent().find('.precio').val(),cantidad:$(this).parent().parent().parent().find('.cantidad').val() };
//                    $.ajax({
//                        url: url,
//                        type: 'post',
//                        data:data
//                    }).done(function(data) {
//                        $.pjax.reload('#' + $.trim(pjaxContainer), {async:false});
//                        $.pjax.reload({container: '#my_pjax1', async:false});
//                    });
//                }else{
//                    $.notify({
//                        message: 'La cantidad ingresada, no puede superar la cantidad existente.' 
//                    },{
//                        type: 'danger'
//                    });
//                }
//            }else{
//                $.notify({
//                    message: 'Debe adicionar la cantidad' 
//                },{
//                    type: 'danger'
//                });
//            }
//        }else{
//            $.notify({
//                message: 'Debe adicionar el precio' 
//            },{
//                type: 'danger'
//            });
//        }
//    });
    
    $('a[href=\"#productos_agregados\"]').click(function(){
        //$.pjax.reload({container:\"#grid_agregados-pjax\"});
      });
      
    $('a[href=\"#productos_disponibles\"]').click(function(){
        //$.pjax.reload({container:\"#grid_disponibles-pjax\"});
      });

    $('#frm_pedido_1').on('afterValidate', function (event, messages) {
       // $('#frm_pedido').yiiActiveForm('validate', true);
    });
    
    $('#frm_pedido').submit(function(e){
        e.preventDefault();
    });

    String.prototype.replaceAll = function(searchStr, replaceStr) {
        var str = this;
        
        // escape regexp special characters in search string
        searchStr = searchStr.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        
        return str.replace(new RegExp(searchStr, 'gi'), replaceStr);
    };
    
    $('#frm_pedido_1').submit(function(e){
        data = $('#frm_pedido').serializeArray();
        for(x in data){ 
            valor = data[x].value.replaceAll('\"', \"'\");
            $(this).append('<input type=\"hidden\" name=\"'+data[x].name+'\" value=\"'+valor+'\" /> ');
        }
        return true;
    });
    
    $(function(){
        calcularTotales();
        $('#sider_menu_superior').trigger('click');
        
        $(document).on('click', '.ajaxDelete', function() {
        var deleteUrl = $(this).attr('delete-url');
        var pjaxContainer = $(this).attr('pjax-container');
        if(confirm('Realmente desea eliminar este producto?')){
                $.ajax({
                        url: deleteUrl,
                        type: 'post',
                        dataType: 'json',
                        error: function(xhr, status, error) {
                        alert('There was an error with your request.' + xhr.responseText);
                    }
                }).done(function(data) {
                    $.pjax.reload({container: '#my_pjax1', async:false});
                    $.pjax.reload({container: '#my_pjax', async:false});
                });        	
        }        
        });
        
    $('#my_pjax1').on('pjax:end',   function(d) { 
       calcularTotales();
    });
    
});

    
    $('#pedido-nm_porcentaje_descuento').change(function(e){
        //if(parseFloat($('#factura-nm_porcentaje_descuento').val()) > 0 && parseFloat($('#factura-nm_porcentaje_descuento').val()) > 0)
            calcularTotales();
    });


    $('#pedido-porcentaje_iva').change(function(e){
            calcularTotales();
    });
    
    
    $('#btn_nuevo_cliente').click(function(e){
        e.preventDefault();
        $('#btn_abrir_cliente').trigger('click');
    });
    
    function calcularTotales(){
        $.getJSON('" . \yii\helpers\Url::to(['pedido/obtenertotalespedido']) . "',{CS_PEDIDO_ID:'".$model->CS_PEDIDO_ID."'},function(data){
           },'JSON').done(function(data){
            var subtotal = 0, valor_iva = 0, total;
            $(data).each(function(i,element){
                subtotal += element.NM_PRECIO_TOTAL_PRODUCTO;
                
            }).promise().done(function(){ 
                valor_descuento = subtotal * ($('#pedido-nm_porcentaje_descuento').val()/100);
                valor_iva = subtotal * ($('#pedido-porcentaje_iva').val()/100);
                total = subtotal + valor_iva - (valor_descuento + valor_iva); 
                
                $('#pedido-nm_precio_iva').val(valor_iva);
                $('#precio_iva').html(number_format(valor_iva,2,'.',','));

                $('#pedido-nm_precio_subtotal').val(subtotal);
                $('#precio_subtotal').html(number_format(subtotal,2,'.',','));
                
                $('#pedido-nm_precio_total').val(total);
                $('#precio_total').html(number_format(total,2,'.',','));
                
                $('#precio_descuento').html(number_format(valor_descuento,2,'.',','));
                $('#pedido-nm_precio_descuento').val(valor_descuento);
                });
            }); 
    }
    
    ");
// $searchModelPrd,
// $dataProviderPrd
//var_dump(Yii::$app->session->getId());
?>

<?php
$form = ActiveForm::begin([
            "id" => "frm_pedido"
        ]);
?>
<div class="factura-form box box-primary">
    <div class="box-header with-border"><h4 class="d-inline-block" style="display: inline-block">Datos del pedido</h4><div class="badge badge-primary" style="background-color: #337ab7; display: inline-block; float: right;"><?= $model->eSTADO->DES_ESTADO ?></div></div>

    <div class="box-body table-responsive">
        <div class="row">
            <div class="col col-md-6">
                <?= $form->field($model, 'DS_CODIGO_PEDIDO')->textInput(['readonly' => true, 'maxlength' => true]) ?>
            </div>
            <div class="col col-md-6">
                <?= $form->field($model, 'DS_ACTOR')->textInput(['readonly' => true]) ?>
                <?= $form->field($model, 'NM_USUARIO_CREADOR_ID')->hiddenInput()->label(false) ?>
            </div>
        </div>
        <div class="row">
            <div class="col col-md-6">
                <?php //var_dump($model->DT_FECHA_CREACION); die; ?>

                <?= $form->field($model, 'DT_FECHA_CREACION')->input('date', ['readonly' => true]) ?>                
            </div> 
            <div class="col col-md-6">
                <div class="row">
                    <div class="col col-md-12">
                        <?=
                        $form->field($model, 'NM_EMPRESA')->widget(\kartik\select2\Select2::classname(), [
                            'initValueText' => is_object($model->nMEMPRESA) ? $model->nMEMPRESA->DS_NOMBRES_ACTOR: "",
                            //'data' => \yii\helpers\ArrayHelper::map(app\models\Producto::find()->all(),'CS_PRODUCTO_ID','DS_NOMBRE_PRODUCTO'),                        
                            'options' => ['placeholder' => 'Seleccione la empresa'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'ajax' => [
                                    'url' => yii\helpers\Url::to(['actor/obteneractortiposelect2']),
                                    'dataType' => 'json',
                                    'delay' => 250,
                                    'data' => new JsExpression('function(params) { return {q:params.term, page: params.page, tipo: 11}; }'),
                                    'processResults' => new JsExpression($resultsJs),
                                    'cache' => true
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
//                            'templateResult' => new JsExpression('formatRepo'),
//                            'templateSelection' => new JsExpression('formatRepoSelection'),
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col col-md-3" style="margin-top: 22px; display:none">
                        <button class="btn btn-primary pull-right" id="btn_nuevo_cliente">Nuevo cliente</button>
                    </div>    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-md-6">
                <div class="row">
                    <div class="col col-md-12">
                        <?=
                        $form->field($model, 'ID_CLIENTE_FINAL')->widget(\kartik\select2\Select2::classname(), [
                            'initValueText' => is_object($model->iDCLIENTEFINAL) ? $model->iDCLIENTEFINAL->DS_NOMBRES_ACTOR : "",
                            //'data' => \yii\helpers\ArrayHelper::map(app\models\Producto::find()->all(),'CS_PRODUCTO_ID','DS_NOMBRE_PRODUCTO'),                        
                            'options' => ['placeholder' => 'Seleccione el cliente final'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'ajax' => [
                                    'url' => yii\helpers\Url::to(['actor/obteneractortiposelect2']),
                                    'dataType' => 'json',
                                    'delay' => 250,
                                    'data' => new JsExpression('function(params) { return {q:params.term, page: params.page, tipo: 8}; }'),
                                    'processResults' => new JsExpression($resultsJs),
                                    'cache' => true
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
//                            'templateResult' => new JsExpression('formatRepo'),
//                            'templateSelection' => new JsExpression('formatRepoSelection'),
                            ],
                        ]);
                        ?>
                    </div>
                    <div class="col col-md-3" style="margin-top: 22px; display:none">
                        <button class="btn btn-primary pull-right" id="btn_nuevo_cliente">Nuevo cliente</button>
                    </div>    
                </div>
            </div>
            <div class="col col-md-6">
                <?= $form->field($model, 'DS_NOTAS_PEDIDO')->textArea(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col col-md-12"> 
                <?= $form->field($model, 'PRODUCTOS_PEDIDO')->textInput(['style' => 'display:none', 'htmlOptions' => ['style' => 'display:none']])->label(false) ?>  
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<div class="factura-form box box-success">
    <div class="box-header with-border"><h4>Productos del pedido</h4></div>
    <div class="box-body table-responsive">
        <div class="col col-md-12">
            <?php
            echo TabsX::widget([
                'position' => TabsX::POS_ABOVE,
                'align' => TabsX::ALIGN_LEFT,
                'items' => [
                    [
                        'label' => 'Productos disponibles',
                        'content' => $this->render('_productos_disponibles', [
                            'dataProviderPrd' => $dataProviderPrd,
                            'searchModelPrd' => $searchModelPrd,
                            'modelP' => $model
                        ]),
                        'active' => true,
                    ],
                    [
                        'label' => 'Productos agregados',
                        'content' =>  $this->render('_productos_agregados', [
                            'dataProviderPedidoDetalle' => $dataProviderPedidoDetalle,
                            'searchModelPedidoDetalle' => $searchModelPedidoDetalle,
                            'modelP' => $model
                        ]),
                        'headerOptions' => ['style' => 'font-weight:bold'],
                        'options' => ['id' => 'productos_agregados'],
                    ],
                ],
                'bordered'=>true,
            ]);
            ?>
        </div>
    </div>



</div>

<?php
$form = ActiveForm::begin([
            "id" => "frm_pedido_1"
        ]);
?>

<div class="factura-form box box-success">
    <div class="box-body table-responsive">    
        <div class="row">
            <?php if(!$model->isNewRecord){ ?>

                <div class="col col-md-12">
                <?= $form->field($model, 'ID_ESTADO')->dropDownList(yii\helpers\ArrayHelper::map(\app\models\EstadoPedido::find()->all(), 'ID_ESTADO', 'DES_ESTADO'), ['prompt' => 'Seleccione']) ?>
                </div>

            <?php } ?>
            <div class="col col-md-12">
                <?= $form->field($model, 'NM_PORCENTAJE_DESCUENTO')->textInput(['type' => 'number', 'value' => empty($model->NM_PORCENTAJE_DESCUENTO) ? 0 : $model->NM_PORCENTAJE_DESCUENTO]) ?>
            </div>
            <div class="col col-md-12">
                <?= $form->field($model, 'PORCENTAJE_IVA')->textInput(['type' => 'number', 'value' => empty($model->PORCENTAJE_IVA) ? $configuracion->IVA_FACTURA : $model->PORCENTAJE_IVA]) ?>
            </div>
            <div class="col col-md-12" style="">
                <?= $form->field($model, 'NM_PRECIO_DESCUENTO')->hiddenInput(['value' => empty($model->NM_PRECIO_DESCUENTO) ? 0 : $model->NM_PRECIO_DESCUENTO]) ?>
                <pre class="form-control" id="precio_descuento" style="margin-top:-16px"><?= empty($model->NM_PRECIO_DESCUENTO) ? 0 : $model->NM_PRECIO_DESCUENTO ?></pre>
            </div>
            <div class="col col-md-12">        
                <?= $form->field($model, 'NM_PRECIO_IVA')->hiddenInput(['value' => empty($model->NM_PRECIO_IVA) ? 0 : $model->NM_PRECIO_IVA]) ?>
                <pre class="form-control" id="precio_iva" style="margin-top:-16px"><?= empty($model->NM_PRECIO_IVA) ? 0 : $model->NM_PRECIO_IVA ?></pre>
            </div>
        </div>
        <div class="row">
            <div class="col col-md-12">
                <?= $form->field($model, 'NM_PRECIO_SUBTOTAL')->hiddenInput(['value' => empty($model->NM_PRECIO_SUBTOTAL) ? 0 : $model->NM_PRECIO_SUBTOTAL]) ?>
                <pre class="form-control" id="precio_subtotal" style="margin-top:-16px"><?= empty($model->NM_PRECIO_SUBTOTAL) ? 0 : $model->NM_PRECIO_SUBTOTAL ?></pre>
            </div>
        </div>

        <div class="row">
            <div class="col col-md-12">

                <?= $form->field($model, 'NM_PRECIO_TOTAL')->hiddenInput(['value' => empty($model->NM_PRECIO_TOTAL) ? 0 : $model->NM_PRECIO_TOTAL]) ?>
                <pre class="form-control" id="precio_total" style="margin-top:-16px"><?= empty($model->NM_PRECIO_TOTAL) ? 0 : $model->NM_PRECIO_TOTAL ?></pre>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
