<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pedido */

$this->title = Yii::t('app', 'Creación del Pedido');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pedidos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedido-create">

    <?= $this->render('_form', [
        'configuracion' => $configuracion,
        'dataProviderPrd' => $dataProviderPrd,
        'searchModelPrd' => $searchModelPrd,
        'dataProviderPedidoDetalle' => $dataProviderPedidoDetalle,
        'searchModelPedidoDetalle' => $searchModelPedidoDetalle,
        'model' => $model,
    ]) ?>

</div>
