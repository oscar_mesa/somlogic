<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PedidoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pedidos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pedido-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'DS_CODIGO_PEDIDO',
                [
                   'label' => 'Creador',
                    'value' => 'nMACTORCREADOR.DS_NOMBRES_ACTOR' 
                ],
                'DS_NOTAS_PEDIDO:ntext',
                //'NM_PRECIO_SUBTOTAL',
                // 'NM_PRECIO_DESCUENTO',
                // 'NM_PRECIO_TOTAL',
                // 'NM_PRECIO_IVA',
                 'DT_FECHA_CREACION',
                [ 
                    'label' => 'Empresa',
                    'value' => 'nMEMPRESA.DS_NOMBRES_ACTOR'
                ],
                // 'DS_CLIENTE',
                [ 
                    'attribute' => 'ID_ESTADO',
                    'filter'=>yii\helpers\ArrayHelper::map(\app\models\EstadoPedido::find()->asArray()->all(), 'ID_ESTADO', 'DES_ESTADO'),
                    'label' => 'Estado',
                    'value' => 'eSTADO.DES_ESTADO'
                ],
                // 'NM_PORCENTAJE_DESCUENTO',
                // 'PORCENTAJE_IVA',
                // 'GUSTO_PRODUCTOS_CLIENTE:ntext',

                ['class' => 'yii\grid\ActionColumn'],
            ],
            'exportConfig' => [
                 GridView::PDF => [],
                  GridView::EXCEL => [],
            ],
            'pjax' => false,
            'bordered' => true,
            'striped' => false,
            'responsive' => true,
            'floatHeaderOptions' => ['scrollingTop' => '50'],
            'panel' => [
                'type' => GridView::TYPE_INFO,
                'before'=> Html::a(Yii::t('app', 'Crear Pedido'), ['create'], ['class' => 'btn btn-success btn-flat']),

            ], 
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
