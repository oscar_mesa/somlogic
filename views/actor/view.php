<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\actor */

$this->title = $model->ID_ACTOR;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Actores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="actor-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID_ACTOR], ['class' => 'btn btn-primary btn-flat']) ?>
        
        <?php if(Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == app\models\TipoActor::ADMINISTRADOR){?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID_ACTOR], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?php } ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ID_ACTOR',
                'NM_DOCUMENTO_ID',
                [
                    'attribute' => 'NM_TIPO_DOCUMENTO_ID',
                    'value' => (is_object($model->nMTIPODOCUMENTO)?$model->nMTIPODOCUMENTO->DS_NOMBRE_TIPO_DOCUMENTO : ''),
                ],
                'DS_NOMBRES_ACTOR',
                'DS_APELLIDOS_ACTOR',
                'NM_TELEFONO',
                'NM_CELULAR',
                'DS_CORREO',
                'DS_DIRECCION',
                'PAGINA_WEB',
                [
                    'attribute' => 'NM_TIPO_ACTOR_ID',
                    'value' => (is_object($model->nMTipoActor)?$model->nMTipoActor->DS_NOMBRE_TIPO_ACTOR:''),
                ],
                [
                    'attribute' => 'NM_ESTADO_ID',
                    'value' => (is_object($model->nMESTADO)?$model->nMESTADO->DS_NOMBRE_ESTADO:''),
                ],
                'DT_FECHA_CREACION',
                'DF_FECHA_ACTUALIZACION',
            ],
        ]) ?>
    </div>
</div>
