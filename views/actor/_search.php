<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\actorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="actor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID_ACTOR') ?>

    <?= $form->field($model, 'NM_DOCUMENTO_ID') ?>

    <?= $form->field($model, 'NM_TIPO_DOCUMENTO_ID') ?>

    <?= $form->field($model, 'DS_NOMBRES_ACTOR') ?>

    <?= $form->field($model, 'DS_APELLIDOS_ACTOR') ?>

    <?php // echo $form->field($model, 'NM_TELEFONO') ?>

    <?php // echo $form->field($model, 'NM_CELULAR') ?>

    <?php // echo $form->field($model, 'DS_CORREO') ?>

    <?php // echo $form->field($model, 'DS_DIRECCION') ?>

    <?php // echo $form->field($model, 'DS_CONTRASENA') ?>

    <?php // echo $form->field($model, 'NM_TIPO_ACTOR_ID') ?>

    <?php // echo $form->field($model, 'NM_ESTADO_ID') ?>

    <?php // echo $form->field($model, 'DT_FECHA_CREACION') ?>

    <?php // echo $form->field($model, 'DF_FECHA_ACTUALIZACION') ?>

    <?php // echo $form->field($model, 'CLAVE_AUTENTICACION') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
