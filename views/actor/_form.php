<?php

use yii\helpers\Html;
use conquer\select2\Select2Widget;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\actor */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="actor-form box box-primary">
        <?php $form = yii\bootstrap\ActiveForm::begin(); ?>
        <div class="box-body table-responsive" style="overflow: hidden;">
            
        <?php //$form->errorSummary($model); ?>

            <div class="form-row">
                <div class="row">
                    <div class="col col-md-6">
                        <?= $form->field($model, 'NM_DOCUMENTO_ID', ['enableAjaxValidation' => true])->textInput(['readonly' => $model->scenario == 'creacion_modificacion_no_requridos' && !empty($model->NM_DOCUMENTO_ID)]) ?>
                    </div>
                    <div class="col col-md-6">
                        <?=
                        $form->field($model, 'NM_TIPO_DOCUMENTO_ID')->widget(
                                Select2Widget::className(), [
                            'items' => ArrayHelper::map(\app\models\TipoDocumento::find()->all(), 'CS_TIPO_DOCUMENTO_ID', 'DS_NOMBRE_TIPO_DOCUMENTO'),
                            'options' => [
                                'style' => 'width:98%',
                            ]
                                ]
                        )
                        ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <?= $form->field($model, 'DS_NOMBRES_ACTOR')->textInput(['maxlength' => true]) ?>
                    </div> 
                    <div class="col col-md-6">
                        <?= $form->field($model, 'DS_APELLIDOS_ACTOR')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row"> 
                    <div class="col col-md-6">
                        <?= $form->field($model, 'NM_TELEFONO')->textInput() ?>
                    </div>
                    <div class="col col-md-6">
                        <?= $form->field($model, 'NM_CELULAR')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <?= $form->field($model, 'DS_CORREO', ['enableAjaxValidation' => true])->input("email",['maxlength' => true]) ?>
                    </div>
                    <div class="col col-md-6">
                        <?= $form->field($model, 'DS_DIRECCION')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <?= $form->field($model, 'DS_CONTRASENA')->passwordInput() ?>
                    </div>
                    <div class="col col-md-6">
                        <?= $form->field($model, 'CONFIRMAR_CONTRASENA')->passwordInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <?= $form->field($model, 'CONTACTO')->textInput([]) ?>
                    </div>
                    <div class="col col-md-6">
                        <?= $form->field($model, 'CARGO_CONTACTO')->textInput([]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-6">
                        <?= $form->field($model, 'PAGINA_WEB')->textInput(['type' => 'url']) ?>
                    </div>
                <?php if(is_object(Yii::$app->user->getIdentity()) && Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == app\models\TipoActor::ADMINISTRADOR){ ?>
                    <div class="col col-md-6">
                        <?=
                        $form->field($model, 'NM_TIPO_ACTOR_ID')->widget(
                                Select2Widget::className(), [
                            'items' => ArrayHelper::map(\app\models\TipoActor::find()->all(), 'CS_TIPO_ACTOR', 'DS_NOMBRE_TIPO_ACTOR'),
                            'options' => [
                                'style' => 'width:98%',
//                                'options' => [3=>["selected"=>"selected"]]
                            ]
                                ]
                        )
                        ?>
                    </div>

                <?php } ?>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success btn-flat']) ?>
        </div>
        <?php yii\bootstrap\ActiveForm::end(); ?>
    </div>