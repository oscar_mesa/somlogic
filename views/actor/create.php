<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\actor */

$this->title = Yii::t('app', 'Crear Actor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'actors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="actor-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
