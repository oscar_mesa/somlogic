<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\actor */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'actor',
]) . $model->ID_ACTOR;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'actors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_ACTOR, 'url' => ['view', 'id' => $model->ID_ACTOR]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="actor-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
