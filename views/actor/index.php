<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\actorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Actores');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="actor-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'ID_ACTOR',
                'NM_DOCUMENTO_ID',
                [
                    'attribute' => 'NM_TIPO_DOCUMENTO_ID',
                    'value' => 'nMTIPODOCUMENTO.DS_NOMBRE_TIPO_DOCUMENTO',
                     'filter'=>\yii\helpers\ArrayHelper::map(app\models\TipoDocumento::find()->all(),'CS_TIPO_DOCUMENTO_ID','DS_NOMBRE_TIPO_DOCUMENTO'),
                ],
                'DS_NOMBRES_ACTOR',
                'DS_APELLIDOS_ACTOR',
                'NM_TELEFONO',
                'DS_CORREO',
                [
                    'attribute' => 'NM_TIPO_ACTOR_ID',
                    'value' => 'nMTipoActor.DS_NOMBRE_TIPO_ACTOR',
                     'filter'=>\yii\helpers\ArrayHelper::map(app\models\TipoActor::find()->all(),'CS_TIPO_ACTOR','DS_NOMBRE_TIPO_ACTOR'),
                ],
                // 'DS_DIRECCION',
                // 'DS_CONTRASENA',
                // 'NM_TIPO_ACTOR_ID',
                // 'NM_ESTADO_ID',
                // 'DT_FECHA_CREACION',
                // 'DF_FECHA_ACTUALIZACION',
                // 'CLAVE_AUTENTICACION',

                ['class' => 'yii\grid\ActionColumn'],
            ],
            'exportConfig' => [
                 GridView::PDF => [],
                  GridView::EXCEL => [],
            ],
            'pjax' => false,
            'bordered' => true,
            'striped' => false,
            'responsive' => true,
            'floatHeaderOptions' => ['scrollingTop' => '50'],
            'panel' => [
                'type' => GridView::TYPE_INFO,
                'before'=> Html::a(Yii::t('app', 'Crear actor'), ['create'], ['class' => 'btn btn-success btn-flat']),

            ], 
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
