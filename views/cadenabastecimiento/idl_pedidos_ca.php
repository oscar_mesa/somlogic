<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\VwTotalIndicadoresCaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Total indicadores, cadena de '.$model->iDEMPRESA->DS_NOMBRES_ACTOR);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vw-total-indicadores-ca-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-body table-responsive no-padding">
        <?php
            $columnas = [['class' => 'kartik\grid\SerialColumn'], 
                [
                    "attribute" => "DS_CODIGO_PEDIDO",
                    "label" => "Pedido"
                ], 

            ];
            foreach ($etapas as $etapa) {
                $columnas[] = [
                    "attribute" => trim($etapa->ETAPA.$etapa->ID_ETAPA),
                    "label" => $etapa->ETAPA,
                    'pageSummary' => function ($summary, $data, $widget) { return $summary."%"; },
                ];
            }
        ?>

         <?= GridView::widget([
            'dataProvider' => $dataProvider,
            //'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => $columnas,
            'pjax' => true,
            'bordered' => true,
            'striped' => false,
            'condensed' => false,
            'responsive' => true,
            'hover' => true,
            //'floatHeader' => true,
            'floatHeaderOptions' => ['scrollingTop' => '50'],
            'showPageSummary' => true,
            'panel' => [
                'type' => GridView::TYPE_INFO
            ], 
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>

<?php 
    $this->registerJs("$(document).ready(function(e){ 
        });
    ");
?>
