<?php if($index%2==0){ 
     $this->registerJs("
                $('input[rel=\"txtTooltip\"]').tooltip(); 
                ");
    ?>
    <div class="row">
<?php
} ?>
<div class="col-md-6"> 
          <!-- DIRECT CHAT PRIMARY -->
          <div class="box box-success direct-chat direct-chat-success">
            <div class="box-header with-border">
              <h3 class="box-title"><?= $model->iNDICADOR->DS_NOMBRE_INDICADOR ?></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool btn_ocultar" data-widget="remove" style="display: none"><i class="fa fa-times"></i></button>
                <button type="button" class="btn btn-box-tool btn_eliminar_indicador"><i class="fa fa-times"></i></button>
                <button type="button" class="btn btn-box-tool duplicar" title="duplicar" ID_INDICADOR=<?= $model->ID_INDICADOR ?> ID_ETP_CAD=<?= $model->ID_CADENA ?>><i class="fa fa-clone"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
             	<div class="table-responsive">
             		<form class="frm_indicador">
             		<table formula="<?= $model->iNDICADOR->DS_FORMULA ?>" NM_ID_TIPO_IND="<?= $model->iNDICADOR->NM_ID_TIPO_IND ?>" class="table table-bordered">
             			<tr>
             				<th>Definición</th>
             				<th>Area</th>
             				<th>Indicador</th>
             				<th>Nomenclatura</th>
             			</tr>
             			<tr>
             				<td><?= $model->iNDICADOR->DEFINICION ?></td>
             				<td><?= $model->iNDICADOR->aREA->DS_AREA ?></td>
             				<td><?= $model->iNDICADOR->DS_NOMBRE_INDICADOR ?></td>
             				<td><?= $model->iNDICADOR->DS_NOMENCLATURA ?></td>
             			</tr>
             			<tr>
             				<td colspan="4">
             					<?php 
             						foreach (explode('%', $model->iNDICADOR->DS_FORMULA) as $val) {
					                    if (is_numeric($val)) {
					                    	//die($val);
					                    	$campoCadena = \app\models\CampoCadenaIndicador::find()->where(['ID_CAMPO' => $val,'ID_CADENA_INDICADOR'=>$model->ID_INDICADOR_CADENA])->one();
					                        if(is_null($campoCadena)) {
						                        $campo = \app\models\CampoCaculo::find()->where(["ID_CAMPO"=>$val])->one();
						                        $model->iNDICADOR->DS_FORMULA = str_replace('%'.$val.'%', ' <input type="number" name="CampoCadenaIndicador['.$val.']" style="width: 20%;display: inline-block;" class="form-control campo_formula" idcampo="'.$campo->ID_CAMPO.'" data-toggle="tooltip" title="'.$campo->DS_CAMPO_CALCULO.'" rel="txtTooltip"/> ', $model->iNDICADOR->DS_FORMULA);
					                        }else{
					                        	$model->iNDICADOR->DS_FORMULA = str_replace('%'.$val.'%', ' <input type="number" value="'.$campoCadena->VALOR.'" name="CampoCadenaIndicador['.$val.']" style="width: 20%;display: inline-block;" class="form-control campo_formula" idcampo="'.$campoCadena->cAMPO->ID_CAMPO.'" data-toggle="tooltip" title="'.$campoCadena->cAMPO->DS_CAMPO_CALCULO.'" rel="txtTooltip"/> ', $model->iNDICADOR->DS_FORMULA);
					                        }
					                    }
					                }
             						echo $model->iNDICADOR->DS_FORMULA;
             					?>
             				</td>
             			</tr>
             			<tr>
             				<td>Total Indicador: </td>
             				<td>
             					<span class="valor"><?php 
                                    if($model->iNDICADOR->NM_ID_TIPO_IND == 1){
                                        echo !empty($model->NM_RESULTADO_INDICADOR)?round($model->NM_RESULTADO_INDICADOR*100,3):0;
                                    }else{
                                        echo !empty($model->NM_RESULTADO_INDICADOR)?round($model->NM_RESULTADO_INDICADOR,3):0;
                                    }
                                ?></span>
             					<input type="hidden" name="CadenaIndicador[NM_RESULTADO_INDICADOR]" class="NM_RESULTADO_INDICADOR" value="<?= !empty($model->NM_RESULTADO_INDICADOR)?$model->NM_RESULTADO_INDICADOR:0?>">
             					<input type="hidden" name="CadenaIndicador[ID_INDICADOR_CADENA]" value="<?= $model->ID_INDICADOR_CADENA ?>">
             				</td>
                            <td colspan="2">
                                <b><?= $model->iNDICADOR->UNIDAD ?></b>
                            </td>
             			</tr>
             			<tr style="display: none;">
             				<td colspan="2"><?= $model->getAttributeLabel("NM_PORCENTAJE_SIGNIFICANCIA") ?>: <input  style="width: 60%;display: inline-block;" type="number" value="<?= !empty($model->NM_PORCENTAJE_SIGNIFICANCIA)?$model->NM_PORCENTAJE_SIGNIFICANCIA:0?>" name="CadenaIndicador[NM_PORCENTAJE_SIGNIFICANCIA]" class="form-control"></td>
                            
             			</tr> 
                        <tr>
                            <td><?= $model->getAttributeLabel("META") ?>: 
                                <input style="width: 80%;display: inline-block;" type="number" value="<?= !empty($model->META)?$model->META:0?>" name="CadenaIndicador[META]" class="form-control cadena_indicaro_meta">
                            </td>
                            <td colspan="3"><?= $model->getAttributeLabel("ID_PEDIDO") ?>:<br/> <select  style="display: inline-block;" name="CadenaIndicador[ID_PEDIDO]" class="form-control">
                                <option value=''></option>
                                <?php 
                                    if(!is_null($model->cADENAETAPA->cADENA) && !is_null($model->cADENAETAPA->cADENA->pEDIDOS)){
                                        foreach ($model->cADENAETAPA->cADENA->pEDIDOS as $pedido) { ?>
                                            <option <?php echo ($model->ID_PEDIDO == $pedido->CS_PEDIDO_ID?'selected':'')?> value="<?= $pedido->CS_PEDIDO_ID?>"><?= $pedido->DS_CODIGO_PEDIDO ?></option>
                                <?php   
                                        }   
                                    }
                                ?>
                            </select>
                            </td>
                        </tr>
                        <?php if($model->iNDICADOR->tIPOINDICADOR->ID_TIPO == 2 || $model->iNDICADOR->tIPOINDICADOR->ID_TIPO == 3){ ?>
                            <tr>
                                <td colspan="2">
                                    Porcentaje de cumplimiento
                                </td>
                                <td colspan="2">
                                    <span class="porcentaje_cumplimiento"><?= round(($model->NM_RESULTADO_INDICADOR/(!empty($model->META)?$model->META:1))*100,3) ?>%</span>
                                </td>
                            </tr>
                        <?php } ?>
             		</table> 
                    <input type="hidden" name="CadenaIndicador[IDCANDEABAS]" value="<?= $model->cADENAETAPA->cADENA->NM_CADENA_ID ?>">
             		</form>
         		</div>
              <!-- /.direct-chat-pane -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="input-group">
                  <button type="submit" class="btn btn-primary btn-flat btn_guardar_indicador">Guardar</button>
                </div>
            </div>
            <!-- /.box-footer-->
          </div>
          <!--/.direct-chat -->
        </div>
<?php if($index%2!=0){ ?>
    </div>
<?php
} ?>        