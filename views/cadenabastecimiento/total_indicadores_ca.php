<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\VwTotalIndicadoresCaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Total indicadores, cadena de '.$model->iDEMPRESA->DS_NOMBRES_ACTOR);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vw-total-indicadores-ca-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'showPageSummary' => true,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'DS_CODIGO_PEDIDO',
                'DS_NOMBRE_INDICADOR',
                'ETAPA',
                [
                    'attribute' => 'NM_RESULTADO_INDICADOR',
                    'value' => function($row){
                        if($row->NM_ID_TIPO_IND == 1){
                            return round($row->NM_RESULTADO_INDICADOR*100, 3)."%";
                        }else{
                            return round($row->NM_RESULTADO_INDICADOR, 3);
                        }
                    }
                    //'pageSummary' => true,
                ],
                //'UNIDAD',
                [
                    'attribute' => 'PORCENTAJE_DE_CUMPLIMIENTO',
                    'value' => function($row){
                        if($row->NM_ID_TIPO_IND == 2 || $row->NM_ID_TIPO_IND == 3){
                            return round(($row->NM_RESULTADO_INDICADOR/(!empty($row->META)?$row->META:1))*100,3)."%";
                        }else{
                            return '';
                        }
                    }
                ],
                'META',
                'FECHA_CREACION',
                'FECHA_ACTUALIZACION',
                 'USUARIO_ACTUALIZACION',
                 'USUARIO_CREACION',
                // 'DEFINICION:ntext',
                // 'ID_INDICADOR_CADENA',

            ],
            'exportConfig' => [
                 GridView::PDF => [],
                  GridView::EXCEL => [],
            ],
            'pjax' => false,
            'bordered' => true,
            'striped' => false,
            'responsive' => true,
            'floatHeaderOptions' => ['scrollingTop' => '50'],
            'panel' => [
                'type' => GridView::TYPE_INFO   
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>

<?php 
    $this->registerJs("$(document).ready(function(e){ 

    });");
?>
