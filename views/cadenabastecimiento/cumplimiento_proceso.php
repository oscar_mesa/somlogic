<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\VwTotalIndicadoresCaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Total indicadores, cadena de '.$model->iDEMPRESA->DS_NOMBRES_ACTOR);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vw-total-indicadores-ca-index box box-primary">
    <?= Html::beginForm(['#'], 'post', ['enctype' => 'multipart/form-data', 'id' => 'total_etapas_frm']) ?>
    <?php Pjax::begin([
                        'id' => 'grid_cumplimiento_proceos',
                        'timeout' => 5000,
    ]); ?>

    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'showPageSummary' => true,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
               // 'eTTAPA',
                [
                    'label' => 'Actor',
                    'attribute'=> 'NOMBREACTORETAPA',
                    'value'=>'aCTOR.DS_NOMBRES_ACTOR',
                ],
                [
                    'label' => 'Proceso',
                    'attribute'=> 'NOMBREDELAETAPA',
                    'value'=>'eTAPA.ETAPA',
                ],
                [
                    'attribute' => 'CUMPLIMIENTO_PROMEDIO',
                    'value' => function($row){
                        return round($row->CUMPLIMIENTO_PROMEDIO*100, 3)."%";
                    }
                ],
                [
                    'filter' => false,
                    'attribute' => 'NM_PORCENTAJE_SIGNIFICANCIA',
                    'format' => 'raw',
                    'value' => function($data){

                        return "<div style='display: inline-block'>".\yii\bootstrap4\Html::textInput('CadenaEtapa[NM_PORCENTAJE_PONDERACION]['.$data->ID_ETP_CAD.']', $data->NM_PORCENTAJE_SIGNIFICANCIA, ['class' => 'form-control ponderacion_significancia'])."</div><div style='display: inline-block;margin-left:5px;'><span style='display:none' class='span_porcentaje_significancia'>".$data->NM_PORCENTAJE_SIGNIFICANCIA."</span>%</div>";
                        //empty($data->NM_PORCENTAJE_SIGNIFICANCIA)?"":$data->NM_PORCENTAJE_SIGNIFICANCIA."%";
                    }
                ],
                [
                    'attribute' => 'idl',
                    'value' => function($row){
                        return $row->CUMPLIMIENTO_PROMEDIO*($row->NM_PORCENTAJE_SIGNIFICANCIA/100);
                    },
                    'pageSummary' => function ($summary, $data, $widget) { return $summary; }
                ]
            ],
            'exportConfig' => [
                 GridView::PDF => [],
                  GridView::EXCEL => [],
            ],
            'pjax' => true,
            'bordered' => true,
            'striped' => false,
            'responsive' => true,
            'floatHeaderOptions' => ['scrollingTop' => '50'],
            'panel' => [
                'type' => GridView::TYPE_INFO   
            ],
        ]); ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success btn-block', 'id' => 'btn_guargar_total_indicadores']) ?>
    </div>
    <?php Pjax::end(); ?>
    <?= Html::endForm() ?>
</div>

<?php 
    $this->registerJs("$(document).ready(function(e){ 
        $(document).on('change', '.ponderacion_significancia',function(e){
            
        });

        $(document).on('click', '#btn_guargar_total_indicadores',function(e){
            e.preventDefault();
            var t = this;
            var total = 0;
            $(this).css('pointer-events', 'none');
            $(this).parent().append(\"<img id='img_preload_modal' src='/dist/img/loading-plugin.gif'/>\");

             $('.ponderacion_significancia').each(function(e,r){ 
                $(r).parent().parent().find('.span_porcentaje_significancia').html($(r).val());
                total += (Number.isInteger($(r).val()) || isDecimal($(r).val()))?parseFloat($(r).val()):0; 
            }).promise().done(function(){
                if(total <= 100){ 
                    $.post('".yii\helpers\Url::to(['indicador/almacenarinformacioncumplimientoproceso'])."' ,$('#total_etapas_frm').serialize(), function(r){
                            if(r.respuesta){
                                $.notify({\"message\":r.mensaje,\"icon\":\"fa fa-cubes\",\"url\":\"\",\"target\":\"_blank\"}, {
                                                type: \"success\",
                                                align: \"right\",
                                                from: \"top\"
                                            });
                            }else{
                                $.notify({\"message\":r.mensaje,\"icon\":\"fa fa-cubes\",\"url\":\"\",\"target\":\"_blank\"}, {
                                            type: \"danger\",
                                            align: \"right\",
                                            from: \"top\"
                                        });
                            }
                            $('#img_preload_modal').remove();
                            $(t).css('pointer-events', 'auto');
                            $.pjax.reload({container: '#grid_cumplimiento_proceos', async:false});
                    }, 'JSON');
                }else{
                      $.notify({\"message\":'La ponderación no puede superar el 100%. La sumatoria total de momento es '+total+'%',\"icon\":\"fa fa-cubes\",\"url\":\"\",\"target\":\"_blank\"}, {
                            type: \"danger\",
                            align: \"right\",
                            from: \"top\"
                        }); 
                    $('#img_preload_modal').remove();
                    $(t).css('pointer-events', 'auto');
                }
            });
        });
    });

    function isDecimal(n){
        if(n == \"\")
            return false;

        var strCheck = \"0123456789\";
        var i;

        for(i in n){
            if(strCheck.indexOf(n[i]) == -1)
                return false;
        }
        return true;
    }");
?>
