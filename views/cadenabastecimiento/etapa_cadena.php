<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;
use lo\widgets\modal\ModalAjax;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model app\models\CadenaEtapa */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('app', $model->cADENA->iDEMPRESA->DS_NOMBRES_ACTOR." en el proceso ".$model->eTAPA->ETAPA);
$this->params['breadcrumbs'][] = $this->title;

$resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;

$this->registerCss("#btn_cargar_indicadores { margin-top: 24px; }");
?>

<div class="cadena-etapa-form box box-primary">
    <div class="box-body table-responsive">
        <div class="form-row">
        <?php $form = ActiveForm::begin([
            'id' => 'frm_etapa_cadena'
        ]); 
            //print_r($model);die;
        ?>

        <?= $form->field($model, 'ID_ETAPA')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'ID_CADENA')->hiddenInput()->label(false) ?>
        <div class="row">
                <div class="col col-md-6">
                    <?php
                      $model->TIPO_ACTOR = is_object($model->aCTOR)?$model->aCTOR->NM_TIPO_ACTOR_ID:null;
                       echo $form->field($model, 'TIPO_ACTOR')->widget(\kartik\select2\Select2::classname(), [
                            'data' =>  ArrayHelper::map(\app\models\TipoActor::find()->where(['not in','CS_TIPO_ACTOR',[1]])->all(), 'CS_TIPO_ACTOR', 'DS_NOMBRE_TIPO_ACTOR'),
                            'language' => 'es',
                            'options' => ['placeholder' => 'Seleccione un tipo de actor'],
                            'pluginEvents' => [
                                    "change" => "function() { $('#cadenaetapa-id_actor').empty().trigger('change'); 
                                }",
                             ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                    ?>
                </div>
                <div class="col col-md-6">

             <?=
                $form->field($model, 'ID_ACTOR')->widget(\kartik\select2\Select2::classname(), [
                'initValueText' => is_object($model->aCTOR)?$model->aCTOR->DS_NOMBRES_ACTOR." ".$model->aCTOR->DS_APELLIDOS_ACTOR:"",
                //'data' => \yii\helpers\ArrayHelper::map(app\models\Producto::find()->all(),'CS_PRODUCTO_ID','DS_NOMBRE_PRODUCTO'),                        
                'options' => ['placeholder' => 'Seleccione la empresa'],
                'pluginEvents' => [
                        "change" => "function() { 
                            $.post('".yii\helpers\Url::to(['cadenaetapa/update', 'id' => $model->ID_ETP_CAD])."', $('#frm_etapa_cadena').serialize(), function(r){
                                },'json') 
                        }",
                    ],
                'pluginOptions' => [
                    'allowClear' => true,
                    'ajax' => [
                        'url' => yii\helpers\Url::to(['actor/obteneractortiposelect2']),
                        'dataType' => 'json',
                        'delay' => 250,
                        'data' => new JsExpression('function(params) { return {q:params.term, page: params.page, tipo: $("#cadenaetapa-tipo_actor").val()}; }'),
                        'processResults' => new JsExpression($resultsJs),
                        'cache' => true
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),

    //                            'templateResult' => new JsExpression('formatRepo'),
    //                            'templateSelection' => new JsExpression('formatRepoSelection'),
                ],
            ]);
            ?>
        </div>
         <div class="col col-md-6">
            <?php // $form->field($model, 'CUMPLIMIENTO_PROMEDIO')->textInput(['type' => 'number', 'readonly' => true]) ?>
            <label class="control-label"><?= $model->getAttributeLabel("CUMPLIMIENTO_PROMEDIO") ?></label>
            <pre id="cadenaetapa-cumplimiento_promedio" style="background-color: #e8e8e8;" class="form-control"><?= round($model->CUMPLIMIENTO_PROMEDIO*100, 3) ?>%</pre>
         </div> 
            <div class="col col-md-6">
                <div class="form-group">
            <?php
                    echo ModalAjax::widget([
                        'id' => 'cargarInidicadores',
                        'header' => 'Indicadores',
                        'toggleButton' => [
                            'label' => 'Cargar Indicadores',
                            'class' => 'btn btn-primary pull-left',
                            'id' => 'btn_cargar_indicadores'
                        ],
                        'url' => \yii\helpers\Url::to(['indicador/indexmodal', 'ID_ETP_CAD' => $model->ID_ETP_CAD]), // Ajax view with form to load
                        'ajaxSubmit' => true,
                        'size' => ModalAjax::SIZE_LARGE,
                        'events' => [
                            ModalAjax::EVENT_MODAL_SUBMIT => new \yii\web\JsExpression(' 
                                function(event, data, status, xhr, selector) {
                                     try{
                                        if(data.respuesta == true){
                                            $.notify({"message":data.mensaje,"icon":"fa fa-cubes","url":"","target":"_blank"}, {
                                                type: "success",
                                                align: "right",
                                                from: "top"
                                            });
                                            $("#cargarInidicadores").modal("toggle");
                                            $("#img_preload_modal").remove();    
                                            $.pjax.reload({container: "#some_pjax_id", async: false});
                                        }
                                    }catch(e){
                                        console.log("No es json");
                                    }
                                }
                           '),
                        ]
                    ]);
                    ?>
                </div>
            </div>
    </div>
    <?php ActiveForm::end(); ?>
    </div>
    <div class="form-row" id="cnt_indicadores">

    </div>
    </div>
</div>


<div class="cadena-etapa-form box box-primary">
    <div class="box-header with-border">
        <h3 style="margin-top: 2px;margin-bottom: 2px;">Indicadores</h3>
    </div>
    <div class="box-body table-responsive" style="background-color: #ecf0f5;">
        <?php Pjax::begin(['id' => 'some_pjax_id']) ?>
        <?php
            echo ListView::widget([
                'id' => 'lista_indicadores',
                'dataProvider' => $dataProvider,
                'itemView' => '_indicador',
                'options' => ['data-pjax' => true ],
                'itemOptions' => [
                    'tag' => false
                ],
            ]);
        ?>
     <?php Pjax::end() ?>
 </div>
</div>

<?php 
    $this->registerJs("$(document).ready(function(e){
                $('input[rel=\"txtTooltip\"]').tooltip(); 

                function calcular_cumplimiento_promedio(){
                    $.post('".yii\helpers\Url::to(['indicador/calcularcumplimientopromedio'])."', {ID_ETP_CAD : ".$model->ID_ETP_CAD."}, function(r){
                        $('#cadenaetapa-cumplimiento_promedio').html(r+'%');
                    }, 'JSON');
                }

                function roundToXDigits(value, digits)
                {
                    if (!digits) {
                        digits = 2;
                    }

                    value = value * Math.pow(10, digits);
                    value = Math.round(value);
                    value = value / Math.pow(10, digits);

                    return value
                }

                $(document).on('click', '.duplicar',function(e){
                    e.preventDefault();
                    var t1 = this;

                    bootbox.confirm({
                            title: 'Duplicar Indicador', 
                            locale: 'es', 
                            message: 'Realmente duplicar este indicador en este proceso?',
                            callback: function(result) {                
                                if(result){
                                    $.post('".yii\helpers\Url::to(['indicador/guardarindicadores'])."',{Indicador:[$(t1).attr('ID_INDICADOR')], ID_ETP_CAD:$(t1).attr('ID_ETP_CAD')},function(r){
                                        if(r.respuesta){
                                            $.pjax.reload({container: '#some_pjax_id', async: false});
                                            calcular_cumplimiento_promedio();
                                            $.notify({\"message\":r.mensaje,\"icon\":\"fa fa-cubes\",\"url\":\"\",\"target\":\"_blank\"}, {
                                                            type: \"success\",
                                                            align: \"right\",
                                                            from: \"top\"
                                                        });
                                        }else{
                                            $.each(r.errors, function(i, error){
                                                $.notify({\"message\":error[0],\"icon\":\"fa fa-cubes\",\"url\":\"\",\"target\":\"_blank\"}, {
                                                            type: \"danger\",
                                                            align: \"right\",
                                                            from: \"top\"
                                                        });
                                                return false;
                                            });
                                        }
                                    }, 'JSON');

                                }
                            }
                            });

                });
                
                $(document).on('click', '.btn_eliminar_indicador',function(e){
                    e.preventDefault();
                    var t1 = this;


                    bootbox.confirm({
                            title: 'Eliminar Indicador', 
                            locale: 'es', 
                            message: 'Realmente desea eliminar indicador?',
                            callback: function(result) {                
                                if(result){
                                   

                                    var t = $(t1).parent().parent().parent().find('.btn_guardar_indicador');
                                    t.css('pointer-events', 'none');
                                    t.parent().append(\"<img id='img_preload_modal' src='/dist/img/loading-plugin.gif'/>\");
                                    $.post('".yii\helpers\Url::to(['indicador/eliminarindicadoretapaca'])."' ,$(t1).parent().parent().parent().find('.frm_indicador').serialize(), function(r){
                                            calcular_cumplimiento_promedio();
                                            $.notify({\"message\":r.mensaje,\"icon\":\"fa fa-cubes\",\"url\":\"\",\"target\":\"_blank\"}, {
                                                                type: \"danger\",
                                                                align: \"right\",
                                                                from: \"top\"
                                                            });
                                            $('#img_preload_modal').remove();
                                            $(t).css('pointer-events', 'none');
                                            var pd = $(t1).parent().parent().parent().parent().parent();
                                            $(t1).find('.btn_ocultar').trigger('click');
                                            setTimeout(function(){ pd.remove(); $.pjax.reload({container: '#some_pjax_id', async: false}); }, 10);
                                        }, 'JSON');




                                }    
                            },
                            buttons: {
                                confirm: {
                                    label: 'Eliminar'
                                },
                                cancel: {
                                    label: 'Cancelar',
                                }
                            },
                    });
                });

                $(document).on('click', '.btn_guardar_indicador',function(e){
                    e.preventDefault();
                    var t = this;
                    $(this).css('pointer-events', 'none');
                    $(this).parent().append(\"<img id='img_preload_modal' src='/dist/img/loading-plugin.gif'/>\");
                    $.post('".yii\helpers\Url::to(['indicador/guardarindicadoretapaca'])."' ,$(this).parent().parent().parent().find('.frm_indicador').serialize(), function(r){
                            if(r.respuesta){
                                calcular_cumplimiento_promedio();
                                $.notify({\"message\":r.mensaje,\"icon\":\"fa fa-cubes\",\"url\":\"\",\"target\":\"_blank\"}, {
                                                type: \"success\",
                                                align: \"right\",
                                                from: \"top\"
                                            });
                            }else{
                                $.each(r.errors, function(i, error){
                                    $.notify({\"message\":error[0],\"icon\":\"fa fa-cubes\",\"url\":\"\",\"target\":\"_blank\"}, {
                                                type: \"danger\",
                                                align: \"right\",
                                                from: \"top\"
                                            });
                                    return false;
                                });
                            }
                            $('#img_preload_modal').remove();
                            $(t).css('pointer-events', 'auto');
                        }, 'JSON');
                });

                $(document).on('change', '.cadena_indicaro_meta',function(){
                    var t = this;
                    calcular_porcentaje_cumplimiento(t);
                });

                $(document).on('change', '.campo_formula',function(){
                    var t = this;
                    var formula = $(t).parent().parent().parent().parent().attr('formula');
                    $(t).parent().parent().parent().parent().find('.campo_formula').each(function(i, campo){
                        formula = formula.replace('%'+$(campo).attr('idcampo')+'%', $(campo).val());
                    }).promise().done(function(){ 
                        valor_text = formula.split('=')[1];
                        try{
                            valor_indicador = eval(valor_text);
                            console.log();
                            if($(t).parent().parent().parent().parent().attr('NM_ID_TIPO_IND') == 1){
                                 $(t).parent().parent().parent().parent().find('.valor').html(roundToXDigits(valor_indicador*100,3));
                            }else{
                                $(t).parent().parent().parent().parent().find('.valor').html(roundToXDigits(valor_indicador,3));
                            }
                            $(t).parent().parent().parent().parent().find('.NM_RESULTADO_INDICADOR').val(valor_indicador);
                            calcular_porcentaje_cumplimiento(t);
                        }catch(e){
                            $(t).parent().parent().parent().parent().find('.valor').html(0);
                            $(t).parent().parent().parent().parent().find('.NM_RESULTADO_INDICADOR').val(0);
                        }
                    });               
                });
                ".($model->CUMPLIMIENTO_PROMEDIO==0?"calcular_cumplimiento_promedio();":"")."

                function calcular_porcentaje_cumplimiento(t){
                     if($(t).parent().parent().parent().parent().attr('NM_ID_TIPO_IND') != 1){
                        r_indicador = $(t).parent().parent().parent().parent().find('.NM_RESULTADO_INDICADOR').val();
                        meta = $(t).parent().parent().parent().parent().find('.cadena_indicaro_meta').val();
                        por_cumplimiento = r_indicador/((meta.length<=0 || meta==0)?1:meta);
                        $(t).parent().parent().parent().parent().find('.porcentaje_cumplimiento').html(roundToXDigits(por_cumplimiento*100,3)+'%');
                    }
                }
        });");
?>