<?php 

use yii\helpers\Html;
use conquer\select2\Select2Widget;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use \yii\web\JsExpression;
use kartik\datetime\DateTimePicker;

$resultsJs = <<< JS
function (data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;

?>

<div class="cliente-form box box-primary">
    <?php $form = yii\bootstrap\ActiveForm::begin(); ?>
    <div class="box-body table-responsive">
        <div class="form-row">
            <div class="row">
                <div class="col col-md-6">
                    <?php //print_r($model);die; ?>
                    <?=
                $form->field($model, 'ID_EMPRESA')->widget(\kartik\select2\Select2::classname(), [
                    'initValueText' => is_object($model->iDEMPRESA)?$model->iDEMPRESA->DS_NOMBRES_ACTOR." ".$model->iDEMPRESA->DS_APELLIDOS_ACTOR." - ".$model->iDEMPRESA->NM_DOCUMENTO_ID:"",
                    //'data' => \yii\helpers\ArrayHelper::map(app\models\Producto::find()->all(),'CS_PRODUCTO_ID','DS_NOMBRE_PRODUCTO'),                        
                    'options' => ['placeholder' => 'Seleccione la empresa'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'ajax' => [
                            'url' => yii\helpers\Url::to(['actor/obteneractortiposelect2']),
                            'dataType' => 'json',
                            'delay' => 250,
                            'data' => new JsExpression('function(params) { return {q:params.term, page: params.page, tipo: 11}; }'),
                            'processResults' => new JsExpression($resultsJs),
                            'cache' => true
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
//                            'templateResult' => new JsExpression('formatRepo'),
//                            'templateSelection' => new JsExpression('formatRepoSelection'),
                    ],
                ]);
                ?>
                </div>
                <div class="col col-md-6">
                    <?php 
                    if($model->isNewRecord){
                    echo $form->field($model, 'PEDIDOS')->widget(\kartik\select2\Select2::classname(), [
                    //'data' => \yii\helpers\ArrayHelper::map(app\models\Producto::find()->all(),'CS_PRODUCTO_ID','DS_NOMBRE_PRODUCTO'),   
                    'options' => ['placeholder' => 'Seleccione los pedidos de la empresa seleccionada'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => true,
                        'ajax' => [
                            'url' => yii\helpers\Url::to(['pedido/obtenerpedidosselect2']),
                            'dataType' => 'json',
                            'delay' => 250,
                            'data' => new JsExpression('function(params) { 
                                console.log($("#cadenadeabastecimiento-id_empresa").select2("data")[0].id.length);
                                if($("#cadenadeabastecimiento-id_empresa").select2("data")[0].id.length > 0)
                                return {q:params.term, page: params.page, idempresa: $("#cadenadeabastecimiento-id_empresa").select2("data")[0].id}; }'),
                            'processResults' => new JsExpression($resultsJs),
                            'cache' => true
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
//                            'templateResult' => new JsExpression('formatRepo'),
//                            'templateSelection' => new JsExpression('formatRepoSelection'),
                    ],
                ]);
                }else{
               //echo "<pre>";     print_r();echo "</pre>";
         echo \yii\bootstrap4\Html::label($model->getAttributeLabel('PEDIDOS'));        
        echo \kartik\select2\Select2::widget([
            //'model' => $model,
            //'attribute' => 'DS_CLIENTE',
            'name' => 'Cadenadeabastecimiento[PEDIDOS][]',
            'id' => 'cadenadeabastecimiento-pedidos',
            //'data' => $model->DS_CLIENTE,
            'initValueText' => \yii\helpers\ArrayHelper::getColumn($model->pEDIDOS, 'DS_CODIGO_PEDIDO'),
            'value' => \yii\helpers\ArrayHelper::getColumn($model->pEDIDOS, 'CS_PEDIDO_ID'),
            'options' => ['placeholder' => 'Seleccione los pedidos de la empresa seleccionada', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true,
                'ajax' => [
                    'url' => yii\helpers\Url::to(['pedido/obtenerpedidosselect2']),
                    'dataType' => 'json',
                    'delay' => 250,
                    'data' => new JsExpression('function(params) { 
                                console.log($("#cadenadeabastecimiento-id_empresa").select2("data")[0].id.length);
                                if($("#cadenadeabastecimiento-id_empresa").select2("data")[0].id.length > 0)
                                return {q:params.term, page: params.page, idempresa: $("#cadenadeabastecimiento-id_empresa").select2("data")[0].id}; }'),
                    'processResults' => new JsExpression('
                            function (data, params) {
                                params.page = params.page || 1;
                                return {
                                    results: data.items,
                                    pagination: {
                                        more: (params.page * 30) < data.total_count
                                    }
                                };
                            }
                        '),
                    'cache' => true
                ],
                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
//                            'templateResult' => new JsExpression('formatRepo'),
//                            'templateSelection' => new JsExpression('formatRepoSelection'),
            ],
        ]);
                }
                ?>
                </div>
            </div>
           
         
            <div class="row">
                <div class="col col-md-6">
                    <?php 
                    	echo  $form->field($model, 'FECHA_PEDIDO')->widget(DateTimePicker::classname(), [
						'value' => !is_null($model->FECHA_PEDIDO)?date('Y-m-d', strtotime($model->FECHA_PEDIDO)):'',
						'options' => [
								'placeholder' => 'Seleccione en que ingreso el pedido ...', 
								'autocomplete' => 'off'
							],
						'pluginOptions' => [
							'format' => 'yyyy-mm-dd HH:mm',
							'todayHighlight' => true
						]
					]);
                    ?>
                </div>
                <div class="col col-md-6">
                    <?php 
                    	echo  $form->field($model, 'FECHA_FIN')->widget(DateTimePicker::classname(), [
						'value' => !is_null($model->FECHA_FIN)?date('Y-m-d', strtotime($model->FECHA_FIN)):'',
						'options' => [
							'placeholder' => 'Seleccione en que ingreso el pedido ...',
							'autocomplete' => 'off'
						],
						'pluginOptions' => [
							'format' => 'yyyy-mm-dd HH:mm',
							'todayHighlight' => true
						]
					]);
                    ?>
                </div>
            </div>

            <div class="row">
                <div class="col col-md-12">
                    <?= $form->field($model, 'DS_CADENA_ABASTECIMIENTO') ?>
                </div>
            </div>

        </div>
    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat btn_guardar_modal', "id" => '']) ?>
    </div>
    <?php yii\bootstrap\ActiveForm::end() ?>
</div>