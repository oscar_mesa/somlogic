<?php
use yii\helpers\Html;

$this->title = Yii::t('app', 'Escribir información de cadena sobre el contrato');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Listado'), 'url' => ['cadenabastecimiento/verca']];
$this->params['breadcrumbs'][] = $this->title;
$fecha=$cadena->FECHA_PEDIDO."- ".$cadena->FECHA_FIN;
$idcuenta=$cadena->iDEMPRESA->DS_NOMBRES_ACTOR.' - '.$cadena->iDEMPRESA->NM_DOCUMENTO_ID;
//echo "<pre>";print_r($cadena->iNDICADORESTOTALES);
$data=array_map(@create_function('$m','return $m->getAttributes(array(\'DS_NOMBRE_INDICADOR\',\'NM_RESULTADO_INDICADOR\', \'NM_PORCENTAJE_SIGNIFICANCIA\', \'RESULTADO_SIGNIFICANCIA\', \'DS_CODIGO_PEDIDO\',\'META\', \'UNIDAD\'));'),$cadena->iNDICADORESTOTALES);
$data1 = array_map(@create_function('$m','return $m->getAttributes(array(\'DS_CODIGO_PEDIDO\'));'),$cadena->pEDIDOS);
//echo json_encode($cadena->iNDICADORESTOTALES);die;
//$data=array_map(@create_function('$m','return $m->getAttribute(\'DS_CODIGO_PEDIDO\');'),$cadena->pEDIDOS);
$this->registerJs("
		$(document).on('click', '#btn_escribir_contrato', function(){
			var cuenta = '".$idcuenta ."';
			var cadena = '".$cadena->NM_CADENA_ID."';
			var pedidos = '".json_encode($data1)."';
			var indicadores = '".json_encode($data)."';
			console.log('Pedidos es de tipo: ',typeof(pedidos))
			var fecha = '".$fecha."';
			//console.log(indicadores,pedidos);
  			nodo.emit('escribir', {cuenta: cuenta, cadena: cadena, pedidos: pedidos, indicadores: indicadores, fecha: fecha});
  			$(this).css('pointer-events', 'none');
            $(this).parent().append(\"<img id='img_preload_modal' src='/dist/img/loading-plugin.gif'/>\");
		});

		nodo.on('escrituraexitosa', function(data){
			$('#btn_escribir_contrato').css('pointer-events', 'auto');
			$('#img_preload_modal').remove();
			$.notify({\"message\":\"<div style='overflow-wrap: break-word;'>La cadena fue almacenada en la siguiente dirección \"+data+'</div>',\"icon\":\"fa fa-cubes\",\"url\":\"\",\"target\":\"_blank\"}, {
                                                type: \"success\",
                                                align: \"right\",
                                                from: \"top\"
                                            });
		});


	");
?>

<div class="cadena-etapa-form box box-primary">
    <div class="box-body table-responsive">
        <div class="form-row">

<table class="table table-bordered">
	<tr>
		<th>Id cadena</th>
		<td><?= $cadena->NM_CADENA_ID ?></td>
	</tr>
	<tr>
		<th>Cuenta - Documento o NIT</th>
		<td><?= $cadena->iDEMPRESA->DS_NOMBRES_ACTOR.' - '.$cadena->iDEMPRESA->NM_DOCUMENTO_ID ?></td>
	</tr>
	<tr>
		<th>Fecha de medición</th>
		<td><?= $cadena->FECHA_PEDIDO." - ".$cadena->FECHA_FIN ?></td>
	</tr>
	<tr>
		<th>Peididos</th>
		<td><ul><?php 
			foreach ($cadena->pEDIDOS as  $pedido) { ?>
				<li><?= $pedido->DS_CODIGO_PEDIDO ?></li> 
		<?php
			}
		?></ul></td>
	</tr>
	<tr>
		<th>
			Indicadores
		</th>
		<td>
		<table class="table table-bordered">
			<thead> <tr>
				<th>Pedido</th>
				<th>Indicador</th>
				<th>Resultado Indicador</th>
				<th>Meta</th>
				<th>Ponderación</th>
				<th>Unidad</th>
			</tr></thead>
			<tbody>
				<?php 
					foreach ($cadena->iNDICADORESTOTALES as $cadenaIndicador) { ?>
				<tr>
					<td><?= $cadenaIndicador->DS_CODIGO_PEDIDO ?></td>
					<td><?= $cadenaIndicador->DS_NOMBRE_INDICADOR ?></td>
					<td><?= $cadenaIndicador->NM_RESULTADO_INDICADOR ?></td>
					<td><?= $cadenaIndicador->META ?></td>
					<td><?= $cadenaIndicador->NM_PORCENTAJE_SIGNIFICANCIA ?></td>
					<td><?= $cadenaIndicador->UNIDAD ?></td>
				</tr>
				<?php		
					}
				?>
			</tbody>
		</table>
	</td>
	</tr>
</table>

</div>
</div>
<div class="box-footer text-center">
   <?= Html::submitButton(Yii::t('app', 'Escribir contrato'), ['class' => 'btn btn-success btn-flat', 'id' => 'btn_escribir_contrato']) ?>
</div>
</div>