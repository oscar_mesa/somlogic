<?php
/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Cadena de abastecimiento de la empresa '.$model->iDEMPRESA->DS_NOMBRES_ACTOR);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cadena de abastecimiento'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


echo $this->render('_form_ingreso', [
	'model' => $model,
]);


