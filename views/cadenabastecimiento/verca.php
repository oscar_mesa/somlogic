<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\CadenadeabastecimientoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Cadena de abastecimiento');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="actor-index box box-primary">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Realizar ingreso a la CA'), ['ingresoca'], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'NM_CADENA_ID',
            [ 
                'attribute' => 'ID_EMPRESA',
                'value' => function($data){
                    return $data->iDEMPRESA->DS_NOMBRES_ACTOR;
                },
                'filter' =>  \yii\helpers\ArrayHelper::map(\app\models\Actor::find()->where(['NM_TIPO_ACTOR_ID'=>11])->asArray()->all(), 'ID_ACTOR', 'DS_NOMBRES_ACTOR')
            ],
            'DS_CADENA_ABASTECIMIENTO',
            'FECHA_PEDIDO',
            'FECHA_FIN',
            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{vercadenacompleta}{consultar_cadena_contrato}{escribir_cadena_contrato}',
                    'buttons' => [
                        'vercadenacompleta' => function ($url, $model) {

                            return Html::a('<i class="fa fa-arrows-alt" aria-hidden="true"></i>', \yii\helpers\Url::to(['gestionarcadena', 'id' => $model->NM_CADENA_ID, 'secccion' => 'actualizar_cadena']), ['style'=>'    margin-right: 8px;', 'title' => Yii::t('app', 'Gestioanr cadena'), 'target' => '_blank']);
                        },
                        'consultar_cadena_contrato' => function ($url, $model) {

                            return Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', \yii\helpers\Url::to(['consultarcadenacontrato', 'id' => $model->NM_CADENA_ID]), ['style'=>'    margin-right: 8px;','title' => Yii::t('app', 'Consultar información de cadena sobre el contrato'), 'target' => '_blank']);
                        },
                        'escribir_cadena_contrato' => function ($url, $model) {

                            return Html::a('<i class="fa fa-save " aria-hidden="true"></i>', \yii\helpers\Url::to(['escribircadenacontrato', 'id' => $model->NM_CADENA_ID]), ['title' => Yii::t('app', 'Escribir información de cadena sobre el contrato'), 'target' => '_blank']);
                        }
                    ],
                ],
        ],
    ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>