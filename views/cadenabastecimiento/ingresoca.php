<?php
/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Ingreso cadena de abastecimiento');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'ca'), 'url' => ['verca']];
$this->params['breadcrumbs'][] = $this->title;


echo $this->render('_form_ingreso', [
	'model' => $model,
]);


