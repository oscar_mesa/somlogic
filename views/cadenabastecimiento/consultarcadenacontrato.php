<?php
$this->title = Yii::t('app', 'Consultar información de cadena sobre el contrato');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Listado'), 'url' => ['cadenabastecimiento/verca']];
$this->params['breadcrumbs'][] = $this->title;


?>

<script type="text/javascript">
	
	nodo.emit('consultar', "<?= $_GET['id']?>");
	
</script>
<div id="load" style="position: absolute;width: 83%; height: 100%;z-index:1;background: #474F4D;opacity: 0.5;"><center><img src="https://acegif.com/wp-content/uploads/loading-25.gif" style="margin-top:15%;width: 10%"></center></div>
<div id="idcanena" style="display:none"><?= $_GET['id']?></div>

<div class="cadena-etapa-form box box-primary">
    <div class="box-body table-responsive">
        <div class="form-row">

<table class="table table-bordered">
	<tr>
		<th>Id cadena</th>
		<td id="cadena"></td>
	</tr>
	<tr>
		<th>Cuenta - Documento o NIT</th>
		<td id="cuenta"></td>
	</tr>
	<tr>
		<th>Fecha de medición</th>
		<td id="fecha"></td>
	</tr>
	<tr>
		<th>Peididos</th>
		<td><ul id="pedidos"></ul></td>
	</tr>
	<tr>
		<th>
			Indicadores
		</th>
		<td>
		<table class="table table-bordered">
			<thead> <tr>
				<th>Pedido</th>
				<th>Indicador</th>
				<th>Resultado Indicador</th>
				<th>Meta</th>
				<th>Ponderación</th>
				<th>Unidad</th>
			</tr></thead>
			<tbody id="indicadores">
				
			</tbody>
		</table>
	</td>
	</tr>
</table>

</div>
</div>

</div>