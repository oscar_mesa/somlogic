<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TiempoCargaPedidoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tiempo Carga Pedidos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tiempo-carga-pedido-index box box-primary">
    <?php Pjax::begin(); ?>
    <div class="box-header with-border">
        <?= Html::a(Yii::t('app', 'Create Tiempo Carga Pedido'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'ID_TIEMPO_CARGA',
                'FECHA_INGRESO',
                'TIEMPO_INICIO',
                'TIEMPO_FIN',
                'USUARIO_CREADOR',
                // 'TIEMPO_TOTAL',
                // 'TIEMPO_TOTAL_SOFTWARE_EMPRESA',
                // 'ID_PEDIDO_CADENA',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
