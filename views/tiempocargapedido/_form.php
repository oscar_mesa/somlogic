<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TiempoCargaPedido */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tiempo-carga-pedido-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'FECHA_INGRESO')->textInput() ?>

        <?= $form->field($model, 'TIEMPO_INICIO')->textInput() ?>

        <?= $form->field($model, 'TIEMPO_FIN')->textInput() ?>

        <?= $form->field($model, 'USUARIO_CREADOR')->textInput() ?>

        <?= $form->field($model, 'TIEMPO_TOTAL')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'TIEMPO_TOTAL_SOFTWARE_EMPRESA')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'ID_PEDIDO_CADENA')->textInput() ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
