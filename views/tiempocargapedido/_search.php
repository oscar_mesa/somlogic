<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TiempoCargaPedidoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tiempo-carga-pedido-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'ID_TIEMPO_CARGA') ?>

    <?= $form->field($model, 'FECHA_INGRESO') ?>

    <?= $form->field($model, 'TIEMPO_INICIO') ?>

    <?= $form->field($model, 'TIEMPO_FIN') ?>

    <?= $form->field($model, 'USUARIO_CREADOR') ?>

    <?php // echo $form->field($model, 'TIEMPO_TOTAL') ?>

    <?php // echo $form->field($model, 'TIEMPO_TOTAL_SOFTWARE_EMPRESA') ?>

    <?php // echo $form->field($model, 'ID_PEDIDO_CADENA') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
