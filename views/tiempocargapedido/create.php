<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TiempoCargaPedido */

$this->title = Yii::t('app', 'Create Tiempo Carga Pedido');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tiempo Carga Pedidos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tiempo-carga-pedido-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
