<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\TiempoCargaPedido */

$this->title = $model->ID_TIEMPO_CARGA;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tiempo Carga Pedidos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tiempo-carga-pedido-view box box-primary">
    <div class="box-header">
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID_TIEMPO_CARGA], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID_TIEMPO_CARGA], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ID_TIEMPO_CARGA',
                'FECHA_INGRESO',
                'TIEMPO_INICIO',
                'TIEMPO_FIN',
                'USUARIO_CREADOR',
                'TIEMPO_TOTAL',
                'TIEMPO_TOTAL_SOFTWARE_EMPRESA',
                'ID_PEDIDO_CADENA',
            ],
        ]) ?>
    </div>
</div>
