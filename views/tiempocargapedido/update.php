<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TiempoCargaPedido */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Tiempo Carga Pedido',
]) . $model->ID_TIEMPO_CARGA;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tiempo Carga Pedidos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID_TIEMPO_CARGA, 'url' => ['view', 'id' => $model->ID_TIEMPO_CARGA]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="tiempo-carga-pedido-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
