<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$nombre = ucwords(Yii::$app->user->getIdentity()->DS_NOMBRES_ACTOR." ".Yii::$app->user->getIdentity()->DS_APELLIDOS_ACTOR);

$format = <<< SCRIPT
        
$(document).ready(function () {
    mueveReloj();
});        

function mueveReloj() {
    momentoActual = new Date()
    hora = momentoActual.getHours()
    minuto = momentoActual.getMinutes()
    segundo = momentoActual.getSeconds()
    horaImprimible = hora + ":" + minuto + ":" + segundo
    $("#reloj").html(horaImprimible);
    setTimeout("mueveReloj()", 1000);
}
SCRIPT;

//$this->registerJsFile('http://trabajogrado.ingercode.com:8280/socket.io/socket.io.js');
$this->registerJs($format, yii\web\View::POS_END);

?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg" style="font-size: 17px;">' . date("Y-m-d") . ' <span id="reloj">'.date("H:i:s").'</span></span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" id="sider_menu_superior" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- Messages: style can be found in dropdown.less-->
                
                <!-- Tasks: style can be found in dropdown.less -->
                
                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user"></i>
                        <span class="hidden-xs"><?=$nombre?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?= yii\bootstrap\Html::img('@web/dist/img/UserNoSign.png', ['class'=>"img-circle"]) ?>
                            <p>
                                <?php
                                echo $nombre." - ".Yii::$app->user->getIdentity()->nMTipoActor->DS_NOMBRE_TIPO_ACTOR;
                                ?>
                                <small>Miembro desde <?= date("M",strtotime(Yii::$app->user->getIdentity()->DT_FECHA_CREACION)).". ".date("Y",strtotime(Yii::$app->user->getIdentity()->DT_FECHA_CREACION)) ?></small>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a("Perfil", ["actor/update","id"=>Yii::$app->user->getIdentity()->ID_ACTOR], ["class"=>"btn btn-default btn-flat"]) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Salir',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
