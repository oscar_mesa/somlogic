<?php
use yii\helpers\Html;
use kartik\growl\Growl;
/* @var $this \yii\web\View */
/* @var $content string */
app\components\MyNotifyWidget::widget();
if (Yii::$app->controller->action->id === 'login' || Yii::$app->controller->action->id === 'regitrar' || Yii::$app->controller->action->id === 'restaurarcredenciales' || Yii::$app->controller->action->id === 'cambiocontrasena') {
/**
 * Do not use this code in your template. Remove it. 
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {
    if(!Yii::$app->user->getIsGuest()){
    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }
    app\assets\AppAsset::register($this);
    app\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
         <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl."/dist/img/favicon.ico"; ?>" type="image/x-icon" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <!--<script src="http://sgl.elpoli.edu.co:8280/socket.io/socket.io.js"></script>-->
        <?php $this->head() ?>
    </head>
    <body class="<?= \dmstr\helpers\AdminLteHelper::skinClass() ?>">
    <?php $this->beginBody() ?>
    <?php

$this->registerJs('
            //var nodo = io.connect("http://sgl.elpoli.edu.co:8280");
            //nodo.on("consulta", consulta);
		
String.prototype.replaceAll = function(target, replacement) {
  return this.split(target).join(replacement);
};
            function consulta(data)
            {
               console.log(data);
               
               if(data[0]==$("#idcanena").text())
               {

               
                var cadena=data[0];
                var pedidos=data[1];
                var indicadores=data[2];
                var fecha=data[3];
                var cuenta=data[4];

                $("#cadena").text(cadena);
                $("#fecha").text(fecha);
                $("#cuenta").text(cuenta);

                $.each(JSON.parse(pedidos), function( key, value ) {
                    //console.log(value);
                    $("#pedidos").append( "<li>"+value[\'DS_CODIGO_PEDIDO\']+"</li>" );
                });
		indicadores = indicadores.replaceAll("\r", " ");
		indicadores = indicadores.replaceAll("\n", " ");	
                 $.each(JSON.parse(indicadores), function( key, value ) {
                    console.log(value);
                 $("#indicadores").append( "<tr><td>"+value[\'DS_CODIGO_PEDIDO\']+"</td><td>"+value[\'DS_NOMBRE_INDICADOR\']+"</td><td>"+value[\'NM_RESULTADO_INDICADOR\']+"</td><td>"+value[\'META\']+"</td><td>"+value[\'NM_PORCENTAJE_SIGNIFICANCIA\']+"</td><td>"+value[\'UNIDAD\']+"</td></tr>" );
                });

                $("#load").hide();

             }else
            {
                 $("#load").html("<div style=\'margin-top:150px;color:#fff;font-size:30px\'><center>Esta cadena no se encuentra en la blockchain</center></div>");
            }

                
            }

            function mifun(){
            var cadena = "0";
              socket.emit("consultar", cadena);
            }
    ', yii\web\View::POS_BEGIN);
    
    foreach (Yii::$app->session->getAllFlashes() as $message):; ?>
            <?php
            echo Growl::widget([
                'type' => (!empty($message['type'])) ? $message['type'] : 'danger',
                'title' => (!empty($message['title'])) ? Html::encode($message['title']) : 'Titulo no asigando!',
                'icon' => (!empty($message['icon'])) ? $message['icon'] : 'fa fa-info',
                'body' => (!empty($message['message'])) ? Html::encode($message['message']) : 'Mensaje no asignado!',
                'showSeparator' => true,
                'delay' => 1, //This delay is how long before the message shows
                'pluginOptions' => [
                    'delay' => (!empty($message['duration'])) ? $message['duration'] : 3000, //This delay is how long the message shows for
                    'placement' => [
                        'from' => (!empty($message['positonY'])) ? $message['positonY'] : 'top',
                        'align' => (!empty($message['positonX'])) ? $message['positonX'] : 'right',
                    ]
                ]
            ]);
            ?>
        <?php endforeach; ?>
    <div class="wrapper">

        <?= $this->render(
            'header.php',
            [
                'directoryAsset' => $directoryAsset,
            ]
        ) ?>

        <?= $this->render(
            'left.php',
            [
                'directoryAsset' => $directoryAsset,
            ]
        )
        ?>

        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset,]
        ) ?>

    </div>

    <?php $this->endBody() ?>
    </body>
    <script src="<?= Yii::$app->request->baseUrl."/js/somlogic.js"?>" type="text/javascript"></script>
    </html>
    <?php $this->endPage() ?>
<?php 
    }else{
        Yii::$app->response->redirect(['site/login']);
    }
} ?>
