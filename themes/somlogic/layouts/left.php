<?php

use app\models\TipoActor;
use app\components\AuthHandler;
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <?= yii\bootstrap\Html::img('@web/dist/img/logo_blue.png?v=5') ?>
            <small style="display: block;text-align: center;color: #3c8dbc">Sistema de Gestión Logística</small>
        </div>
<?php // var_dump(YII_ENV); die;?>
        <?=
        dmstr\widgets\Menu::widget(
                [
                    'labelTemplate' => '<span style="overflow-wrap: break-word;    white-space: normal;">{label}</span>',
                    'options' => [
                        'class' => 'sidebar-menu tree', 
                        'data-widget' => 'tree',
                    ],
                    'items' => [
                        ['label' => '', 'options' => ['class' => 'header']],

                        [
                            'label' => 'Actores',
                            'icon' => 'globe',
                            'url' => '#',
                            'visible' => (true),
                            'items' => [
                                ['label' => 'Listar', 'icon' => 'list-alt', 'url' => ['/actor'], 'visible' => AuthHandler::checkPermisionUser(['/actor', '/actor/*'])],
                                ['label' => 'Crear', 'icon' => 'plus', 'url' => ['/actor/create'], 'visible' => AuthHandler::checkPermisionUser(['/actor/create', '/pedido/*'])],
                            ]
                        ],
                        [
                            'label' => 'Productos',
                            'icon' => 'server',
                            'url' => '#',
                            'visible' => (true),
                            'items' => [
                                ['label' => 'Listar', 'icon' => 'list-alt', 'url' => ['/producto'], 'visible' => AuthHandler::checkPermisionUser(['/producto', '/producto/*'])],
                                ['label' => 'Crear', 'icon' => 'plus', 'url' => ['/producto/create'], 'visible' => AuthHandler::checkPermisionUser(['/producto/create', '/producto/*'])],
                            ]
                        ],
                        [
                            'label' => 'Pedidos',
                            'icon' => 'location-arrow',
                            'url' => '#',
                            'visible' => (true),
                            'items' => [
                                ['label' => 'Listar', 'icon' => 'list-alt', 'url' => ['/pedido'], 'visible' => AuthHandler::checkPermisionUser(['/pedido', '/pedido/*'])],
                                ['label' => 'Crear', 'icon' => 'plus', 'url' => ['/pedido/create'], 'visible' => AuthHandler::checkPermisionUser(['/pedido/create', '/pedido/*'])],
                            ]
                        ],
                       /* ['label' => 'Producto', 'icon' => 'dashboard', 'url' => ['/producto'],'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR || Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::VENDEDOR)],
                        ['label' => 'Venta', 'icon' => 'file-code-o', 'url' => ['/venta'],'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR || Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::VENDEDOR)],
                        ['label' => 'Entradas', 'icon' => 'bars', 'url' => ['/inventario'],'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR)],
                        ['label' => 'Proveedor', 'icon' => 'building-o', 'url' => ['/proveedor'],'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR)],
                        [
                            'label' => 'Informes gráficos',
                            'icon' => 'bar-chart',
                            'url' => '#',
                            'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR),
                            'items' => [
                                ['label' => 'Producto mas vendido', 'icon' => 'list-alt', 'url' => ['/estadisticas/graficoproductomasvendido'],],
                                ['label' => 'Ventas por vendedor', 'icon' => 'user-circle-o', 'url' => ['/estadisticas/graficoventasvendedor'],],
                            ]
                        ], 
			
                        [
                            'label' => 'Estadisticas',
                            'icon' => 'table',
                            'url' => '#',
                            'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR  || Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::VENDEDOR),
                            'items' => [
                                ['label' => 'Entradas', 'icon' => 'list-alt', 'url' => ['/estadisticas/inventario'], 'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR)],
                                [
                                    'label' => 'Venta de productos',
                                    'icon' => 'truck',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Todos', 'icon' => 'truck', 'url' => ['/estadisticas/todosproductosventas'],'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR)],
                                        ['label' => 'Diarios', 'icon' => 'cart-plus', 'url' => ['/estadisticas/ventasproductosdiarios'],],
                                        ['label' => 'Mensuales', 'icon' => 'shopping-cart', 'url' => ['/estadisticas/ventasproductosmensuales'],'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR)],
                                        ['label' => 'Anuales', 'icon' => 'shopping-basket', 'url' => ['/estadisticas/ventasproductosanuales'],'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR)],
                                    ]
                                ],
                                [
                                    'label' => 'Facturación',
                                    'icon' => 'bar-chart',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Todas', 'icon' => 'truck', 'url' => ['/estadisticas/facturastodas'],'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR)],
                                        ['label' => 'Diarias', 'icon' => 'cart-plus', 'url' => ['/estadisticas/facturasdiarios'],],
                                        ['label' => 'Mensuales', 'icon' => 'shopping-cart', 'url' => ['/estadisticas/facturasmensuales'],'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR)],
                                        ['label' => 'Anuales', 'icon' => 'shopping-basket', 'url' => ['/estadisticas/facturasanuales'],'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR)],
                                    ]
                                ],
                            ]
                        ],
                        [
                            'label' => 'Unidad', 
                            'url' => ['/unidad'], 
                            'icon' => 'database',
                            'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR)
                        ],
                        [
                            'label' => 'Pedidos',
                            'icon' => 'location-arrow',
                            'url' => '#',
                            'visible' => (true),
                            'items' => [
                                ['label' => 'Listar', 'icon' => 'list-alt', 'url' => ['/pedido'], 'visible' => AuthHandler::checkPermisionUser(['/pedido', '/pedido/*'])],
                                ['label' => 'Crear', 'icon' => 'bus', 'url' => ['/pedido/create'], 'visible' => AuthHandler::checkPermisionUser(['/pedido/create', '/pedido/*'])],
                            ]
                        ],
                         ['label' => 'Empresa', 'icon' => 'vcard-o', 'url' => ['/empresa'],'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR)
                        ],
                        ['label' => 'Proveedor', 'icon' => 'vcard-o', 'url' => ['/proveedor'],'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR)
                        ],*/

                        [
                            'label' => 'Gestión de la cadena de suministro', 
                            'icon' => 'bus', 
                            'url' => '#',
                            'items' => [
                                [
                                    'label' => 'Ingreso', 
                                    'icon' => 'anchor',
                                    'visible' => AuthHandler::checkPermisionUser(['/cadenabastecimiento/ingresoca', '/cadenabastecimiento/*']),
                                    'url' => ['/cadenabastecimiento/ingresoca']
                                ],
                                [
                                    'label' => 'Listado de cadenas', 
                                    'icon' => 'th-list',
                                    'visible' => AuthHandler::checkPermisionUser(['/cadenabastecimiento', '/cadenabastecimiento/verca']),
                                    'url' => ['/cadenabastecimiento/verca']
                                ],
                                [
                                    'label' => 'Campos de la Fórmula', 
                                    'icon' => 'outdent',
                                    'visible' => AuthHandler::checkPermisionUser(['/campocaculo', '/campocaculo/index']),
                                    'url' => ['/campocaculo']
                                ],
                                [
                                    'label' => 'Gestión de indicadores', 
                                    'icon' => 'line-chart',
                                    'visible' => AuthHandler::checkPermisionUser(['/indicador', '/indicador/index']),
                                    'url' => ['/indicador']
                                ]
                                
                            ],
                        ],
                        /*[
                            'label' => 'Indicadores', 
                            'icon' => 'line-chart', 
                            'url' => '#',
                            'items' => [
                                [
                                    'label' => 'Tiempos', 
                                    'icon' => 'anchor',
                                    'url' => ['/indicadores/tiempo']
                                ],
                                [
                                    'label' => 'Costos', 
                                    'icon' => 'th-list',
                                    'url' => ['/indicadores/costo']
                                ]
                                ,
                                [
                                    'label' => 'Transporte', 
                                    'icon' => 'bus',
                                    'url' => ['/indicadores/transporte']
                                ]
                            ],
                        ],*/
                        /*[
                            'label' => 'Desarrollo',
                            'icon' => 'share',
                            'url' => '#',
                            'visible' => (Yii::$app->user->getIdentity()->NM_TIPO_ACTOR_ID == TipoActor::ADMINISTRADOR),
                            'items' => [
                                ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'], 'visible' => (YII_ENV=="dev")],
                                ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'], 'visible' => (YII_ENV=="dev")],
                                ['label' => 'Respaldo', 'icon' => 'dashboard', 'url' => ['/db-manager'],],
                            ],
                        ],*/
			[
                            'label' => 'Permisos',
                            'icon' => 'unlock-alt',
                            'url' => '#',
                            'visible' => (true),
                            'items' => [
                                ['label' => 'Asignaciones', 'icon' => 'address-card-o', 'url' => ['/rbac/assignment'], 'visible' => AuthHandler::checkPermisionUser(['/rbac/assignment', '/rbac/*'])],
                                ['label' => 'Rutas', 'icon' => 'fighter-jet', 'url' => ['/rbac/route'], 'visible' => AuthHandler::checkPermisionUser(['/rbac/route', '/rbac/*'])],
                                ['label' => 'Permiso', 'icon' => 'unlock', 'url' => ['/rbac/permission'], 'visible' => AuthHandler::checkPermisionUser(['/rbac/permission', '/rbac/*'])],
                                ['label' => 'Roles', 'icon' => 'user-circle', 'url' => ['/rbac/role'], 'visible' => AuthHandler::checkPermisionUser(['/rbac/role', '/rbac/*'])],
                            ],                            
                        ],
                        ['label' => 'Acerca de', 'icon' => 'magic', 'url' => ['/site/about']],
                        ['label' => 'Ayuda', 'icon' => 'cogs', 'url' => ['/site/ayuda']],
                        ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                        ['label' => 'Salir', 'url' => ['/site/logout']],
                    ],
                ]
        )
        ?>

    </section>

</aside>
