<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = Yii::t("app", "Iniciar sesión");

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>SGL</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Inicia sesión.</p>

        <?php $form = yii\bootstrap\ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form->field($model, 'email', $fieldOptions1)->input("email",['placeholder' => $model->getAttributeLabel('email')]) ?>

        <?= $form->field($model, 'password', $fieldOptions2)->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton('Iniciar', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>


        <?php ActiveForm::end(); ?>

        <div class="social-auth-links text-center">
            <!--<p>- O -</p>
            <a href="<?= \yii\helpers\Url::to(["site/auth", "authclient"=>"google"]) ?>" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i>Inicia sesión usando Gmail</a>-->
            <?php            
//               yii\authclient\widgets\AuthChoice::widget([
//                'baseAuthUrl' => ['site/auth'],
//                'popupMode' => false,
//                ]) 
                ?>
        </div>
        <!-- /.social-auth-links -->

        <a href="<?= \yii\helpers\Url::to(["site/restaurarcredenciales"]) ?>" class="">Olvidé mi contraseña</a><br>
       

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
