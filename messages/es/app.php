<?php

/**
 * Translation map for nl-NL
 */
return [
    'welcome' => 'Bienvenido',
    'name' => 'Nombre',
    'email' => 'Correo',
    'id_card' => 'Cédula',
    'id_type' => 'Tipo',
    'Update' => 'Actualizar',
    'Delete' => 'Eliminar',
    'Create {element}' => 'Crear {element}',
    'Create {element} {element}' => 'Crear {element} {element}',
    'Save' => "Guardar",
    'Are you sure you want to delete this item?' => '¿Desea eliminar este registro?',
    'Update {modelClass}: ' => 'Actualizar {modelClass}: ',
    'Forbidden' => 'Prohibido'
];