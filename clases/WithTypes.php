<?php
class WithTypes
{
    /** @var string $name */
    private $name;

    /** @var DateTime */
    private $dob;

    // This one does not need a doc block since the type is extracted
    // directly from the method's signature
    public function __construct(DateTime $dob)
    {
        $this->dob = $dob;
    }

    /**
     * @param string $name
     * @return void
     */
    public function rename($name)
    {
        $this->name = $name
    }
}